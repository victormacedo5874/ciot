package br.adm.ipc.schemas.efrete.motoristas;

public class MotoristasServiceSoapProxy implements br.adm.ipc.schemas.efrete.motoristas.MotoristasServiceSoap {
  private String _endpoint = null;
  private br.adm.ipc.schemas.efrete.motoristas.MotoristasServiceSoap motoristasServiceSoap = null;
  
  public MotoristasServiceSoapProxy() {
    _initMotoristasServiceSoapProxy();
  }
  
  public MotoristasServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initMotoristasServiceSoapProxy();
  }
  
  private void _initMotoristasServiceSoapProxy() {
    try {
      motoristasServiceSoap = (new br.adm.ipc.schemas.efrete.motoristas.MotoristasServiceLocator()).getMotoristasServiceSoap();
      if (motoristasServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)motoristasServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)motoristasServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (motoristasServiceSoap != null)
      ((javax.xml.rpc.Stub)motoristasServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public br.adm.ipc.schemas.efrete.motoristas.MotoristasServiceSoap getMotoristasServiceSoap() {
    if (motoristasServiceSoap == null)
      _initMotoristasServiceSoapProxy();
    return motoristasServiceSoap;
  }
  
  public br.adm.ipc.schemas.efrete.motoristas.objects.ObterResponse obter(br.adm.ipc.schemas.efrete.motoristas.objects.ObterRequest obterRequest) throws java.rmi.RemoteException{
    if (motoristasServiceSoap == null)
      _initMotoristasServiceSoapProxy();
    return motoristasServiceSoap.obter(obterRequest);
  }
  
  public br.adm.ipc.schemas.efrete.motoristas.objects.GravarResponse gravar(br.adm.ipc.schemas.efrete.motoristas.objects.GravarRequest gravarRequest) throws java.rmi.RemoteException{
    if (motoristasServiceSoap == null)
      _initMotoristasServiceSoapProxy();
    return motoristasServiceSoap.gravar(gravarRequest);
  }
  
  
}