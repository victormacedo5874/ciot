/**
 * MotoristasService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.motoristas;

public interface MotoristasService extends javax.xml.rpc.Service {
    public java.lang.String getMotoristasServiceSoapAddress();

    public br.adm.ipc.schemas.efrete.motoristas.MotoristasServiceSoap getMotoristasServiceSoap() throws javax.xml.rpc.ServiceException;

    public br.adm.ipc.schemas.efrete.motoristas.MotoristasServiceSoap getMotoristasServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
