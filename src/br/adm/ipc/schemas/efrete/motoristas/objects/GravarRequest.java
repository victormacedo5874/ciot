/**
 * GravarRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.motoristas.objects;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class GravarRequest  extends br.adm.ipc.schemas.efrete.objects.Request  implements java.io.Serializable {
    private String CPF;

    private java.lang.String nome;

    private String CNH;

    private java.util.Calendar dataNascimento;

    private java.lang.String nomeDeSolteiraDaMae;

    private br.adm.ipc.schemas.efrete.objects.Endereco endereco;

    private br.adm.ipc.schemas.efrete.objects.Telefones telefones;

    public GravarRequest() {
    }

    public GravarRequest(
           java.lang.String token,
           java.lang.String integrador,
           int versao,
           String CPF,
           java.lang.String nome,
           String CNH,
           java.util.Calendar dataNascimento,
           java.lang.String nomeDeSolteiraDaMae,
           br.adm.ipc.schemas.efrete.objects.Endereco endereco,
           br.adm.ipc.schemas.efrete.objects.Telefones telefones) {
        super(
            token,
            integrador,
            versao);
        this.CPF = CPF;
        this.nome = nome;
        this.CNH = CNH;
        this.dataNascimento = dataNascimento;
        this.nomeDeSolteiraDaMae = nomeDeSolteiraDaMae;
        this.endereco = endereco;
        this.telefones = telefones;
    }


    /**
     * Gets the CPF value for this GravarRequest.
     * 
     * @return CPF
     */
    public String getCPF() {
        return CPF;
    }


    /**
     * Sets the CPF value for this GravarRequest.
     * 
     * @param CPF
     */
    public void setCPF(String CPF) {
        this.CPF = CPF;
    }


    /**
     * Gets the nome value for this GravarRequest.
     * 
     * @return nome
     */
    public java.lang.String getNome() {
        return nome;
    }


    /**
     * Sets the nome value for this GravarRequest.
     * 
     * @param nome
     */
    public void setNome(java.lang.String nome) {
        this.nome = nome;
    }


    /**
     * Gets the CNH value for this GravarRequest.
     * 
     * @return CNH
     */
    public String getCNH() {
        return CNH;
    }


    /**
     * Sets the CNH value for this GravarRequest.
     * 
     * @param CNH
     */
    public void setCNH(String CNH) {
        this.CNH = CNH;
    }


    /**
     * Gets the dataNascimento value for this GravarRequest.
     * 
     * @return dataNascimento
     */
    public java.util.Calendar getDataNascimento() {
        return dataNascimento;
    }


    /**
     * Sets the dataNascimento value for this GravarRequest.
     * 
     * @param dataNascimento
     */
    public void setDataNascimento(java.util.Calendar dataNascimento) {
        this.dataNascimento = dataNascimento;
    }


    /**
     * Gets the nomeDeSolteiraDaMae value for this GravarRequest.
     * 
     * @return nomeDeSolteiraDaMae
     */
    public java.lang.String getNomeDeSolteiraDaMae() {
        return nomeDeSolteiraDaMae;
    }


    /**
     * Sets the nomeDeSolteiraDaMae value for this GravarRequest.
     * 
     * @param nomeDeSolteiraDaMae
     */
    public void setNomeDeSolteiraDaMae(java.lang.String nomeDeSolteiraDaMae) {
        this.nomeDeSolteiraDaMae = nomeDeSolteiraDaMae;
    }


    /**
     * Gets the endereco value for this GravarRequest.
     * 
     * @return endereco
     */
    public br.adm.ipc.schemas.efrete.objects.Endereco getEndereco() {
        return endereco;
    }


    /**
     * Sets the endereco value for this GravarRequest.
     * 
     * @param endereco
     */
    public void setEndereco(br.adm.ipc.schemas.efrete.objects.Endereco endereco) {
        this.endereco = endereco;
    }


    /**
     * Gets the telefones value for this GravarRequest.
     * 
     * @return telefones
     */
    public br.adm.ipc.schemas.efrete.objects.Telefones getTelefones() {
        return telefones;
    }


    /**
     * Sets the telefones value for this GravarRequest.
     * 
     * @param telefones
     */
    public void setTelefones(br.adm.ipc.schemas.efrete.objects.Telefones telefones) {
        this.telefones = telefones;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GravarRequest)) return false;
        GravarRequest other = (GravarRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.CPF == other.getCPF() &&
            ((this.nome==null && other.getNome()==null) || 
             (this.nome!=null &&
              this.nome.equals(other.getNome()))) &&
            this.CNH == other.getCNH() &&
            ((this.dataNascimento==null && other.getDataNascimento()==null) || 
             (this.dataNascimento!=null &&
              this.dataNascimento.equals(other.getDataNascimento()))) &&
            ((this.nomeDeSolteiraDaMae==null && other.getNomeDeSolteiraDaMae()==null) || 
             (this.nomeDeSolteiraDaMae!=null &&
              this.nomeDeSolteiraDaMae.equals(other.getNomeDeSolteiraDaMae()))) &&
            ((this.endereco==null && other.getEndereco()==null) || 
             (this.endereco!=null &&
              this.endereco.equals(other.getEndereco()))) &&
            ((this.telefones==null && other.getTelefones()==null) || 
             (this.telefones!=null &&
              this.telefones.equals(other.getTelefones())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += new Long(getCPF()).hashCode();
        if (getNome() != null) {
            _hashCode += getNome().hashCode();
        }
        _hashCode += new Long(getCNH()).hashCode();
        if (getDataNascimento() != null) {
            _hashCode += getDataNascimento().hashCode();
        }
        if (getNomeDeSolteiraDaMae() != null) {
            _hashCode += getNomeDeSolteiraDaMae().hashCode();
        }
        if (getEndereco() != null) {
            _hashCode += getEndereco().hashCode();
        }
        if (getTelefones() != null) {
            _hashCode += getTelefones().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GravarRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/motoristas/objects", "GravarRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CPF");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/motoristas/objects", "CPF"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nome");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/motoristas/objects", "Nome"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CNH");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/motoristas/objects", "CNH"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataNascimento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/motoristas/objects", "DataNascimento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomeDeSolteiraDaMae");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/motoristas/objects", "NomeDeSolteiraDaMae"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endereco");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/motoristas/objects", "Endereco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/objects", "Endereco"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("telefones");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/motoristas/objects", "Telefones"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/objects", "Telefones"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
