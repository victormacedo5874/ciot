/**
 * ObterRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.motoristas.objects;

public class ObterRequest  extends br.adm.ipc.schemas.efrete.objects.Request  implements java.io.Serializable {
    private long CPF;

    private long CNH;

    public ObterRequest() {
    }

    public ObterRequest(
           java.lang.String token,
           java.lang.String integrador,
           int versao,
           long CPF,
           long CNH) {
        super(
            token,
            integrador,
            versao);
        this.CPF = CPF;
        this.CNH = CNH;
    }


    /**
     * Gets the CPF value for this ObterRequest.
     * 
     * @return CPF
     */
    public long getCPF() {
        return CPF;
    }


    /**
     * Sets the CPF value for this ObterRequest.
     * 
     * @param CPF
     */
    public void setCPF(long CPF) {
        this.CPF = CPF;
    }


    /**
     * Gets the CNH value for this ObterRequest.
     * 
     * @return CNH
     */
    public long getCNH() {
        return CNH;
    }


    /**
     * Sets the CNH value for this ObterRequest.
     * 
     * @param CNH
     */
    public void setCNH(long CNH) {
        this.CNH = CNH;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ObterRequest)) return false;
        ObterRequest other = (ObterRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.CPF == other.getCPF() &&
            this.CNH == other.getCNH();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += new Long(getCPF()).hashCode();
        _hashCode += new Long(getCNH()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ObterRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/motoristas/objects", "ObterRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CPF");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/motoristas/objects", "CPF"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CNH");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/motoristas/objects", "CNH"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
