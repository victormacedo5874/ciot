/**
 * MotoristasServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.motoristas;

public interface MotoristasServiceSoap extends java.rmi.Remote {
    public br.adm.ipc.schemas.efrete.motoristas.objects.ObterResponse obter(br.adm.ipc.schemas.efrete.motoristas.objects.ObterRequest obterRequest) throws java.rmi.RemoteException;
    public br.adm.ipc.schemas.efrete.motoristas.objects.GravarResponse gravar(br.adm.ipc.schemas.efrete.motoristas.objects.GravarRequest gravarRequest) throws java.rmi.RemoteException;
}
