/**
 * Telefones.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.objects;

public class Telefones  implements java.io.Serializable {
    private br.adm.ipc.schemas.efrete.objects.Telefone celular;

    private br.adm.ipc.schemas.efrete.objects.Telefone fixo;

    private br.adm.ipc.schemas.efrete.objects.Telefone fax;

    public Telefones() {
    }

    public Telefones(
           br.adm.ipc.schemas.efrete.objects.Telefone celular,
           br.adm.ipc.schemas.efrete.objects.Telefone fixo,
           br.adm.ipc.schemas.efrete.objects.Telefone fax) {
           this.celular = celular;
           this.fixo = fixo;
           this.fax = fax;
    }


    /**
     * Gets the celular value for this Telefones.
     * 
     * @return celular
     */
    public br.adm.ipc.schemas.efrete.objects.Telefone getCelular() {
        return celular;
    }


    /**
     * Sets the celular value for this Telefones.
     * 
     * @param celular
     */
    public void setCelular(br.adm.ipc.schemas.efrete.objects.Telefone celular) {
        this.celular = celular;
    }


    /**
     * Gets the fixo value for this Telefones.
     * 
     * @return fixo
     */
    public br.adm.ipc.schemas.efrete.objects.Telefone getFixo() {
        return fixo;
    }


    /**
     * Sets the fixo value for this Telefones.
     * 
     * @param fixo
     */
    public void setFixo(br.adm.ipc.schemas.efrete.objects.Telefone fixo) {
        this.fixo = fixo;
    }


    /**
     * Gets the fax value for this Telefones.
     * 
     * @return fax
     */
    public br.adm.ipc.schemas.efrete.objects.Telefone getFax() {
        return fax;
    }


    /**
     * Sets the fax value for this Telefones.
     * 
     * @param fax
     */
    public void setFax(br.adm.ipc.schemas.efrete.objects.Telefone fax) {
        this.fax = fax;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Telefones)) return false;
        Telefones other = (Telefones) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.celular==null && other.getCelular()==null) || 
             (this.celular!=null &&
              this.celular.equals(other.getCelular()))) &&
            ((this.fixo==null && other.getFixo()==null) || 
             (this.fixo!=null &&
              this.fixo.equals(other.getFixo()))) &&
            ((this.fax==null && other.getFax()==null) || 
             (this.fax!=null &&
              this.fax.equals(other.getFax())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCelular() != null) {
            _hashCode += getCelular().hashCode();
        }
        if (getFixo() != null) {
            _hashCode += getFixo().hashCode();
        }
        if (getFax() != null) {
            _hashCode += getFax().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Telefones.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/objects", "Telefones"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("celular");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/objects", "Celular"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/objects", "Telefone"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fixo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/objects", "Fixo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/objects", "Telefone"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fax");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/objects", "Fax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/objects", "Telefone"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
