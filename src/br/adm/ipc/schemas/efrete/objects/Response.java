/**
 * Response.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.objects;

public abstract class Response  implements java.io.Serializable {
    private int versao;

    private boolean sucesso;

    private br.adm.ipc.schemas.efrete.objects.Excecao excecao;

    private java.lang.Long protocoloServico;

    public Response() {
    }

    public Response(
           int versao,
           boolean sucesso,
           br.adm.ipc.schemas.efrete.objects.Excecao excecao,
           java.lang.Long protocoloServico) {
           this.versao = versao;
           this.sucesso = sucesso;
           this.excecao = excecao;
           this.protocoloServico = protocoloServico;
    }


    /**
     * Gets the versao value for this Response.
     * 
     * @return versao
     */
    public int getVersao() {
        return versao;
    }


    /**
     * Sets the versao value for this Response.
     * 
     * @param versao
     */
    public void setVersao(int versao) {
        this.versao = versao;
    }


    /**
     * Gets the sucesso value for this Response.
     * 
     * @return sucesso
     */
    public boolean isSucesso() {
        return sucesso;
    }


    /**
     * Sets the sucesso value for this Response.
     * 
     * @param sucesso
     */
    public void setSucesso(boolean sucesso) {
        this.sucesso = sucesso;
    }


    /**
     * Gets the excecao value for this Response.
     * 
     * @return excecao
     */
    public br.adm.ipc.schemas.efrete.objects.Excecao getExcecao() {
        return excecao;
    }


    /**
     * Sets the excecao value for this Response.
     * 
     * @param excecao
     */
    public void setExcecao(br.adm.ipc.schemas.efrete.objects.Excecao excecao) {
        this.excecao = excecao;
    }


    /**
     * Gets the protocoloServico value for this Response.
     * 
     * @return protocoloServico
     */
    public java.lang.Long getProtocoloServico() {
        return protocoloServico;
    }


    /**
     * Sets the protocoloServico value for this Response.
     * 
     * @param protocoloServico
     */
    public void setProtocoloServico(java.lang.Long protocoloServico) {
        this.protocoloServico = protocoloServico;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Response)) return false;
        Response other = (Response) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.versao == other.getVersao() &&
            this.sucesso == other.isSucesso() &&
            ((this.excecao==null && other.getExcecao()==null) || 
             (this.excecao!=null &&
              this.excecao.equals(other.getExcecao()))) &&
            ((this.protocoloServico==null && other.getProtocoloServico()==null) || 
             (this.protocoloServico!=null &&
              this.protocoloServico.equals(other.getProtocoloServico())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getVersao();
        _hashCode += (isSucesso() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getExcecao() != null) {
            _hashCode += getExcecao().hashCode();
        }
        if (getProtocoloServico() != null) {
            _hashCode += getProtocoloServico().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Response.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/objects", "Response"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("versao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/objects", "Versao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sucesso");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/objects", "Sucesso"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("excecao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/objects", "Excecao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/objects", "Excecao"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("protocoloServico");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/objects", "ProtocoloServico"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
