/**
 * Pagamento.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects;

public class Pagamento  implements java.io.Serializable {
    private java.lang.String idPagamentoCliente;

    private java.util.Calendar dataDeRegistro;

    private java.util.Calendar dataDeLiberacao;

    private java.math.BigDecimal valor;

    private br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.TipoPagamento tipoPagamento;

    private br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.CategoriaPagamento categoria;

    private java.lang.String documento;

    private br.adm.ipc.schemas.efrete.pef.objects.InformacoesBancarias informacoesBancarias;

    private java.lang.String informacaoAdicional;

    private br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.PagamentoCancelamento cancelado;

    private java.lang.String cnpjFilialAbastecimento;

    public Pagamento() {
    }

    public Pagamento(
           java.lang.String idPagamentoCliente,
           java.util.Calendar dataDeRegistro,
           java.util.Calendar dataDeLiberacao,
           java.math.BigDecimal valor,
           br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.TipoPagamento tipoPagamento,
           br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.CategoriaPagamento categoria,
           java.lang.String documento,
           br.adm.ipc.schemas.efrete.pef.objects.InformacoesBancarias informacoesBancarias,
           java.lang.String informacaoAdicional,
           br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.PagamentoCancelamento cancelado,
           java.lang.String cnpjFilialAbastecimento) {
           this.idPagamentoCliente = idPagamentoCliente;
           this.dataDeRegistro = dataDeRegistro;
           this.dataDeLiberacao = dataDeLiberacao;
           this.valor = valor;
           this.tipoPagamento = tipoPagamento;
           this.categoria = categoria;
           this.documento = documento;
           this.informacoesBancarias = informacoesBancarias;
           this.informacaoAdicional = informacaoAdicional;
           this.cancelado = cancelado;
           this.cnpjFilialAbastecimento = cnpjFilialAbastecimento;
    }


    /**
     * Gets the idPagamentoCliente value for this Pagamento.
     * 
     * @return idPagamentoCliente
     */
    public java.lang.String getIdPagamentoCliente() {
        return idPagamentoCliente;
    }


    /**
     * Sets the idPagamentoCliente value for this Pagamento.
     * 
     * @param idPagamentoCliente
     */
    public void setIdPagamentoCliente(java.lang.String idPagamentoCliente) {
        this.idPagamentoCliente = idPagamentoCliente;
    }


    /**
     * Gets the dataDeRegistro value for this Pagamento.
     * 
     * @return dataDeRegistro
     */
    public java.util.Calendar getDataDeRegistro() {
        return dataDeRegistro;
    }


    /**
     * Sets the dataDeRegistro value for this Pagamento.
     * 
     * @param dataDeRegistro
     */
    public void setDataDeRegistro(java.util.Calendar dataDeRegistro) {
        this.dataDeRegistro = dataDeRegistro;
    }


    /**
     * Gets the dataDeLiberacao value for this Pagamento.
     * 
     * @return dataDeLiberacao
     */
    public java.util.Calendar getDataDeLiberacao() {
        return dataDeLiberacao;
    }


    /**
     * Sets the dataDeLiberacao value for this Pagamento.
     * 
     * @param dataDeLiberacao
     */
    public void setDataDeLiberacao(java.util.Calendar dataDeLiberacao) {
        this.dataDeLiberacao = dataDeLiberacao;
    }


    /**
     * Gets the valor value for this Pagamento.
     * 
     * @return valor
     */
    public java.math.BigDecimal getValor() {
        return valor;
    }


    /**
     * Sets the valor value for this Pagamento.
     * 
     * @param valor
     */
    public void setValor(java.math.BigDecimal valor) {
        this.valor = valor;
    }


    /**
     * Gets the tipoPagamento value for this Pagamento.
     * 
     * @return tipoPagamento
     */
    public br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }


    /**
     * Sets the tipoPagamento value for this Pagamento.
     * 
     * @param tipoPagamento
     */
    public void setTipoPagamento(br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }


    /**
     * Gets the categoria value for this Pagamento.
     * 
     * @return categoria
     */
    public br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.CategoriaPagamento getCategoria() {
        return categoria;
    }


    /**
     * Sets the categoria value for this Pagamento.
     * 
     * @param categoria
     */
    public void setCategoria(br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.CategoriaPagamento categoria) {
        this.categoria = categoria;
    }


    /**
     * Gets the documento value for this Pagamento.
     * 
     * @return documento
     */
    public java.lang.String getDocumento() {
        return documento;
    }


    /**
     * Sets the documento value for this Pagamento.
     * 
     * @param documento
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }


    /**
     * Gets the informacoesBancarias value for this Pagamento.
     * 
     * @return informacoesBancarias
     */
    public br.adm.ipc.schemas.efrete.pef.objects.InformacoesBancarias getInformacoesBancarias() {
        return informacoesBancarias;
    }


    /**
     * Sets the informacoesBancarias value for this Pagamento.
     * 
     * @param informacoesBancarias
     */
    public void setInformacoesBancarias(br.adm.ipc.schemas.efrete.pef.objects.InformacoesBancarias informacoesBancarias) {
        this.informacoesBancarias = informacoesBancarias;
    }


    /**
     * Gets the informacaoAdicional value for this Pagamento.
     * 
     * @return informacaoAdicional
     */
    public java.lang.String getInformacaoAdicional() {
        return informacaoAdicional;
    }


    /**
     * Sets the informacaoAdicional value for this Pagamento.
     * 
     * @param informacaoAdicional
     */
    public void setInformacaoAdicional(java.lang.String informacaoAdicional) {
        this.informacaoAdicional = informacaoAdicional;
    }


    /**
     * Gets the cancelado value for this Pagamento.
     * 
     * @return cancelado
     */
    public br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.PagamentoCancelamento getCancelado() {
        return cancelado;
    }


    /**
     * Sets the cancelado value for this Pagamento.
     * 
     * @param cancelado
     */
    public void setCancelado(br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.PagamentoCancelamento cancelado) {
        this.cancelado = cancelado;
    }


    /**
     * Gets the cnpjFilialAbastecimento value for this Pagamento.
     * 
     * @return cnpjFilialAbastecimento
     */
    public java.lang.String getCnpjFilialAbastecimento() {
        return cnpjFilialAbastecimento;
    }


    /**
     * Sets the cnpjFilialAbastecimento value for this Pagamento.
     * 
     * @param cnpjFilialAbastecimento
     */
    public void setCnpjFilialAbastecimento(java.lang.String cnpjFilialAbastecimento) {
        this.cnpjFilialAbastecimento = cnpjFilialAbastecimento;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Pagamento)) return false;
        Pagamento other = (Pagamento) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.idPagamentoCliente==null && other.getIdPagamentoCliente()==null) || 
             (this.idPagamentoCliente!=null &&
              this.idPagamentoCliente.equals(other.getIdPagamentoCliente()))) &&
            ((this.dataDeRegistro==null && other.getDataDeRegistro()==null) || 
             (this.dataDeRegistro!=null &&
              this.dataDeRegistro.equals(other.getDataDeRegistro()))) &&
            ((this.dataDeLiberacao==null && other.getDataDeLiberacao()==null) || 
             (this.dataDeLiberacao!=null &&
              this.dataDeLiberacao.equals(other.getDataDeLiberacao()))) &&
            ((this.valor==null && other.getValor()==null) || 
             (this.valor!=null &&
              this.valor.equals(other.getValor()))) &&
            ((this.tipoPagamento==null && other.getTipoPagamento()==null) || 
             (this.tipoPagamento!=null &&
              this.tipoPagamento.equals(other.getTipoPagamento()))) &&
            ((this.categoria==null && other.getCategoria()==null) || 
             (this.categoria!=null &&
              this.categoria.equals(other.getCategoria()))) &&
            ((this.documento==null && other.getDocumento()==null) || 
             (this.documento!=null &&
              this.documento.equals(other.getDocumento()))) &&
            ((this.informacoesBancarias==null && other.getInformacoesBancarias()==null) || 
             (this.informacoesBancarias!=null &&
              this.informacoesBancarias.equals(other.getInformacoesBancarias()))) &&
            ((this.informacaoAdicional==null && other.getInformacaoAdicional()==null) || 
             (this.informacaoAdicional!=null &&
              this.informacaoAdicional.equals(other.getInformacaoAdicional()))) &&
            ((this.cancelado==null && other.getCancelado()==null) || 
             (this.cancelado!=null &&
              this.cancelado.equals(other.getCancelado()))) &&
            ((this.cnpjFilialAbastecimento==null && other.getCnpjFilialAbastecimento()==null) || 
             (this.cnpjFilialAbastecimento!=null &&
              this.cnpjFilialAbastecimento.equals(other.getCnpjFilialAbastecimento())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIdPagamentoCliente() != null) {
            _hashCode += getIdPagamentoCliente().hashCode();
        }
        if (getDataDeRegistro() != null) {
            _hashCode += getDataDeRegistro().hashCode();
        }
        if (getDataDeLiberacao() != null) {
            _hashCode += getDataDeLiberacao().hashCode();
        }
        if (getValor() != null) {
            _hashCode += getValor().hashCode();
        }
        if (getTipoPagamento() != null) {
            _hashCode += getTipoPagamento().hashCode();
        }
        if (getCategoria() != null) {
            _hashCode += getCategoria().hashCode();
        }
        if (getDocumento() != null) {
            _hashCode += getDocumento().hashCode();
        }
        if (getInformacoesBancarias() != null) {
            _hashCode += getInformacoesBancarias().hashCode();
        }
        if (getInformacaoAdicional() != null) {
            _hashCode += getInformacaoAdicional().hashCode();
        }
        if (getCancelado() != null) {
            _hashCode += getCancelado().hashCode();
        }
        if (getCnpjFilialAbastecimento() != null) {
            _hashCode += getCnpjFilialAbastecimento().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Pagamento.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Pagamento"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idPagamentoCliente");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "IdPagamentoCliente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataDeRegistro");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "DataDeRegistro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataDeLiberacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "DataDeLiberacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valor");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Valor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoPagamento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "TipoPagamento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "TipoPagamento"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("categoria");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Categoria"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "CategoriaPagamento"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("documento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Documento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("informacoesBancarias");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "InformacoesBancarias"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "InformacoesBancarias"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("informacaoAdicional");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "InformacaoAdicional"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cancelado");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Cancelado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "PagamentoCancelamento"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cnpjFilialAbastecimento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "CnpjFilialAbastecimento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
