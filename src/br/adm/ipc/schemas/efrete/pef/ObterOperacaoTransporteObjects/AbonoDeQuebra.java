/**
 * AbonoDeQuebra.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects;

public class AbonoDeQuebra  implements java.io.Serializable {
    private br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.TipoAbonoDeQuebra tipo;

    private java.math.BigDecimal valor;

    public AbonoDeQuebra() {
    }

    public AbonoDeQuebra(
           br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.TipoAbonoDeQuebra tipo,
           java.math.BigDecimal valor) {
           this.tipo = tipo;
           this.valor = valor;
    }


    /**
     * Gets the tipo value for this AbonoDeQuebra.
     * 
     * @return tipo
     */
    public br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.TipoAbonoDeQuebra getTipo() {
        return tipo;
    }


    /**
     * Sets the tipo value for this AbonoDeQuebra.
     * 
     * @param tipo
     */
    public void setTipo(br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.TipoAbonoDeQuebra tipo) {
        this.tipo = tipo;
    }


    /**
     * Gets the valor value for this AbonoDeQuebra.
     * 
     * @return valor
     */
    public java.math.BigDecimal getValor() {
        return valor;
    }


    /**
     * Sets the valor value for this AbonoDeQuebra.
     * 
     * @param valor
     */
    public void setValor(java.math.BigDecimal valor) {
        this.valor = valor;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AbonoDeQuebra)) return false;
        AbonoDeQuebra other = (AbonoDeQuebra) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.tipo==null && other.getTipo()==null) || 
             (this.tipo!=null &&
              this.tipo.equals(other.getTipo()))) &&
            ((this.valor==null && other.getValor()==null) || 
             (this.valor!=null &&
              this.valor.equals(other.getValor())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTipo() != null) {
            _hashCode += getTipo().hashCode();
        }
        if (getValor() != null) {
            _hashCode += getValor().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AbonoDeQuebra.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "AbonoDeQuebra"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Tipo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "TipoAbonoDeQuebra"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valor");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Valor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
