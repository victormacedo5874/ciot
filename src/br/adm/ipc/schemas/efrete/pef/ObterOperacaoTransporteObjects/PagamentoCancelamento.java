/**
 * PagamentoCancelamento.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects;

public class PagamentoCancelamento  implements java.io.Serializable {
    private boolean estaCancelado;

    private java.lang.String motivo;

    private java.util.Calendar dataCancelamento;

    private java.lang.String solicitante;

    public PagamentoCancelamento() {
    }

    public PagamentoCancelamento(
           boolean estaCancelado,
           java.lang.String motivo,
           java.util.Calendar dataCancelamento,
           java.lang.String solicitante) {
           this.estaCancelado = estaCancelado;
           this.motivo = motivo;
           this.dataCancelamento = dataCancelamento;
           this.solicitante = solicitante;
    }


    /**
     * Gets the estaCancelado value for this PagamentoCancelamento.
     * 
     * @return estaCancelado
     */
    public boolean isEstaCancelado() {
        return estaCancelado;
    }


    /**
     * Sets the estaCancelado value for this PagamentoCancelamento.
     * 
     * @param estaCancelado
     */
    public void setEstaCancelado(boolean estaCancelado) {
        this.estaCancelado = estaCancelado;
    }


    /**
     * Gets the motivo value for this PagamentoCancelamento.
     * 
     * @return motivo
     */
    public java.lang.String getMotivo() {
        return motivo;
    }


    /**
     * Sets the motivo value for this PagamentoCancelamento.
     * 
     * @param motivo
     */
    public void setMotivo(java.lang.String motivo) {
        this.motivo = motivo;
    }


    /**
     * Gets the dataCancelamento value for this PagamentoCancelamento.
     * 
     * @return dataCancelamento
     */
    public java.util.Calendar getDataCancelamento() {
        return dataCancelamento;
    }


    /**
     * Sets the dataCancelamento value for this PagamentoCancelamento.
     * 
     * @param dataCancelamento
     */
    public void setDataCancelamento(java.util.Calendar dataCancelamento) {
        this.dataCancelamento = dataCancelamento;
    }


    /**
     * Gets the solicitante value for this PagamentoCancelamento.
     * 
     * @return solicitante
     */
    public java.lang.String getSolicitante() {
        return solicitante;
    }


    /**
     * Sets the solicitante value for this PagamentoCancelamento.
     * 
     * @param solicitante
     */
    public void setSolicitante(java.lang.String solicitante) {
        this.solicitante = solicitante;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PagamentoCancelamento)) return false;
        PagamentoCancelamento other = (PagamentoCancelamento) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.estaCancelado == other.isEstaCancelado() &&
            ((this.motivo==null && other.getMotivo()==null) || 
             (this.motivo!=null &&
              this.motivo.equals(other.getMotivo()))) &&
            ((this.dataCancelamento==null && other.getDataCancelamento()==null) || 
             (this.dataCancelamento!=null &&
              this.dataCancelamento.equals(other.getDataCancelamento()))) &&
            ((this.solicitante==null && other.getSolicitante()==null) || 
             (this.solicitante!=null &&
              this.solicitante.equals(other.getSolicitante())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += (isEstaCancelado() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getMotivo() != null) {
            _hashCode += getMotivo().hashCode();
        }
        if (getDataCancelamento() != null) {
            _hashCode += getDataCancelamento().hashCode();
        }
        if (getSolicitante() != null) {
            _hashCode += getSolicitante().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PagamentoCancelamento.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "PagamentoCancelamento"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estaCancelado");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "EstaCancelado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("motivo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Motivo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataCancelamento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "DataCancelamento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("solicitante");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Solicitante"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
