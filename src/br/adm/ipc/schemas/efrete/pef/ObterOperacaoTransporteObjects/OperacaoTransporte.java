/**
 * OperacaoTransporte.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects;

public class OperacaoTransporte  implements java.io.Serializable {
    private br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Cancelamento cancelamento;

    private br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Encerramento encerramento;

    private java.lang.String codigoIdentificacaoOperacao;

    private br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.TipoViagem tipoViagem;

    private long transacao;

    private long matrizCNPJ;

    private java.lang.Long filialCNPJ;

    private java.lang.String idOperacaoCliente;

    private java.util.Calendar dataRegistro;

    private java.util.Calendar dataInicioViagem;

    private java.util.Calendar dataFimViagem;

    private java.util.Calendar dataRetificacao;

    private int codigoNCMNaturezaCarga;

    private org.apache.axis.types.UnsignedShort codigoTipoCarga;

    private java.lang.Boolean altoDesempenho;

    private java.lang.Boolean destinacaoComercial;

    private java.lang.Boolean freteRetorno;

    private java.lang.String cepRetorno;

    private java.lang.Long distanciaRetorno;

    private java.math.BigDecimal pesoCarga;

    private br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Viagem[] viagens;

    private br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Pagamento[] pagamentos;

    private br.adm.ipc.schemas.efrete.pef.objects.Impostos impostos;

    private br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Contratado contratado;

    private br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Motorista motorista;

    private br.adm.ipc.schemas.efrete.pef.objects.Destinatario destinatario;

    private br.adm.ipc.schemas.efrete.pef.objects.Contratante contratante;

    private br.adm.ipc.schemas.efrete.pef.objects.Subcontratante subcontratante;

    private br.adm.ipc.schemas.efrete.pef.objects.Consignatario consignatario;

    private br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Veiculo[] veiculos;

    private java.lang.String avisoTransportador;

    private java.lang.String[] observacoesAoTransportador;

    private java.lang.String[] observacoesAoCredenciado;

    private br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.TipoPagamento tipoPagamento;

    private java.lang.Integer quantidadeSaques;

    private java.lang.Integer quantidadeTransferencias;

    private java.math.BigDecimal valorSaques;

    private java.math.BigDecimal valorTransferencias;

    private int versaoServico;

    public OperacaoTransporte() {
    }

    public OperacaoTransporte(
           br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Cancelamento cancelamento,
           br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Encerramento encerramento,
           java.lang.String codigoIdentificacaoOperacao,
           br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.TipoViagem tipoViagem,
           long transacao,
           long matrizCNPJ,
           java.lang.Long filialCNPJ,
           java.lang.String idOperacaoCliente,
           java.util.Calendar dataRegistro,
           java.util.Calendar dataInicioViagem,
           java.util.Calendar dataFimViagem,
           java.util.Calendar dataRetificacao,
           int codigoNCMNaturezaCarga,
           org.apache.axis.types.UnsignedShort codigoTipoCarga,
           java.lang.Boolean altoDesempenho,
           java.lang.Boolean destinacaoComercial,
           java.lang.Boolean freteRetorno,
           java.lang.String cepRetorno,
           java.lang.Long distanciaRetorno,
           java.math.BigDecimal pesoCarga,
           br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Viagem[] viagens,
           br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Pagamento[] pagamentos,
           br.adm.ipc.schemas.efrete.pef.objects.Impostos impostos,
           br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Contratado contratado,
           br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Motorista motorista,
           br.adm.ipc.schemas.efrete.pef.objects.Destinatario destinatario,
           br.adm.ipc.schemas.efrete.pef.objects.Contratante contratante,
           br.adm.ipc.schemas.efrete.pef.objects.Subcontratante subcontratante,
           br.adm.ipc.schemas.efrete.pef.objects.Consignatario consignatario,
           br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Veiculo[] veiculos,
           java.lang.String avisoTransportador,
           java.lang.String[] observacoesAoTransportador,
           java.lang.String[] observacoesAoCredenciado,
           br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.TipoPagamento tipoPagamento,
           java.lang.Integer quantidadeSaques,
           java.lang.Integer quantidadeTransferencias,
           java.math.BigDecimal valorSaques,
           java.math.BigDecimal valorTransferencias,
           int versaoServico) {
           this.cancelamento = cancelamento;
           this.encerramento = encerramento;
           this.codigoIdentificacaoOperacao = codigoIdentificacaoOperacao;
           this.tipoViagem = tipoViagem;
           this.transacao = transacao;
           this.matrizCNPJ = matrizCNPJ;
           this.filialCNPJ = filialCNPJ;
           this.idOperacaoCliente = idOperacaoCliente;
           this.dataRegistro = dataRegistro;
           this.dataInicioViagem = dataInicioViagem;
           this.dataFimViagem = dataFimViagem;
           this.dataRetificacao = dataRetificacao;
           this.codigoNCMNaturezaCarga = codigoNCMNaturezaCarga;
           this.codigoTipoCarga = codigoTipoCarga;
           this.altoDesempenho = altoDesempenho;
           this.destinacaoComercial = destinacaoComercial;
           this.freteRetorno = freteRetorno;
           this.cepRetorno = cepRetorno;
           this.distanciaRetorno = distanciaRetorno;
           this.pesoCarga = pesoCarga;
           this.viagens = viagens;
           this.pagamentos = pagamentos;
           this.impostos = impostos;
           this.contratado = contratado;
           this.motorista = motorista;
           this.destinatario = destinatario;
           this.contratante = contratante;
           this.subcontratante = subcontratante;
           this.consignatario = consignatario;
           this.veiculos = veiculos;
           this.avisoTransportador = avisoTransportador;
           this.observacoesAoTransportador = observacoesAoTransportador;
           this.observacoesAoCredenciado = observacoesAoCredenciado;
           this.tipoPagamento = tipoPagamento;
           this.quantidadeSaques = quantidadeSaques;
           this.quantidadeTransferencias = quantidadeTransferencias;
           this.valorSaques = valorSaques;
           this.valorTransferencias = valorTransferencias;
           this.versaoServico = versaoServico;
    }


    /**
     * Gets the cancelamento value for this OperacaoTransporte.
     * 
     * @return cancelamento
     */
    public br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Cancelamento getCancelamento() {
        return cancelamento;
    }


    /**
     * Sets the cancelamento value for this OperacaoTransporte.
     * 
     * @param cancelamento
     */
    public void setCancelamento(br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Cancelamento cancelamento) {
        this.cancelamento = cancelamento;
    }


    /**
     * Gets the encerramento value for this OperacaoTransporte.
     * 
     * @return encerramento
     */
    public br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Encerramento getEncerramento() {
        return encerramento;
    }


    /**
     * Sets the encerramento value for this OperacaoTransporte.
     * 
     * @param encerramento
     */
    public void setEncerramento(br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Encerramento encerramento) {
        this.encerramento = encerramento;
    }


    /**
     * Gets the codigoIdentificacaoOperacao value for this OperacaoTransporte.
     * 
     * @return codigoIdentificacaoOperacao
     */
    public java.lang.String getCodigoIdentificacaoOperacao() {
        return codigoIdentificacaoOperacao;
    }


    /**
     * Sets the codigoIdentificacaoOperacao value for this OperacaoTransporte.
     * 
     * @param codigoIdentificacaoOperacao
     */
    public void setCodigoIdentificacaoOperacao(java.lang.String codigoIdentificacaoOperacao) {
        this.codigoIdentificacaoOperacao = codigoIdentificacaoOperacao;
    }


    /**
     * Gets the tipoViagem value for this OperacaoTransporte.
     * 
     * @return tipoViagem
     */
    public br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.TipoViagem getTipoViagem() {
        return tipoViagem;
    }


    /**
     * Sets the tipoViagem value for this OperacaoTransporte.
     * 
     * @param tipoViagem
     */
    public void setTipoViagem(br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.TipoViagem tipoViagem) {
        this.tipoViagem = tipoViagem;
    }


    /**
     * Gets the transacao value for this OperacaoTransporte.
     * 
     * @return transacao
     */
    public long getTransacao() {
        return transacao;
    }


    /**
     * Sets the transacao value for this OperacaoTransporte.
     * 
     * @param transacao
     */
    public void setTransacao(long transacao) {
        this.transacao = transacao;
    }


    /**
     * Gets the matrizCNPJ value for this OperacaoTransporte.
     * 
     * @return matrizCNPJ
     */
    public long getMatrizCNPJ() {
        return matrizCNPJ;
    }


    /**
     * Sets the matrizCNPJ value for this OperacaoTransporte.
     * 
     * @param matrizCNPJ
     */
    public void setMatrizCNPJ(long matrizCNPJ) {
        this.matrizCNPJ = matrizCNPJ;
    }


    /**
     * Gets the filialCNPJ value for this OperacaoTransporte.
     * 
     * @return filialCNPJ
     */
    public java.lang.Long getFilialCNPJ() {
        return filialCNPJ;
    }


    /**
     * Sets the filialCNPJ value for this OperacaoTransporte.
     * 
     * @param filialCNPJ
     */
    public void setFilialCNPJ(java.lang.Long filialCNPJ) {
        this.filialCNPJ = filialCNPJ;
    }


    /**
     * Gets the idOperacaoCliente value for this OperacaoTransporte.
     * 
     * @return idOperacaoCliente
     */
    public java.lang.String getIdOperacaoCliente() {
        return idOperacaoCliente;
    }


    /**
     * Sets the idOperacaoCliente value for this OperacaoTransporte.
     * 
     * @param idOperacaoCliente
     */
    public void setIdOperacaoCliente(java.lang.String idOperacaoCliente) {
        this.idOperacaoCliente = idOperacaoCliente;
    }


    /**
     * Gets the dataRegistro value for this OperacaoTransporte.
     * 
     * @return dataRegistro
     */
    public java.util.Calendar getDataRegistro() {
        return dataRegistro;
    }


    /**
     * Sets the dataRegistro value for this OperacaoTransporte.
     * 
     * @param dataRegistro
     */
    public void setDataRegistro(java.util.Calendar dataRegistro) {
        this.dataRegistro = dataRegistro;
    }


    /**
     * Gets the dataInicioViagem value for this OperacaoTransporte.
     * 
     * @return dataInicioViagem
     */
    public java.util.Calendar getDataInicioViagem() {
        return dataInicioViagem;
    }


    /**
     * Sets the dataInicioViagem value for this OperacaoTransporte.
     * 
     * @param dataInicioViagem
     */
    public void setDataInicioViagem(java.util.Calendar dataInicioViagem) {
        this.dataInicioViagem = dataInicioViagem;
    }


    /**
     * Gets the dataFimViagem value for this OperacaoTransporte.
     * 
     * @return dataFimViagem
     */
    public java.util.Calendar getDataFimViagem() {
        return dataFimViagem;
    }


    /**
     * Sets the dataFimViagem value for this OperacaoTransporte.
     * 
     * @param dataFimViagem
     */
    public void setDataFimViagem(java.util.Calendar dataFimViagem) {
        this.dataFimViagem = dataFimViagem;
    }


    /**
     * Gets the dataRetificacao value for this OperacaoTransporte.
     * 
     * @return dataRetificacao
     */
    public java.util.Calendar getDataRetificacao() {
        return dataRetificacao;
    }


    /**
     * Sets the dataRetificacao value for this OperacaoTransporte.
     * 
     * @param dataRetificacao
     */
    public void setDataRetificacao(java.util.Calendar dataRetificacao) {
        this.dataRetificacao = dataRetificacao;
    }


    /**
     * Gets the codigoNCMNaturezaCarga value for this OperacaoTransporte.
     * 
     * @return codigoNCMNaturezaCarga
     */
    public int getCodigoNCMNaturezaCarga() {
        return codigoNCMNaturezaCarga;
    }


    /**
     * Sets the codigoNCMNaturezaCarga value for this OperacaoTransporte.
     * 
     * @param codigoNCMNaturezaCarga
     */
    public void setCodigoNCMNaturezaCarga(int codigoNCMNaturezaCarga) {
        this.codigoNCMNaturezaCarga = codigoNCMNaturezaCarga;
    }


    /**
     * Gets the codigoTipoCarga value for this OperacaoTransporte.
     * 
     * @return codigoTipoCarga
     */
    public org.apache.axis.types.UnsignedShort getCodigoTipoCarga() {
        return codigoTipoCarga;
    }


    /**
     * Sets the codigoTipoCarga value for this OperacaoTransporte.
     * 
     * @param codigoTipoCarga
     */
    public void setCodigoTipoCarga(org.apache.axis.types.UnsignedShort codigoTipoCarga) {
        this.codigoTipoCarga = codigoTipoCarga;
    }


    /**
     * Gets the altoDesempenho value for this OperacaoTransporte.
     * 
     * @return altoDesempenho
     */
    public java.lang.Boolean getAltoDesempenho() {
        return altoDesempenho;
    }


    /**
     * Sets the altoDesempenho value for this OperacaoTransporte.
     * 
     * @param altoDesempenho
     */
    public void setAltoDesempenho(java.lang.Boolean altoDesempenho) {
        this.altoDesempenho = altoDesempenho;
    }


    /**
     * Gets the destinacaoComercial value for this OperacaoTransporte.
     * 
     * @return destinacaoComercial
     */
    public java.lang.Boolean getDestinacaoComercial() {
        return destinacaoComercial;
    }


    /**
     * Sets the destinacaoComercial value for this OperacaoTransporte.
     * 
     * @param destinacaoComercial
     */
    public void setDestinacaoComercial(java.lang.Boolean destinacaoComercial) {
        this.destinacaoComercial = destinacaoComercial;
    }


    /**
     * Gets the freteRetorno value for this OperacaoTransporte.
     * 
     * @return freteRetorno
     */
    public java.lang.Boolean getFreteRetorno() {
        return freteRetorno;
    }


    /**
     * Sets the freteRetorno value for this OperacaoTransporte.
     * 
     * @param freteRetorno
     */
    public void setFreteRetorno(java.lang.Boolean freteRetorno) {
        this.freteRetorno = freteRetorno;
    }


    /**
     * Gets the cepRetorno value for this OperacaoTransporte.
     * 
     * @return cepRetorno
     */
    public java.lang.String getCepRetorno() {
        return cepRetorno;
    }


    /**
     * Sets the cepRetorno value for this OperacaoTransporte.
     * 
     * @param cepRetorno
     */
    public void setCepRetorno(java.lang.String cepRetorno) {
        this.cepRetorno = cepRetorno;
    }


    /**
     * Gets the distanciaRetorno value for this OperacaoTransporte.
     * 
     * @return distanciaRetorno
     */
    public java.lang.Long getDistanciaRetorno() {
        return distanciaRetorno;
    }


    /**
     * Sets the distanciaRetorno value for this OperacaoTransporte.
     * 
     * @param distanciaRetorno
     */
    public void setDistanciaRetorno(java.lang.Long distanciaRetorno) {
        this.distanciaRetorno = distanciaRetorno;
    }


    /**
     * Gets the pesoCarga value for this OperacaoTransporte.
     * 
     * @return pesoCarga
     */
    public java.math.BigDecimal getPesoCarga() {
        return pesoCarga;
    }


    /**
     * Sets the pesoCarga value for this OperacaoTransporte.
     * 
     * @param pesoCarga
     */
    public void setPesoCarga(java.math.BigDecimal pesoCarga) {
        this.pesoCarga = pesoCarga;
    }


    /**
     * Gets the viagens value for this OperacaoTransporte.
     * 
     * @return viagens
     */
    public br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Viagem[] getViagens() {
        return viagens;
    }


    /**
     * Sets the viagens value for this OperacaoTransporte.
     * 
     * @param viagens
     */
    public void setViagens(br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Viagem[] viagens) {
        this.viagens = viagens;
    }


    /**
     * Gets the pagamentos value for this OperacaoTransporte.
     * 
     * @return pagamentos
     */
    public br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Pagamento[] getPagamentos() {
        return pagamentos;
    }


    /**
     * Sets the pagamentos value for this OperacaoTransporte.
     * 
     * @param pagamentos
     */
    public void setPagamentos(br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Pagamento[] pagamentos) {
        this.pagamentos = pagamentos;
    }


    /**
     * Gets the impostos value for this OperacaoTransporte.
     * 
     * @return impostos
     */
    public br.adm.ipc.schemas.efrete.pef.objects.Impostos getImpostos() {
        return impostos;
    }


    /**
     * Sets the impostos value for this OperacaoTransporte.
     * 
     * @param impostos
     */
    public void setImpostos(br.adm.ipc.schemas.efrete.pef.objects.Impostos impostos) {
        this.impostos = impostos;
    }


    /**
     * Gets the contratado value for this OperacaoTransporte.
     * 
     * @return contratado
     */
    public br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Contratado getContratado() {
        return contratado;
    }


    /**
     * Sets the contratado value for this OperacaoTransporte.
     * 
     * @param contratado
     */
    public void setContratado(br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Contratado contratado) {
        this.contratado = contratado;
    }


    /**
     * Gets the motorista value for this OperacaoTransporte.
     * 
     * @return motorista
     */
    public br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Motorista getMotorista() {
        return motorista;
    }


    /**
     * Sets the motorista value for this OperacaoTransporte.
     * 
     * @param motorista
     */
    public void setMotorista(br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Motorista motorista) {
        this.motorista = motorista;
    }


    /**
     * Gets the destinatario value for this OperacaoTransporte.
     * 
     * @return destinatario
     */
    public br.adm.ipc.schemas.efrete.pef.objects.Destinatario getDestinatario() {
        return destinatario;
    }


    /**
     * Sets the destinatario value for this OperacaoTransporte.
     * 
     * @param destinatario
     */
    public void setDestinatario(br.adm.ipc.schemas.efrete.pef.objects.Destinatario destinatario) {
        this.destinatario = destinatario;
    }


    /**
     * Gets the contratante value for this OperacaoTransporte.
     * 
     * @return contratante
     */
    public br.adm.ipc.schemas.efrete.pef.objects.Contratante getContratante() {
        return contratante;
    }


    /**
     * Sets the contratante value for this OperacaoTransporte.
     * 
     * @param contratante
     */
    public void setContratante(br.adm.ipc.schemas.efrete.pef.objects.Contratante contratante) {
        this.contratante = contratante;
    }


    /**
     * Gets the subcontratante value for this OperacaoTransporte.
     * 
     * @return subcontratante
     */
    public br.adm.ipc.schemas.efrete.pef.objects.Subcontratante getSubcontratante() {
        return subcontratante;
    }


    /**
     * Sets the subcontratante value for this OperacaoTransporte.
     * 
     * @param subcontratante
     */
    public void setSubcontratante(br.adm.ipc.schemas.efrete.pef.objects.Subcontratante subcontratante) {
        this.subcontratante = subcontratante;
    }


    /**
     * Gets the consignatario value for this OperacaoTransporte.
     * 
     * @return consignatario
     */
    public br.adm.ipc.schemas.efrete.pef.objects.Consignatario getConsignatario() {
        return consignatario;
    }


    /**
     * Sets the consignatario value for this OperacaoTransporte.
     * 
     * @param consignatario
     */
    public void setConsignatario(br.adm.ipc.schemas.efrete.pef.objects.Consignatario consignatario) {
        this.consignatario = consignatario;
    }


    /**
     * Gets the veiculos value for this OperacaoTransporte.
     * 
     * @return veiculos
     */
    public br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Veiculo[] getVeiculos() {
        return veiculos;
    }


    /**
     * Sets the veiculos value for this OperacaoTransporte.
     * 
     * @param veiculos
     */
    public void setVeiculos(br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Veiculo[] veiculos) {
        this.veiculos = veiculos;
    }


    /**
     * Gets the avisoTransportador value for this OperacaoTransporte.
     * 
     * @return avisoTransportador
     */
    public java.lang.String getAvisoTransportador() {
        return avisoTransportador;
    }


    /**
     * Sets the avisoTransportador value for this OperacaoTransporte.
     * 
     * @param avisoTransportador
     */
    public void setAvisoTransportador(java.lang.String avisoTransportador) {
        this.avisoTransportador = avisoTransportador;
    }


    /**
     * Gets the observacoesAoTransportador value for this OperacaoTransporte.
     * 
     * @return observacoesAoTransportador
     */
    public java.lang.String[] getObservacoesAoTransportador() {
        return observacoesAoTransportador;
    }


    /**
     * Sets the observacoesAoTransportador value for this OperacaoTransporte.
     * 
     * @param observacoesAoTransportador
     */
    public void setObservacoesAoTransportador(java.lang.String[] observacoesAoTransportador) {
        this.observacoesAoTransportador = observacoesAoTransportador;
    }


    /**
     * Gets the observacoesAoCredenciado value for this OperacaoTransporte.
     * 
     * @return observacoesAoCredenciado
     */
    public java.lang.String[] getObservacoesAoCredenciado() {
        return observacoesAoCredenciado;
    }


    /**
     * Sets the observacoesAoCredenciado value for this OperacaoTransporte.
     * 
     * @param observacoesAoCredenciado
     */
    public void setObservacoesAoCredenciado(java.lang.String[] observacoesAoCredenciado) {
        this.observacoesAoCredenciado = observacoesAoCredenciado;
    }


    /**
     * Gets the tipoPagamento value for this OperacaoTransporte.
     * 
     * @return tipoPagamento
     */
    public br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }


    /**
     * Sets the tipoPagamento value for this OperacaoTransporte.
     * 
     * @param tipoPagamento
     */
    public void setTipoPagamento(br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }


    /**
     * Gets the quantidadeSaques value for this OperacaoTransporte.
     * 
     * @return quantidadeSaques
     */
    public java.lang.Integer getQuantidadeSaques() {
        return quantidadeSaques;
    }


    /**
     * Sets the quantidadeSaques value for this OperacaoTransporte.
     * 
     * @param quantidadeSaques
     */
    public void setQuantidadeSaques(java.lang.Integer quantidadeSaques) {
        this.quantidadeSaques = quantidadeSaques;
    }


    /**
     * Gets the quantidadeTransferencias value for this OperacaoTransporte.
     * 
     * @return quantidadeTransferencias
     */
    public java.lang.Integer getQuantidadeTransferencias() {
        return quantidadeTransferencias;
    }


    /**
     * Sets the quantidadeTransferencias value for this OperacaoTransporte.
     * 
     * @param quantidadeTransferencias
     */
    public void setQuantidadeTransferencias(java.lang.Integer quantidadeTransferencias) {
        this.quantidadeTransferencias = quantidadeTransferencias;
    }


    /**
     * Gets the valorSaques value for this OperacaoTransporte.
     * 
     * @return valorSaques
     */
    public java.math.BigDecimal getValorSaques() {
        return valorSaques;
    }


    /**
     * Sets the valorSaques value for this OperacaoTransporte.
     * 
     * @param valorSaques
     */
    public void setValorSaques(java.math.BigDecimal valorSaques) {
        this.valorSaques = valorSaques;
    }


    /**
     * Gets the valorTransferencias value for this OperacaoTransporte.
     * 
     * @return valorTransferencias
     */
    public java.math.BigDecimal getValorTransferencias() {
        return valorTransferencias;
    }


    /**
     * Sets the valorTransferencias value for this OperacaoTransporte.
     * 
     * @param valorTransferencias
     */
    public void setValorTransferencias(java.math.BigDecimal valorTransferencias) {
        this.valorTransferencias = valorTransferencias;
    }


    /**
     * Gets the versaoServico value for this OperacaoTransporte.
     * 
     * @return versaoServico
     */
    public int getVersaoServico() {
        return versaoServico;
    }


    /**
     * Sets the versaoServico value for this OperacaoTransporte.
     * 
     * @param versaoServico
     */
    public void setVersaoServico(int versaoServico) {
        this.versaoServico = versaoServico;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OperacaoTransporte)) return false;
        OperacaoTransporte other = (OperacaoTransporte) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cancelamento==null && other.getCancelamento()==null) || 
             (this.cancelamento!=null &&
              this.cancelamento.equals(other.getCancelamento()))) &&
            ((this.encerramento==null && other.getEncerramento()==null) || 
             (this.encerramento!=null &&
              this.encerramento.equals(other.getEncerramento()))) &&
            ((this.codigoIdentificacaoOperacao==null && other.getCodigoIdentificacaoOperacao()==null) || 
             (this.codigoIdentificacaoOperacao!=null &&
              this.codigoIdentificacaoOperacao.equals(other.getCodigoIdentificacaoOperacao()))) &&
            ((this.tipoViagem==null && other.getTipoViagem()==null) || 
             (this.tipoViagem!=null &&
              this.tipoViagem.equals(other.getTipoViagem()))) &&
            this.transacao == other.getTransacao() &&
            this.matrizCNPJ == other.getMatrizCNPJ() &&
            ((this.filialCNPJ==null && other.getFilialCNPJ()==null) || 
             (this.filialCNPJ!=null &&
              this.filialCNPJ.equals(other.getFilialCNPJ()))) &&
            ((this.idOperacaoCliente==null && other.getIdOperacaoCliente()==null) || 
             (this.idOperacaoCliente!=null &&
              this.idOperacaoCliente.equals(other.getIdOperacaoCliente()))) &&
            ((this.dataRegistro==null && other.getDataRegistro()==null) || 
             (this.dataRegistro!=null &&
              this.dataRegistro.equals(other.getDataRegistro()))) &&
            ((this.dataInicioViagem==null && other.getDataInicioViagem()==null) || 
             (this.dataInicioViagem!=null &&
              this.dataInicioViagem.equals(other.getDataInicioViagem()))) &&
            ((this.dataFimViagem==null && other.getDataFimViagem()==null) || 
             (this.dataFimViagem!=null &&
              this.dataFimViagem.equals(other.getDataFimViagem()))) &&
            ((this.dataRetificacao==null && other.getDataRetificacao()==null) || 
             (this.dataRetificacao!=null &&
              this.dataRetificacao.equals(other.getDataRetificacao()))) &&
            this.codigoNCMNaturezaCarga == other.getCodigoNCMNaturezaCarga() &&
            ((this.codigoTipoCarga==null && other.getCodigoTipoCarga()==null) || 
             (this.codigoTipoCarga!=null &&
              this.codigoTipoCarga.equals(other.getCodigoTipoCarga()))) &&
            ((this.altoDesempenho==null && other.getAltoDesempenho()==null) || 
             (this.altoDesempenho!=null &&
              this.altoDesempenho.equals(other.getAltoDesempenho()))) &&
            ((this.destinacaoComercial==null && other.getDestinacaoComercial()==null) || 
             (this.destinacaoComercial!=null &&
              this.destinacaoComercial.equals(other.getDestinacaoComercial()))) &&
            ((this.freteRetorno==null && other.getFreteRetorno()==null) || 
             (this.freteRetorno!=null &&
              this.freteRetorno.equals(other.getFreteRetorno()))) &&
            ((this.cepRetorno==null && other.getCepRetorno()==null) || 
             (this.cepRetorno!=null &&
              this.cepRetorno.equals(other.getCepRetorno()))) &&
            ((this.distanciaRetorno==null && other.getDistanciaRetorno()==null) || 
             (this.distanciaRetorno!=null &&
              this.distanciaRetorno.equals(other.getDistanciaRetorno()))) &&
            ((this.pesoCarga==null && other.getPesoCarga()==null) || 
             (this.pesoCarga!=null &&
              this.pesoCarga.equals(other.getPesoCarga()))) &&
            ((this.viagens==null && other.getViagens()==null) || 
             (this.viagens!=null &&
              java.util.Arrays.equals(this.viagens, other.getViagens()))) &&
            ((this.pagamentos==null && other.getPagamentos()==null) || 
             (this.pagamentos!=null &&
              java.util.Arrays.equals(this.pagamentos, other.getPagamentos()))) &&
            ((this.impostos==null && other.getImpostos()==null) || 
             (this.impostos!=null &&
              this.impostos.equals(other.getImpostos()))) &&
            ((this.contratado==null && other.getContratado()==null) || 
             (this.contratado!=null &&
              this.contratado.equals(other.getContratado()))) &&
            ((this.motorista==null && other.getMotorista()==null) || 
             (this.motorista!=null &&
              this.motorista.equals(other.getMotorista()))) &&
            ((this.destinatario==null && other.getDestinatario()==null) || 
             (this.destinatario!=null &&
              this.destinatario.equals(other.getDestinatario()))) &&
            ((this.contratante==null && other.getContratante()==null) || 
             (this.contratante!=null &&
              this.contratante.equals(other.getContratante()))) &&
            ((this.subcontratante==null && other.getSubcontratante()==null) || 
             (this.subcontratante!=null &&
              this.subcontratante.equals(other.getSubcontratante()))) &&
            ((this.consignatario==null && other.getConsignatario()==null) || 
             (this.consignatario!=null &&
              this.consignatario.equals(other.getConsignatario()))) &&
            ((this.veiculos==null && other.getVeiculos()==null) || 
             (this.veiculos!=null &&
              java.util.Arrays.equals(this.veiculos, other.getVeiculos()))) &&
            ((this.avisoTransportador==null && other.getAvisoTransportador()==null) || 
             (this.avisoTransportador!=null &&
              this.avisoTransportador.equals(other.getAvisoTransportador()))) &&
            ((this.observacoesAoTransportador==null && other.getObservacoesAoTransportador()==null) || 
             (this.observacoesAoTransportador!=null &&
              java.util.Arrays.equals(this.observacoesAoTransportador, other.getObservacoesAoTransportador()))) &&
            ((this.observacoesAoCredenciado==null && other.getObservacoesAoCredenciado()==null) || 
             (this.observacoesAoCredenciado!=null &&
              java.util.Arrays.equals(this.observacoesAoCredenciado, other.getObservacoesAoCredenciado()))) &&
            ((this.tipoPagamento==null && other.getTipoPagamento()==null) || 
             (this.tipoPagamento!=null &&
              this.tipoPagamento.equals(other.getTipoPagamento()))) &&
            ((this.quantidadeSaques==null && other.getQuantidadeSaques()==null) || 
             (this.quantidadeSaques!=null &&
              this.quantidadeSaques.equals(other.getQuantidadeSaques()))) &&
            ((this.quantidadeTransferencias==null && other.getQuantidadeTransferencias()==null) || 
             (this.quantidadeTransferencias!=null &&
              this.quantidadeTransferencias.equals(other.getQuantidadeTransferencias()))) &&
            ((this.valorSaques==null && other.getValorSaques()==null) || 
             (this.valorSaques!=null &&
              this.valorSaques.equals(other.getValorSaques()))) &&
            ((this.valorTransferencias==null && other.getValorTransferencias()==null) || 
             (this.valorTransferencias!=null &&
              this.valorTransferencias.equals(other.getValorTransferencias()))) &&
            this.versaoServico == other.getVersaoServico();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCancelamento() != null) {
            _hashCode += getCancelamento().hashCode();
        }
        if (getEncerramento() != null) {
            _hashCode += getEncerramento().hashCode();
        }
        if (getCodigoIdentificacaoOperacao() != null) {
            _hashCode += getCodigoIdentificacaoOperacao().hashCode();
        }
        if (getTipoViagem() != null) {
            _hashCode += getTipoViagem().hashCode();
        }
        _hashCode += new Long(getTransacao()).hashCode();
        _hashCode += new Long(getMatrizCNPJ()).hashCode();
        if (getFilialCNPJ() != null) {
            _hashCode += getFilialCNPJ().hashCode();
        }
        if (getIdOperacaoCliente() != null) {
            _hashCode += getIdOperacaoCliente().hashCode();
        }
        if (getDataRegistro() != null) {
            _hashCode += getDataRegistro().hashCode();
        }
        if (getDataInicioViagem() != null) {
            _hashCode += getDataInicioViagem().hashCode();
        }
        if (getDataFimViagem() != null) {
            _hashCode += getDataFimViagem().hashCode();
        }
        if (getDataRetificacao() != null) {
            _hashCode += getDataRetificacao().hashCode();
        }
        _hashCode += getCodigoNCMNaturezaCarga();
        if (getCodigoTipoCarga() != null) {
            _hashCode += getCodigoTipoCarga().hashCode();
        }
        if (getAltoDesempenho() != null) {
            _hashCode += getAltoDesempenho().hashCode();
        }
        if (getDestinacaoComercial() != null) {
            _hashCode += getDestinacaoComercial().hashCode();
        }
        if (getFreteRetorno() != null) {
            _hashCode += getFreteRetorno().hashCode();
        }
        if (getCepRetorno() != null) {
            _hashCode += getCepRetorno().hashCode();
        }
        if (getDistanciaRetorno() != null) {
            _hashCode += getDistanciaRetorno().hashCode();
        }
        if (getPesoCarga() != null) {
            _hashCode += getPesoCarga().hashCode();
        }
        if (getViagens() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getViagens());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getViagens(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPagamentos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPagamentos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPagamentos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getImpostos() != null) {
            _hashCode += getImpostos().hashCode();
        }
        if (getContratado() != null) {
            _hashCode += getContratado().hashCode();
        }
        if (getMotorista() != null) {
            _hashCode += getMotorista().hashCode();
        }
        if (getDestinatario() != null) {
            _hashCode += getDestinatario().hashCode();
        }
        if (getContratante() != null) {
            _hashCode += getContratante().hashCode();
        }
        if (getSubcontratante() != null) {
            _hashCode += getSubcontratante().hashCode();
        }
        if (getConsignatario() != null) {
            _hashCode += getConsignatario().hashCode();
        }
        if (getVeiculos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getVeiculos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getVeiculos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getAvisoTransportador() != null) {
            _hashCode += getAvisoTransportador().hashCode();
        }
        if (getObservacoesAoTransportador() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getObservacoesAoTransportador());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getObservacoesAoTransportador(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getObservacoesAoCredenciado() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getObservacoesAoCredenciado());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getObservacoesAoCredenciado(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTipoPagamento() != null) {
            _hashCode += getTipoPagamento().hashCode();
        }
        if (getQuantidadeSaques() != null) {
            _hashCode += getQuantidadeSaques().hashCode();
        }
        if (getQuantidadeTransferencias() != null) {
            _hashCode += getQuantidadeTransferencias().hashCode();
        }
        if (getValorSaques() != null) {
            _hashCode += getValorSaques().hashCode();
        }
        if (getValorTransferencias() != null) {
            _hashCode += getValorTransferencias().hashCode();
        }
        _hashCode += getVersaoServico();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OperacaoTransporte.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "OperacaoTransporte"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cancelamento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Cancelamento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Cancelamento"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("encerramento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Encerramento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Encerramento"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoIdentificacaoOperacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "CodigoIdentificacaoOperacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoViagem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "TipoViagem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "TipoViagem"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Transacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("matrizCNPJ");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "MatrizCNPJ"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filialCNPJ");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "FilialCNPJ"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idOperacaoCliente");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "IdOperacaoCliente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataRegistro");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "DataRegistro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataInicioViagem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "DataInicioViagem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataFimViagem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "DataFimViagem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataRetificacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "DataRetificacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoNCMNaturezaCarga");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "CodigoNCMNaturezaCarga"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoTipoCarga");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "CodigoTipoCarga"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedShort"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("altoDesempenho");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "AltoDesempenho"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("destinacaoComercial");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "DestinacaoComercial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("freteRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "FreteRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "CepRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("distanciaRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "DistanciaRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pesoCarga");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "PesoCarga"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("viagens");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Viagens"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Viagem"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Viagem"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pagamentos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Pagamentos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Pagamento"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Pagamento"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("impostos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Impostos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Impostos"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contratado");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Contratado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Contratado"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("motorista");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Motorista"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Motorista"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("destinatario");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Destinatario"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Destinatario"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contratante");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Contratante"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Contratante"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subcontratante");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Subcontratante"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Subcontratante"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consignatario");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Consignatario"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Consignatario"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("veiculos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Veiculos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Veiculo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Veiculo"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avisoTransportador");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "AvisoTransportador"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("observacoesAoTransportador");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "ObservacoesAoTransportador"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("observacoesAoCredenciado");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "ObservacoesAoCredenciado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoPagamento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "TipoPagamento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "TipoPagamento"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantidadeSaques");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "QuantidadeSaques"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantidadeTransferencias");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "QuantidadeTransferencias"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valorSaques");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "ValorSaques"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valorTransferencias");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "ValorTransferencias"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("versaoServico");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "VersaoServico"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
