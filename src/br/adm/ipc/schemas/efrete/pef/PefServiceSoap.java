/**
 * PefServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef;

public interface PefServiceSoap extends java.rmi.Remote {
    public br.adm.ipc.schemas.efrete.pef.objects.AdicionarOperacaoTransporteResponse adicionarOperacaoTransporte(br.adm.ipc.schemas.efrete.pef.objects.AdicionarOperacaoTransporteRequest adicionarOperacaoTransporteRequest) throws java.rmi.RemoteException;
    public br.adm.ipc.schemas.efrete.pef.objects.CancelarOperacaoTransporteResponse cancelarOperacaoTransporte(br.adm.ipc.schemas.efrete.pef.objects.CancelarOperacaoTransporteRequest cancelarOperacaoTransporteRequest) throws java.rmi.RemoteException;
    public br.adm.ipc.schemas.efrete.pef.objects.RetificarOperacaoTransporteResponse retificarOperacaoTransporte(br.adm.ipc.schemas.efrete.pef.objects.RetificarOperacaoTransporteRequest retificarOperacaoTransporteRequest) throws java.rmi.RemoteException;
    public br.adm.ipc.schemas.efrete.pef.objects.EncerrarOperacaoTransporteResponse encerrarOperacaoTransporte(br.adm.ipc.schemas.efrete.pef.objects.EncerrarOperacaoTransporteRequest encerrarOperacaoTransporteRequest) throws java.rmi.RemoteException;
    public br.adm.ipc.schemas.efrete.pef.objects.AdicionarViagemResponse adicionarViagem(br.adm.ipc.schemas.efrete.pef.objects.AdicionarViagemRequest adicionarViagemRequest) throws java.rmi.RemoteException;
    public br.adm.ipc.schemas.efrete.pef.objects.AdicionarPagamentoResponse adicionarPagamento(br.adm.ipc.schemas.efrete.pef.objects.AdicionarPagamentoRequest adicionarPagamentoRequest) throws java.rmi.RemoteException;
    public br.adm.ipc.schemas.efrete.pef.objects.CancelarPagamentoResponse cancelarPagamento(br.adm.ipc.schemas.efrete.pef.objects.CancelarPagamentoRequest cancelarPagamentoRequest) throws java.rmi.RemoteException;
    public br.adm.ipc.schemas.efrete.pef.objects.AlterarDataLiberacaoPagamentoResponse alterarDataLiberacaoPagamento(br.adm.ipc.schemas.efrete.pef.objects.AlterarDataLiberacaoPagamentoRequest editarPagamentoRequest) throws java.rmi.RemoteException;
    public br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransporteParaImpressaoResponse obterOperacaoTransporteParaImpressao(br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransporteParaImpressaoRequest obterOperacaoTransporteParaImpressaoRequest) throws java.rmi.RemoteException;
    public br.adm.ipc.schemas.efrete.pef.objects.ConsultarDownloadDocumentoResponse consultarDownloadDocumento(br.adm.ipc.schemas.efrete.pef.objects.ConsultarDownloadDocumentoRequest consultarDownloadDocumentoRequest) throws java.rmi.RemoteException;
    public br.adm.ipc.schemas.efrete.pef.objects.DownloadDocumentoResponse downloadDocumento(br.adm.ipc.schemas.efrete.pef.objects.DownloadDocumentoRequest downloadDocumentoRequest) throws java.rmi.RemoteException;
    public br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransportePdfResponse obterOperacaoTransportePdf(br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransportePdfRequest obterOperacaoTransportePdfRequest) throws java.rmi.RemoteException;
    public br.adm.ipc.schemas.efrete.pef.ObterOperacoesTransporteAgrupadasPdfResponse obterOperacoesTransporteAgrupadasPdf(br.adm.ipc.schemas.efrete.pef.objects.ObterOperacoesTransporteAgrupadasPdfRequest obterOperacoesTransporteAgrupadasPdfRequest) throws java.rmi.RemoteException;
    public br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransporteResponse obterOperacaoTransporte(br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransporteRequest obterOperacaoTransporteRequest) throws java.rmi.RemoteException;
    public br.adm.ipc.schemas.efrete.pef.objects.AbonarQuebraDeFreteResponse abonarQuebraDeFrete(br.adm.ipc.schemas.efrete.pef.objects.AbonarQuebraDeFreteRequest abonarQuebraDeFreteRequest) throws java.rmi.RemoteException;
    public br.adm.ipc.schemas.efrete.pef.objects.ObterTransacoesResponse obterTransacoes(br.adm.ipc.schemas.efrete.pef.objects.ObterTransacoesRequest obterTransacoesRequest) throws java.rmi.RemoteException;
    public br.adm.ipc.schemas.efrete.pef.objects.ObterVinculoParaTransporteResponse obterVinculoParaTransporte(br.adm.ipc.schemas.efrete.pef.objects.ObterVinculoParaTransporteRequest obterVinculoParaTransporteRequest) throws java.rmi.RemoteException;
    public br.adm.ipc.schemas.efrete.pef.objects.ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteResponse obterCodigoIdentificacaoOperacaoTransportePorIdOperacaoCliente(br.adm.ipc.schemas.efrete.pef.objects.ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteRequest obterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteRequest) throws java.rmi.RemoteException;
    public br.adm.ipc.schemas.efrete.pef.objects.RegistrarQuantidadeDaMercadoriaNoDesembarqueResponse registrarQuantidadeDaMercadoriaNoDesembarque(br.adm.ipc.schemas.efrete.pef.objects.RegistrarQuantidadeDaMercadoriaNoDesembarqueRequest registrarQuantidadeDaMercadoriaNoDesembarqueRequest) throws java.rmi.RemoteException;
    public br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoResponse registrarPagamento(br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoRequest registrarPagamentoRequest) throws java.rmi.RemoteException;
    public br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoQuitacaoResponse registrarPagamentoQuitacao(br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoQuitacaoRequest registrarPagamentoQuitacaoRequest) throws java.rmi.RemoteException;
    public br.adm.ipc.schemas.efrete.pef.objects.AlterarEntregaDocumentacaoResponse alterarEntregaDocumentacao(br.adm.ipc.schemas.efrete.pef.objects.AlterarEntregaDocumentacaoRequest alterarEntregaDocumentacaoRequest) throws java.rmi.RemoteException;
    public br.adm.ipc.schemas.efrete.pef.objects.AdicionarValeFreteResponse adicionarValeFrete(br.adm.ipc.schemas.efrete.pef.objects.AdicionarValeFreteRequest adicionarValeFreteRequest) throws java.rmi.RemoteException;
    public br.adm.ipc.schemas.efrete.pef.objects.ConsultarTipoCargaResponse consultarTipoCarga(br.adm.ipc.schemas.efrete.pef.objects.ConsultarTipoCargaRequest consultarTipoCargaRequest) throws java.rmi.RemoteException;
}
