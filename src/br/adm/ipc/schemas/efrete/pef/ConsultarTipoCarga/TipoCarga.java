/**
 * TipoCarga.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.ConsultarTipoCarga;

public class TipoCarga  implements java.io.Serializable {
    private int codigoTipoCarga;

    private java.lang.String descricaoTipoCarga;

    public TipoCarga() {
    }

    public TipoCarga(
           int codigoTipoCarga,
           java.lang.String descricaoTipoCarga) {
           this.codigoTipoCarga = codigoTipoCarga;
           this.descricaoTipoCarga = descricaoTipoCarga;
    }


    /**
     * Gets the codigoTipoCarga value for this TipoCarga.
     * 
     * @return codigoTipoCarga
     */
    public int getCodigoTipoCarga() {
        return codigoTipoCarga;
    }


    /**
     * Sets the codigoTipoCarga value for this TipoCarga.
     * 
     * @param codigoTipoCarga
     */
    public void setCodigoTipoCarga(int codigoTipoCarga) {
        this.codigoTipoCarga = codigoTipoCarga;
    }


    /**
     * Gets the descricaoTipoCarga value for this TipoCarga.
     * 
     * @return descricaoTipoCarga
     */
    public java.lang.String getDescricaoTipoCarga() {
        return descricaoTipoCarga;
    }


    /**
     * Sets the descricaoTipoCarga value for this TipoCarga.
     * 
     * @param descricaoTipoCarga
     */
    public void setDescricaoTipoCarga(java.lang.String descricaoTipoCarga) {
        this.descricaoTipoCarga = descricaoTipoCarga;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TipoCarga)) return false;
        TipoCarga other = (TipoCarga) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.codigoTipoCarga == other.getCodigoTipoCarga() &&
            ((this.descricaoTipoCarga==null && other.getDescricaoTipoCarga()==null) || 
             (this.descricaoTipoCarga!=null &&
              this.descricaoTipoCarga.equals(other.getDescricaoTipoCarga())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getCodigoTipoCarga();
        if (getDescricaoTipoCarga() != null) {
            _hashCode += getDescricaoTipoCarga().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TipoCarga.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ConsultarTipoCarga", "TipoCarga"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoTipoCarga");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CodigoTipoCarga"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descricaoTipoCarga");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DescricaoTipoCarga"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
