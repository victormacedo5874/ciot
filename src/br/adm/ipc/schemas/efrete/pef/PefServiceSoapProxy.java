package br.adm.ipc.schemas.efrete.pef;

public class PefServiceSoapProxy implements br.adm.ipc.schemas.efrete.pef.PefServiceSoap {
  private String _endpoint = null;
  private br.adm.ipc.schemas.efrete.pef.PefServiceSoap pefServiceSoap = null;
  
  public PefServiceSoapProxy() {
    _initPefServiceSoapProxy();
  }
  
  public PefServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initPefServiceSoapProxy();
  }
  
  private void _initPefServiceSoapProxy() {
    try {
      pefServiceSoap = (new br.adm.ipc.schemas.efrete.pef.PefServiceLocator()).getPefServiceSoap();
      if (pefServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)pefServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)pefServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (pefServiceSoap != null)
      ((javax.xml.rpc.Stub)pefServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public br.adm.ipc.schemas.efrete.pef.PefServiceSoap getPefServiceSoap() {
    if (pefServiceSoap == null)
      _initPefServiceSoapProxy();
    return pefServiceSoap;
  }
  
  public br.adm.ipc.schemas.efrete.pef.objects.AdicionarOperacaoTransporteResponse adicionarOperacaoTransporte(br.adm.ipc.schemas.efrete.pef.objects.AdicionarOperacaoTransporteRequest adicionarOperacaoTransporteRequest) throws java.rmi.RemoteException{
    if (pefServiceSoap == null)
      _initPefServiceSoapProxy();
    return pefServiceSoap.adicionarOperacaoTransporte(adicionarOperacaoTransporteRequest);
  }
  
  public br.adm.ipc.schemas.efrete.pef.objects.CancelarOperacaoTransporteResponse cancelarOperacaoTransporte(br.adm.ipc.schemas.efrete.pef.objects.CancelarOperacaoTransporteRequest cancelarOperacaoTransporteRequest) throws java.rmi.RemoteException{
    if (pefServiceSoap == null)
      _initPefServiceSoapProxy();
    return pefServiceSoap.cancelarOperacaoTransporte(cancelarOperacaoTransporteRequest);
  }
  
  public br.adm.ipc.schemas.efrete.pef.objects.RetificarOperacaoTransporteResponse retificarOperacaoTransporte(br.adm.ipc.schemas.efrete.pef.objects.RetificarOperacaoTransporteRequest retificarOperacaoTransporteRequest) throws java.rmi.RemoteException{
    if (pefServiceSoap == null)
      _initPefServiceSoapProxy();
    return pefServiceSoap.retificarOperacaoTransporte(retificarOperacaoTransporteRequest);
  }
  
  public br.adm.ipc.schemas.efrete.pef.objects.EncerrarOperacaoTransporteResponse encerrarOperacaoTransporte(br.adm.ipc.schemas.efrete.pef.objects.EncerrarOperacaoTransporteRequest encerrarOperacaoTransporteRequest) throws java.rmi.RemoteException{
    if (pefServiceSoap == null)
      _initPefServiceSoapProxy();
    return pefServiceSoap.encerrarOperacaoTransporte(encerrarOperacaoTransporteRequest);
  }
  
  public br.adm.ipc.schemas.efrete.pef.objects.AdicionarViagemResponse adicionarViagem(br.adm.ipc.schemas.efrete.pef.objects.AdicionarViagemRequest adicionarViagemRequest) throws java.rmi.RemoteException{
    if (pefServiceSoap == null)
      _initPefServiceSoapProxy();
    return pefServiceSoap.adicionarViagem(adicionarViagemRequest);
  }
  
  public br.adm.ipc.schemas.efrete.pef.objects.AdicionarPagamentoResponse adicionarPagamento(br.adm.ipc.schemas.efrete.pef.objects.AdicionarPagamentoRequest adicionarPagamentoRequest) throws java.rmi.RemoteException{
    if (pefServiceSoap == null)
      _initPefServiceSoapProxy();
    return pefServiceSoap.adicionarPagamento(adicionarPagamentoRequest);
  }
  
  public br.adm.ipc.schemas.efrete.pef.objects.CancelarPagamentoResponse cancelarPagamento(br.adm.ipc.schemas.efrete.pef.objects.CancelarPagamentoRequest cancelarPagamentoRequest) throws java.rmi.RemoteException{
    if (pefServiceSoap == null)
      _initPefServiceSoapProxy();
    return pefServiceSoap.cancelarPagamento(cancelarPagamentoRequest);
  }
  
  public br.adm.ipc.schemas.efrete.pef.objects.AlterarDataLiberacaoPagamentoResponse alterarDataLiberacaoPagamento(br.adm.ipc.schemas.efrete.pef.objects.AlterarDataLiberacaoPagamentoRequest editarPagamentoRequest) throws java.rmi.RemoteException{
    if (pefServiceSoap == null)
      _initPefServiceSoapProxy();
    return pefServiceSoap.alterarDataLiberacaoPagamento(editarPagamentoRequest);
  }
  
  public br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransporteParaImpressaoResponse obterOperacaoTransporteParaImpressao(br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransporteParaImpressaoRequest obterOperacaoTransporteParaImpressaoRequest) throws java.rmi.RemoteException{
    if (pefServiceSoap == null)
      _initPefServiceSoapProxy();
    return pefServiceSoap.obterOperacaoTransporteParaImpressao(obterOperacaoTransporteParaImpressaoRequest);
  }
  
  public br.adm.ipc.schemas.efrete.pef.objects.ConsultarDownloadDocumentoResponse consultarDownloadDocumento(br.adm.ipc.schemas.efrete.pef.objects.ConsultarDownloadDocumentoRequest consultarDownloadDocumentoRequest) throws java.rmi.RemoteException{
    if (pefServiceSoap == null)
      _initPefServiceSoapProxy();
    return pefServiceSoap.consultarDownloadDocumento(consultarDownloadDocumentoRequest);
  }
  
  public br.adm.ipc.schemas.efrete.pef.objects.DownloadDocumentoResponse downloadDocumento(br.adm.ipc.schemas.efrete.pef.objects.DownloadDocumentoRequest downloadDocumentoRequest) throws java.rmi.RemoteException{
    if (pefServiceSoap == null)
      _initPefServiceSoapProxy();
    return pefServiceSoap.downloadDocumento(downloadDocumentoRequest);
  }
  
  public br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransportePdfResponse obterOperacaoTransportePdf(br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransportePdfRequest obterOperacaoTransportePdfRequest) throws java.rmi.RemoteException{
    if (pefServiceSoap == null)
      _initPefServiceSoapProxy();
    return pefServiceSoap.obterOperacaoTransportePdf(obterOperacaoTransportePdfRequest);
  }
  
  public br.adm.ipc.schemas.efrete.pef.ObterOperacoesTransporteAgrupadasPdfResponse obterOperacoesTransporteAgrupadasPdf(br.adm.ipc.schemas.efrete.pef.objects.ObterOperacoesTransporteAgrupadasPdfRequest obterOperacoesTransporteAgrupadasPdfRequest) throws java.rmi.RemoteException{
    if (pefServiceSoap == null)
      _initPefServiceSoapProxy();
    return pefServiceSoap.obterOperacoesTransporteAgrupadasPdf(obterOperacoesTransporteAgrupadasPdfRequest);
  }
  
  public br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransporteResponse obterOperacaoTransporte(br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransporteRequest obterOperacaoTransporteRequest) throws java.rmi.RemoteException{
    if (pefServiceSoap == null)
      _initPefServiceSoapProxy();
    return pefServiceSoap.obterOperacaoTransporte(obterOperacaoTransporteRequest);
  }
  
  public br.adm.ipc.schemas.efrete.pef.objects.AbonarQuebraDeFreteResponse abonarQuebraDeFrete(br.adm.ipc.schemas.efrete.pef.objects.AbonarQuebraDeFreteRequest abonarQuebraDeFreteRequest) throws java.rmi.RemoteException{
    if (pefServiceSoap == null)
      _initPefServiceSoapProxy();
    return pefServiceSoap.abonarQuebraDeFrete(abonarQuebraDeFreteRequest);
  }
  
  public br.adm.ipc.schemas.efrete.pef.objects.ObterTransacoesResponse obterTransacoes(br.adm.ipc.schemas.efrete.pef.objects.ObterTransacoesRequest obterTransacoesRequest) throws java.rmi.RemoteException{
    if (pefServiceSoap == null)
      _initPefServiceSoapProxy();
    return pefServiceSoap.obterTransacoes(obterTransacoesRequest);
  }
  
  public br.adm.ipc.schemas.efrete.pef.objects.ObterVinculoParaTransporteResponse obterVinculoParaTransporte(br.adm.ipc.schemas.efrete.pef.objects.ObterVinculoParaTransporteRequest obterVinculoParaTransporteRequest) throws java.rmi.RemoteException{
    if (pefServiceSoap == null)
      _initPefServiceSoapProxy();
    return pefServiceSoap.obterVinculoParaTransporte(obterVinculoParaTransporteRequest);
  }
  
  public br.adm.ipc.schemas.efrete.pef.objects.ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteResponse obterCodigoIdentificacaoOperacaoTransportePorIdOperacaoCliente(br.adm.ipc.schemas.efrete.pef.objects.ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteRequest obterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteRequest) throws java.rmi.RemoteException{
    if (pefServiceSoap == null)
      _initPefServiceSoapProxy();
    return pefServiceSoap.obterCodigoIdentificacaoOperacaoTransportePorIdOperacaoCliente(obterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteRequest);
  }
  
  public br.adm.ipc.schemas.efrete.pef.objects.RegistrarQuantidadeDaMercadoriaNoDesembarqueResponse registrarQuantidadeDaMercadoriaNoDesembarque(br.adm.ipc.schemas.efrete.pef.objects.RegistrarQuantidadeDaMercadoriaNoDesembarqueRequest registrarQuantidadeDaMercadoriaNoDesembarqueRequest) throws java.rmi.RemoteException{
    if (pefServiceSoap == null)
      _initPefServiceSoapProxy();
    return pefServiceSoap.registrarQuantidadeDaMercadoriaNoDesembarque(registrarQuantidadeDaMercadoriaNoDesembarqueRequest);
  }
  
  public br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoResponse registrarPagamento(br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoRequest registrarPagamentoRequest) throws java.rmi.RemoteException{
    if (pefServiceSoap == null)
      _initPefServiceSoapProxy();
    return pefServiceSoap.registrarPagamento(registrarPagamentoRequest);
  }
  
  public br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoQuitacaoResponse registrarPagamentoQuitacao(br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoQuitacaoRequest registrarPagamentoQuitacaoRequest) throws java.rmi.RemoteException{
    if (pefServiceSoap == null)
      _initPefServiceSoapProxy();
    return pefServiceSoap.registrarPagamentoQuitacao(registrarPagamentoQuitacaoRequest);
  }
  
  public br.adm.ipc.schemas.efrete.pef.objects.AlterarEntregaDocumentacaoResponse alterarEntregaDocumentacao(br.adm.ipc.schemas.efrete.pef.objects.AlterarEntregaDocumentacaoRequest alterarEntregaDocumentacaoRequest) throws java.rmi.RemoteException{
    if (pefServiceSoap == null)
      _initPefServiceSoapProxy();
    return pefServiceSoap.alterarEntregaDocumentacao(alterarEntregaDocumentacaoRequest);
  }
  
  public br.adm.ipc.schemas.efrete.pef.objects.AdicionarValeFreteResponse adicionarValeFrete(br.adm.ipc.schemas.efrete.pef.objects.AdicionarValeFreteRequest adicionarValeFreteRequest) throws java.rmi.RemoteException{
    if (pefServiceSoap == null)
      _initPefServiceSoapProxy();
    return pefServiceSoap.adicionarValeFrete(adicionarValeFreteRequest);
  }
  
  public br.adm.ipc.schemas.efrete.pef.objects.ConsultarTipoCargaResponse consultarTipoCarga(br.adm.ipc.schemas.efrete.pef.objects.ConsultarTipoCargaRequest consultarTipoCargaRequest) throws java.rmi.RemoteException{
    if (pefServiceSoap == null)
      _initPefServiceSoapProxy();
    return pefServiceSoap.consultarTipoCarga(consultarTipoCargaRequest);
  }
  
  
}