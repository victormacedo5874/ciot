/**
 * TipoDeCobranca.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.ObterTransacoesObjects;

public class TipoDeCobranca implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected TipoDeCobranca(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _SemCategoria = "SemCategoria";
    public static final java.lang.String _Adiantamento = "Adiantamento";
    public static final java.lang.String _Quitacao = "Quitacao";
    public static final java.lang.String _Estadia = "Estadia";
    public static final java.lang.String _Frota = "Frota";
    public static final TipoDeCobranca SemCategoria = new TipoDeCobranca(_SemCategoria);
    public static final TipoDeCobranca Adiantamento = new TipoDeCobranca(_Adiantamento);
    public static final TipoDeCobranca Quitacao = new TipoDeCobranca(_Quitacao);
    public static final TipoDeCobranca Estadia = new TipoDeCobranca(_Estadia);
    public static final TipoDeCobranca Frota = new TipoDeCobranca(_Frota);
    public java.lang.String getValue() { return _value_;}
    public static TipoDeCobranca fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        TipoDeCobranca enumeration = (TipoDeCobranca)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static TipoDeCobranca fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TipoDeCobranca.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "TipoDeCobranca"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
