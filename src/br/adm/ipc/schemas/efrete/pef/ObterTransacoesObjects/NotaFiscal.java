/**
 * NotaFiscal.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.ObterTransacoesObjects;

public class NotaFiscal  implements java.io.Serializable {
    private java.lang.String numero;

    private java.lang.String serie;

    private java.util.Calendar data;

    private java.math.BigDecimal valorDaMercadoriaPorQuantidadeUnitaria;

    private int codigoDaMercadoria;

    private java.lang.String descricaoMercadoria;

    private java.math.BigDecimal quantidadeDeSaida;

    private java.math.BigDecimal quantidadeDeChegada;

    private java.lang.String unidadeDeMedidaDaQuantidade;

    private java.math.BigDecimal valorDaQuebraDeFrete;

    private java.math.BigDecimal valorDaDiferencaDeFrete;

    private int porcentagemDeAbonoDeQuebra;

    private long CPFUsuarioQueAbonouQuebra;

    public NotaFiscal() {
    }

    public NotaFiscal(
           java.lang.String numero,
           java.lang.String serie,
           java.util.Calendar data,
           java.math.BigDecimal valorDaMercadoriaPorQuantidadeUnitaria,
           int codigoDaMercadoria,
           java.lang.String descricaoMercadoria,
           java.math.BigDecimal quantidadeDeSaida,
           java.math.BigDecimal quantidadeDeChegada,
           java.lang.String unidadeDeMedidaDaQuantidade,
           java.math.BigDecimal valorDaQuebraDeFrete,
           java.math.BigDecimal valorDaDiferencaDeFrete,
           int porcentagemDeAbonoDeQuebra,
           long CPFUsuarioQueAbonouQuebra) {
           this.numero = numero;
           this.serie = serie;
           this.data = data;
           this.valorDaMercadoriaPorQuantidadeUnitaria = valorDaMercadoriaPorQuantidadeUnitaria;
           this.codigoDaMercadoria = codigoDaMercadoria;
           this.descricaoMercadoria = descricaoMercadoria;
           this.quantidadeDeSaida = quantidadeDeSaida;
           this.quantidadeDeChegada = quantidadeDeChegada;
           this.unidadeDeMedidaDaQuantidade = unidadeDeMedidaDaQuantidade;
           this.valorDaQuebraDeFrete = valorDaQuebraDeFrete;
           this.valorDaDiferencaDeFrete = valorDaDiferencaDeFrete;
           this.porcentagemDeAbonoDeQuebra = porcentagemDeAbonoDeQuebra;
           this.CPFUsuarioQueAbonouQuebra = CPFUsuarioQueAbonouQuebra;
    }


    /**
     * Gets the numero value for this NotaFiscal.
     * 
     * @return numero
     */
    public java.lang.String getNumero() {
        return numero;
    }


    /**
     * Sets the numero value for this NotaFiscal.
     * 
     * @param numero
     */
    public void setNumero(java.lang.String numero) {
        this.numero = numero;
    }


    /**
     * Gets the serie value for this NotaFiscal.
     * 
     * @return serie
     */
    public java.lang.String getSerie() {
        return serie;
    }


    /**
     * Sets the serie value for this NotaFiscal.
     * 
     * @param serie
     */
    public void setSerie(java.lang.String serie) {
        this.serie = serie;
    }


    /**
     * Gets the data value for this NotaFiscal.
     * 
     * @return data
     */
    public java.util.Calendar getData() {
        return data;
    }


    /**
     * Sets the data value for this NotaFiscal.
     * 
     * @param data
     */
    public void setData(java.util.Calendar data) {
        this.data = data;
    }


    /**
     * Gets the valorDaMercadoriaPorQuantidadeUnitaria value for this NotaFiscal.
     * 
     * @return valorDaMercadoriaPorQuantidadeUnitaria
     */
    public java.math.BigDecimal getValorDaMercadoriaPorQuantidadeUnitaria() {
        return valorDaMercadoriaPorQuantidadeUnitaria;
    }


    /**
     * Sets the valorDaMercadoriaPorQuantidadeUnitaria value for this NotaFiscal.
     * 
     * @param valorDaMercadoriaPorQuantidadeUnitaria
     */
    public void setValorDaMercadoriaPorQuantidadeUnitaria(java.math.BigDecimal valorDaMercadoriaPorQuantidadeUnitaria) {
        this.valorDaMercadoriaPorQuantidadeUnitaria = valorDaMercadoriaPorQuantidadeUnitaria;
    }


    /**
     * Gets the codigoDaMercadoria value for this NotaFiscal.
     * 
     * @return codigoDaMercadoria
     */
    public int getCodigoDaMercadoria() {
        return codigoDaMercadoria;
    }


    /**
     * Sets the codigoDaMercadoria value for this NotaFiscal.
     * 
     * @param codigoDaMercadoria
     */
    public void setCodigoDaMercadoria(int codigoDaMercadoria) {
        this.codigoDaMercadoria = codigoDaMercadoria;
    }


    /**
     * Gets the descricaoMercadoria value for this NotaFiscal.
     * 
     * @return descricaoMercadoria
     */
    public java.lang.String getDescricaoMercadoria() {
        return descricaoMercadoria;
    }


    /**
     * Sets the descricaoMercadoria value for this NotaFiscal.
     * 
     * @param descricaoMercadoria
     */
    public void setDescricaoMercadoria(java.lang.String descricaoMercadoria) {
        this.descricaoMercadoria = descricaoMercadoria;
    }


    /**
     * Gets the quantidadeDeSaida value for this NotaFiscal.
     * 
     * @return quantidadeDeSaida
     */
    public java.math.BigDecimal getQuantidadeDeSaida() {
        return quantidadeDeSaida;
    }


    /**
     * Sets the quantidadeDeSaida value for this NotaFiscal.
     * 
     * @param quantidadeDeSaida
     */
    public void setQuantidadeDeSaida(java.math.BigDecimal quantidadeDeSaida) {
        this.quantidadeDeSaida = quantidadeDeSaida;
    }


    /**
     * Gets the quantidadeDeChegada value for this NotaFiscal.
     * 
     * @return quantidadeDeChegada
     */
    public java.math.BigDecimal getQuantidadeDeChegada() {
        return quantidadeDeChegada;
    }


    /**
     * Sets the quantidadeDeChegada value for this NotaFiscal.
     * 
     * @param quantidadeDeChegada
     */
    public void setQuantidadeDeChegada(java.math.BigDecimal quantidadeDeChegada) {
        this.quantidadeDeChegada = quantidadeDeChegada;
    }


    /**
     * Gets the unidadeDeMedidaDaQuantidade value for this NotaFiscal.
     * 
     * @return unidadeDeMedidaDaQuantidade
     */
    public java.lang.String getUnidadeDeMedidaDaQuantidade() {
        return unidadeDeMedidaDaQuantidade;
    }


    /**
     * Sets the unidadeDeMedidaDaQuantidade value for this NotaFiscal.
     * 
     * @param unidadeDeMedidaDaQuantidade
     */
    public void setUnidadeDeMedidaDaQuantidade(java.lang.String unidadeDeMedidaDaQuantidade) {
        this.unidadeDeMedidaDaQuantidade = unidadeDeMedidaDaQuantidade;
    }


    /**
     * Gets the valorDaQuebraDeFrete value for this NotaFiscal.
     * 
     * @return valorDaQuebraDeFrete
     */
    public java.math.BigDecimal getValorDaQuebraDeFrete() {
        return valorDaQuebraDeFrete;
    }


    /**
     * Sets the valorDaQuebraDeFrete value for this NotaFiscal.
     * 
     * @param valorDaQuebraDeFrete
     */
    public void setValorDaQuebraDeFrete(java.math.BigDecimal valorDaQuebraDeFrete) {
        this.valorDaQuebraDeFrete = valorDaQuebraDeFrete;
    }


    /**
     * Gets the valorDaDiferencaDeFrete value for this NotaFiscal.
     * 
     * @return valorDaDiferencaDeFrete
     */
    public java.math.BigDecimal getValorDaDiferencaDeFrete() {
        return valorDaDiferencaDeFrete;
    }


    /**
     * Sets the valorDaDiferencaDeFrete value for this NotaFiscal.
     * 
     * @param valorDaDiferencaDeFrete
     */
    public void setValorDaDiferencaDeFrete(java.math.BigDecimal valorDaDiferencaDeFrete) {
        this.valorDaDiferencaDeFrete = valorDaDiferencaDeFrete;
    }


    /**
     * Gets the porcentagemDeAbonoDeQuebra value for this NotaFiscal.
     * 
     * @return porcentagemDeAbonoDeQuebra
     */
    public int getPorcentagemDeAbonoDeQuebra() {
        return porcentagemDeAbonoDeQuebra;
    }


    /**
     * Sets the porcentagemDeAbonoDeQuebra value for this NotaFiscal.
     * 
     * @param porcentagemDeAbonoDeQuebra
     */
    public void setPorcentagemDeAbonoDeQuebra(int porcentagemDeAbonoDeQuebra) {
        this.porcentagemDeAbonoDeQuebra = porcentagemDeAbonoDeQuebra;
    }


    /**
     * Gets the CPFUsuarioQueAbonouQuebra value for this NotaFiscal.
     * 
     * @return CPFUsuarioQueAbonouQuebra
     */
    public long getCPFUsuarioQueAbonouQuebra() {
        return CPFUsuarioQueAbonouQuebra;
    }


    /**
     * Sets the CPFUsuarioQueAbonouQuebra value for this NotaFiscal.
     * 
     * @param CPFUsuarioQueAbonouQuebra
     */
    public void setCPFUsuarioQueAbonouQuebra(long CPFUsuarioQueAbonouQuebra) {
        this.CPFUsuarioQueAbonouQuebra = CPFUsuarioQueAbonouQuebra;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof NotaFiscal)) return false;
        NotaFiscal other = (NotaFiscal) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.numero==null && other.getNumero()==null) || 
             (this.numero!=null &&
              this.numero.equals(other.getNumero()))) &&
            ((this.serie==null && other.getSerie()==null) || 
             (this.serie!=null &&
              this.serie.equals(other.getSerie()))) &&
            ((this.data==null && other.getData()==null) || 
             (this.data!=null &&
              this.data.equals(other.getData()))) &&
            ((this.valorDaMercadoriaPorQuantidadeUnitaria==null && other.getValorDaMercadoriaPorQuantidadeUnitaria()==null) || 
             (this.valorDaMercadoriaPorQuantidadeUnitaria!=null &&
              this.valorDaMercadoriaPorQuantidadeUnitaria.equals(other.getValorDaMercadoriaPorQuantidadeUnitaria()))) &&
            this.codigoDaMercadoria == other.getCodigoDaMercadoria() &&
            ((this.descricaoMercadoria==null && other.getDescricaoMercadoria()==null) || 
             (this.descricaoMercadoria!=null &&
              this.descricaoMercadoria.equals(other.getDescricaoMercadoria()))) &&
            ((this.quantidadeDeSaida==null && other.getQuantidadeDeSaida()==null) || 
             (this.quantidadeDeSaida!=null &&
              this.quantidadeDeSaida.equals(other.getQuantidadeDeSaida()))) &&
            ((this.quantidadeDeChegada==null && other.getQuantidadeDeChegada()==null) || 
             (this.quantidadeDeChegada!=null &&
              this.quantidadeDeChegada.equals(other.getQuantidadeDeChegada()))) &&
            ((this.unidadeDeMedidaDaQuantidade==null && other.getUnidadeDeMedidaDaQuantidade()==null) || 
             (this.unidadeDeMedidaDaQuantidade!=null &&
              this.unidadeDeMedidaDaQuantidade.equals(other.getUnidadeDeMedidaDaQuantidade()))) &&
            ((this.valorDaQuebraDeFrete==null && other.getValorDaQuebraDeFrete()==null) || 
             (this.valorDaQuebraDeFrete!=null &&
              this.valorDaQuebraDeFrete.equals(other.getValorDaQuebraDeFrete()))) &&
            ((this.valorDaDiferencaDeFrete==null && other.getValorDaDiferencaDeFrete()==null) || 
             (this.valorDaDiferencaDeFrete!=null &&
              this.valorDaDiferencaDeFrete.equals(other.getValorDaDiferencaDeFrete()))) &&
            this.porcentagemDeAbonoDeQuebra == other.getPorcentagemDeAbonoDeQuebra() &&
            this.CPFUsuarioQueAbonouQuebra == other.getCPFUsuarioQueAbonouQuebra();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNumero() != null) {
            _hashCode += getNumero().hashCode();
        }
        if (getSerie() != null) {
            _hashCode += getSerie().hashCode();
        }
        if (getData() != null) {
            _hashCode += getData().hashCode();
        }
        if (getValorDaMercadoriaPorQuantidadeUnitaria() != null) {
            _hashCode += getValorDaMercadoriaPorQuantidadeUnitaria().hashCode();
        }
        _hashCode += getCodigoDaMercadoria();
        if (getDescricaoMercadoria() != null) {
            _hashCode += getDescricaoMercadoria().hashCode();
        }
        if (getQuantidadeDeSaida() != null) {
            _hashCode += getQuantidadeDeSaida().hashCode();
        }
        if (getQuantidadeDeChegada() != null) {
            _hashCode += getQuantidadeDeChegada().hashCode();
        }
        if (getUnidadeDeMedidaDaQuantidade() != null) {
            _hashCode += getUnidadeDeMedidaDaQuantidade().hashCode();
        }
        if (getValorDaQuebraDeFrete() != null) {
            _hashCode += getValorDaQuebraDeFrete().hashCode();
        }
        if (getValorDaDiferencaDeFrete() != null) {
            _hashCode += getValorDaDiferencaDeFrete().hashCode();
        }
        _hashCode += getPorcentagemDeAbonoDeQuebra();
        _hashCode += new Long(getCPFUsuarioQueAbonouQuebra()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(NotaFiscal.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "NotaFiscal"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numero");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "Numero"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serie");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "Serie"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("data");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "Data"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valorDaMercadoriaPorQuantidadeUnitaria");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "ValorDaMercadoriaPorQuantidadeUnitaria"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoDaMercadoria");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "CodigoDaMercadoria"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descricaoMercadoria");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "DescricaoMercadoria"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantidadeDeSaida");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "QuantidadeDeSaida"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantidadeDeChegada");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "QuantidadeDeChegada"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unidadeDeMedidaDaQuantidade");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "UnidadeDeMedidaDaQuantidade"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valorDaQuebraDeFrete");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "ValorDaQuebraDeFrete"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valorDaDiferencaDeFrete");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "ValorDaDiferencaDeFrete"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("porcentagemDeAbonoDeQuebra");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "PorcentagemDeAbonoDeQuebra"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CPFUsuarioQueAbonouQuebra");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "CPFUsuarioQueAbonouQuebra"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
