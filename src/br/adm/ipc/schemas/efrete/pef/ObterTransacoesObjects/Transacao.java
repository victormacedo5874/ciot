/**
 * Transacao.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.ObterTransacoesObjects;

public class Transacao  implements java.io.Serializable {
    private java.lang.String id;

    private long transportadoraCNPJ;

    private long credenciadoCNPJ;

    private java.util.Calendar dataDeOperacao;

    private java.lang.String CIOT;

    private java.lang.String documentoViagem;

    private int transacaoDoCIOT;

    private br.adm.ipc.schemas.efrete.pef.ObterTransacoesObjects.TipoDeCobranca tipoDeCobranca;

    private br.adm.ipc.schemas.efrete.pef.ObterTransacoesObjects.TransacaoStatus status;

    private br.adm.ipc.schemas.efrete.pef.ObterTransacoesObjects.NotaFiscal[] notasFiscais;

    private java.math.BigDecimal valorPago;

    public Transacao() {
    }

    public Transacao(
           java.lang.String id,
           long transportadoraCNPJ,
           long credenciadoCNPJ,
           java.util.Calendar dataDeOperacao,
           java.lang.String CIOT,
           java.lang.String documentoViagem,
           int transacaoDoCIOT,
           br.adm.ipc.schemas.efrete.pef.ObterTransacoesObjects.TipoDeCobranca tipoDeCobranca,
           br.adm.ipc.schemas.efrete.pef.ObterTransacoesObjects.TransacaoStatus status,
           br.adm.ipc.schemas.efrete.pef.ObterTransacoesObjects.NotaFiscal[] notasFiscais,
           java.math.BigDecimal valorPago) {
           this.id = id;
           this.transportadoraCNPJ = transportadoraCNPJ;
           this.credenciadoCNPJ = credenciadoCNPJ;
           this.dataDeOperacao = dataDeOperacao;
           this.CIOT = CIOT;
           this.documentoViagem = documentoViagem;
           this.transacaoDoCIOT = transacaoDoCIOT;
           this.tipoDeCobranca = tipoDeCobranca;
           this.status = status;
           this.notasFiscais = notasFiscais;
           this.valorPago = valorPago;
    }


    /**
     * Gets the id value for this Transacao.
     * 
     * @return id
     */
    public java.lang.String getId() {
        return id;
    }


    /**
     * Sets the id value for this Transacao.
     * 
     * @param id
     */
    public void setId(java.lang.String id) {
        this.id = id;
    }


    /**
     * Gets the transportadoraCNPJ value for this Transacao.
     * 
     * @return transportadoraCNPJ
     */
    public long getTransportadoraCNPJ() {
        return transportadoraCNPJ;
    }


    /**
     * Sets the transportadoraCNPJ value for this Transacao.
     * 
     * @param transportadoraCNPJ
     */
    public void setTransportadoraCNPJ(long transportadoraCNPJ) {
        this.transportadoraCNPJ = transportadoraCNPJ;
    }


    /**
     * Gets the credenciadoCNPJ value for this Transacao.
     * 
     * @return credenciadoCNPJ
     */
    public long getCredenciadoCNPJ() {
        return credenciadoCNPJ;
    }


    /**
     * Sets the credenciadoCNPJ value for this Transacao.
     * 
     * @param credenciadoCNPJ
     */
    public void setCredenciadoCNPJ(long credenciadoCNPJ) {
        this.credenciadoCNPJ = credenciadoCNPJ;
    }


    /**
     * Gets the dataDeOperacao value for this Transacao.
     * 
     * @return dataDeOperacao
     */
    public java.util.Calendar getDataDeOperacao() {
        return dataDeOperacao;
    }


    /**
     * Sets the dataDeOperacao value for this Transacao.
     * 
     * @param dataDeOperacao
     */
    public void setDataDeOperacao(java.util.Calendar dataDeOperacao) {
        this.dataDeOperacao = dataDeOperacao;
    }


    /**
     * Gets the CIOT value for this Transacao.
     * 
     * @return CIOT
     */
    public java.lang.String getCIOT() {
        return CIOT;
    }


    /**
     * Sets the CIOT value for this Transacao.
     * 
     * @param CIOT
     */
    public void setCIOT(java.lang.String CIOT) {
        this.CIOT = CIOT;
    }


    /**
     * Gets the documentoViagem value for this Transacao.
     * 
     * @return documentoViagem
     */
    public java.lang.String getDocumentoViagem() {
        return documentoViagem;
    }


    /**
     * Sets the documentoViagem value for this Transacao.
     * 
     * @param documentoViagem
     */
    public void setDocumentoViagem(java.lang.String documentoViagem) {
        this.documentoViagem = documentoViagem;
    }


    /**
     * Gets the transacaoDoCIOT value for this Transacao.
     * 
     * @return transacaoDoCIOT
     */
    public int getTransacaoDoCIOT() {
        return transacaoDoCIOT;
    }


    /**
     * Sets the transacaoDoCIOT value for this Transacao.
     * 
     * @param transacaoDoCIOT
     */
    public void setTransacaoDoCIOT(int transacaoDoCIOT) {
        this.transacaoDoCIOT = transacaoDoCIOT;
    }


    /**
     * Gets the tipoDeCobranca value for this Transacao.
     * 
     * @return tipoDeCobranca
     */
    public br.adm.ipc.schemas.efrete.pef.ObterTransacoesObjects.TipoDeCobranca getTipoDeCobranca() {
        return tipoDeCobranca;
    }


    /**
     * Sets the tipoDeCobranca value for this Transacao.
     * 
     * @param tipoDeCobranca
     */
    public void setTipoDeCobranca(br.adm.ipc.schemas.efrete.pef.ObterTransacoesObjects.TipoDeCobranca tipoDeCobranca) {
        this.tipoDeCobranca = tipoDeCobranca;
    }


    /**
     * Gets the status value for this Transacao.
     * 
     * @return status
     */
    public br.adm.ipc.schemas.efrete.pef.ObterTransacoesObjects.TransacaoStatus getStatus() {
        return status;
    }


    /**
     * Sets the status value for this Transacao.
     * 
     * @param status
     */
    public void setStatus(br.adm.ipc.schemas.efrete.pef.ObterTransacoesObjects.TransacaoStatus status) {
        this.status = status;
    }


    /**
     * Gets the notasFiscais value for this Transacao.
     * 
     * @return notasFiscais
     */
    public br.adm.ipc.schemas.efrete.pef.ObterTransacoesObjects.NotaFiscal[] getNotasFiscais() {
        return notasFiscais;
    }


    /**
     * Sets the notasFiscais value for this Transacao.
     * 
     * @param notasFiscais
     */
    public void setNotasFiscais(br.adm.ipc.schemas.efrete.pef.ObterTransacoesObjects.NotaFiscal[] notasFiscais) {
        this.notasFiscais = notasFiscais;
    }


    /**
     * Gets the valorPago value for this Transacao.
     * 
     * @return valorPago
     */
    public java.math.BigDecimal getValorPago() {
        return valorPago;
    }


    /**
     * Sets the valorPago value for this Transacao.
     * 
     * @param valorPago
     */
    public void setValorPago(java.math.BigDecimal valorPago) {
        this.valorPago = valorPago;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Transacao)) return false;
        Transacao other = (Transacao) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            this.transportadoraCNPJ == other.getTransportadoraCNPJ() &&
            this.credenciadoCNPJ == other.getCredenciadoCNPJ() &&
            ((this.dataDeOperacao==null && other.getDataDeOperacao()==null) || 
             (this.dataDeOperacao!=null &&
              this.dataDeOperacao.equals(other.getDataDeOperacao()))) &&
            ((this.CIOT==null && other.getCIOT()==null) || 
             (this.CIOT!=null &&
              this.CIOT.equals(other.getCIOT()))) &&
            ((this.documentoViagem==null && other.getDocumentoViagem()==null) || 
             (this.documentoViagem!=null &&
              this.documentoViagem.equals(other.getDocumentoViagem()))) &&
            this.transacaoDoCIOT == other.getTransacaoDoCIOT() &&
            ((this.tipoDeCobranca==null && other.getTipoDeCobranca()==null) || 
             (this.tipoDeCobranca!=null &&
              this.tipoDeCobranca.equals(other.getTipoDeCobranca()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.notasFiscais==null && other.getNotasFiscais()==null) || 
             (this.notasFiscais!=null &&
              java.util.Arrays.equals(this.notasFiscais, other.getNotasFiscais()))) &&
            ((this.valorPago==null && other.getValorPago()==null) || 
             (this.valorPago!=null &&
              this.valorPago.equals(other.getValorPago())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        _hashCode += new Long(getTransportadoraCNPJ()).hashCode();
        _hashCode += new Long(getCredenciadoCNPJ()).hashCode();
        if (getDataDeOperacao() != null) {
            _hashCode += getDataDeOperacao().hashCode();
        }
        if (getCIOT() != null) {
            _hashCode += getCIOT().hashCode();
        }
        if (getDocumentoViagem() != null) {
            _hashCode += getDocumentoViagem().hashCode();
        }
        _hashCode += getTransacaoDoCIOT();
        if (getTipoDeCobranca() != null) {
            _hashCode += getTipoDeCobranca().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getNotasFiscais() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getNotasFiscais());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getNotasFiscais(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getValorPago() != null) {
            _hashCode += getValorPago().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Transacao.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "Transacao"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transportadoraCNPJ");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "TransportadoraCNPJ"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("credenciadoCNPJ");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "CredenciadoCNPJ"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataDeOperacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "DataDeOperacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CIOT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "CIOT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("documentoViagem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "DocumentoViagem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transacaoDoCIOT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "TransacaoDoCIOT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoDeCobranca");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "TipoDeCobranca"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "TipoDeCobranca"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "TransacaoStatus"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("notasFiscais");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "NotasFiscais"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "NotaFiscal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "NotaFiscal"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valorPago");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "ValorPago"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
