/**
 * NotaFiscal.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.AdicionarViagem;

public class NotaFiscal  implements java.io.Serializable {
    private java.lang.String numero;

    private java.lang.String serie;

    private java.util.Calendar data;

    private java.math.BigDecimal valorTotal;

    private java.math.BigDecimal valorDaMercadoriaPorUnidade;

    private int codigoNCMNaturezaCarga;

    private java.lang.String descricaoDaMercadoria;

    private br.adm.ipc.schemas.efrete.pef.objects.UnidadeDeMedidaDaMercadoria unidadeDeMedidaDaMercadoria;

    private br.adm.ipc.schemas.efrete.pef.objects.ViagemTipoDeCalculo tipoDeCalculo;

    private java.math.BigDecimal valorDoFretePorUnidadeDeMercadoria;

    private java.math.BigDecimal quantidadeDaMercadoriaNoEmbarque;

    private br.adm.ipc.schemas.efrete.pef.objects.ToleranciaDePerdaDeMercadoria toleranciaDePerdaDeMercadoria;

    public NotaFiscal() {
    }

    public NotaFiscal(
           java.lang.String numero,
           java.lang.String serie,
           java.util.Calendar data,
           java.math.BigDecimal valorTotal,
           java.math.BigDecimal valorDaMercadoriaPorUnidade,
           int codigoNCMNaturezaCarga,
           java.lang.String descricaoDaMercadoria,
           br.adm.ipc.schemas.efrete.pef.objects.UnidadeDeMedidaDaMercadoria unidadeDeMedidaDaMercadoria,
           br.adm.ipc.schemas.efrete.pef.objects.ViagemTipoDeCalculo tipoDeCalculo,
           java.math.BigDecimal valorDoFretePorUnidadeDeMercadoria,
           java.math.BigDecimal quantidadeDaMercadoriaNoEmbarque,
           br.adm.ipc.schemas.efrete.pef.objects.ToleranciaDePerdaDeMercadoria toleranciaDePerdaDeMercadoria) {
           this.numero = numero;
           this.serie = serie;
           this.data = data;
           this.valorTotal = valorTotal;
           this.valorDaMercadoriaPorUnidade = valorDaMercadoriaPorUnidade;
           this.codigoNCMNaturezaCarga = codigoNCMNaturezaCarga;
           this.descricaoDaMercadoria = descricaoDaMercadoria;
           this.unidadeDeMedidaDaMercadoria = unidadeDeMedidaDaMercadoria;
           this.tipoDeCalculo = tipoDeCalculo;
           this.valorDoFretePorUnidadeDeMercadoria = valorDoFretePorUnidadeDeMercadoria;
           this.quantidadeDaMercadoriaNoEmbarque = quantidadeDaMercadoriaNoEmbarque;
           this.toleranciaDePerdaDeMercadoria = toleranciaDePerdaDeMercadoria;
    }


    /**
     * Gets the numero value for this NotaFiscal.
     * 
     * @return numero
     */
    public java.lang.String getNumero() {
        return numero;
    }


    /**
     * Sets the numero value for this NotaFiscal.
     * 
     * @param numero
     */
    public void setNumero(java.lang.String numero) {
        this.numero = numero;
    }


    /**
     * Gets the serie value for this NotaFiscal.
     * 
     * @return serie
     */
    public java.lang.String getSerie() {
        return serie;
    }


    /**
     * Sets the serie value for this NotaFiscal.
     * 
     * @param serie
     */
    public void setSerie(java.lang.String serie) {
        this.serie = serie;
    }


    /**
     * Gets the data value for this NotaFiscal.
     * 
     * @return data
     */
    public java.util.Calendar getData() {
        return data;
    }


    /**
     * Sets the data value for this NotaFiscal.
     * 
     * @param data
     */
    public void setData(java.util.Calendar data) {
        this.data = data;
    }


    /**
     * Gets the valorTotal value for this NotaFiscal.
     * 
     * @return valorTotal
     */
    public java.math.BigDecimal getValorTotal() {
        return valorTotal;
    }


    /**
     * Sets the valorTotal value for this NotaFiscal.
     * 
     * @param valorTotal
     */
    public void setValorTotal(java.math.BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }


    /**
     * Gets the valorDaMercadoriaPorUnidade value for this NotaFiscal.
     * 
     * @return valorDaMercadoriaPorUnidade
     */
    public java.math.BigDecimal getValorDaMercadoriaPorUnidade() {
        return valorDaMercadoriaPorUnidade;
    }


    /**
     * Sets the valorDaMercadoriaPorUnidade value for this NotaFiscal.
     * 
     * @param valorDaMercadoriaPorUnidade
     */
    public void setValorDaMercadoriaPorUnidade(java.math.BigDecimal valorDaMercadoriaPorUnidade) {
        this.valorDaMercadoriaPorUnidade = valorDaMercadoriaPorUnidade;
    }


    /**
     * Gets the codigoNCMNaturezaCarga value for this NotaFiscal.
     * 
     * @return codigoNCMNaturezaCarga
     */
    public int getCodigoNCMNaturezaCarga() {
        return codigoNCMNaturezaCarga;
    }


    /**
     * Sets the codigoNCMNaturezaCarga value for this NotaFiscal.
     * 
     * @param codigoNCMNaturezaCarga
     */
    public void setCodigoNCMNaturezaCarga(int codigoNCMNaturezaCarga) {
        this.codigoNCMNaturezaCarga = codigoNCMNaturezaCarga;
    }


    /**
     * Gets the descricaoDaMercadoria value for this NotaFiscal.
     * 
     * @return descricaoDaMercadoria
     */
    public java.lang.String getDescricaoDaMercadoria() {
        return descricaoDaMercadoria;
    }


    /**
     * Sets the descricaoDaMercadoria value for this NotaFiscal.
     * 
     * @param descricaoDaMercadoria
     */
    public void setDescricaoDaMercadoria(java.lang.String descricaoDaMercadoria) {
        this.descricaoDaMercadoria = descricaoDaMercadoria;
    }


    /**
     * Gets the unidadeDeMedidaDaMercadoria value for this NotaFiscal.
     * 
     * @return unidadeDeMedidaDaMercadoria
     */
    public br.adm.ipc.schemas.efrete.pef.objects.UnidadeDeMedidaDaMercadoria getUnidadeDeMedidaDaMercadoria() {
        return unidadeDeMedidaDaMercadoria;
    }


    /**
     * Sets the unidadeDeMedidaDaMercadoria value for this NotaFiscal.
     * 
     * @param unidadeDeMedidaDaMercadoria
     */
    public void setUnidadeDeMedidaDaMercadoria(br.adm.ipc.schemas.efrete.pef.objects.UnidadeDeMedidaDaMercadoria unidadeDeMedidaDaMercadoria) {
        this.unidadeDeMedidaDaMercadoria = unidadeDeMedidaDaMercadoria;
    }


    /**
     * Gets the tipoDeCalculo value for this NotaFiscal.
     * 
     * @return tipoDeCalculo
     */
    public br.adm.ipc.schemas.efrete.pef.objects.ViagemTipoDeCalculo getTipoDeCalculo() {
        return tipoDeCalculo;
    }


    /**
     * Sets the tipoDeCalculo value for this NotaFiscal.
     * 
     * @param tipoDeCalculo
     */
    public void setTipoDeCalculo(br.adm.ipc.schemas.efrete.pef.objects.ViagemTipoDeCalculo tipoDeCalculo) {
        this.tipoDeCalculo = tipoDeCalculo;
    }


    /**
     * Gets the valorDoFretePorUnidadeDeMercadoria value for this NotaFiscal.
     * 
     * @return valorDoFretePorUnidadeDeMercadoria
     */
    public java.math.BigDecimal getValorDoFretePorUnidadeDeMercadoria() {
        return valorDoFretePorUnidadeDeMercadoria;
    }


    /**
     * Sets the valorDoFretePorUnidadeDeMercadoria value for this NotaFiscal.
     * 
     * @param valorDoFretePorUnidadeDeMercadoria
     */
    public void setValorDoFretePorUnidadeDeMercadoria(java.math.BigDecimal valorDoFretePorUnidadeDeMercadoria) {
        this.valorDoFretePorUnidadeDeMercadoria = valorDoFretePorUnidadeDeMercadoria;
    }


    /**
     * Gets the quantidadeDaMercadoriaNoEmbarque value for this NotaFiscal.
     * 
     * @return quantidadeDaMercadoriaNoEmbarque
     */
    public java.math.BigDecimal getQuantidadeDaMercadoriaNoEmbarque() {
        return quantidadeDaMercadoriaNoEmbarque;
    }


    /**
     * Sets the quantidadeDaMercadoriaNoEmbarque value for this NotaFiscal.
     * 
     * @param quantidadeDaMercadoriaNoEmbarque
     */
    public void setQuantidadeDaMercadoriaNoEmbarque(java.math.BigDecimal quantidadeDaMercadoriaNoEmbarque) {
        this.quantidadeDaMercadoriaNoEmbarque = quantidadeDaMercadoriaNoEmbarque;
    }


    /**
     * Gets the toleranciaDePerdaDeMercadoria value for this NotaFiscal.
     * 
     * @return toleranciaDePerdaDeMercadoria
     */
    public br.adm.ipc.schemas.efrete.pef.objects.ToleranciaDePerdaDeMercadoria getToleranciaDePerdaDeMercadoria() {
        return toleranciaDePerdaDeMercadoria;
    }


    /**
     * Sets the toleranciaDePerdaDeMercadoria value for this NotaFiscal.
     * 
     * @param toleranciaDePerdaDeMercadoria
     */
    public void setToleranciaDePerdaDeMercadoria(br.adm.ipc.schemas.efrete.pef.objects.ToleranciaDePerdaDeMercadoria toleranciaDePerdaDeMercadoria) {
        this.toleranciaDePerdaDeMercadoria = toleranciaDePerdaDeMercadoria;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof NotaFiscal)) return false;
        NotaFiscal other = (NotaFiscal) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.numero==null && other.getNumero()==null) || 
             (this.numero!=null &&
              this.numero.equals(other.getNumero()))) &&
            ((this.serie==null && other.getSerie()==null) || 
             (this.serie!=null &&
              this.serie.equals(other.getSerie()))) &&
            ((this.data==null && other.getData()==null) || 
             (this.data!=null &&
              this.data.equals(other.getData()))) &&
            ((this.valorTotal==null && other.getValorTotal()==null) || 
             (this.valorTotal!=null &&
              this.valorTotal.equals(other.getValorTotal()))) &&
            ((this.valorDaMercadoriaPorUnidade==null && other.getValorDaMercadoriaPorUnidade()==null) || 
             (this.valorDaMercadoriaPorUnidade!=null &&
              this.valorDaMercadoriaPorUnidade.equals(other.getValorDaMercadoriaPorUnidade()))) &&
            this.codigoNCMNaturezaCarga == other.getCodigoNCMNaturezaCarga() &&
            ((this.descricaoDaMercadoria==null && other.getDescricaoDaMercadoria()==null) || 
             (this.descricaoDaMercadoria!=null &&
              this.descricaoDaMercadoria.equals(other.getDescricaoDaMercadoria()))) &&
            ((this.unidadeDeMedidaDaMercadoria==null && other.getUnidadeDeMedidaDaMercadoria()==null) || 
             (this.unidadeDeMedidaDaMercadoria!=null &&
              this.unidadeDeMedidaDaMercadoria.equals(other.getUnidadeDeMedidaDaMercadoria()))) &&
            ((this.tipoDeCalculo==null && other.getTipoDeCalculo()==null) || 
             (this.tipoDeCalculo!=null &&
              this.tipoDeCalculo.equals(other.getTipoDeCalculo()))) &&
            ((this.valorDoFretePorUnidadeDeMercadoria==null && other.getValorDoFretePorUnidadeDeMercadoria()==null) || 
             (this.valorDoFretePorUnidadeDeMercadoria!=null &&
              this.valorDoFretePorUnidadeDeMercadoria.equals(other.getValorDoFretePorUnidadeDeMercadoria()))) &&
            ((this.quantidadeDaMercadoriaNoEmbarque==null && other.getQuantidadeDaMercadoriaNoEmbarque()==null) || 
             (this.quantidadeDaMercadoriaNoEmbarque!=null &&
              this.quantidadeDaMercadoriaNoEmbarque.equals(other.getQuantidadeDaMercadoriaNoEmbarque()))) &&
            ((this.toleranciaDePerdaDeMercadoria==null && other.getToleranciaDePerdaDeMercadoria()==null) || 
             (this.toleranciaDePerdaDeMercadoria!=null &&
              this.toleranciaDePerdaDeMercadoria.equals(other.getToleranciaDePerdaDeMercadoria())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNumero() != null) {
            _hashCode += getNumero().hashCode();
        }
        if (getSerie() != null) {
            _hashCode += getSerie().hashCode();
        }
        if (getData() != null) {
            _hashCode += getData().hashCode();
        }
        if (getValorTotal() != null) {
            _hashCode += getValorTotal().hashCode();
        }
        if (getValorDaMercadoriaPorUnidade() != null) {
            _hashCode += getValorDaMercadoriaPorUnidade().hashCode();
        }
        _hashCode += getCodigoNCMNaturezaCarga();
        if (getDescricaoDaMercadoria() != null) {
            _hashCode += getDescricaoDaMercadoria().hashCode();
        }
        if (getUnidadeDeMedidaDaMercadoria() != null) {
            _hashCode += getUnidadeDeMedidaDaMercadoria().hashCode();
        }
        if (getTipoDeCalculo() != null) {
            _hashCode += getTipoDeCalculo().hashCode();
        }
        if (getValorDoFretePorUnidadeDeMercadoria() != null) {
            _hashCode += getValorDoFretePorUnidadeDeMercadoria().hashCode();
        }
        if (getQuantidadeDaMercadoriaNoEmbarque() != null) {
            _hashCode += getQuantidadeDaMercadoriaNoEmbarque().hashCode();
        }
        if (getToleranciaDePerdaDeMercadoria() != null) {
            _hashCode += getToleranciaDePerdaDeMercadoria().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(NotaFiscal.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarViagem", "NotaFiscal"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numero");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarViagem", "Numero"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serie");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarViagem", "Serie"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("data");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarViagem", "Data"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valorTotal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarViagem", "ValorTotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valorDaMercadoriaPorUnidade");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarViagem", "ValorDaMercadoriaPorUnidade"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoNCMNaturezaCarga");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarViagem", "CodigoNCMNaturezaCarga"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descricaoDaMercadoria");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarViagem", "DescricaoDaMercadoria"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unidadeDeMedidaDaMercadoria");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "UnidadeDeMedidaDaMercadoria"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "UnidadeDeMedidaDaMercadoria"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoDeCalculo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TipoDeCalculo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ViagemTipoDeCalculo"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valorDoFretePorUnidadeDeMercadoria");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarViagem", "ValorDoFretePorUnidadeDeMercadoria"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantidadeDaMercadoriaNoEmbarque");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarViagem", "QuantidadeDaMercadoriaNoEmbarque"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("toleranciaDePerdaDeMercadoria");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ToleranciaDePerdaDeMercadoria"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ToleranciaDePerdaDeMercadoria"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
