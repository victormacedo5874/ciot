/**
 * PefServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef;

public class PefServiceLocator extends org.apache.axis.client.Service implements br.adm.ipc.schemas.efrete.pef.PefService {

    public PefServiceLocator() {
    }


    public PefServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public PefServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for PefServiceSoap
    private java.lang.String PefServiceSoap_address = "https://sistema.efrete.com/Services/PefService.asmx";

    public java.lang.String getPefServiceSoapAddress() {
        return PefServiceSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String PefServiceSoapWSDDServiceName = "PefServiceSoap";

    public java.lang.String getPefServiceSoapWSDDServiceName() {
        return PefServiceSoapWSDDServiceName;
    }

    public void setPefServiceSoapWSDDServiceName(java.lang.String name) {
        PefServiceSoapWSDDServiceName = name;
    }

    public br.adm.ipc.schemas.efrete.pef.PefServiceSoap getPefServiceSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(PefServiceSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getPefServiceSoap(endpoint);
    }

    public br.adm.ipc.schemas.efrete.pef.PefServiceSoap getPefServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            br.adm.ipc.schemas.efrete.pef.PefServiceSoapStub _stub = new br.adm.ipc.schemas.efrete.pef.PefServiceSoapStub(portAddress, this);
            _stub.setPortName(getPefServiceSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setPefServiceSoapEndpointAddress(java.lang.String address) {
        PefServiceSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (br.adm.ipc.schemas.efrete.pef.PefServiceSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                br.adm.ipc.schemas.efrete.pef.PefServiceSoapStub _stub = new br.adm.ipc.schemas.efrete.pef.PefServiceSoapStub(new java.net.URL(PefServiceSoap_address), this);
                _stub.setPortName(getPefServiceSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("PefServiceSoap".equals(inputPortName)) {
            return getPefServiceSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef", "PefService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef", "PefServiceSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("PefServiceSoap".equals(portName)) {
            setPefServiceSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
