/**
 * Viagem.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.EncerrarOperacaoTransporte;

public class Viagem  implements java.io.Serializable {
    private java.lang.String documentoViagem;

    private String codigoMunicipioOrigem;

    private String codigoMunicipioDestino;

    private java.lang.String cepOrigem;

    private java.lang.String cepDestino;

    private br.adm.ipc.schemas.efrete.pef.objects.ViagemValores valores;

    private br.adm.ipc.schemas.efrete.pef.objects.TipoPagamento tipoPagamento;

    private br.adm.ipc.schemas.efrete.pef.objects.InformacoesBancarias informacoesBancarias;

    private br.adm.ipc.schemas.efrete.pef.EncerrarOperacaoTransporte.NotaFiscal[] notasFiscais;

    public Viagem() {
    }

    public Viagem(
           java.lang.String documentoViagem,
           String codigoMunicipioOrigem,
           String codigoMunicipioDestino,
           java.lang.String cepOrigem,
           java.lang.String cepDestino,
           br.adm.ipc.schemas.efrete.pef.objects.ViagemValores valores,
           br.adm.ipc.schemas.efrete.pef.objects.TipoPagamento tipoPagamento,
           br.adm.ipc.schemas.efrete.pef.objects.InformacoesBancarias informacoesBancarias,
           br.adm.ipc.schemas.efrete.pef.EncerrarOperacaoTransporte.NotaFiscal[] notasFiscais) {
           this.documentoViagem = documentoViagem;
           this.codigoMunicipioOrigem = codigoMunicipioOrigem;
           this.codigoMunicipioDestino = codigoMunicipioDestino;
           this.cepOrigem = cepOrigem;
           this.cepDestino = cepDestino;
           this.valores = valores;
           this.tipoPagamento = tipoPagamento;
           this.informacoesBancarias = informacoesBancarias;
           this.notasFiscais = notasFiscais;
    }


    /**
     * Gets the documentoViagem value for this Viagem.
     * 
     * @return documentoViagem
     */
    public java.lang.String getDocumentoViagem() {
        return documentoViagem;
    }


    /**
     * Sets the documentoViagem value for this Viagem.
     * 
     * @param documentoViagem
     */
    public void setDocumentoViagem(java.lang.String documentoViagem) {
        this.documentoViagem = documentoViagem;
    }


    /**
     * Gets the codigoMunicipioOrigem value for this Viagem.
     * 
     * @return codigoMunicipioOrigem
     */
    public String getCodigoMunicipioOrigem() {
        return codigoMunicipioOrigem;
    }


    /**
     * Sets the codigoMunicipioOrigem value for this Viagem.
     * 
     * @param codigoMunicipioOrigem
     */
    public void setCodigoMunicipioOrigem(String codigoMunicipioOrigem) {
        this.codigoMunicipioOrigem = codigoMunicipioOrigem;
    }


    /**
     * Gets the codigoMunicipioDestino value for this Viagem.
     * 
     * @return codigoMunicipioDestino
     */
    public String getCodigoMunicipioDestino() {
        return codigoMunicipioDestino;
    }


    /**
     * Sets the codigoMunicipioDestino value for this Viagem.
     * 
     * @param codigoMunicipioDestino
     */
    public void setCodigoMunicipioDestino(String codigoMunicipioDestino) {
        this.codigoMunicipioDestino = codigoMunicipioDestino;
    }


    /**
     * Gets the cepOrigem value for this Viagem.
     * 
     * @return cepOrigem
     */
    public java.lang.String getCepOrigem() {
        return cepOrigem;
    }


    /**
     * Sets the cepOrigem value for this Viagem.
     * 
     * @param cepOrigem
     */
    public void setCepOrigem(java.lang.String cepOrigem) {
        this.cepOrigem = cepOrigem;
    }


    /**
     * Gets the cepDestino value for this Viagem.
     * 
     * @return cepDestino
     */
    public java.lang.String getCepDestino() {
        return cepDestino;
    }


    /**
     * Sets the cepDestino value for this Viagem.
     * 
     * @param cepDestino
     */
    public void setCepDestino(java.lang.String cepDestino) {
        this.cepDestino = cepDestino;
    }


    /**
     * Gets the valores value for this Viagem.
     * 
     * @return valores
     */
    public br.adm.ipc.schemas.efrete.pef.objects.ViagemValores getValores() {
        return valores;
    }


    /**
     * Sets the valores value for this Viagem.
     * 
     * @param valores
     */
    public void setValores(br.adm.ipc.schemas.efrete.pef.objects.ViagemValores valores) {
        this.valores = valores;
    }


    /**
     * Gets the tipoPagamento value for this Viagem.
     * 
     * @return tipoPagamento
     */
    public br.adm.ipc.schemas.efrete.pef.objects.TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }


    /**
     * Sets the tipoPagamento value for this Viagem.
     * 
     * @param tipoPagamento
     */
    public void setTipoPagamento(br.adm.ipc.schemas.efrete.pef.objects.TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }


    /**
     * Gets the informacoesBancarias value for this Viagem.
     * 
     * @return informacoesBancarias
     */
    public br.adm.ipc.schemas.efrete.pef.objects.InformacoesBancarias getInformacoesBancarias() {
        return informacoesBancarias;
    }


    /**
     * Sets the informacoesBancarias value for this Viagem.
     * 
     * @param informacoesBancarias
     */
    public void setInformacoesBancarias(br.adm.ipc.schemas.efrete.pef.objects.InformacoesBancarias informacoesBancarias) {
        this.informacoesBancarias = informacoesBancarias;
    }


    /**
     * Gets the notasFiscais value for this Viagem.
     * 
     * @return notasFiscais
     */
    public br.adm.ipc.schemas.efrete.pef.EncerrarOperacaoTransporte.NotaFiscal[] getNotasFiscais() {
        return notasFiscais;
    }


    /**
     * Sets the notasFiscais value for this Viagem.
     * 
     * @param notasFiscais
     */
    public void setNotasFiscais(br.adm.ipc.schemas.efrete.pef.EncerrarOperacaoTransporte.NotaFiscal[] notasFiscais) {
        this.notasFiscais = notasFiscais;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Viagem)) return false;
        Viagem other = (Viagem) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.documentoViagem==null && other.getDocumentoViagem()==null) || 
             (this.documentoViagem!=null &&
              this.documentoViagem.equals(other.getDocumentoViagem()))) &&
            this.codigoMunicipioOrigem == other.getCodigoMunicipioOrigem() &&
            this.codigoMunicipioDestino == other.getCodigoMunicipioDestino() &&
            ((this.cepOrigem==null && other.getCepOrigem()==null) || 
             (this.cepOrigem!=null &&
              this.cepOrigem.equals(other.getCepOrigem()))) &&
            ((this.cepDestino==null && other.getCepDestino()==null) || 
             (this.cepDestino!=null &&
              this.cepDestino.equals(other.getCepDestino()))) &&
            ((this.valores==null && other.getValores()==null) || 
             (this.valores!=null &&
              this.valores.equals(other.getValores()))) &&
            ((this.tipoPagamento==null && other.getTipoPagamento()==null) || 
             (this.tipoPagamento!=null &&
              this.tipoPagamento.equals(other.getTipoPagamento()))) &&
            ((this.informacoesBancarias==null && other.getInformacoesBancarias()==null) || 
             (this.informacoesBancarias!=null &&
              this.informacoesBancarias.equals(other.getInformacoesBancarias()))) &&
            ((this.notasFiscais==null && other.getNotasFiscais()==null) || 
             (this.notasFiscais!=null &&
              java.util.Arrays.equals(this.notasFiscais, other.getNotasFiscais())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDocumentoViagem() != null) {
            _hashCode += getDocumentoViagem().hashCode();
        }
        if (getCepOrigem() != null) {
            _hashCode += getCepOrigem().hashCode();
        }
        if (getCepDestino() != null) {
            _hashCode += getCepDestino().hashCode();
        }
        if (getValores() != null) {
            _hashCode += getValores().hashCode();
        }
        if (getTipoPagamento() != null) {
            _hashCode += getTipoPagamento().hashCode();
        }
        if (getInformacoesBancarias() != null) {
            _hashCode += getInformacoesBancarias().hashCode();
        }
        if (getNotasFiscais() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getNotasFiscais());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getNotasFiscais(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Viagem.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/EncerrarOperacaoTransporte", "Viagem"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("documentoViagem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/EncerrarOperacaoTransporte", "DocumentoViagem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoMunicipioOrigem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/EncerrarOperacaoTransporte", "CodigoMunicipioOrigem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoMunicipioDestino");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/EncerrarOperacaoTransporte", "CodigoMunicipioDestino"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepOrigem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/EncerrarOperacaoTransporte", "CepOrigem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepDestino");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/EncerrarOperacaoTransporte", "CepDestino"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valores");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Valores"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ViagemValores"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoPagamento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TipoPagamento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TipoPagamento"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("informacoesBancarias");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "InformacoesBancarias"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "InformacoesBancarias"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("notasFiscais");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/EncerrarOperacaoTransporte", "NotasFiscais"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/EncerrarOperacaoTransporte", "NotaFiscal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/EncerrarOperacaoTransporte", "NotaFiscal"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
