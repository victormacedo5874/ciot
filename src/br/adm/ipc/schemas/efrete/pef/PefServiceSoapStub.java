/**
 * PefServiceSoapStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef;

public class PefServiceSoapStub extends org.apache.axis.client.Stub implements br.adm.ipc.schemas.efrete.pef.PefServiceSoap {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[24];
        _initOperationDesc1();
        _initOperationDesc2();
        _initOperationDesc3();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AdicionarOperacaoTransporte");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AdicionarOperacaoTransporteRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AdicionarOperacaoTransporteRequest"), br.adm.ipc.schemas.efrete.pef.objects.AdicionarOperacaoTransporteRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AdicionarOperacaoTransporteResponse"));
        oper.setReturnClass(br.adm.ipc.schemas.efrete.pef.objects.AdicionarOperacaoTransporteResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AdicionarOperacaoTransporteResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CancelarOperacaoTransporte");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CancelarOperacaoTransporteRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CancelarOperacaoTransporteRequest"), br.adm.ipc.schemas.efrete.pef.objects.CancelarOperacaoTransporteRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CancelarOperacaoTransporteResponse"));
        oper.setReturnClass(br.adm.ipc.schemas.efrete.pef.objects.CancelarOperacaoTransporteResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CancelarOperacaoTransporteResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RetificarOperacaoTransporte");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "RetificarOperacaoTransporteRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "RetificarOperacaoTransporteRequest"), br.adm.ipc.schemas.efrete.pef.objects.RetificarOperacaoTransporteRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "RetificarOperacaoTransporteResponse"));
        oper.setReturnClass(br.adm.ipc.schemas.efrete.pef.objects.RetificarOperacaoTransporteResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "RetificarOperacaoTransporteResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("EncerrarOperacaoTransporte");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "EncerrarOperacaoTransporteRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "EncerrarOperacaoTransporteRequest"), br.adm.ipc.schemas.efrete.pef.objects.EncerrarOperacaoTransporteRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "EncerrarOperacaoTransporteResponse"));
        oper.setReturnClass(br.adm.ipc.schemas.efrete.pef.objects.EncerrarOperacaoTransporteResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "EncerrarOperacaoTransporteResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AdicionarViagem");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AdicionarViagemRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AdicionarViagemRequest"), br.adm.ipc.schemas.efrete.pef.objects.AdicionarViagemRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AdicionarViagemResponse"));
        oper.setReturnClass(br.adm.ipc.schemas.efrete.pef.objects.AdicionarViagemResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AdicionarViagemResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AdicionarPagamento");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AdicionarPagamentoRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AdicionarPagamentoRequest"), br.adm.ipc.schemas.efrete.pef.objects.AdicionarPagamentoRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AdicionarPagamentoResponse"));
        oper.setReturnClass(br.adm.ipc.schemas.efrete.pef.objects.AdicionarPagamentoResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AdicionarPagamentoResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CancelarPagamento");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CancelarPagamentoRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CancelarPagamentoRequest"), br.adm.ipc.schemas.efrete.pef.objects.CancelarPagamentoRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CancelarPagamentoResponse"));
        oper.setReturnClass(br.adm.ipc.schemas.efrete.pef.objects.CancelarPagamentoResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CancelarPagamentoResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AlterarDataLiberacaoPagamento");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "EditarPagamentoRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AlterarDataLiberacaoPagamentoRequest"), br.adm.ipc.schemas.efrete.pef.objects.AlterarDataLiberacaoPagamentoRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AlterarDataLiberacaoPagamentoResponse"));
        oper.setReturnClass(br.adm.ipc.schemas.efrete.pef.objects.AlterarDataLiberacaoPagamentoResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AlterarDataLiberacaoPagamentoResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ObterOperacaoTransporteParaImpressao");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterOperacaoTransporteParaImpressaoRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterOperacaoTransporteParaImpressaoRequest"), br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransporteParaImpressaoRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterOperacaoTransporteParaImpressaoResponse"));
        oper.setReturnClass(br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransporteParaImpressaoResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterOperacaoTransporteParaImpressaoResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ConsultarDownloadDocumento");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ConsultarDownloadDocumentoRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ConsultarDownloadDocumentoRequest"), br.adm.ipc.schemas.efrete.pef.objects.ConsultarDownloadDocumentoRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ConsultarDownloadDocumentoResponse"));
        oper.setReturnClass(br.adm.ipc.schemas.efrete.pef.objects.ConsultarDownloadDocumentoResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ConsultarDownloadDocumentoResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DownloadDocumento");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DownloadDocumentoRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DownloadDocumentoRequest"), br.adm.ipc.schemas.efrete.pef.objects.DownloadDocumentoRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DownloadDocumentoResponse"));
        oper.setReturnClass(br.adm.ipc.schemas.efrete.pef.objects.DownloadDocumentoResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DownloadDocumentoResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ObterOperacaoTransportePdf");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterOperacaoTransportePdfRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterOperacaoTransportePdfRequest"), br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransportePdfRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterOperacaoTransportePdfResponse"));
        oper.setReturnClass(br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransportePdfResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterOperacaoTransportePdfResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ObterOperacoesTransporteAgrupadasPdf");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterOperacoesTransporteAgrupadasPdfRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterOperacoesTransporteAgrupadasPdfRequest"), br.adm.ipc.schemas.efrete.pef.objects.ObterOperacoesTransporteAgrupadasPdfRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef", "ObterOperacoesTransporteAgrupadasPdfResponse"));
        oper.setReturnClass(br.adm.ipc.schemas.efrete.pef.ObterOperacoesTransporteAgrupadasPdfResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef", "ObterOperacoesTransporteAgrupadasPdfResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ObterOperacaoTransporte");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterOperacaoTransporteRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterOperacaoTransporteRequest"), br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransporteRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterOperacaoTransporteResponse"));
        oper.setReturnClass(br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransporteResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterOperacaoTransporteResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AbonarQuebraDeFrete");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AbonarQuebraDeFreteRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AbonarQuebraDeFreteRequest"), br.adm.ipc.schemas.efrete.pef.objects.AbonarQuebraDeFreteRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AbonarQuebraDeFreteResponse"));
        oper.setReturnClass(br.adm.ipc.schemas.efrete.pef.objects.AbonarQuebraDeFreteResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AbonarQuebraDeFreteResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ObterTransacoes");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterTransacoesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterTransacoesRequest"), br.adm.ipc.schemas.efrete.pef.objects.ObterTransacoesRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterTransacoesResponse"));
        oper.setReturnClass(br.adm.ipc.schemas.efrete.pef.objects.ObterTransacoesResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterTransacoesResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ObterVinculoParaTransporte");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterVinculoParaTransporteRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterVinculoParaTransporteRequest"), br.adm.ipc.schemas.efrete.pef.objects.ObterVinculoParaTransporteRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterVinculoParaTransporteResponse"));
        oper.setReturnClass(br.adm.ipc.schemas.efrete.pef.objects.ObterVinculoParaTransporteResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterVinculoParaTransporteResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[16] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoCliente");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteRequest"), br.adm.ipc.schemas.efrete.pef.objects.ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteResponse"));
        oper.setReturnClass(br.adm.ipc.schemas.efrete.pef.objects.ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[17] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RegistrarQuantidadeDaMercadoriaNoDesembarque");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "RegistrarQuantidadeDaMercadoriaNoDesembarqueRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "RegistrarQuantidadeDaMercadoriaNoDesembarqueRequest"), br.adm.ipc.schemas.efrete.pef.objects.RegistrarQuantidadeDaMercadoriaNoDesembarqueRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "RegistrarQuantidadeDaMercadoriaNoDesembarqueResponse"));
        oper.setReturnClass(br.adm.ipc.schemas.efrete.pef.objects.RegistrarQuantidadeDaMercadoriaNoDesembarqueResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "RegistrarQuantidadeDaMercadoriaNoDesembarqueResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[18] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RegistrarPagamento");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "RegistrarPagamentoRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "RegistrarPagamentoRequest"), br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "RegistrarPagamentoResponse"));
        oper.setReturnClass(br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "RegistrarPagamentoResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[19] = oper;

    }

    private static void _initOperationDesc3(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RegistrarPagamentoQuitacao");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "RegistrarPagamentoQuitacaoRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "RegistrarPagamentoQuitacaoRequest"), br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoQuitacaoRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "RegistrarPagamentoQuitacaoResponse"));
        oper.setReturnClass(br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoQuitacaoResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "RegistrarPagamentoQuitacaoResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[20] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AlterarEntregaDocumentacao");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AlterarEntregaDocumentacaoRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AlterarEntregaDocumentacaoRequest"), br.adm.ipc.schemas.efrete.pef.objects.AlterarEntregaDocumentacaoRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AlterarEntregaDocumentacaoResponse"));
        oper.setReturnClass(br.adm.ipc.schemas.efrete.pef.objects.AlterarEntregaDocumentacaoResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AlterarEntregaDocumentacaoResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[21] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AdicionarValeFrete");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AdicionarValeFreteRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AdicionarValeFreteRequest"), br.adm.ipc.schemas.efrete.pef.objects.AdicionarValeFreteRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AdicionarValeFreteResponse"));
        oper.setReturnClass(br.adm.ipc.schemas.efrete.pef.objects.AdicionarValeFreteResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AdicionarValeFreteResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[22] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ConsultarTipoCarga");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ConsultarTipoCargaRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ConsultarTipoCargaRequest"), br.adm.ipc.schemas.efrete.pef.objects.ConsultarTipoCargaRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ConsultarTipoCargaResponse"));
        oper.setReturnClass(br.adm.ipc.schemas.efrete.pef.objects.ConsultarTipoCargaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ConsultarTipoCargaResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[23] = oper;

    }

    public PefServiceSoapStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public PefServiceSoapStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public PefServiceSoapStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
        addBindings0();
        addBindings1();
    }

    private void addBindings0() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://microsoft.com/wsdl/types/", "guid");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/objects", "Endereco");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.objects.Endereco.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/objects", "Excecao");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.objects.Excecao.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/objects", "Request");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.objects.Request.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/objects", "Response");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.objects.Response.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/objects", "Telefone");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.objects.Telefone.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/objects", "Telefones");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.objects.Telefones.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarOperacaoTransporte", "ArrayOfNotaFiscal");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.NotaFiscal[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarOperacaoTransporte", "NotaFiscal");
            qName2 = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarOperacaoTransporte", "NotaFiscal");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarOperacaoTransporte", "Contratado");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Contratado.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarOperacaoTransporte", "Motorista");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Motorista.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarOperacaoTransporte", "NotaFiscal");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.NotaFiscal.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarOperacaoTransporte", "Pagamento");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Pagamento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarOperacaoTransporte", "TipoToleranciaDePerdaDeMercadoria");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.TipoToleranciaDePerdaDeMercadoria.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarOperacaoTransporte", "ToleranciaDePerdaDeMercadoria");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.ToleranciaDePerdaDeMercadoria.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarOperacaoTransporte", "UnidadeDeMedidaDaMercadoria");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.UnidadeDeMedidaDaMercadoria.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarOperacaoTransporte", "Veiculo");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Veiculo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarOperacaoTransporte", "Viagem");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Viagem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarOperacaoTransporte", "ViagemTipoDeCalculo");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.ViagemTipoDeCalculo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarPagamento", "ArrayOfPagamento");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.AdicionarPagamento.Pagamento[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarPagamento", "Pagamento");
            qName2 = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarPagamento", "Pagamento");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarPagamento", "Pagamento");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.AdicionarPagamento.Pagamento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarViagem", "ArrayOfPagamento");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.AdicionarViagem.Pagamento[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarViagem", "Pagamento");
            qName2 = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarViagem", "Pagamento");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarViagem", "ArrayOfViagem");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.AdicionarViagem.Viagem[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarViagem", "Viagem");
            qName2 = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarViagem", "Viagem");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarViagem", "NotaFiscal");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.AdicionarViagem.NotaFiscal.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarViagem", "Pagamento");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.AdicionarViagem.Pagamento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarViagem", "Viagem");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.AdicionarViagem.Viagem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ConsultarTipoCarga", "TipoCarga");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ConsultarTipoCarga.TipoCarga.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/EncerrarOperacaoTransporte", "ArrayOfNotaFiscal");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.EncerrarOperacaoTransporte.NotaFiscal[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/EncerrarOperacaoTransporte", "NotaFiscal");
            qName2 = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/EncerrarOperacaoTransporte", "NotaFiscal");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/EncerrarOperacaoTransporte", "ArrayOfPagamento");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.EncerrarOperacaoTransporte.Pagamento[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/EncerrarOperacaoTransporte", "Pagamento");
            qName2 = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/EncerrarOperacaoTransporte", "Pagamento");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/EncerrarOperacaoTransporte", "ArrayOfViagem");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.EncerrarOperacaoTransporte.Viagem[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/EncerrarOperacaoTransporte", "Viagem");
            qName2 = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/EncerrarOperacaoTransporte", "Viagem");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/EncerrarOperacaoTransporte", "NotaFiscal");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.EncerrarOperacaoTransporte.NotaFiscal.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/EncerrarOperacaoTransporte", "Pagamento");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.EncerrarOperacaoTransporte.Pagamento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/EncerrarOperacaoTransporte", "TipoPagamento");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.EncerrarOperacaoTransporte.TipoPagamento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/EncerrarOperacaoTransporte", "Viagem");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.EncerrarOperacaoTransporte.Viagem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects/AbonarQuebraDeFrete", "AbonoDeQuebra");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.AbonarQuebraDeFrete.AbonoDeQuebra.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects/AbonarQuebraDeFrete", "ArrayOfNotaFiscal");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.AbonarQuebraDeFrete.NotaFiscal[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects/AbonarQuebraDeFrete", "NotaFiscal");
            qName2 = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects/AbonarQuebraDeFrete", "NotaFiscal");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects/AbonarQuebraDeFrete", "NotaFiscal");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.AbonarQuebraDeFrete.NotaFiscal.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects/RegistrarPagamentoQuitacao", "ArrayOfNotaFiscal");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoQuitacao.NotaFiscal[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects/RegistrarPagamentoQuitacao", "NotaFiscal");
            qName2 = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects/RegistrarPagamentoQuitacao", "NotaFiscal");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects/RegistrarPagamentoQuitacao", "NotaFiscal");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoQuitacao.NotaFiscal.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects/RegistrarQuantidadeDaMercadoriaNoDesembarque", "ArrayOfNotaFiscal");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.RegistrarQuantidadeDaMercadoriaNoDesembarque.NotaFiscal[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects/RegistrarQuantidadeDaMercadoriaNoDesembarque", "NotaFiscal");
            qName2 = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects/RegistrarQuantidadeDaMercadoriaNoDesembarque", "NotaFiscal");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects/RegistrarQuantidadeDaMercadoriaNoDesembarque", "NotaFiscal");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.RegistrarQuantidadeDaMercadoriaNoDesembarque.NotaFiscal.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AbonarQuebraDeFreteRequest");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.AbonarQuebraDeFreteRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AbonarQuebraDeFreteResponse");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.AbonarQuebraDeFreteResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AdicionarOperacaoTransporteRequest");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.AdicionarOperacaoTransporteRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AdicionarOperacaoTransporteResponse");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.AdicionarOperacaoTransporteResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AdicionarPagamentoRequest");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.AdicionarPagamentoRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AdicionarPagamentoResponse");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.AdicionarPagamentoResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AdicionarValeFreteRequest");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.AdicionarValeFreteRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AdicionarValeFreteResponse");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.AdicionarValeFreteResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AdicionarViagemRequest");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.AdicionarViagemRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AdicionarViagemResponse");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.AdicionarViagemResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AlterarDataLiberacaoPagamentoRequest");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.AlterarDataLiberacaoPagamentoRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AlterarDataLiberacaoPagamentoResponse");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.AlterarDataLiberacaoPagamentoResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AlterarEntregaDocumentacaoRequest");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.AlterarEntregaDocumentacaoRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AlterarEntregaDocumentacaoResponse");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.AlterarEntregaDocumentacaoResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ArrayOfDocumento");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.Documento[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Documento");
            qName2 = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Documento");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ArrayOfGuid");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://microsoft.com/wsdl/types/", "guid");
            qName2 = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "guid");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ArrayOfLong");
            cachedSerQNames.add(qName);
            cls = long[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long");
            qName2 = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "long");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ArrayOfNotaFiscal");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.AbonarQuebraDeFrete.NotaFiscal[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects/AbonarQuebraDeFrete", "NotaFiscal");
            qName2 = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "NotaFiscal");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ArrayOfString");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "string");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ArrayOfTipoCarga");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ConsultarTipoCarga.TipoCarga[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ConsultarTipoCarga", "TipoCarga");
            qName2 = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TipoCarga");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ArrayOfTransacao");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterTransacoesObjects.Transacao[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "Transacao");
            qName2 = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Transacao");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ArrayOfVeiculoRequest");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterVinculoParaTransporteObjects.VeiculoRequest[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterVinculoParaTransporteObjects", "VeiculoRequest");
            qName2 = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "VeiculoRequest");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ArrayOfVeiculoResponse");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterVinculoParaTransporteObjects.VeiculoResponse[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterVinculoParaTransporteObjects", "VeiculoResponse");
            qName2 = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "VeiculoResponse");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CancelarOperacaoTransporteRequest");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.CancelarOperacaoTransporteRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CancelarOperacaoTransporteResponse");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.CancelarOperacaoTransporteResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CancelarPagamentoRequest");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.CancelarPagamentoRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CancelarPagamentoResponse");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.CancelarPagamentoResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CategoriaPagamento");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.CategoriaPagamento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Consignatario");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.Consignatario.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ConsultarDownloadDocumentoRequest");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.ConsultarDownloadDocumentoRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ConsultarDownloadDocumentoResponse");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.ConsultarDownloadDocumentoResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ConsultarTipoCargaRequest");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.ConsultarTipoCargaRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ConsultarTipoCargaResponse");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.ConsultarTipoCargaResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Contratante");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.Contratante.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Destinatario");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.Destinatario.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DiferencaDeFrete");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.DiferencaDeFrete.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DiferencaFreteBaseCalculo");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.DiferencaFreteBaseCalculo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DiferencaFreteMargem");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.DiferencaFreteMargem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DiferencaFreteTipo");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.DiferencaFreteTipo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DiferencaFreteTolerancia");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.DiferencaFreteTolerancia.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Documento");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.Documento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DownloadDocumentoRequest");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.DownloadDocumentoRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DownloadDocumentoResponse");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.DownloadDocumentoResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "EncerrarOperacaoTransporteRequest");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.EncerrarOperacaoTransporteRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "EncerrarOperacaoTransporteResponse");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.EncerrarOperacaoTransporteResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "EstadoCiot");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.EstadoCiot.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Impostos");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.Impostos.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "InformacoesBancarias");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.InformacoesBancarias.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteRequest");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteResponse");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterOperacaoTransporteParaImpressaoRequest");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransporteParaImpressaoRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterOperacaoTransporteParaImpressaoResponse");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransporteParaImpressaoResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterOperacaoTransportePdfRequest");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransportePdfRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterOperacaoTransportePdfResponse");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransportePdfResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterOperacaoTransporteRequest");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransporteRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterOperacaoTransporteResponse");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransporteResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterOperacoesTransporteAgrupadasPdfRequest");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.ObterOperacoesTransporteAgrupadasPdfRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterTransacoesRequest");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.ObterTransacoesRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterTransacoesResponse");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.ObterTransacoesResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterVinculoParaTransporteRequest");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.ObterVinculoParaTransporteRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }
    private void addBindings1() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterVinculoParaTransporteResponse");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.ObterVinculoParaTransporteResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Pessoa");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.Pessoa.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ProprietarioCarga");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.ProprietarioCarga.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "RegistrarPagamentoQuitacaoRequest");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoQuitacaoRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "RegistrarPagamentoQuitacaoResponse");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoQuitacaoResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "RegistrarPagamentoRequest");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "RegistrarPagamentoResponse");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "RegistrarQuantidadeDaMercadoriaNoDesembarqueRequest");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.RegistrarQuantidadeDaMercadoriaNoDesembarqueRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "RegistrarQuantidadeDaMercadoriaNoDesembarqueResponse");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.RegistrarQuantidadeDaMercadoriaNoDesembarqueResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Remetente");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.Remetente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "RetificarOperacaoTransporteRequest");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.RetificarOperacaoTransporteRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "RetificarOperacaoTransporteResponse");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.RetificarOperacaoTransporteResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Subcontratante");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.Subcontratante.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TipoAbonoDeQuebra");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.TipoAbonoDeQuebra.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TipoConta");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.TipoConta.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TipoEmbalagem");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.TipoEmbalagem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TipoEntregaDocumentacao");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.TipoEntregaDocumentacao.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TipoPagamento");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.TipoPagamento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TipoProporcao");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.TipoProporcao.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TipoToleranciaDePerdaDeMercadoria");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.TipoToleranciaDePerdaDeMercadoria.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TipoViagem");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.TipoViagem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ToleranciaDePerdaDeMercadoria");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.ToleranciaDePerdaDeMercadoria.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TomadorServico");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.TomadorServico.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "UnidadeDeMedidaDaMercadoria");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.UnidadeDeMedidaDaMercadoria.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ViagemTipoDeCalculo");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.ViagemTipoDeCalculo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ViagemValores");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.objects.ViagemValores.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "AbonoDeQuebra");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.AbonoDeQuebra.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "ArrayOfNotaFiscal");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.NotaFiscal[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "NotaFiscal");
            qName2 = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "NotaFiscal");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "ArrayOfPagamento");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Pagamento[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Pagamento");
            qName2 = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Pagamento");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "ArrayOfString");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "string");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "ArrayOfVeiculo");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Veiculo[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Veiculo");
            qName2 = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Veiculo");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "ArrayOfViagem");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Viagem[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Viagem");
            qName2 = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Viagem");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Cancelamento");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Cancelamento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "CategoriaPagamento");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.CategoriaPagamento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Contratado");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Contratado.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Encerramento");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Encerramento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Motorista");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Motorista.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "NotaFiscal");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.NotaFiscal.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "OperacaoTransporte");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.OperacaoTransporte.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Pagamento");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Pagamento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "PagamentoCancelamento");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.PagamentoCancelamento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "TipoAbonoDeQuebra");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.TipoAbonoDeQuebra.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "TipoPagamento");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.TipoPagamento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "TipoViagem");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.TipoViagem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "UnidadeDeMedidaDaMercadoria");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.UnidadeDeMedidaDaMercadoria.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Veiculo");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Veiculo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "Viagem");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.Viagem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "ViagemTipoDeCalculo");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.ViagemTipoDeCalculo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "ArrayOfNotaFiscal");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterTransacoesObjects.NotaFiscal[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "NotaFiscal");
            qName2 = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "NotaFiscal");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "NotaFiscal");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterTransacoesObjects.NotaFiscal.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "TipoDeCobranca");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterTransacoesObjects.TipoDeCobranca.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "Transacao");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterTransacoesObjects.Transacao.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "TransacaoStatus");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterTransacoesObjects.TransacaoStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterVinculoParaTransporteObjects", "VeiculoRequest");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterVinculoParaTransporteObjects.VeiculoRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterVinculoParaTransporteObjects", "VeiculoResponse");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterVinculoParaTransporteObjects.VeiculoResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/RetificarOperacaoTransporte", "ArrayOfVeiculo");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.RetificarOperacaoTransporte.Veiculo[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/RetificarOperacaoTransporte", "Veiculo");
            qName2 = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/RetificarOperacaoTransporte", "Veiculo");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/RetificarOperacaoTransporte", "Veiculo");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.RetificarOperacaoTransporte.Veiculo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef", "ObterOperacoesTransporteAgrupadasPdfResponse");
            cachedSerQNames.add(qName);
            cls = br.adm.ipc.schemas.efrete.pef.ObterOperacoesTransporteAgrupadasPdfResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public br.adm.ipc.schemas.efrete.pef.objects.AdicionarOperacaoTransporteResponse adicionarOperacaoTransporte(br.adm.ipc.schemas.efrete.pef.objects.AdicionarOperacaoTransporteRequest adicionarOperacaoTransporteRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.ipc.adm.br/efrete/pef/AdicionarOperacaoTransporte");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef", "AdicionarOperacaoTransporte"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {adicionarOperacaoTransporteRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.adm.ipc.schemas.efrete.pef.objects.AdicionarOperacaoTransporteResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.adm.ipc.schemas.efrete.pef.objects.AdicionarOperacaoTransporteResponse) org.apache.axis.utils.JavaUtils.convert(_resp, br.adm.ipc.schemas.efrete.pef.objects.AdicionarOperacaoTransporteResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.adm.ipc.schemas.efrete.pef.objects.CancelarOperacaoTransporteResponse cancelarOperacaoTransporte(br.adm.ipc.schemas.efrete.pef.objects.CancelarOperacaoTransporteRequest cancelarOperacaoTransporteRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.ipc.adm.br/efrete/pef/CancelarOperacaoTransporte");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef", "CancelarOperacaoTransporte"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {cancelarOperacaoTransporteRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.adm.ipc.schemas.efrete.pef.objects.CancelarOperacaoTransporteResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.adm.ipc.schemas.efrete.pef.objects.CancelarOperacaoTransporteResponse) org.apache.axis.utils.JavaUtils.convert(_resp, br.adm.ipc.schemas.efrete.pef.objects.CancelarOperacaoTransporteResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.adm.ipc.schemas.efrete.pef.objects.RetificarOperacaoTransporteResponse retificarOperacaoTransporte(br.adm.ipc.schemas.efrete.pef.objects.RetificarOperacaoTransporteRequest retificarOperacaoTransporteRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.ipc.adm.br/efrete/pef/RetificarOperacaoTransporte");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef", "RetificarOperacaoTransporte"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {retificarOperacaoTransporteRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.adm.ipc.schemas.efrete.pef.objects.RetificarOperacaoTransporteResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.adm.ipc.schemas.efrete.pef.objects.RetificarOperacaoTransporteResponse) org.apache.axis.utils.JavaUtils.convert(_resp, br.adm.ipc.schemas.efrete.pef.objects.RetificarOperacaoTransporteResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.adm.ipc.schemas.efrete.pef.objects.EncerrarOperacaoTransporteResponse encerrarOperacaoTransporte(br.adm.ipc.schemas.efrete.pef.objects.EncerrarOperacaoTransporteRequest encerrarOperacaoTransporteRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.ipc.adm.br/efrete/pef/EncerrarOperacaoTransporte");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef", "EncerrarOperacaoTransporte"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {encerrarOperacaoTransporteRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.adm.ipc.schemas.efrete.pef.objects.EncerrarOperacaoTransporteResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.adm.ipc.schemas.efrete.pef.objects.EncerrarOperacaoTransporteResponse) org.apache.axis.utils.JavaUtils.convert(_resp, br.adm.ipc.schemas.efrete.pef.objects.EncerrarOperacaoTransporteResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.adm.ipc.schemas.efrete.pef.objects.AdicionarViagemResponse adicionarViagem(br.adm.ipc.schemas.efrete.pef.objects.AdicionarViagemRequest adicionarViagemRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.ipc.adm.br/efrete/pef/AdicionarViagem");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef", "AdicionarViagem"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {adicionarViagemRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.adm.ipc.schemas.efrete.pef.objects.AdicionarViagemResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.adm.ipc.schemas.efrete.pef.objects.AdicionarViagemResponse) org.apache.axis.utils.JavaUtils.convert(_resp, br.adm.ipc.schemas.efrete.pef.objects.AdicionarViagemResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.adm.ipc.schemas.efrete.pef.objects.AdicionarPagamentoResponse adicionarPagamento(br.adm.ipc.schemas.efrete.pef.objects.AdicionarPagamentoRequest adicionarPagamentoRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.ipc.adm.br/efrete/pef/AdicionarPagamento");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef", "AdicionarPagamento"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {adicionarPagamentoRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.adm.ipc.schemas.efrete.pef.objects.AdicionarPagamentoResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.adm.ipc.schemas.efrete.pef.objects.AdicionarPagamentoResponse) org.apache.axis.utils.JavaUtils.convert(_resp, br.adm.ipc.schemas.efrete.pef.objects.AdicionarPagamentoResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.adm.ipc.schemas.efrete.pef.objects.CancelarPagamentoResponse cancelarPagamento(br.adm.ipc.schemas.efrete.pef.objects.CancelarPagamentoRequest cancelarPagamentoRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.ipc.adm.br/efrete/pef/CancelarPagamento");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef", "CancelarPagamento"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {cancelarPagamentoRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.adm.ipc.schemas.efrete.pef.objects.CancelarPagamentoResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.adm.ipc.schemas.efrete.pef.objects.CancelarPagamentoResponse) org.apache.axis.utils.JavaUtils.convert(_resp, br.adm.ipc.schemas.efrete.pef.objects.CancelarPagamentoResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.adm.ipc.schemas.efrete.pef.objects.AlterarDataLiberacaoPagamentoResponse alterarDataLiberacaoPagamento(br.adm.ipc.schemas.efrete.pef.objects.AlterarDataLiberacaoPagamentoRequest editarPagamentoRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.ipc.adm.br/efrete/pef/AlterarDataLiberacaoPagamento");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef", "AlterarDataLiberacaoPagamento"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {editarPagamentoRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.adm.ipc.schemas.efrete.pef.objects.AlterarDataLiberacaoPagamentoResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.adm.ipc.schemas.efrete.pef.objects.AlterarDataLiberacaoPagamentoResponse) org.apache.axis.utils.JavaUtils.convert(_resp, br.adm.ipc.schemas.efrete.pef.objects.AlterarDataLiberacaoPagamentoResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransporteParaImpressaoResponse obterOperacaoTransporteParaImpressao(br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransporteParaImpressaoRequest obterOperacaoTransporteParaImpressaoRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteParaImpressao");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef", "ObterOperacaoTransporteParaImpressao"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {obterOperacaoTransporteParaImpressaoRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransporteParaImpressaoResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransporteParaImpressaoResponse) org.apache.axis.utils.JavaUtils.convert(_resp, br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransporteParaImpressaoResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.adm.ipc.schemas.efrete.pef.objects.ConsultarDownloadDocumentoResponse consultarDownloadDocumento(br.adm.ipc.schemas.efrete.pef.objects.ConsultarDownloadDocumentoRequest consultarDownloadDocumentoRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.ipc.adm.br/efrete/pef/ConsultarDownloadDocumento");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef", "ConsultarDownloadDocumento"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {consultarDownloadDocumentoRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.adm.ipc.schemas.efrete.pef.objects.ConsultarDownloadDocumentoResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.adm.ipc.schemas.efrete.pef.objects.ConsultarDownloadDocumentoResponse) org.apache.axis.utils.JavaUtils.convert(_resp, br.adm.ipc.schemas.efrete.pef.objects.ConsultarDownloadDocumentoResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.adm.ipc.schemas.efrete.pef.objects.DownloadDocumentoResponse downloadDocumento(br.adm.ipc.schemas.efrete.pef.objects.DownloadDocumentoRequest downloadDocumentoRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.ipc.adm.br/efrete/pef/DownloadDocumento");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef", "DownloadDocumento"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {downloadDocumentoRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.adm.ipc.schemas.efrete.pef.objects.DownloadDocumentoResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.adm.ipc.schemas.efrete.pef.objects.DownloadDocumentoResponse) org.apache.axis.utils.JavaUtils.convert(_resp, br.adm.ipc.schemas.efrete.pef.objects.DownloadDocumentoResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransportePdfResponse obterOperacaoTransportePdf(br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransportePdfRequest obterOperacaoTransportePdfRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransportePdf");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef", "ObterOperacaoTransportePdf"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {obterOperacaoTransportePdfRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransportePdfResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransportePdfResponse) org.apache.axis.utils.JavaUtils.convert(_resp, br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransportePdfResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.adm.ipc.schemas.efrete.pef.ObterOperacoesTransporteAgrupadasPdfResponse obterOperacoesTransporteAgrupadasPdf(br.adm.ipc.schemas.efrete.pef.objects.ObterOperacoesTransporteAgrupadasPdfRequest obterOperacoesTransporteAgrupadasPdfRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.ipc.adm.br/efrete/pef/ObterOperacoesTransporteAgrupadasPdf");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef", "ObterOperacoesTransporteAgrupadasPdf"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {obterOperacoesTransporteAgrupadasPdfRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.adm.ipc.schemas.efrete.pef.ObterOperacoesTransporteAgrupadasPdfResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.adm.ipc.schemas.efrete.pef.ObterOperacoesTransporteAgrupadasPdfResponse) org.apache.axis.utils.JavaUtils.convert(_resp, br.adm.ipc.schemas.efrete.pef.ObterOperacoesTransporteAgrupadasPdfResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransporteResponse obterOperacaoTransporte(br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransporteRequest obterOperacaoTransporteRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporte");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef", "ObterOperacaoTransporte"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {obterOperacaoTransporteRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransporteResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransporteResponse) org.apache.axis.utils.JavaUtils.convert(_resp, br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransporteResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.adm.ipc.schemas.efrete.pef.objects.AbonarQuebraDeFreteResponse abonarQuebraDeFrete(br.adm.ipc.schemas.efrete.pef.objects.AbonarQuebraDeFreteRequest abonarQuebraDeFreteRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.ipc.adm.br/efrete/pef/AbonarQuebraDeFrete");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef", "AbonarQuebraDeFrete"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {abonarQuebraDeFreteRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.adm.ipc.schemas.efrete.pef.objects.AbonarQuebraDeFreteResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.adm.ipc.schemas.efrete.pef.objects.AbonarQuebraDeFreteResponse) org.apache.axis.utils.JavaUtils.convert(_resp, br.adm.ipc.schemas.efrete.pef.objects.AbonarQuebraDeFreteResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.adm.ipc.schemas.efrete.pef.objects.ObterTransacoesResponse obterTransacoes(br.adm.ipc.schemas.efrete.pef.objects.ObterTransacoesRequest obterTransacoesRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoes");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef", "ObterTransacoes"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {obterTransacoesRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.adm.ipc.schemas.efrete.pef.objects.ObterTransacoesResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.adm.ipc.schemas.efrete.pef.objects.ObterTransacoesResponse) org.apache.axis.utils.JavaUtils.convert(_resp, br.adm.ipc.schemas.efrete.pef.objects.ObterTransacoesResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.adm.ipc.schemas.efrete.pef.objects.ObterVinculoParaTransporteResponse obterVinculoParaTransporte(br.adm.ipc.schemas.efrete.pef.objects.ObterVinculoParaTransporteRequest obterVinculoParaTransporteRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.ipc.adm.br/efrete/pef/ObterVinculoParaTransporte");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef", "ObterVinculoParaTransporte"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {obterVinculoParaTransporteRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.adm.ipc.schemas.efrete.pef.objects.ObterVinculoParaTransporteResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.adm.ipc.schemas.efrete.pef.objects.ObterVinculoParaTransporteResponse) org.apache.axis.utils.JavaUtils.convert(_resp, br.adm.ipc.schemas.efrete.pef.objects.ObterVinculoParaTransporteResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.adm.ipc.schemas.efrete.pef.objects.ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteResponse obterCodigoIdentificacaoOperacaoTransportePorIdOperacaoCliente(br.adm.ipc.schemas.efrete.pef.objects.ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteRequest obterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[17]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.ipc.adm.br/efrete/pef/ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoCliente");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef", "ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoCliente"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {obterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.adm.ipc.schemas.efrete.pef.objects.ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.adm.ipc.schemas.efrete.pef.objects.ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteResponse) org.apache.axis.utils.JavaUtils.convert(_resp, br.adm.ipc.schemas.efrete.pef.objects.ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.adm.ipc.schemas.efrete.pef.objects.RegistrarQuantidadeDaMercadoriaNoDesembarqueResponse registrarQuantidadeDaMercadoriaNoDesembarque(br.adm.ipc.schemas.efrete.pef.objects.RegistrarQuantidadeDaMercadoriaNoDesembarqueRequest registrarQuantidadeDaMercadoriaNoDesembarqueRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[18]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.ipc.adm.br/efrete/pef/RegistrarQuantidadeDaMercadoriaNoDesembarque");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef", "RegistrarQuantidadeDaMercadoriaNoDesembarque"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {registrarQuantidadeDaMercadoriaNoDesembarqueRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.adm.ipc.schemas.efrete.pef.objects.RegistrarQuantidadeDaMercadoriaNoDesembarqueResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.adm.ipc.schemas.efrete.pef.objects.RegistrarQuantidadeDaMercadoriaNoDesembarqueResponse) org.apache.axis.utils.JavaUtils.convert(_resp, br.adm.ipc.schemas.efrete.pef.objects.RegistrarQuantidadeDaMercadoriaNoDesembarqueResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoResponse registrarPagamento(br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoRequest registrarPagamentoRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[19]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.ipc.adm.br/efrete/pef/RegistrarPagamento");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef", "RegistrarPagamento"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {registrarPagamentoRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoResponse) org.apache.axis.utils.JavaUtils.convert(_resp, br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoQuitacaoResponse registrarPagamentoQuitacao(br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoQuitacaoRequest registrarPagamentoQuitacaoRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[20]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.ipc.adm.br/efrete/pef/RegistrarPagamentoQuitacao");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef", "RegistrarPagamentoQuitacao"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {registrarPagamentoQuitacaoRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoQuitacaoResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoQuitacaoResponse) org.apache.axis.utils.JavaUtils.convert(_resp, br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoQuitacaoResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.adm.ipc.schemas.efrete.pef.objects.AlterarEntregaDocumentacaoResponse alterarEntregaDocumentacao(br.adm.ipc.schemas.efrete.pef.objects.AlterarEntregaDocumentacaoRequest alterarEntregaDocumentacaoRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[21]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.ipc.adm.br/efrete/pef/AlterarEntregaDocumentacao");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef", "AlterarEntregaDocumentacao"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {alterarEntregaDocumentacaoRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.adm.ipc.schemas.efrete.pef.objects.AlterarEntregaDocumentacaoResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.adm.ipc.schemas.efrete.pef.objects.AlterarEntregaDocumentacaoResponse) org.apache.axis.utils.JavaUtils.convert(_resp, br.adm.ipc.schemas.efrete.pef.objects.AlterarEntregaDocumentacaoResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.adm.ipc.schemas.efrete.pef.objects.AdicionarValeFreteResponse adicionarValeFrete(br.adm.ipc.schemas.efrete.pef.objects.AdicionarValeFreteRequest adicionarValeFreteRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[22]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.ipc.adm.br/efrete/pef/AdicionarValeFrete");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef", "AdicionarValeFrete"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {adicionarValeFreteRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.adm.ipc.schemas.efrete.pef.objects.AdicionarValeFreteResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.adm.ipc.schemas.efrete.pef.objects.AdicionarValeFreteResponse) org.apache.axis.utils.JavaUtils.convert(_resp, br.adm.ipc.schemas.efrete.pef.objects.AdicionarValeFreteResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.adm.ipc.schemas.efrete.pef.objects.ConsultarTipoCargaResponse consultarTipoCarga(br.adm.ipc.schemas.efrete.pef.objects.ConsultarTipoCargaRequest consultarTipoCargaRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[23]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.ipc.adm.br/efrete/pef/ConsultarTipoCarga");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef", "ConsultarTipoCarga"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {consultarTipoCargaRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.adm.ipc.schemas.efrete.pef.objects.ConsultarTipoCargaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.adm.ipc.schemas.efrete.pef.objects.ConsultarTipoCargaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, br.adm.ipc.schemas.efrete.pef.objects.ConsultarTipoCargaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
