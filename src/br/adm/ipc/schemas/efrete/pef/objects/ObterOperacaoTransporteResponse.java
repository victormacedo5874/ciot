/**
 * ObterOperacaoTransporteResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.objects;

public class ObterOperacaoTransporteResponse  extends br.adm.ipc.schemas.efrete.objects.Response  implements java.io.Serializable {
    private br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.OperacaoTransporte operacaoTransporte;

    public ObterOperacaoTransporteResponse() {
    }

    public ObterOperacaoTransporteResponse(
           int versao,
           boolean sucesso,
           br.adm.ipc.schemas.efrete.objects.Excecao excecao,
           java.lang.Long protocoloServico,
           br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.OperacaoTransporte operacaoTransporte) {
        super(
            versao,
            sucesso,
            excecao,
            protocoloServico);
        this.operacaoTransporte = operacaoTransporte;
    }


    /**
     * Gets the operacaoTransporte value for this ObterOperacaoTransporteResponse.
     * 
     * @return operacaoTransporte
     */
    public br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.OperacaoTransporte getOperacaoTransporte() {
        return operacaoTransporte;
    }


    /**
     * Sets the operacaoTransporte value for this ObterOperacaoTransporteResponse.
     * 
     * @param operacaoTransporte
     */
    public void setOperacaoTransporte(br.adm.ipc.schemas.efrete.pef.ObterOperacaoTransporteObjects.OperacaoTransporte operacaoTransporte) {
        this.operacaoTransporte = operacaoTransporte;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ObterOperacaoTransporteResponse)) return false;
        ObterOperacaoTransporteResponse other = (ObterOperacaoTransporteResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.operacaoTransporte==null && other.getOperacaoTransporte()==null) || 
             (this.operacaoTransporte!=null &&
              this.operacaoTransporte.equals(other.getOperacaoTransporte())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getOperacaoTransporte() != null) {
            _hashCode += getOperacaoTransporte().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ObterOperacaoTransporteResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterOperacaoTransporteResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operacaoTransporte");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "OperacaoTransporte"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterOperacaoTransporteObjects", "OperacaoTransporte"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
