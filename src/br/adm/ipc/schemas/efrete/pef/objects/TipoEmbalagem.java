/**
 * TipoEmbalagem.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.objects;

public class TipoEmbalagem implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected TipoEmbalagem(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _Indefinido = "Indefinido";
    public static final java.lang.String _Bigbag = "Bigbag";
    public static final java.lang.String _Pallet = "Pallet";
    public static final java.lang.String _Granel = "Granel";
    public static final java.lang.String _Container = "Container";
    public static final java.lang.String _Saco = "Saco";
    public static final java.lang.String _Caixa = "Caixa";
    public static final java.lang.String _Unitario = "Unitario";
    public static final java.lang.String _Fardo = "Fardo";
    public static final java.lang.String _Tanque = "Tanque";
    public static final TipoEmbalagem Indefinido = new TipoEmbalagem(_Indefinido);
    public static final TipoEmbalagem Bigbag = new TipoEmbalagem(_Bigbag);
    public static final TipoEmbalagem Pallet = new TipoEmbalagem(_Pallet);
    public static final TipoEmbalagem Granel = new TipoEmbalagem(_Granel);
    public static final TipoEmbalagem Container = new TipoEmbalagem(_Container);
    public static final TipoEmbalagem Saco = new TipoEmbalagem(_Saco);
    public static final TipoEmbalagem Caixa = new TipoEmbalagem(_Caixa);
    public static final TipoEmbalagem Unitario = new TipoEmbalagem(_Unitario);
    public static final TipoEmbalagem Fardo = new TipoEmbalagem(_Fardo);
    public static final TipoEmbalagem Tanque = new TipoEmbalagem(_Tanque);
    public java.lang.String getValue() { return _value_;}
    public static TipoEmbalagem fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        TipoEmbalagem enumeration = (TipoEmbalagem)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static TipoEmbalagem fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TipoEmbalagem.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TipoEmbalagem"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
