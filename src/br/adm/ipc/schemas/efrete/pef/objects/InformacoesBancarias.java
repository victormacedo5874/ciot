/**
 * InformacoesBancarias.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.objects;

public class InformacoesBancarias  implements java.io.Serializable {
    private java.lang.String instituicaoBancaria;

    private java.lang.String agencia;

    private java.lang.String conta;

    private br.adm.ipc.schemas.efrete.pef.objects.TipoConta tipoConta;

    public InformacoesBancarias() {
    }

    public InformacoesBancarias(
           java.lang.String instituicaoBancaria,
           java.lang.String agencia,
           java.lang.String conta,
           br.adm.ipc.schemas.efrete.pef.objects.TipoConta tipoConta) {
           this.instituicaoBancaria = instituicaoBancaria;
           this.agencia = agencia;
           this.conta = conta;
           this.tipoConta = tipoConta;
    }


    /**
     * Gets the instituicaoBancaria value for this InformacoesBancarias.
     * 
     * @return instituicaoBancaria
     */
    public java.lang.String getInstituicaoBancaria() {
        return instituicaoBancaria;
    }


    /**
     * Sets the instituicaoBancaria value for this InformacoesBancarias.
     * 
     * @param instituicaoBancaria
     */
    public void setInstituicaoBancaria(java.lang.String instituicaoBancaria) {
        this.instituicaoBancaria = instituicaoBancaria;
    }


    /**
     * Gets the agencia value for this InformacoesBancarias.
     * 
     * @return agencia
     */
    public java.lang.String getAgencia() {
        return agencia;
    }


    /**
     * Sets the agencia value for this InformacoesBancarias.
     * 
     * @param agencia
     */
    public void setAgencia(java.lang.String agencia) {
        this.agencia = agencia;
    }


    /**
     * Gets the conta value for this InformacoesBancarias.
     * 
     * @return conta
     */
    public java.lang.String getConta() {
        return conta;
    }


    /**
     * Sets the conta value for this InformacoesBancarias.
     * 
     * @param conta
     */
    public void setConta(java.lang.String conta) {
        this.conta = conta;
    }


    /**
     * Gets the tipoConta value for this InformacoesBancarias.
     * 
     * @return tipoConta
     */
    public br.adm.ipc.schemas.efrete.pef.objects.TipoConta getTipoConta() {
        return tipoConta;
    }


    /**
     * Sets the tipoConta value for this InformacoesBancarias.
     * 
     * @param tipoConta
     */
    public void setTipoConta(br.adm.ipc.schemas.efrete.pef.objects.TipoConta tipoConta) {
        this.tipoConta = tipoConta;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof InformacoesBancarias)) return false;
        InformacoesBancarias other = (InformacoesBancarias) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.instituicaoBancaria==null && other.getInstituicaoBancaria()==null) || 
             (this.instituicaoBancaria!=null &&
              this.instituicaoBancaria.equals(other.getInstituicaoBancaria()))) &&
            ((this.agencia==null && other.getAgencia()==null) || 
             (this.agencia!=null &&
              this.agencia.equals(other.getAgencia()))) &&
            ((this.conta==null && other.getConta()==null) || 
             (this.conta!=null &&
              this.conta.equals(other.getConta()))) &&
            ((this.tipoConta==null && other.getTipoConta()==null) || 
             (this.tipoConta!=null &&
              this.tipoConta.equals(other.getTipoConta())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getInstituicaoBancaria() != null) {
            _hashCode += getInstituicaoBancaria().hashCode();
        }
        if (getAgencia() != null) {
            _hashCode += getAgencia().hashCode();
        }
        if (getConta() != null) {
            _hashCode += getConta().hashCode();
        }
        if (getTipoConta() != null) {
            _hashCode += getTipoConta().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(InformacoesBancarias.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "InformacoesBancarias"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("instituicaoBancaria");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "InstituicaoBancaria"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("agencia");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Agencia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("conta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Conta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoConta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TipoConta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TipoConta"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
