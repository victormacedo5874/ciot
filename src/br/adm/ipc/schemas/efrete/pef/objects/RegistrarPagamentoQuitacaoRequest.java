/**
 * RegistrarPagamentoQuitacaoRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.objects;

public class RegistrarPagamentoQuitacaoRequest  extends br.adm.ipc.schemas.efrete.objects.Request  implements java.io.Serializable {
    private java.lang.String codigoIdentificacaoOperacao;

    private br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoQuitacao.NotaFiscal[] notasFiscais;

    private java.lang.String tokenCompra;

    public RegistrarPagamentoQuitacaoRequest() {
    }

    public RegistrarPagamentoQuitacaoRequest(
           java.lang.String token,
           java.lang.String integrador,
           int versao,
           java.lang.String codigoIdentificacaoOperacao,
           br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoQuitacao.NotaFiscal[] notasFiscais,
           java.lang.String tokenCompra) {
        super(
            token,
            integrador,
            versao);
        this.codigoIdentificacaoOperacao = codigoIdentificacaoOperacao;
        this.notasFiscais = notasFiscais;
        this.tokenCompra = tokenCompra;
    }


    /**
     * Gets the codigoIdentificacaoOperacao value for this RegistrarPagamentoQuitacaoRequest.
     * 
     * @return codigoIdentificacaoOperacao
     */
    public java.lang.String getCodigoIdentificacaoOperacao() {
        return codigoIdentificacaoOperacao;
    }


    /**
     * Sets the codigoIdentificacaoOperacao value for this RegistrarPagamentoQuitacaoRequest.
     * 
     * @param codigoIdentificacaoOperacao
     */
    public void setCodigoIdentificacaoOperacao(java.lang.String codigoIdentificacaoOperacao) {
        this.codigoIdentificacaoOperacao = codigoIdentificacaoOperacao;
    }


    /**
     * Gets the notasFiscais value for this RegistrarPagamentoQuitacaoRequest.
     * 
     * @return notasFiscais
     */
    public br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoQuitacao.NotaFiscal[] getNotasFiscais() {
        return notasFiscais;
    }


    /**
     * Sets the notasFiscais value for this RegistrarPagamentoQuitacaoRequest.
     * 
     * @param notasFiscais
     */
    public void setNotasFiscais(br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoQuitacao.NotaFiscal[] notasFiscais) {
        this.notasFiscais = notasFiscais;
    }


    /**
     * Gets the tokenCompra value for this RegistrarPagamentoQuitacaoRequest.
     * 
     * @return tokenCompra
     */
    public java.lang.String getTokenCompra() {
        return tokenCompra;
    }


    /**
     * Sets the tokenCompra value for this RegistrarPagamentoQuitacaoRequest.
     * 
     * @param tokenCompra
     */
    public void setTokenCompra(java.lang.String tokenCompra) {
        this.tokenCompra = tokenCompra;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RegistrarPagamentoQuitacaoRequest)) return false;
        RegistrarPagamentoQuitacaoRequest other = (RegistrarPagamentoQuitacaoRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.codigoIdentificacaoOperacao==null && other.getCodigoIdentificacaoOperacao()==null) || 
             (this.codigoIdentificacaoOperacao!=null &&
              this.codigoIdentificacaoOperacao.equals(other.getCodigoIdentificacaoOperacao()))) &&
            ((this.notasFiscais==null && other.getNotasFiscais()==null) || 
             (this.notasFiscais!=null &&
              java.util.Arrays.equals(this.notasFiscais, other.getNotasFiscais()))) &&
            ((this.tokenCompra==null && other.getTokenCompra()==null) || 
             (this.tokenCompra!=null &&
              this.tokenCompra.equals(other.getTokenCompra())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCodigoIdentificacaoOperacao() != null) {
            _hashCode += getCodigoIdentificacaoOperacao().hashCode();
        }
        if (getNotasFiscais() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getNotasFiscais());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getNotasFiscais(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTokenCompra() != null) {
            _hashCode += getTokenCompra().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RegistrarPagamentoQuitacaoRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "RegistrarPagamentoQuitacaoRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoIdentificacaoOperacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CodigoIdentificacaoOperacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("notasFiscais");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects/RegistrarPagamentoQuitacao", "NotasFiscais"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects/RegistrarPagamentoQuitacao", "ArrayOfNotaFiscal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tokenCompra");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TokenCompra"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
