/**
 * RegistrarPagamentoQuitacaoResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.objects;

public class RegistrarPagamentoQuitacaoResponse  extends br.adm.ipc.schemas.efrete.objects.Response  implements java.io.Serializable {
    private java.lang.String codigoIdentificacaoOperacao;

    private java.lang.String transacaoId;

    private java.math.BigDecimal valorLiquido;

    private java.math.BigDecimal valorQuebra;

    private java.math.BigDecimal valorDiferencaDeFrete;

    public RegistrarPagamentoQuitacaoResponse() {
    }

    public RegistrarPagamentoQuitacaoResponse(
           int versao,
           boolean sucesso,
           br.adm.ipc.schemas.efrete.objects.Excecao excecao,
           java.lang.Long protocoloServico,
           java.lang.String codigoIdentificacaoOperacao,
           java.lang.String transacaoId,
           java.math.BigDecimal valorLiquido,
           java.math.BigDecimal valorQuebra,
           java.math.BigDecimal valorDiferencaDeFrete) {
        super(
            versao,
            sucesso,
            excecao,
            protocoloServico);
        this.codigoIdentificacaoOperacao = codigoIdentificacaoOperacao;
        this.transacaoId = transacaoId;
        this.valorLiquido = valorLiquido;
        this.valorQuebra = valorQuebra;
        this.valorDiferencaDeFrete = valorDiferencaDeFrete;
    }


    /**
     * Gets the codigoIdentificacaoOperacao value for this RegistrarPagamentoQuitacaoResponse.
     * 
     * @return codigoIdentificacaoOperacao
     */
    public java.lang.String getCodigoIdentificacaoOperacao() {
        return codigoIdentificacaoOperacao;
    }


    /**
     * Sets the codigoIdentificacaoOperacao value for this RegistrarPagamentoQuitacaoResponse.
     * 
     * @param codigoIdentificacaoOperacao
     */
    public void setCodigoIdentificacaoOperacao(java.lang.String codigoIdentificacaoOperacao) {
        this.codigoIdentificacaoOperacao = codigoIdentificacaoOperacao;
    }


    /**
     * Gets the transacaoId value for this RegistrarPagamentoQuitacaoResponse.
     * 
     * @return transacaoId
     */
    public java.lang.String getTransacaoId() {
        return transacaoId;
    }


    /**
     * Sets the transacaoId value for this RegistrarPagamentoQuitacaoResponse.
     * 
     * @param transacaoId
     */
    public void setTransacaoId(java.lang.String transacaoId) {
        this.transacaoId = transacaoId;
    }


    /**
     * Gets the valorLiquido value for this RegistrarPagamentoQuitacaoResponse.
     * 
     * @return valorLiquido
     */
    public java.math.BigDecimal getValorLiquido() {
        return valorLiquido;
    }


    /**
     * Sets the valorLiquido value for this RegistrarPagamentoQuitacaoResponse.
     * 
     * @param valorLiquido
     */
    public void setValorLiquido(java.math.BigDecimal valorLiquido) {
        this.valorLiquido = valorLiquido;
    }


    /**
     * Gets the valorQuebra value for this RegistrarPagamentoQuitacaoResponse.
     * 
     * @return valorQuebra
     */
    public java.math.BigDecimal getValorQuebra() {
        return valorQuebra;
    }


    /**
     * Sets the valorQuebra value for this RegistrarPagamentoQuitacaoResponse.
     * 
     * @param valorQuebra
     */
    public void setValorQuebra(java.math.BigDecimal valorQuebra) {
        this.valorQuebra = valorQuebra;
    }


    /**
     * Gets the valorDiferencaDeFrete value for this RegistrarPagamentoQuitacaoResponse.
     * 
     * @return valorDiferencaDeFrete
     */
    public java.math.BigDecimal getValorDiferencaDeFrete() {
        return valorDiferencaDeFrete;
    }


    /**
     * Sets the valorDiferencaDeFrete value for this RegistrarPagamentoQuitacaoResponse.
     * 
     * @param valorDiferencaDeFrete
     */
    public void setValorDiferencaDeFrete(java.math.BigDecimal valorDiferencaDeFrete) {
        this.valorDiferencaDeFrete = valorDiferencaDeFrete;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RegistrarPagamentoQuitacaoResponse)) return false;
        RegistrarPagamentoQuitacaoResponse other = (RegistrarPagamentoQuitacaoResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.codigoIdentificacaoOperacao==null && other.getCodigoIdentificacaoOperacao()==null) || 
             (this.codigoIdentificacaoOperacao!=null &&
              this.codigoIdentificacaoOperacao.equals(other.getCodigoIdentificacaoOperacao()))) &&
            ((this.transacaoId==null && other.getTransacaoId()==null) || 
             (this.transacaoId!=null &&
              this.transacaoId.equals(other.getTransacaoId()))) &&
            ((this.valorLiquido==null && other.getValorLiquido()==null) || 
             (this.valorLiquido!=null &&
              this.valorLiquido.equals(other.getValorLiquido()))) &&
            ((this.valorQuebra==null && other.getValorQuebra()==null) || 
             (this.valorQuebra!=null &&
              this.valorQuebra.equals(other.getValorQuebra()))) &&
            ((this.valorDiferencaDeFrete==null && other.getValorDiferencaDeFrete()==null) || 
             (this.valorDiferencaDeFrete!=null &&
              this.valorDiferencaDeFrete.equals(other.getValorDiferencaDeFrete())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCodigoIdentificacaoOperacao() != null) {
            _hashCode += getCodigoIdentificacaoOperacao().hashCode();
        }
        if (getTransacaoId() != null) {
            _hashCode += getTransacaoId().hashCode();
        }
        if (getValorLiquido() != null) {
            _hashCode += getValorLiquido().hashCode();
        }
        if (getValorQuebra() != null) {
            _hashCode += getValorQuebra().hashCode();
        }
        if (getValorDiferencaDeFrete() != null) {
            _hashCode += getValorDiferencaDeFrete().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RegistrarPagamentoQuitacaoResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "RegistrarPagamentoQuitacaoResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoIdentificacaoOperacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CodigoIdentificacaoOperacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transacaoId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TransacaoId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valorLiquido");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ValorLiquido"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valorQuebra");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ValorQuebra"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valorDiferencaDeFrete");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ValorDiferencaDeFrete"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
