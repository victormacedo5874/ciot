/**
 * ObterOperacoesTransporteAgrupadasPdfRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.objects;

public class ObterOperacoesTransporteAgrupadasPdfRequest  extends br.adm.ipc.schemas.efrete.objects.Request  implements java.io.Serializable {
    private java.lang.String[] codigosIdentificacaoOperacao;

    private java.lang.String identificacaoDoLote;

    private java.lang.String[] observacoesDasViagens;

    public ObterOperacoesTransporteAgrupadasPdfRequest() {
    }

    public ObterOperacoesTransporteAgrupadasPdfRequest(
           java.lang.String token,
           java.lang.String integrador,
           int versao,
           java.lang.String[] codigosIdentificacaoOperacao,
           java.lang.String identificacaoDoLote,
           java.lang.String[] observacoesDasViagens) {
        super(
            token,
            integrador,
            versao);
        this.codigosIdentificacaoOperacao = codigosIdentificacaoOperacao;
        this.identificacaoDoLote = identificacaoDoLote;
        this.observacoesDasViagens = observacoesDasViagens;
    }


    /**
     * Gets the codigosIdentificacaoOperacao value for this ObterOperacoesTransporteAgrupadasPdfRequest.
     * 
     * @return codigosIdentificacaoOperacao
     */
    public java.lang.String[] getCodigosIdentificacaoOperacao() {
        return codigosIdentificacaoOperacao;
    }


    /**
     * Sets the codigosIdentificacaoOperacao value for this ObterOperacoesTransporteAgrupadasPdfRequest.
     * 
     * @param codigosIdentificacaoOperacao
     */
    public void setCodigosIdentificacaoOperacao(java.lang.String[] codigosIdentificacaoOperacao) {
        this.codigosIdentificacaoOperacao = codigosIdentificacaoOperacao;
    }


    /**
     * Gets the identificacaoDoLote value for this ObterOperacoesTransporteAgrupadasPdfRequest.
     * 
     * @return identificacaoDoLote
     */
    public java.lang.String getIdentificacaoDoLote() {
        return identificacaoDoLote;
    }


    /**
     * Sets the identificacaoDoLote value for this ObterOperacoesTransporteAgrupadasPdfRequest.
     * 
     * @param identificacaoDoLote
     */
    public void setIdentificacaoDoLote(java.lang.String identificacaoDoLote) {
        this.identificacaoDoLote = identificacaoDoLote;
    }


    /**
     * Gets the observacoesDasViagens value for this ObterOperacoesTransporteAgrupadasPdfRequest.
     * 
     * @return observacoesDasViagens
     */
    public java.lang.String[] getObservacoesDasViagens() {
        return observacoesDasViagens;
    }


    /**
     * Sets the observacoesDasViagens value for this ObterOperacoesTransporteAgrupadasPdfRequest.
     * 
     * @param observacoesDasViagens
     */
    public void setObservacoesDasViagens(java.lang.String[] observacoesDasViagens) {
        this.observacoesDasViagens = observacoesDasViagens;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ObterOperacoesTransporteAgrupadasPdfRequest)) return false;
        ObterOperacoesTransporteAgrupadasPdfRequest other = (ObterOperacoesTransporteAgrupadasPdfRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.codigosIdentificacaoOperacao==null && other.getCodigosIdentificacaoOperacao()==null) || 
             (this.codigosIdentificacaoOperacao!=null &&
              java.util.Arrays.equals(this.codigosIdentificacaoOperacao, other.getCodigosIdentificacaoOperacao()))) &&
            ((this.identificacaoDoLote==null && other.getIdentificacaoDoLote()==null) || 
             (this.identificacaoDoLote!=null &&
              this.identificacaoDoLote.equals(other.getIdentificacaoDoLote()))) &&
            ((this.observacoesDasViagens==null && other.getObservacoesDasViagens()==null) || 
             (this.observacoesDasViagens!=null &&
              java.util.Arrays.equals(this.observacoesDasViagens, other.getObservacoesDasViagens())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCodigosIdentificacaoOperacao() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCodigosIdentificacaoOperacao());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCodigosIdentificacaoOperacao(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getIdentificacaoDoLote() != null) {
            _hashCode += getIdentificacaoDoLote().hashCode();
        }
        if (getObservacoesDasViagens() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getObservacoesDasViagens());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getObservacoesDasViagens(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ObterOperacoesTransporteAgrupadasPdfRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterOperacoesTransporteAgrupadasPdfRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigosIdentificacaoOperacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CodigosIdentificacaoOperacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("identificacaoDoLote");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "IdentificacaoDoLote"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("observacoesDasViagens");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObservacoesDasViagens"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "string"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
