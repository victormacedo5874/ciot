/**
 * AlterarDataLiberacaoPagamentoResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.objects;

public class AlterarDataLiberacaoPagamentoResponse  extends br.adm.ipc.schemas.efrete.objects.Response  implements java.io.Serializable {
    private java.lang.String codigoIdentificacaoOperacao;

    private java.lang.String idPagamentoCliente;

    private java.util.Calendar dataLiberacao;

    public AlterarDataLiberacaoPagamentoResponse() {
    }

    public AlterarDataLiberacaoPagamentoResponse(
           int versao,
           boolean sucesso,
           br.adm.ipc.schemas.efrete.objects.Excecao excecao,
           java.lang.Long protocoloServico,
           java.lang.String codigoIdentificacaoOperacao,
           java.lang.String idPagamentoCliente,
           java.util.Calendar dataLiberacao) {
        super(
            versao,
            sucesso,
            excecao,
            protocoloServico);
        this.codigoIdentificacaoOperacao = codigoIdentificacaoOperacao;
        this.idPagamentoCliente = idPagamentoCliente;
        this.dataLiberacao = dataLiberacao;
    }


    /**
     * Gets the codigoIdentificacaoOperacao value for this AlterarDataLiberacaoPagamentoResponse.
     * 
     * @return codigoIdentificacaoOperacao
     */
    public java.lang.String getCodigoIdentificacaoOperacao() {
        return codigoIdentificacaoOperacao;
    }


    /**
     * Sets the codigoIdentificacaoOperacao value for this AlterarDataLiberacaoPagamentoResponse.
     * 
     * @param codigoIdentificacaoOperacao
     */
    public void setCodigoIdentificacaoOperacao(java.lang.String codigoIdentificacaoOperacao) {
        this.codigoIdentificacaoOperacao = codigoIdentificacaoOperacao;
    }


    /**
     * Gets the idPagamentoCliente value for this AlterarDataLiberacaoPagamentoResponse.
     * 
     * @return idPagamentoCliente
     */
    public java.lang.String getIdPagamentoCliente() {
        return idPagamentoCliente;
    }


    /**
     * Sets the idPagamentoCliente value for this AlterarDataLiberacaoPagamentoResponse.
     * 
     * @param idPagamentoCliente
     */
    public void setIdPagamentoCliente(java.lang.String idPagamentoCliente) {
        this.idPagamentoCliente = idPagamentoCliente;
    }


    /**
     * Gets the dataLiberacao value for this AlterarDataLiberacaoPagamentoResponse.
     * 
     * @return dataLiberacao
     */
    public java.util.Calendar getDataLiberacao() {
        return dataLiberacao;
    }


    /**
     * Sets the dataLiberacao value for this AlterarDataLiberacaoPagamentoResponse.
     * 
     * @param dataLiberacao
     */
    public void setDataLiberacao(java.util.Calendar dataLiberacao) {
        this.dataLiberacao = dataLiberacao;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AlterarDataLiberacaoPagamentoResponse)) return false;
        AlterarDataLiberacaoPagamentoResponse other = (AlterarDataLiberacaoPagamentoResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.codigoIdentificacaoOperacao==null && other.getCodigoIdentificacaoOperacao()==null) || 
             (this.codigoIdentificacaoOperacao!=null &&
              this.codigoIdentificacaoOperacao.equals(other.getCodigoIdentificacaoOperacao()))) &&
            ((this.idPagamentoCliente==null && other.getIdPagamentoCliente()==null) || 
             (this.idPagamentoCliente!=null &&
              this.idPagamentoCliente.equals(other.getIdPagamentoCliente()))) &&
            ((this.dataLiberacao==null && other.getDataLiberacao()==null) || 
             (this.dataLiberacao!=null &&
              this.dataLiberacao.equals(other.getDataLiberacao())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCodigoIdentificacaoOperacao() != null) {
            _hashCode += getCodigoIdentificacaoOperacao().hashCode();
        }
        if (getIdPagamentoCliente() != null) {
            _hashCode += getIdPagamentoCliente().hashCode();
        }
        if (getDataLiberacao() != null) {
            _hashCode += getDataLiberacao().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AlterarDataLiberacaoPagamentoResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AlterarDataLiberacaoPagamentoResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoIdentificacaoOperacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CodigoIdentificacaoOperacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idPagamentoCliente");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "IdPagamentoCliente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataLiberacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DataLiberacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
