/**
 * ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.objects;

public class ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteResponse  extends br.adm.ipc.schemas.efrete.objects.Response  implements java.io.Serializable {
    private java.lang.String codigoIdentificacaoOperacao;

    private br.adm.ipc.schemas.efrete.pef.objects.EstadoCiot estadoCiot;

    private java.lang.String protocolo;

    public ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteResponse() {
    }

    public ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteResponse(
           int versao,
           boolean sucesso,
           br.adm.ipc.schemas.efrete.objects.Excecao excecao,
           java.lang.Long protocoloServico,
           java.lang.String codigoIdentificacaoOperacao,
           br.adm.ipc.schemas.efrete.pef.objects.EstadoCiot estadoCiot,
           java.lang.String protocolo) {
        super(
            versao,
            sucesso,
            excecao,
            protocoloServico);
        this.codigoIdentificacaoOperacao = codigoIdentificacaoOperacao;
        this.estadoCiot = estadoCiot;
        this.protocolo = protocolo;
    }


    /**
     * Gets the codigoIdentificacaoOperacao value for this ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteResponse.
     * 
     * @return codigoIdentificacaoOperacao
     */
    public java.lang.String getCodigoIdentificacaoOperacao() {
        return codigoIdentificacaoOperacao;
    }


    /**
     * Sets the codigoIdentificacaoOperacao value for this ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteResponse.
     * 
     * @param codigoIdentificacaoOperacao
     */
    public void setCodigoIdentificacaoOperacao(java.lang.String codigoIdentificacaoOperacao) {
        this.codigoIdentificacaoOperacao = codigoIdentificacaoOperacao;
    }


    /**
     * Gets the estadoCiot value for this ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteResponse.
     * 
     * @return estadoCiot
     */
    public br.adm.ipc.schemas.efrete.pef.objects.EstadoCiot getEstadoCiot() {
        return estadoCiot;
    }


    /**
     * Sets the estadoCiot value for this ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteResponse.
     * 
     * @param estadoCiot
     */
    public void setEstadoCiot(br.adm.ipc.schemas.efrete.pef.objects.EstadoCiot estadoCiot) {
        this.estadoCiot = estadoCiot;
    }


    /**
     * Gets the protocolo value for this ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteResponse.
     * 
     * @return protocolo
     */
    public java.lang.String getProtocolo() {
        return protocolo;
    }


    /**
     * Sets the protocolo value for this ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteResponse.
     * 
     * @param protocolo
     */
    public void setProtocolo(java.lang.String protocolo) {
        this.protocolo = protocolo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteResponse)) return false;
        ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteResponse other = (ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.codigoIdentificacaoOperacao==null && other.getCodigoIdentificacaoOperacao()==null) || 
             (this.codigoIdentificacaoOperacao!=null &&
              this.codigoIdentificacaoOperacao.equals(other.getCodigoIdentificacaoOperacao()))) &&
            ((this.estadoCiot==null && other.getEstadoCiot()==null) || 
             (this.estadoCiot!=null &&
              this.estadoCiot.equals(other.getEstadoCiot()))) &&
            ((this.protocolo==null && other.getProtocolo()==null) || 
             (this.protocolo!=null &&
              this.protocolo.equals(other.getProtocolo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCodigoIdentificacaoOperacao() != null) {
            _hashCode += getCodigoIdentificacaoOperacao().hashCode();
        }
        if (getEstadoCiot() != null) {
            _hashCode += getEstadoCiot().hashCode();
        }
        if (getProtocolo() != null) {
            _hashCode += getProtocolo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoIdentificacaoOperacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CodigoIdentificacaoOperacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estadoCiot");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "EstadoCiot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "EstadoCiot"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("protocolo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Protocolo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
