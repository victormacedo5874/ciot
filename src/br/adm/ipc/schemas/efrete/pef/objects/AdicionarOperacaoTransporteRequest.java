/**
 * AdicionarOperacaoTransporteRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.objects;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AdicionarOperacaoTransporteRequest  extends br.adm.ipc.schemas.efrete.objects.Request  implements java.io.Serializable {
    private br.adm.ipc.schemas.efrete.pef.objects.TipoViagem tipoViagem;

    private java.lang.Boolean emissaoGratuita;

    private br.adm.ipc.schemas.efrete.pef.objects.TipoPagamento tipoPagamento;

    private java.lang.Boolean bloquearNaoEquiparado;

    private String matrizCNPJ;

    private java.lang.Long filialCNPJ;

    private java.lang.String idOperacaoCliente;

    private java.util.Calendar dataInicioViagem;

    private java.util.Calendar dataFimViagem;

    private String codigoNCMNaturezaCarga;

    private java.math.BigDecimal pesoCarga;

    private br.adm.ipc.schemas.efrete.pef.objects.TipoEmbalagem tipoEmbalagem;

    private br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Viagem[] viagens;

    private br.adm.ipc.schemas.efrete.pef.objects.Impostos impostos;

    private br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Pagamento[] pagamentos;

    private br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Contratado contratado;

    private br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Motorista motorista;

    private br.adm.ipc.schemas.efrete.pef.objects.Destinatario destinatario;

    private br.adm.ipc.schemas.efrete.pef.objects.Contratante contratante;

    private br.adm.ipc.schemas.efrete.pef.objects.Subcontratante subcontratante;

    private br.adm.ipc.schemas.efrete.pef.objects.Consignatario consignatario;

    private br.adm.ipc.schemas.efrete.pef.objects.TomadorServico tomadorServico;

    private br.adm.ipc.schemas.efrete.pef.objects.Remetente remetente;

    private br.adm.ipc.schemas.efrete.pef.objects.ProprietarioCarga proprietarioCarga;

    private br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Veiculo[] veiculos;

    private java.lang.String codigoIdentificacaoOperacaoPrincipal;

    private java.lang.String[] observacoesAoTransportador;

    private java.lang.String[] observacoesAoCredenciado;

    private br.adm.ipc.schemas.efrete.pef.objects.TipoEntregaDocumentacao entregaDocumentacao;

    private java.lang.Integer quantidadeSaques;

    private java.lang.Integer quantidadeTransferencias;

    private java.math.BigDecimal valorSaques;

    private java.math.BigDecimal valorTransferencias;

    private org.apache.axis.types.UnsignedShort codigoTipoCarga;

    private java.lang.Boolean altoDesempenho;

    private java.lang.Boolean destinacaoComercial;

    private java.lang.Boolean freteRetorno;

    private java.lang.String cepRetorno;

    private java.lang.Long distanciaRetorno;

    public AdicionarOperacaoTransporteRequest() {
    }

    public AdicionarOperacaoTransporteRequest(
           java.lang.String token,
           java.lang.String integrador,
           int versao,
           br.adm.ipc.schemas.efrete.pef.objects.TipoViagem tipoViagem,
           java.lang.Boolean emissaoGratuita,
           br.adm.ipc.schemas.efrete.pef.objects.TipoPagamento tipoPagamento,
           java.lang.Boolean bloquearNaoEquiparado,
           String matrizCNPJ,
           java.lang.Long filialCNPJ,
           java.lang.String idOperacaoCliente,
           java.util.Calendar dataInicioViagem,
           java.util.Calendar dataFimViagem,
           String codigoNCMNaturezaCarga,
           java.math.BigDecimal pesoCarga,
           br.adm.ipc.schemas.efrete.pef.objects.TipoEmbalagem tipoEmbalagem,
           br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Viagem[] viagens,
           br.adm.ipc.schemas.efrete.pef.objects.Impostos impostos,
           br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Pagamento[] pagamentos,
           br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Contratado contratado,
           br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Motorista motorista,
           br.adm.ipc.schemas.efrete.pef.objects.Destinatario destinatario,
           br.adm.ipc.schemas.efrete.pef.objects.Contratante contratante,
           br.adm.ipc.schemas.efrete.pef.objects.Subcontratante subcontratante,
           br.adm.ipc.schemas.efrete.pef.objects.Consignatario consignatario,
           br.adm.ipc.schemas.efrete.pef.objects.TomadorServico tomadorServico,
           br.adm.ipc.schemas.efrete.pef.objects.Remetente remetente,
           br.adm.ipc.schemas.efrete.pef.objects.ProprietarioCarga proprietarioCarga,
           br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Veiculo[] veiculos,
           java.lang.String codigoIdentificacaoOperacaoPrincipal,
           java.lang.String[] observacoesAoTransportador,
           java.lang.String[] observacoesAoCredenciado,
           br.adm.ipc.schemas.efrete.pef.objects.TipoEntregaDocumentacao entregaDocumentacao,
           java.lang.Integer quantidadeSaques,
           java.lang.Integer quantidadeTransferencias,
           java.math.BigDecimal valorSaques,
           java.math.BigDecimal valorTransferencias,
           org.apache.axis.types.UnsignedShort codigoTipoCarga,
           java.lang.Boolean altoDesempenho,
           java.lang.Boolean destinacaoComercial,
           java.lang.Boolean freteRetorno,
           java.lang.String cepRetorno,
           java.lang.Long distanciaRetorno) {
        super(
            token,
            integrador,
            versao);
        this.tipoViagem = tipoViagem;
        this.emissaoGratuita = emissaoGratuita;
        this.tipoPagamento = tipoPagamento;
        this.bloquearNaoEquiparado = bloquearNaoEquiparado;
        this.matrizCNPJ = matrizCNPJ;
        this.filialCNPJ = filialCNPJ;
        this.idOperacaoCliente = idOperacaoCliente;
        this.dataInicioViagem = dataInicioViagem;
        this.dataFimViagem = dataFimViagem;
        this.codigoNCMNaturezaCarga = codigoNCMNaturezaCarga;
        this.pesoCarga = pesoCarga;
        this.tipoEmbalagem = tipoEmbalagem;
        this.viagens = viagens;
        this.impostos = impostos;
        this.pagamentos = pagamentos;
        this.contratado = contratado;
        this.motorista = motorista;
        this.destinatario = destinatario;
        this.contratante = contratante;
        this.subcontratante = subcontratante;
        this.consignatario = consignatario;
        this.tomadorServico = tomadorServico;
        this.remetente = remetente;
        this.proprietarioCarga = proprietarioCarga;
        this.veiculos = veiculos;
        this.codigoIdentificacaoOperacaoPrincipal = codigoIdentificacaoOperacaoPrincipal;
        this.observacoesAoTransportador = observacoesAoTransportador;
        this.observacoesAoCredenciado = observacoesAoCredenciado;
        this.entregaDocumentacao = entregaDocumentacao;
        this.quantidadeSaques = quantidadeSaques;
        this.quantidadeTransferencias = quantidadeTransferencias;
        this.valorSaques = valorSaques;
        this.valorTransferencias = valorTransferencias;
        this.codigoTipoCarga = codigoTipoCarga;
        this.altoDesempenho = altoDesempenho;
        this.destinacaoComercial = destinacaoComercial;
        this.freteRetorno = freteRetorno;
        this.cepRetorno = cepRetorno;
        this.distanciaRetorno = distanciaRetorno;
    }


    /**
     * Gets the tipoViagem value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return tipoViagem
     */
    public br.adm.ipc.schemas.efrete.pef.objects.TipoViagem getTipoViagem() {
        return tipoViagem;
    }


    /**
     * Sets the tipoViagem value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param tipoViagem
     */
    public void setTipoViagem(br.adm.ipc.schemas.efrete.pef.objects.TipoViagem tipoViagem) {
        this.tipoViagem = tipoViagem;
    }


    /**
     * Gets the emissaoGratuita value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return emissaoGratuita
     */
    public java.lang.Boolean getEmissaoGratuita() {
        return emissaoGratuita;
    }


    /**
     * Sets the emissaoGratuita value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param emissaoGratuita
     */
    public void setEmissaoGratuita(java.lang.Boolean emissaoGratuita) {
        this.emissaoGratuita = emissaoGratuita;
    }


    /**
     * Gets the tipoPagamento value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return tipoPagamento
     */
    public br.adm.ipc.schemas.efrete.pef.objects.TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }


    /**
     * Sets the tipoPagamento value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param tipoPagamento
     */
    public void setTipoPagamento(br.adm.ipc.schemas.efrete.pef.objects.TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }


    /**
     * Gets the bloquearNaoEquiparado value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return bloquearNaoEquiparado
     */
    public java.lang.Boolean getBloquearNaoEquiparado() {
        return bloquearNaoEquiparado;
    }


    /**
     * Sets the bloquearNaoEquiparado value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param bloquearNaoEquiparado
     */
    public void setBloquearNaoEquiparado(java.lang.Boolean bloquearNaoEquiparado) {
        this.bloquearNaoEquiparado = bloquearNaoEquiparado;
    }


    /**
     * Gets the matrizCNPJ value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return matrizCNPJ
     */
    public String getMatrizCNPJ() {
        return matrizCNPJ;
    }


    /**
     * Sets the matrizCNPJ value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param matrizCNPJ
     */
    public void setMatrizCNPJ(String matrizCNPJ) {
        this.matrizCNPJ = matrizCNPJ;
    }


    /**
     * Gets the filialCNPJ value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return filialCNPJ
     */
    public java.lang.Long getFilialCNPJ() {
        return filialCNPJ;
    }


    /**
     * Sets the filialCNPJ value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param filialCNPJ
     */
    public void setFilialCNPJ(java.lang.Long filialCNPJ) {
        this.filialCNPJ = filialCNPJ;
    }


    /**
     * Gets the idOperacaoCliente value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return idOperacaoCliente
     */
    public java.lang.String getIdOperacaoCliente() {
        return idOperacaoCliente;
    }


    /**
     * Sets the idOperacaoCliente value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param idOperacaoCliente
     */
    public void setIdOperacaoCliente(java.lang.String idOperacaoCliente) {
        this.idOperacaoCliente = idOperacaoCliente;
    }


    /**
     * Gets the dataInicioViagem value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return dataInicioViagem
     */
    public java.util.Calendar getDataInicioViagem() {
        return dataInicioViagem;
    }


    /**
     * Sets the dataInicioViagem value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param dataInicioViagem
     */
    public void setDataInicioViagem(java.util.Calendar dataInicioViagem) {
        this.dataInicioViagem = dataInicioViagem;
    }


    /**
     * Gets the dataFimViagem value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return dataFimViagem
     */
    public java.util.Calendar getDataFimViagem() {
        return dataFimViagem;
    }


    /**
     * Sets the dataFimViagem value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param dataFimViagem
     */
    public void setDataFimViagem(java.util.Calendar dataFimViagem) {
        this.dataFimViagem = dataFimViagem;
    }


    /**
     * Gets the codigoNCMNaturezaCarga value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return codigoNCMNaturezaCarga
     */
    public String getCodigoNCMNaturezaCarga() {
        return codigoNCMNaturezaCarga;
    }


    /**
     * Sets the codigoNCMNaturezaCarga value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param codigoNCMNaturezaCarga
     */
    public void setCodigoNCMNaturezaCarga(String codigoNCMNaturezaCarga) {
        this.codigoNCMNaturezaCarga = codigoNCMNaturezaCarga;
    }


    /**
     * Gets the pesoCarga value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return pesoCarga
     */
    public java.math.BigDecimal getPesoCarga() {
        return pesoCarga;
    }


    /**
     * Sets the pesoCarga value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param pesoCarga
     */
    public void setPesoCarga(java.math.BigDecimal pesoCarga) {
        this.pesoCarga = pesoCarga;
    }


    /**
     * Gets the tipoEmbalagem value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return tipoEmbalagem
     */
    public br.adm.ipc.schemas.efrete.pef.objects.TipoEmbalagem getTipoEmbalagem() {
        return tipoEmbalagem;
    }


    /**
     * Sets the tipoEmbalagem value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param tipoEmbalagem
     */
    public void setTipoEmbalagem(br.adm.ipc.schemas.efrete.pef.objects.TipoEmbalagem tipoEmbalagem) {
        this.tipoEmbalagem = tipoEmbalagem;
    }


    /**
     * Gets the viagens value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return viagens
     */
    public br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Viagem[] getViagens() {
        return viagens;
    }


    /**
     * Sets the viagens value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param viagens
     */
    public void setViagens(br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Viagem[] viagens) {
        this.viagens = viagens;
    }

    public br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Viagem getViagens(int i) {
        return this.viagens[i];
    }

    public void setViagens(int i, br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Viagem _value) {
        this.viagens[i] = _value;
    }


    /**
     * Gets the impostos value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return impostos
     */
    public br.adm.ipc.schemas.efrete.pef.objects.Impostos getImpostos() {
        return impostos;
    }


    /**
     * Sets the impostos value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param impostos
     */
    public void setImpostos(br.adm.ipc.schemas.efrete.pef.objects.Impostos impostos) {
        this.impostos = impostos;
    }


    /**
     * Gets the pagamentos value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return pagamentos
     */
    public br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Pagamento[] getPagamentos() {
        return pagamentos;
    }


    /**
     * Sets the pagamentos value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param pagamentos
     */
    public void setPagamentos(br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Pagamento[] pagamentos) {
        this.pagamentos = pagamentos;
    }

    public br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Pagamento getPagamentos(int i) {
        return this.pagamentos[i];
    }

    public void setPagamentos(int i, br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Pagamento _value) {
        this.pagamentos[i] = _value;
    }


    /**
     * Gets the contratado value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return contratado
     */
    public br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Contratado getContratado() {
        return contratado;
    }


    /**
     * Sets the contratado value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param contratado
     */
    public void setContratado(br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Contratado contratado) {
        this.contratado = contratado;
    }


    /**
     * Gets the motorista value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return motorista
     */
    public br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Motorista getMotorista() {
        return motorista;
    }


    /**
     * Sets the motorista value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param motorista
     */
    public void setMotorista(br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Motorista motorista) {
        this.motorista = motorista;
    }


    /**
     * Gets the destinatario value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return destinatario
     */
    public br.adm.ipc.schemas.efrete.pef.objects.Destinatario getDestinatario() {
        return destinatario;
    }


    /**
     * Sets the destinatario value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param destinatario
     */
    public void setDestinatario(br.adm.ipc.schemas.efrete.pef.objects.Destinatario destinatario) {
        this.destinatario = destinatario;
    }


    /**
     * Gets the contratante value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return contratante
     */
    public br.adm.ipc.schemas.efrete.pef.objects.Contratante getContratante() {
        return contratante;
    }


    /**
     * Sets the contratante value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param contratante
     */
    public void setContratante(br.adm.ipc.schemas.efrete.pef.objects.Contratante contratante) {
        this.contratante = contratante;
    }


    /**
     * Gets the subcontratante value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return subcontratante
     */
    public br.adm.ipc.schemas.efrete.pef.objects.Subcontratante getSubcontratante() {
        return subcontratante;
    }


    /**
     * Sets the subcontratante value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param subcontratante
     */
    public void setSubcontratante(br.adm.ipc.schemas.efrete.pef.objects.Subcontratante subcontratante) {
        this.subcontratante = subcontratante;
    }


    /**
     * Gets the consignatario value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return consignatario
     */
    public br.adm.ipc.schemas.efrete.pef.objects.Consignatario getConsignatario() {
        return consignatario;
    }


    /**
     * Sets the consignatario value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param consignatario
     */
    public void setConsignatario(br.adm.ipc.schemas.efrete.pef.objects.Consignatario consignatario) {
        this.consignatario = consignatario;
    }


    /**
     * Gets the tomadorServico value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return tomadorServico
     */
    public br.adm.ipc.schemas.efrete.pef.objects.TomadorServico getTomadorServico() {
        return tomadorServico;
    }


    /**
     * Sets the tomadorServico value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param tomadorServico
     */
    public void setTomadorServico(br.adm.ipc.schemas.efrete.pef.objects.TomadorServico tomadorServico) {
        this.tomadorServico = tomadorServico;
    }


    /**
     * Gets the remetente value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return remetente
     */
    public br.adm.ipc.schemas.efrete.pef.objects.Remetente getRemetente() {
        return remetente;
    }


    /**
     * Sets the remetente value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param remetente
     */
    public void setRemetente(br.adm.ipc.schemas.efrete.pef.objects.Remetente remetente) {
        this.remetente = remetente;
    }


    /**
     * Gets the proprietarioCarga value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return proprietarioCarga
     */
    public br.adm.ipc.schemas.efrete.pef.objects.ProprietarioCarga getProprietarioCarga() {
        return proprietarioCarga;
    }


    /**
     * Sets the proprietarioCarga value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param proprietarioCarga
     */
    public void setProprietarioCarga(br.adm.ipc.schemas.efrete.pef.objects.ProprietarioCarga proprietarioCarga) {
        this.proprietarioCarga = proprietarioCarga;
    }


    /**
     * Gets the veiculos value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return veiculos
     */
    public br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Veiculo[] getVeiculos() {
        return veiculos;
    }


    /**
     * Sets the veiculos value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param veiculos
     */
    public void setVeiculos(br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Veiculo[] veiculos) {
        this.veiculos = veiculos;
    }

    public br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Veiculo getVeiculos(int i) {
        return this.veiculos[i];
    }

    public void setVeiculos(int i, br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Veiculo _value) {
        this.veiculos[i] = _value;
    }


    /**
     * Gets the codigoIdentificacaoOperacaoPrincipal value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return codigoIdentificacaoOperacaoPrincipal
     */
    public java.lang.String getCodigoIdentificacaoOperacaoPrincipal() {
        return codigoIdentificacaoOperacaoPrincipal;
    }


    /**
     * Sets the codigoIdentificacaoOperacaoPrincipal value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param codigoIdentificacaoOperacaoPrincipal
     */
    public void setCodigoIdentificacaoOperacaoPrincipal(java.lang.String codigoIdentificacaoOperacaoPrincipal) {
        this.codigoIdentificacaoOperacaoPrincipal = codigoIdentificacaoOperacaoPrincipal;
    }


    /**
     * Gets the observacoesAoTransportador value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return observacoesAoTransportador
     */
    public java.lang.String[] getObservacoesAoTransportador() {
        return observacoesAoTransportador;
    }


    /**
     * Sets the observacoesAoTransportador value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param observacoesAoTransportador
     */
    public void setObservacoesAoTransportador(java.lang.String[] observacoesAoTransportador) {
        this.observacoesAoTransportador = observacoesAoTransportador;
    }


    /**
     * Gets the observacoesAoCredenciado value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return observacoesAoCredenciado
     */
    public java.lang.String[] getObservacoesAoCredenciado() {
        return observacoesAoCredenciado;
    }


    /**
     * Sets the observacoesAoCredenciado value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param observacoesAoCredenciado
     */
    public void setObservacoesAoCredenciado(java.lang.String[] observacoesAoCredenciado) {
        this.observacoesAoCredenciado = observacoesAoCredenciado;
    }


    /**
     * Gets the entregaDocumentacao value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return entregaDocumentacao
     */
    public br.adm.ipc.schemas.efrete.pef.objects.TipoEntregaDocumentacao getEntregaDocumentacao() {
        return entregaDocumentacao;
    }


    /**
     * Sets the entregaDocumentacao value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param entregaDocumentacao
     */
    public void setEntregaDocumentacao(br.adm.ipc.schemas.efrete.pef.objects.TipoEntregaDocumentacao entregaDocumentacao) {
        this.entregaDocumentacao = entregaDocumentacao;
    }


    /**
     * Gets the quantidadeSaques value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return quantidadeSaques
     */
    public java.lang.Integer getQuantidadeSaques() {
        return quantidadeSaques;
    }


    /**
     * Sets the quantidadeSaques value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param quantidadeSaques
     */
    public void setQuantidadeSaques(java.lang.Integer quantidadeSaques) {
        this.quantidadeSaques = quantidadeSaques;
    }


    /**
     * Gets the quantidadeTransferencias value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return quantidadeTransferencias
     */
    public java.lang.Integer getQuantidadeTransferencias() {
        return quantidadeTransferencias;
    }


    /**
     * Sets the quantidadeTransferencias value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param quantidadeTransferencias
     */
    public void setQuantidadeTransferencias(java.lang.Integer quantidadeTransferencias) {
        this.quantidadeTransferencias = quantidadeTransferencias;
    }


    /**
     * Gets the valorSaques value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return valorSaques
     */
    public java.math.BigDecimal getValorSaques() {
        return valorSaques;
    }


    /**
     * Sets the valorSaques value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param valorSaques
     */
    public void setValorSaques(java.math.BigDecimal valorSaques) {
        this.valorSaques = valorSaques;
    }


    /**
     * Gets the valorTransferencias value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return valorTransferencias
     */
    public java.math.BigDecimal getValorTransferencias() {
        return valorTransferencias;
    }


    /**
     * Sets the valorTransferencias value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param valorTransferencias
     */
    public void setValorTransferencias(java.math.BigDecimal valorTransferencias) {
        this.valorTransferencias = valorTransferencias;
    }


    /**
     * Gets the codigoTipoCarga value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return codigoTipoCarga
     */
    public org.apache.axis.types.UnsignedShort getCodigoTipoCarga() {
        return codigoTipoCarga;
    }


    /**
     * Sets the codigoTipoCarga value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param codigoTipoCarga
     */
    public void setCodigoTipoCarga(org.apache.axis.types.UnsignedShort codigoTipoCarga) {
        this.codigoTipoCarga = codigoTipoCarga;
    }


    /**
     * Gets the altoDesempenho value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return altoDesempenho
     */
    public java.lang.Boolean getAltoDesempenho() {
        return altoDesempenho;
    }


    /**
     * Sets the altoDesempenho value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param altoDesempenho
     */
    public void setAltoDesempenho(java.lang.Boolean altoDesempenho) {
        this.altoDesempenho = altoDesempenho;
    }


    /**
     * Gets the destinacaoComercial value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return destinacaoComercial
     */
    public java.lang.Boolean getDestinacaoComercial() {
        return destinacaoComercial;
    }


    /**
     * Sets the destinacaoComercial value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param destinacaoComercial
     */
    public void setDestinacaoComercial(java.lang.Boolean destinacaoComercial) {
        this.destinacaoComercial = destinacaoComercial;
    }


    /**
     * Gets the freteRetorno value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return freteRetorno
     */
    public java.lang.Boolean getFreteRetorno() {
        return freteRetorno;
    }


    /**
     * Sets the freteRetorno value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param freteRetorno
     */
    public void setFreteRetorno(java.lang.Boolean freteRetorno) {
        this.freteRetorno = freteRetorno;
    }


    /**
     * Gets the cepRetorno value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return cepRetorno
     */
    public java.lang.String getCepRetorno() {
        return cepRetorno;
    }


    /**
     * Sets the cepRetorno value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param cepRetorno
     */
    public void setCepRetorno(java.lang.String cepRetorno) {
        this.cepRetorno = cepRetorno;
    }


    /**
     * Gets the distanciaRetorno value for this AdicionarOperacaoTransporteRequest.
     * 
     * @return distanciaRetorno
     */
    public java.lang.Long getDistanciaRetorno() {
        return distanciaRetorno;
    }


    /**
     * Sets the distanciaRetorno value for this AdicionarOperacaoTransporteRequest.
     * 
     * @param distanciaRetorno
     */
    public void setDistanciaRetorno(java.lang.Long distanciaRetorno) {
        this.distanciaRetorno = distanciaRetorno;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AdicionarOperacaoTransporteRequest)) return false;
        AdicionarOperacaoTransporteRequest other = (AdicionarOperacaoTransporteRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.tipoViagem==null && other.getTipoViagem()==null) || 
             (this.tipoViagem!=null &&
              this.tipoViagem.equals(other.getTipoViagem()))) &&
            ((this.emissaoGratuita==null && other.getEmissaoGratuita()==null) || 
             (this.emissaoGratuita!=null &&
              this.emissaoGratuita.equals(other.getEmissaoGratuita()))) &&
            ((this.tipoPagamento==null && other.getTipoPagamento()==null) || 
             (this.tipoPagamento!=null &&
              this.tipoPagamento.equals(other.getTipoPagamento()))) &&
            ((this.bloquearNaoEquiparado==null && other.getBloquearNaoEquiparado()==null) || 
             (this.bloquearNaoEquiparado!=null &&
              this.bloquearNaoEquiparado.equals(other.getBloquearNaoEquiparado()))) &&
            this.matrizCNPJ == other.getMatrizCNPJ() &&
            ((this.filialCNPJ==null && other.getFilialCNPJ()==null) || 
             (this.filialCNPJ!=null &&
              this.filialCNPJ.equals(other.getFilialCNPJ()))) &&
            ((this.idOperacaoCliente==null && other.getIdOperacaoCliente()==null) || 
             (this.idOperacaoCliente!=null &&
              this.idOperacaoCliente.equals(other.getIdOperacaoCliente()))) &&
            ((this.dataInicioViagem==null && other.getDataInicioViagem()==null) || 
             (this.dataInicioViagem!=null &&
              this.dataInicioViagem.equals(other.getDataInicioViagem()))) &&
            ((this.dataFimViagem==null && other.getDataFimViagem()==null) || 
             (this.dataFimViagem!=null &&
              this.dataFimViagem.equals(other.getDataFimViagem()))) &&
            this.codigoNCMNaturezaCarga == other.getCodigoNCMNaturezaCarga() &&
            ((this.pesoCarga==null && other.getPesoCarga()==null) || 
             (this.pesoCarga!=null &&
              this.pesoCarga.equals(other.getPesoCarga()))) &&
            ((this.tipoEmbalagem==null && other.getTipoEmbalagem()==null) || 
             (this.tipoEmbalagem!=null &&
              this.tipoEmbalagem.equals(other.getTipoEmbalagem()))) &&
            ((this.viagens==null && other.getViagens()==null) || 
             (this.viagens!=null &&
              java.util.Arrays.equals(this.viagens, other.getViagens()))) &&
            ((this.impostos==null && other.getImpostos()==null) || 
             (this.impostos!=null &&
              this.impostos.equals(other.getImpostos()))) &&
            ((this.pagamentos==null && other.getPagamentos()==null) || 
             (this.pagamentos!=null &&
              java.util.Arrays.equals(this.pagamentos, other.getPagamentos()))) &&
            ((this.contratado==null && other.getContratado()==null) || 
             (this.contratado!=null &&
              this.contratado.equals(other.getContratado()))) &&
            ((this.motorista==null && other.getMotorista()==null) || 
             (this.motorista!=null &&
              this.motorista.equals(other.getMotorista()))) &&
            ((this.destinatario==null && other.getDestinatario()==null) || 
             (this.destinatario!=null &&
              this.destinatario.equals(other.getDestinatario()))) &&
            ((this.contratante==null && other.getContratante()==null) || 
             (this.contratante!=null &&
              this.contratante.equals(other.getContratante()))) &&
            ((this.subcontratante==null && other.getSubcontratante()==null) || 
             (this.subcontratante!=null &&
              this.subcontratante.equals(other.getSubcontratante()))) &&
            ((this.consignatario==null && other.getConsignatario()==null) || 
             (this.consignatario!=null &&
              this.consignatario.equals(other.getConsignatario()))) &&
            ((this.tomadorServico==null && other.getTomadorServico()==null) || 
             (this.tomadorServico!=null &&
              this.tomadorServico.equals(other.getTomadorServico()))) &&
            ((this.remetente==null && other.getRemetente()==null) || 
             (this.remetente!=null &&
              this.remetente.equals(other.getRemetente()))) &&
            ((this.proprietarioCarga==null && other.getProprietarioCarga()==null) || 
             (this.proprietarioCarga!=null &&
              this.proprietarioCarga.equals(other.getProprietarioCarga()))) &&
            ((this.veiculos==null && other.getVeiculos()==null) || 
             (this.veiculos!=null &&
              java.util.Arrays.equals(this.veiculos, other.getVeiculos()))) &&
            ((this.codigoIdentificacaoOperacaoPrincipal==null && other.getCodigoIdentificacaoOperacaoPrincipal()==null) || 
             (this.codigoIdentificacaoOperacaoPrincipal!=null &&
              this.codigoIdentificacaoOperacaoPrincipal.equals(other.getCodigoIdentificacaoOperacaoPrincipal()))) &&
            ((this.observacoesAoTransportador==null && other.getObservacoesAoTransportador()==null) || 
             (this.observacoesAoTransportador!=null &&
              java.util.Arrays.equals(this.observacoesAoTransportador, other.getObservacoesAoTransportador()))) &&
            ((this.observacoesAoCredenciado==null && other.getObservacoesAoCredenciado()==null) || 
             (this.observacoesAoCredenciado!=null &&
              java.util.Arrays.equals(this.observacoesAoCredenciado, other.getObservacoesAoCredenciado()))) &&
            ((this.entregaDocumentacao==null && other.getEntregaDocumentacao()==null) || 
             (this.entregaDocumentacao!=null &&
              this.entregaDocumentacao.equals(other.getEntregaDocumentacao()))) &&
            ((this.quantidadeSaques==null && other.getQuantidadeSaques()==null) || 
             (this.quantidadeSaques!=null &&
              this.quantidadeSaques.equals(other.getQuantidadeSaques()))) &&
            ((this.quantidadeTransferencias==null && other.getQuantidadeTransferencias()==null) || 
             (this.quantidadeTransferencias!=null &&
              this.quantidadeTransferencias.equals(other.getQuantidadeTransferencias()))) &&
            ((this.valorSaques==null && other.getValorSaques()==null) || 
             (this.valorSaques!=null &&
              this.valorSaques.equals(other.getValorSaques()))) &&
            ((this.valorTransferencias==null && other.getValorTransferencias()==null) || 
             (this.valorTransferencias!=null &&
              this.valorTransferencias.equals(other.getValorTransferencias()))) &&
            ((this.codigoTipoCarga==null && other.getCodigoTipoCarga()==null) || 
             (this.codigoTipoCarga!=null &&
              this.codigoTipoCarga.equals(other.getCodigoTipoCarga()))) &&
            ((this.altoDesempenho==null && other.getAltoDesempenho()==null) || 
             (this.altoDesempenho!=null &&
              this.altoDesempenho.equals(other.getAltoDesempenho()))) &&
            ((this.destinacaoComercial==null && other.getDestinacaoComercial()==null) || 
             (this.destinacaoComercial!=null &&
              this.destinacaoComercial.equals(other.getDestinacaoComercial()))) &&
            ((this.freteRetorno==null && other.getFreteRetorno()==null) || 
             (this.freteRetorno!=null &&
              this.freteRetorno.equals(other.getFreteRetorno()))) &&
            ((this.cepRetorno==null && other.getCepRetorno()==null) || 
             (this.cepRetorno!=null &&
              this.cepRetorno.equals(other.getCepRetorno()))) &&
            ((this.distanciaRetorno==null && other.getDistanciaRetorno()==null) || 
             (this.distanciaRetorno!=null &&
              this.distanciaRetorno.equals(other.getDistanciaRetorno())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getTipoViagem() != null) {
            _hashCode += getTipoViagem().hashCode();
        }
        if (getEmissaoGratuita() != null) {
            _hashCode += getEmissaoGratuita().hashCode();
        }
        if (getTipoPagamento() != null) {
            _hashCode += getTipoPagamento().hashCode();
        }
        if (getBloquearNaoEquiparado() != null) {
            _hashCode += getBloquearNaoEquiparado().hashCode();
        }
        _hashCode += new Long(getMatrizCNPJ()).hashCode();
        if (getFilialCNPJ() != null) {
            _hashCode += getFilialCNPJ().hashCode();
        }
        if (getIdOperacaoCliente() != null) {
            _hashCode += getIdOperacaoCliente().hashCode();
        }
        if (getDataInicioViagem() != null) {
            _hashCode += getDataInicioViagem().hashCode();
        }
        if (getDataFimViagem() != null) {
            _hashCode += getDataFimViagem().hashCode();
        }
        if (getPesoCarga() != null) {
            _hashCode += getPesoCarga().hashCode();
        }
        if (getTipoEmbalagem() != null) {
            _hashCode += getTipoEmbalagem().hashCode();
        }
        if (getViagens() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getViagens());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getViagens(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getImpostos() != null) {
            _hashCode += getImpostos().hashCode();
        }
        if (getPagamentos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPagamentos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPagamentos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getContratado() != null) {
            _hashCode += getContratado().hashCode();
        }
        if (getMotorista() != null) {
            _hashCode += getMotorista().hashCode();
        }
        if (getDestinatario() != null) {
            _hashCode += getDestinatario().hashCode();
        }
        if (getContratante() != null) {
            _hashCode += getContratante().hashCode();
        }
        if (getSubcontratante() != null) {
            _hashCode += getSubcontratante().hashCode();
        }
        if (getConsignatario() != null) {
            _hashCode += getConsignatario().hashCode();
        }
        if (getTomadorServico() != null) {
            _hashCode += getTomadorServico().hashCode();
        }
        if (getRemetente() != null) {
            _hashCode += getRemetente().hashCode();
        }
        if (getProprietarioCarga() != null) {
            _hashCode += getProprietarioCarga().hashCode();
        }
        if (getVeiculos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getVeiculos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getVeiculos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCodigoIdentificacaoOperacaoPrincipal() != null) {
            _hashCode += getCodigoIdentificacaoOperacaoPrincipal().hashCode();
        }
        if (getObservacoesAoTransportador() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getObservacoesAoTransportador());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getObservacoesAoTransportador(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getObservacoesAoCredenciado() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getObservacoesAoCredenciado());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getObservacoesAoCredenciado(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getEntregaDocumentacao() != null) {
            _hashCode += getEntregaDocumentacao().hashCode();
        }
        if (getQuantidadeSaques() != null) {
            _hashCode += getQuantidadeSaques().hashCode();
        }
        if (getQuantidadeTransferencias() != null) {
            _hashCode += getQuantidadeTransferencias().hashCode();
        }
        if (getValorSaques() != null) {
            _hashCode += getValorSaques().hashCode();
        }
        if (getValorTransferencias() != null) {
            _hashCode += getValorTransferencias().hashCode();
        }
        if (getCodigoTipoCarga() != null) {
            _hashCode += getCodigoTipoCarga().hashCode();
        }
        if (getAltoDesempenho() != null) {
            _hashCode += getAltoDesempenho().hashCode();
        }
        if (getDestinacaoComercial() != null) {
            _hashCode += getDestinacaoComercial().hashCode();
        }
        if (getFreteRetorno() != null) {
            _hashCode += getFreteRetorno().hashCode();
        }
        if (getCepRetorno() != null) {
            _hashCode += getCepRetorno().hashCode();
        }
        if (getDistanciaRetorno() != null) {
            _hashCode += getDistanciaRetorno().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AdicionarOperacaoTransporteRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AdicionarOperacaoTransporteRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoViagem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TipoViagem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TipoViagem"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emissaoGratuita");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "EmissaoGratuita"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoPagamento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TipoPagamento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TipoPagamento"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bloquearNaoEquiparado");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "BloquearNaoEquiparado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("matrizCNPJ");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "MatrizCNPJ"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filialCNPJ");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "FilialCNPJ"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idOperacaoCliente");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "IdOperacaoCliente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataInicioViagem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DataInicioViagem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataFimViagem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DataFimViagem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoNCMNaturezaCarga");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CodigoNCMNaturezaCarga"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pesoCarga");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "PesoCarga"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoEmbalagem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TipoEmbalagem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TipoEmbalagem"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("viagens");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarOperacaoTransporte", "Viagens"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarOperacaoTransporte", "Viagens"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("impostos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Impostos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Impostos"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pagamentos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarOperacaoTransporte", "Pagamentos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarOperacaoTransporte", "Pagamentos"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contratado");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarOperacaoTransporte", "Contratado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarOperacaoTransporte", "Contratado"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("motorista");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarOperacaoTransporte", "Motorista"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarOperacaoTransporte", "Motorista"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("destinatario");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Destinatario"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Destinatario"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contratante");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Contratante"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Contratante"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subcontratante");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Subcontratante"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Subcontratante"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consignatario");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Consignatario"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Consignatario"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tomadorServico");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TomadorServico"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TomadorServico"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("remetente");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Remetente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Remetente"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("proprietarioCarga");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ProprietarioCarga"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ProprietarioCarga"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("veiculos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarOperacaoTransporte", "Veiculos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarOperacaoTransporte", "Veiculos"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoIdentificacaoOperacaoPrincipal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CodigoIdentificacaoOperacaoPrincipal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("observacoesAoTransportador");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObservacoesAoTransportador"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("observacoesAoCredenciado");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObservacoesAoCredenciado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("entregaDocumentacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "EntregaDocumentacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TipoEntregaDocumentacao"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantidadeSaques");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "QuantidadeSaques"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantidadeTransferencias");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "QuantidadeTransferencias"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valorSaques");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ValorSaques"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valorTransferencias");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ValorTransferencias"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoTipoCarga");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CodigoTipoCarga"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedShort"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("altoDesempenho");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AltoDesempenho"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("destinacaoComercial");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DestinacaoComercial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("freteRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "FreteRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CepRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("distanciaRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DistanciaRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
