/**
 * AbonarQuebraDeFreteRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.objects;

public class AbonarQuebraDeFreteRequest  extends br.adm.ipc.schemas.efrete.objects.Request  implements java.io.Serializable {
    private java.lang.String codigoIdentificacaoOperacao;

    private java.lang.String documentoViagem;

    private boolean aplicarEmTodasAsNotasFiscais;

    private br.adm.ipc.schemas.efrete.pef.objects.AbonarQuebraDeFrete.NotaFiscal[] notasFiscais;

    private br.adm.ipc.schemas.efrete.pef.objects.AbonarQuebraDeFrete.AbonoDeQuebra abonoDeQuebra;

    public AbonarQuebraDeFreteRequest() {
    }

    public AbonarQuebraDeFreteRequest(
           java.lang.String token,
           java.lang.String integrador,
           int versao,
           java.lang.String codigoIdentificacaoOperacao,
           java.lang.String documentoViagem,
           boolean aplicarEmTodasAsNotasFiscais,
           br.adm.ipc.schemas.efrete.pef.objects.AbonarQuebraDeFrete.NotaFiscal[] notasFiscais,
           br.adm.ipc.schemas.efrete.pef.objects.AbonarQuebraDeFrete.AbonoDeQuebra abonoDeQuebra) {
        super(
            token,
            integrador,
            versao);
        this.codigoIdentificacaoOperacao = codigoIdentificacaoOperacao;
        this.documentoViagem = documentoViagem;
        this.aplicarEmTodasAsNotasFiscais = aplicarEmTodasAsNotasFiscais;
        this.notasFiscais = notasFiscais;
        this.abonoDeQuebra = abonoDeQuebra;
    }


    /**
     * Gets the codigoIdentificacaoOperacao value for this AbonarQuebraDeFreteRequest.
     * 
     * @return codigoIdentificacaoOperacao
     */
    public java.lang.String getCodigoIdentificacaoOperacao() {
        return codigoIdentificacaoOperacao;
    }


    /**
     * Sets the codigoIdentificacaoOperacao value for this AbonarQuebraDeFreteRequest.
     * 
     * @param codigoIdentificacaoOperacao
     */
    public void setCodigoIdentificacaoOperacao(java.lang.String codigoIdentificacaoOperacao) {
        this.codigoIdentificacaoOperacao = codigoIdentificacaoOperacao;
    }


    /**
     * Gets the documentoViagem value for this AbonarQuebraDeFreteRequest.
     * 
     * @return documentoViagem
     */
    public java.lang.String getDocumentoViagem() {
        return documentoViagem;
    }


    /**
     * Sets the documentoViagem value for this AbonarQuebraDeFreteRequest.
     * 
     * @param documentoViagem
     */
    public void setDocumentoViagem(java.lang.String documentoViagem) {
        this.documentoViagem = documentoViagem;
    }


    /**
     * Gets the aplicarEmTodasAsNotasFiscais value for this AbonarQuebraDeFreteRequest.
     * 
     * @return aplicarEmTodasAsNotasFiscais
     */
    public boolean isAplicarEmTodasAsNotasFiscais() {
        return aplicarEmTodasAsNotasFiscais;
    }


    /**
     * Sets the aplicarEmTodasAsNotasFiscais value for this AbonarQuebraDeFreteRequest.
     * 
     * @param aplicarEmTodasAsNotasFiscais
     */
    public void setAplicarEmTodasAsNotasFiscais(boolean aplicarEmTodasAsNotasFiscais) {
        this.aplicarEmTodasAsNotasFiscais = aplicarEmTodasAsNotasFiscais;
    }


    /**
     * Gets the notasFiscais value for this AbonarQuebraDeFreteRequest.
     * 
     * @return notasFiscais
     */
    public br.adm.ipc.schemas.efrete.pef.objects.AbonarQuebraDeFrete.NotaFiscal[] getNotasFiscais() {
        return notasFiscais;
    }


    /**
     * Sets the notasFiscais value for this AbonarQuebraDeFreteRequest.
     * 
     * @param notasFiscais
     */
    public void setNotasFiscais(br.adm.ipc.schemas.efrete.pef.objects.AbonarQuebraDeFrete.NotaFiscal[] notasFiscais) {
        this.notasFiscais = notasFiscais;
    }


    /**
     * Gets the abonoDeQuebra value for this AbonarQuebraDeFreteRequest.
     * 
     * @return abonoDeQuebra
     */
    public br.adm.ipc.schemas.efrete.pef.objects.AbonarQuebraDeFrete.AbonoDeQuebra getAbonoDeQuebra() {
        return abonoDeQuebra;
    }


    /**
     * Sets the abonoDeQuebra value for this AbonarQuebraDeFreteRequest.
     * 
     * @param abonoDeQuebra
     */
    public void setAbonoDeQuebra(br.adm.ipc.schemas.efrete.pef.objects.AbonarQuebraDeFrete.AbonoDeQuebra abonoDeQuebra) {
        this.abonoDeQuebra = abonoDeQuebra;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AbonarQuebraDeFreteRequest)) return false;
        AbonarQuebraDeFreteRequest other = (AbonarQuebraDeFreteRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.codigoIdentificacaoOperacao==null && other.getCodigoIdentificacaoOperacao()==null) || 
             (this.codigoIdentificacaoOperacao!=null &&
              this.codigoIdentificacaoOperacao.equals(other.getCodigoIdentificacaoOperacao()))) &&
            ((this.documentoViagem==null && other.getDocumentoViagem()==null) || 
             (this.documentoViagem!=null &&
              this.documentoViagem.equals(other.getDocumentoViagem()))) &&
            this.aplicarEmTodasAsNotasFiscais == other.isAplicarEmTodasAsNotasFiscais() &&
            ((this.notasFiscais==null && other.getNotasFiscais()==null) || 
             (this.notasFiscais!=null &&
              java.util.Arrays.equals(this.notasFiscais, other.getNotasFiscais()))) &&
            ((this.abonoDeQuebra==null && other.getAbonoDeQuebra()==null) || 
             (this.abonoDeQuebra!=null &&
              this.abonoDeQuebra.equals(other.getAbonoDeQuebra())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCodigoIdentificacaoOperacao() != null) {
            _hashCode += getCodigoIdentificacaoOperacao().hashCode();
        }
        if (getDocumentoViagem() != null) {
            _hashCode += getDocumentoViagem().hashCode();
        }
        _hashCode += (isAplicarEmTodasAsNotasFiscais() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getNotasFiscais() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getNotasFiscais());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getNotasFiscais(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getAbonoDeQuebra() != null) {
            _hashCode += getAbonoDeQuebra().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AbonarQuebraDeFreteRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AbonarQuebraDeFreteRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoIdentificacaoOperacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CodigoIdentificacaoOperacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("documentoViagem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DocumentoViagem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("aplicarEmTodasAsNotasFiscais");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AplicarEmTodasAsNotasFiscais"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("notasFiscais");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects/AbonarQuebraDeFrete", "NotasFiscais"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects/AbonarQuebraDeFrete", "ArrayOfNotaFiscal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abonoDeQuebra");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AbonoDeQuebra"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects/AbonarQuebraDeFrete", "AbonoDeQuebra"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
