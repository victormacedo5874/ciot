/**
 * AdicionarViagemResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.objects;

public class AdicionarViagemResponse  extends br.adm.ipc.schemas.efrete.objects.Response  implements java.io.Serializable {
    private java.lang.String codigoIdentificacaoOperacao;

    private int quantidadeViagens;

    private java.lang.String[] documentoViagem;

    private int quantidadePagamentos;

    private java.lang.String[] documentoPagamento;

    public AdicionarViagemResponse() {
    }

    public AdicionarViagemResponse(
           int versao,
           boolean sucesso,
           br.adm.ipc.schemas.efrete.objects.Excecao excecao,
           java.lang.Long protocoloServico,
           java.lang.String codigoIdentificacaoOperacao,
           int quantidadeViagens,
           java.lang.String[] documentoViagem,
           int quantidadePagamentos,
           java.lang.String[] documentoPagamento) {
        super(
            versao,
            sucesso,
            excecao,
            protocoloServico);
        this.codigoIdentificacaoOperacao = codigoIdentificacaoOperacao;
        this.quantidadeViagens = quantidadeViagens;
        this.documentoViagem = documentoViagem;
        this.quantidadePagamentos = quantidadePagamentos;
        this.documentoPagamento = documentoPagamento;
    }


    /**
     * Gets the codigoIdentificacaoOperacao value for this AdicionarViagemResponse.
     * 
     * @return codigoIdentificacaoOperacao
     */
    public java.lang.String getCodigoIdentificacaoOperacao() {
        return codigoIdentificacaoOperacao;
    }


    /**
     * Sets the codigoIdentificacaoOperacao value for this AdicionarViagemResponse.
     * 
     * @param codigoIdentificacaoOperacao
     */
    public void setCodigoIdentificacaoOperacao(java.lang.String codigoIdentificacaoOperacao) {
        this.codigoIdentificacaoOperacao = codigoIdentificacaoOperacao;
    }


    /**
     * Gets the quantidadeViagens value for this AdicionarViagemResponse.
     * 
     * @return quantidadeViagens
     */
    public int getQuantidadeViagens() {
        return quantidadeViagens;
    }


    /**
     * Sets the quantidadeViagens value for this AdicionarViagemResponse.
     * 
     * @param quantidadeViagens
     */
    public void setQuantidadeViagens(int quantidadeViagens) {
        this.quantidadeViagens = quantidadeViagens;
    }


    /**
     * Gets the documentoViagem value for this AdicionarViagemResponse.
     * 
     * @return documentoViagem
     */
    public java.lang.String[] getDocumentoViagem() {
        return documentoViagem;
    }


    /**
     * Sets the documentoViagem value for this AdicionarViagemResponse.
     * 
     * @param documentoViagem
     */
    public void setDocumentoViagem(java.lang.String[] documentoViagem) {
        this.documentoViagem = documentoViagem;
    }


    /**
     * Gets the quantidadePagamentos value for this AdicionarViagemResponse.
     * 
     * @return quantidadePagamentos
     */
    public int getQuantidadePagamentos() {
        return quantidadePagamentos;
    }


    /**
     * Sets the quantidadePagamentos value for this AdicionarViagemResponse.
     * 
     * @param quantidadePagamentos
     */
    public void setQuantidadePagamentos(int quantidadePagamentos) {
        this.quantidadePagamentos = quantidadePagamentos;
    }


    /**
     * Gets the documentoPagamento value for this AdicionarViagemResponse.
     * 
     * @return documentoPagamento
     */
    public java.lang.String[] getDocumentoPagamento() {
        return documentoPagamento;
    }


    /**
     * Sets the documentoPagamento value for this AdicionarViagemResponse.
     * 
     * @param documentoPagamento
     */
    public void setDocumentoPagamento(java.lang.String[] documentoPagamento) {
        this.documentoPagamento = documentoPagamento;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AdicionarViagemResponse)) return false;
        AdicionarViagemResponse other = (AdicionarViagemResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.codigoIdentificacaoOperacao==null && other.getCodigoIdentificacaoOperacao()==null) || 
             (this.codigoIdentificacaoOperacao!=null &&
              this.codigoIdentificacaoOperacao.equals(other.getCodigoIdentificacaoOperacao()))) &&
            this.quantidadeViagens == other.getQuantidadeViagens() &&
            ((this.documentoViagem==null && other.getDocumentoViagem()==null) || 
             (this.documentoViagem!=null &&
              java.util.Arrays.equals(this.documentoViagem, other.getDocumentoViagem()))) &&
            this.quantidadePagamentos == other.getQuantidadePagamentos() &&
            ((this.documentoPagamento==null && other.getDocumentoPagamento()==null) || 
             (this.documentoPagamento!=null &&
              java.util.Arrays.equals(this.documentoPagamento, other.getDocumentoPagamento())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCodigoIdentificacaoOperacao() != null) {
            _hashCode += getCodigoIdentificacaoOperacao().hashCode();
        }
        _hashCode += getQuantidadeViagens();
        if (getDocumentoViagem() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDocumentoViagem());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDocumentoViagem(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += getQuantidadePagamentos();
        if (getDocumentoPagamento() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDocumentoPagamento());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDocumentoPagamento(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AdicionarViagemResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AdicionarViagemResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoIdentificacaoOperacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CodigoIdentificacaoOperacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantidadeViagens");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "QuantidadeViagens"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("documentoViagem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DocumentoViagem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantidadePagamentos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "QuantidadePagamentos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("documentoPagamento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DocumentoPagamento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "string"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
