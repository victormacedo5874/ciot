/**
 * Impostos.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.objects;

public class Impostos  implements java.io.Serializable {
    private java.math.BigDecimal IRRF;

    private java.math.BigDecimal sestSenat;

    private java.math.BigDecimal INSS;

    private java.math.BigDecimal ISSQN;

    private java.math.BigDecimal outrosImpostos;

    private java.lang.String descricaoOutrosImpostos;

    public Impostos() {
    }

    public Impostos(
           java.math.BigDecimal IRRF,
           java.math.BigDecimal sestSenat,
           java.math.BigDecimal INSS,
           java.math.BigDecimal ISSQN,
           java.math.BigDecimal outrosImpostos,
           java.lang.String descricaoOutrosImpostos) {
           this.IRRF = IRRF;
           this.sestSenat = sestSenat;
           this.INSS = INSS;
           this.ISSQN = ISSQN;
           this.outrosImpostos = outrosImpostos;
           this.descricaoOutrosImpostos = descricaoOutrosImpostos;
    }


    /**
     * Gets the IRRF value for this Impostos.
     * 
     * @return IRRF
     */
    public java.math.BigDecimal getIRRF() {
        return IRRF;
    }


    /**
     * Sets the IRRF value for this Impostos.
     * 
     * @param IRRF
     */
    public void setIRRF(java.math.BigDecimal IRRF) {
        this.IRRF = IRRF;
    }


    /**
     * Gets the sestSenat value for this Impostos.
     * 
     * @return sestSenat
     */
    public java.math.BigDecimal getSestSenat() {
        return sestSenat;
    }


    /**
     * Sets the sestSenat value for this Impostos.
     * 
     * @param sestSenat
     */
    public void setSestSenat(java.math.BigDecimal sestSenat) {
        this.sestSenat = sestSenat;
    }


    /**
     * Gets the INSS value for this Impostos.
     * 
     * @return INSS
     */
    public java.math.BigDecimal getINSS() {
        return INSS;
    }


    /**
     * Sets the INSS value for this Impostos.
     * 
     * @param INSS
     */
    public void setINSS(java.math.BigDecimal INSS) {
        this.INSS = INSS;
    }


    /**
     * Gets the ISSQN value for this Impostos.
     * 
     * @return ISSQN
     */
    public java.math.BigDecimal getISSQN() {
        return ISSQN;
    }


    /**
     * Sets the ISSQN value for this Impostos.
     * 
     * @param ISSQN
     */
    public void setISSQN(java.math.BigDecimal ISSQN) {
        this.ISSQN = ISSQN;
    }


    /**
     * Gets the outrosImpostos value for this Impostos.
     * 
     * @return outrosImpostos
     */
    public java.math.BigDecimal getOutrosImpostos() {
        return outrosImpostos;
    }


    /**
     * Sets the outrosImpostos value for this Impostos.
     * 
     * @param outrosImpostos
     */
    public void setOutrosImpostos(java.math.BigDecimal outrosImpostos) {
        this.outrosImpostos = outrosImpostos;
    }


    /**
     * Gets the descricaoOutrosImpostos value for this Impostos.
     * 
     * @return descricaoOutrosImpostos
     */
    public java.lang.String getDescricaoOutrosImpostos() {
        return descricaoOutrosImpostos;
    }


    /**
     * Sets the descricaoOutrosImpostos value for this Impostos.
     * 
     * @param descricaoOutrosImpostos
     */
    public void setDescricaoOutrosImpostos(java.lang.String descricaoOutrosImpostos) {
        this.descricaoOutrosImpostos = descricaoOutrosImpostos;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Impostos)) return false;
        Impostos other = (Impostos) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.IRRF==null && other.getIRRF()==null) || 
             (this.IRRF!=null &&
              this.IRRF.equals(other.getIRRF()))) &&
            ((this.sestSenat==null && other.getSestSenat()==null) || 
             (this.sestSenat!=null &&
              this.sestSenat.equals(other.getSestSenat()))) &&
            ((this.INSS==null && other.getINSS()==null) || 
             (this.INSS!=null &&
              this.INSS.equals(other.getINSS()))) &&
            ((this.ISSQN==null && other.getISSQN()==null) || 
             (this.ISSQN!=null &&
              this.ISSQN.equals(other.getISSQN()))) &&
            ((this.outrosImpostos==null && other.getOutrosImpostos()==null) || 
             (this.outrosImpostos!=null &&
              this.outrosImpostos.equals(other.getOutrosImpostos()))) &&
            ((this.descricaoOutrosImpostos==null && other.getDescricaoOutrosImpostos()==null) || 
             (this.descricaoOutrosImpostos!=null &&
              this.descricaoOutrosImpostos.equals(other.getDescricaoOutrosImpostos())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIRRF() != null) {
            _hashCode += getIRRF().hashCode();
        }
        if (getSestSenat() != null) {
            _hashCode += getSestSenat().hashCode();
        }
        if (getINSS() != null) {
            _hashCode += getINSS().hashCode();
        }
        if (getISSQN() != null) {
            _hashCode += getISSQN().hashCode();
        }
        if (getOutrosImpostos() != null) {
            _hashCode += getOutrosImpostos().hashCode();
        }
        if (getDescricaoOutrosImpostos() != null) {
            _hashCode += getDescricaoOutrosImpostos().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Impostos.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Impostos"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IRRF");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "IRRF"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sestSenat");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "SestSenat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("INSS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "INSS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ISSQN");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ISSQN"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outrosImpostos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "OutrosImpostos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descricaoOutrosImpostos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DescricaoOutrosImpostos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
