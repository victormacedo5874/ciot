/**
 * RetificarOperacaoTransporteRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.objects;

public class RetificarOperacaoTransporteRequest  extends br.adm.ipc.schemas.efrete.objects.Request  implements java.io.Serializable {
    private java.lang.String codigoIdentificacaoOperacao;

    private java.util.Calendar dataInicioViagem;

    private java.util.Calendar dataFimViagem;

    private java.lang.Integer codigoNCMNaturezaCarga;

    private java.math.BigDecimal pesoCarga;

    private java.lang.Integer codigoMunicipioOrigem;

    private java.lang.Integer codigoMunicipioDestino;

    private br.adm.ipc.schemas.efrete.pef.RetificarOperacaoTransporte.Veiculo[] veiculos;

    private java.lang.Integer quantidadeSaques;

    private java.lang.Integer quantidadeTransferencias;

    private java.math.BigDecimal valorSaques;

    private java.math.BigDecimal valorTransferencias;

    private org.apache.axis.types.UnsignedShort codigoTipoCarga;

    private java.lang.String cepOrigem;

    private java.lang.String cepDestino;

    private org.apache.axis.types.UnsignedInt distanciaPercorrida;

    public RetificarOperacaoTransporteRequest() {
    }

    public RetificarOperacaoTransporteRequest(
           java.lang.String token,
           java.lang.String integrador,
           int versao,
           java.lang.String codigoIdentificacaoOperacao,
           java.util.Calendar dataInicioViagem,
           java.util.Calendar dataFimViagem,
           java.lang.Integer codigoNCMNaturezaCarga,
           java.math.BigDecimal pesoCarga,
           java.lang.Integer codigoMunicipioOrigem,
           java.lang.Integer codigoMunicipioDestino,
           br.adm.ipc.schemas.efrete.pef.RetificarOperacaoTransporte.Veiculo[] veiculos,
           java.lang.Integer quantidadeSaques,
           java.lang.Integer quantidadeTransferencias,
           java.math.BigDecimal valorSaques,
           java.math.BigDecimal valorTransferencias,
           org.apache.axis.types.UnsignedShort codigoTipoCarga,
           java.lang.String cepOrigem,
           java.lang.String cepDestino,
           org.apache.axis.types.UnsignedInt distanciaPercorrida) {
        super(
            token,
            integrador,
            versao);
        this.codigoIdentificacaoOperacao = codigoIdentificacaoOperacao;
        this.dataInicioViagem = dataInicioViagem;
        this.dataFimViagem = dataFimViagem;
        this.codigoNCMNaturezaCarga = codigoNCMNaturezaCarga;
        this.pesoCarga = pesoCarga;
        this.codigoMunicipioOrigem = codigoMunicipioOrigem;
        this.codigoMunicipioDestino = codigoMunicipioDestino;
        this.veiculos = veiculos;
        this.quantidadeSaques = quantidadeSaques;
        this.quantidadeTransferencias = quantidadeTransferencias;
        this.valorSaques = valorSaques;
        this.valorTransferencias = valorTransferencias;
        this.codigoTipoCarga = codigoTipoCarga;
        this.cepOrigem = cepOrigem;
        this.cepDestino = cepDestino;
        this.distanciaPercorrida = distanciaPercorrida;
    }


    /**
     * Gets the codigoIdentificacaoOperacao value for this RetificarOperacaoTransporteRequest.
     * 
     * @return codigoIdentificacaoOperacao
     */
    public java.lang.String getCodigoIdentificacaoOperacao() {
        return codigoIdentificacaoOperacao;
    }


    /**
     * Sets the codigoIdentificacaoOperacao value for this RetificarOperacaoTransporteRequest.
     * 
     * @param codigoIdentificacaoOperacao
     */
    public void setCodigoIdentificacaoOperacao(java.lang.String codigoIdentificacaoOperacao) {
        this.codigoIdentificacaoOperacao = codigoIdentificacaoOperacao;
    }


    /**
     * Gets the dataInicioViagem value for this RetificarOperacaoTransporteRequest.
     * 
     * @return dataInicioViagem
     */
    public java.util.Calendar getDataInicioViagem() {
        return dataInicioViagem;
    }


    /**
     * Sets the dataInicioViagem value for this RetificarOperacaoTransporteRequest.
     * 
     * @param dataInicioViagem
     */
    public void setDataInicioViagem(java.util.Calendar dataInicioViagem) {
        this.dataInicioViagem = dataInicioViagem;
    }


    /**
     * Gets the dataFimViagem value for this RetificarOperacaoTransporteRequest.
     * 
     * @return dataFimViagem
     */
    public java.util.Calendar getDataFimViagem() {
        return dataFimViagem;
    }


    /**
     * Sets the dataFimViagem value for this RetificarOperacaoTransporteRequest.
     * 
     * @param dataFimViagem
     */
    public void setDataFimViagem(java.util.Calendar dataFimViagem) {
        this.dataFimViagem = dataFimViagem;
    }


    /**
     * Gets the codigoNCMNaturezaCarga value for this RetificarOperacaoTransporteRequest.
     * 
     * @return codigoNCMNaturezaCarga
     */
    public java.lang.Integer getCodigoNCMNaturezaCarga() {
        return codigoNCMNaturezaCarga;
    }


    /**
     * Sets the codigoNCMNaturezaCarga value for this RetificarOperacaoTransporteRequest.
     * 
     * @param codigoNCMNaturezaCarga
     */
    public void setCodigoNCMNaturezaCarga(java.lang.Integer codigoNCMNaturezaCarga) {
        this.codigoNCMNaturezaCarga = codigoNCMNaturezaCarga;
    }


    /**
     * Gets the pesoCarga value for this RetificarOperacaoTransporteRequest.
     * 
     * @return pesoCarga
     */
    public java.math.BigDecimal getPesoCarga() {
        return pesoCarga;
    }


    /**
     * Sets the pesoCarga value for this RetificarOperacaoTransporteRequest.
     * 
     * @param pesoCarga
     */
    public void setPesoCarga(java.math.BigDecimal pesoCarga) {
        this.pesoCarga = pesoCarga;
    }


    /**
     * Gets the codigoMunicipioOrigem value for this RetificarOperacaoTransporteRequest.
     * 
     * @return codigoMunicipioOrigem
     */
    public java.lang.Integer getCodigoMunicipioOrigem() {
        return codigoMunicipioOrigem;
    }


    /**
     * Sets the codigoMunicipioOrigem value for this RetificarOperacaoTransporteRequest.
     * 
     * @param codigoMunicipioOrigem
     */
    public void setCodigoMunicipioOrigem(java.lang.Integer codigoMunicipioOrigem) {
        this.codigoMunicipioOrigem = codigoMunicipioOrigem;
    }


    /**
     * Gets the codigoMunicipioDestino value for this RetificarOperacaoTransporteRequest.
     * 
     * @return codigoMunicipioDestino
     */
    public java.lang.Integer getCodigoMunicipioDestino() {
        return codigoMunicipioDestino;
    }


    /**
     * Sets the codigoMunicipioDestino value for this RetificarOperacaoTransporteRequest.
     * 
     * @param codigoMunicipioDestino
     */
    public void setCodigoMunicipioDestino(java.lang.Integer codigoMunicipioDestino) {
        this.codigoMunicipioDestino = codigoMunicipioDestino;
    }


    /**
     * Gets the veiculos value for this RetificarOperacaoTransporteRequest.
     * 
     * @return veiculos
     */
    public br.adm.ipc.schemas.efrete.pef.RetificarOperacaoTransporte.Veiculo[] getVeiculos() {
        return veiculos;
    }


    /**
     * Sets the veiculos value for this RetificarOperacaoTransporteRequest.
     * 
     * @param veiculos
     */
    public void setVeiculos(br.adm.ipc.schemas.efrete.pef.RetificarOperacaoTransporte.Veiculo[] veiculos) {
        this.veiculos = veiculos;
    }


    /**
     * Gets the quantidadeSaques value for this RetificarOperacaoTransporteRequest.
     * 
     * @return quantidadeSaques
     */
    public java.lang.Integer getQuantidadeSaques() {
        return quantidadeSaques;
    }


    /**
     * Sets the quantidadeSaques value for this RetificarOperacaoTransporteRequest.
     * 
     * @param quantidadeSaques
     */
    public void setQuantidadeSaques(java.lang.Integer quantidadeSaques) {
        this.quantidadeSaques = quantidadeSaques;
    }


    /**
     * Gets the quantidadeTransferencias value for this RetificarOperacaoTransporteRequest.
     * 
     * @return quantidadeTransferencias
     */
    public java.lang.Integer getQuantidadeTransferencias() {
        return quantidadeTransferencias;
    }


    /**
     * Sets the quantidadeTransferencias value for this RetificarOperacaoTransporteRequest.
     * 
     * @param quantidadeTransferencias
     */
    public void setQuantidadeTransferencias(java.lang.Integer quantidadeTransferencias) {
        this.quantidadeTransferencias = quantidadeTransferencias;
    }


    /**
     * Gets the valorSaques value for this RetificarOperacaoTransporteRequest.
     * 
     * @return valorSaques
     */
    public java.math.BigDecimal getValorSaques() {
        return valorSaques;
    }


    /**
     * Sets the valorSaques value for this RetificarOperacaoTransporteRequest.
     * 
     * @param valorSaques
     */
    public void setValorSaques(java.math.BigDecimal valorSaques) {
        this.valorSaques = valorSaques;
    }


    /**
     * Gets the valorTransferencias value for this RetificarOperacaoTransporteRequest.
     * 
     * @return valorTransferencias
     */
    public java.math.BigDecimal getValorTransferencias() {
        return valorTransferencias;
    }


    /**
     * Sets the valorTransferencias value for this RetificarOperacaoTransporteRequest.
     * 
     * @param valorTransferencias
     */
    public void setValorTransferencias(java.math.BigDecimal valorTransferencias) {
        this.valorTransferencias = valorTransferencias;
    }


    /**
     * Gets the codigoTipoCarga value for this RetificarOperacaoTransporteRequest.
     * 
     * @return codigoTipoCarga
     */
    public org.apache.axis.types.UnsignedShort getCodigoTipoCarga() {
        return codigoTipoCarga;
    }


    /**
     * Sets the codigoTipoCarga value for this RetificarOperacaoTransporteRequest.
     * 
     * @param codigoTipoCarga
     */
    public void setCodigoTipoCarga(org.apache.axis.types.UnsignedShort codigoTipoCarga) {
        this.codigoTipoCarga = codigoTipoCarga;
    }


    /**
     * Gets the cepOrigem value for this RetificarOperacaoTransporteRequest.
     * 
     * @return cepOrigem
     */
    public java.lang.String getCepOrigem() {
        return cepOrigem;
    }


    /**
     * Sets the cepOrigem value for this RetificarOperacaoTransporteRequest.
     * 
     * @param cepOrigem
     */
    public void setCepOrigem(java.lang.String cepOrigem) {
        this.cepOrigem = cepOrigem;
    }


    /**
     * Gets the cepDestino value for this RetificarOperacaoTransporteRequest.
     * 
     * @return cepDestino
     */
    public java.lang.String getCepDestino() {
        return cepDestino;
    }


    /**
     * Sets the cepDestino value for this RetificarOperacaoTransporteRequest.
     * 
     * @param cepDestino
     */
    public void setCepDestino(java.lang.String cepDestino) {
        this.cepDestino = cepDestino;
    }


    /**
     * Gets the distanciaPercorrida value for this RetificarOperacaoTransporteRequest.
     * 
     * @return distanciaPercorrida
     */
    public org.apache.axis.types.UnsignedInt getDistanciaPercorrida() {
        return distanciaPercorrida;
    }


    /**
     * Sets the distanciaPercorrida value for this RetificarOperacaoTransporteRequest.
     * 
     * @param distanciaPercorrida
     */
    public void setDistanciaPercorrida(org.apache.axis.types.UnsignedInt distanciaPercorrida) {
        this.distanciaPercorrida = distanciaPercorrida;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RetificarOperacaoTransporteRequest)) return false;
        RetificarOperacaoTransporteRequest other = (RetificarOperacaoTransporteRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.codigoIdentificacaoOperacao==null && other.getCodigoIdentificacaoOperacao()==null) || 
             (this.codigoIdentificacaoOperacao!=null &&
              this.codigoIdentificacaoOperacao.equals(other.getCodigoIdentificacaoOperacao()))) &&
            ((this.dataInicioViagem==null && other.getDataInicioViagem()==null) || 
             (this.dataInicioViagem!=null &&
              this.dataInicioViagem.equals(other.getDataInicioViagem()))) &&
            ((this.dataFimViagem==null && other.getDataFimViagem()==null) || 
             (this.dataFimViagem!=null &&
              this.dataFimViagem.equals(other.getDataFimViagem()))) &&
            ((this.codigoNCMNaturezaCarga==null && other.getCodigoNCMNaturezaCarga()==null) || 
             (this.codigoNCMNaturezaCarga!=null &&
              this.codigoNCMNaturezaCarga.equals(other.getCodigoNCMNaturezaCarga()))) &&
            ((this.pesoCarga==null && other.getPesoCarga()==null) || 
             (this.pesoCarga!=null &&
              this.pesoCarga.equals(other.getPesoCarga()))) &&
            ((this.codigoMunicipioOrigem==null && other.getCodigoMunicipioOrigem()==null) || 
             (this.codigoMunicipioOrigem!=null &&
              this.codigoMunicipioOrigem.equals(other.getCodigoMunicipioOrigem()))) &&
            ((this.codigoMunicipioDestino==null && other.getCodigoMunicipioDestino()==null) || 
             (this.codigoMunicipioDestino!=null &&
              this.codigoMunicipioDestino.equals(other.getCodigoMunicipioDestino()))) &&
            ((this.veiculos==null && other.getVeiculos()==null) || 
             (this.veiculos!=null &&
              java.util.Arrays.equals(this.veiculos, other.getVeiculos()))) &&
            ((this.quantidadeSaques==null && other.getQuantidadeSaques()==null) || 
             (this.quantidadeSaques!=null &&
              this.quantidadeSaques.equals(other.getQuantidadeSaques()))) &&
            ((this.quantidadeTransferencias==null && other.getQuantidadeTransferencias()==null) || 
             (this.quantidadeTransferencias!=null &&
              this.quantidadeTransferencias.equals(other.getQuantidadeTransferencias()))) &&
            ((this.valorSaques==null && other.getValorSaques()==null) || 
             (this.valorSaques!=null &&
              this.valorSaques.equals(other.getValorSaques()))) &&
            ((this.valorTransferencias==null && other.getValorTransferencias()==null) || 
             (this.valorTransferencias!=null &&
              this.valorTransferencias.equals(other.getValorTransferencias()))) &&
            ((this.codigoTipoCarga==null && other.getCodigoTipoCarga()==null) || 
             (this.codigoTipoCarga!=null &&
              this.codigoTipoCarga.equals(other.getCodigoTipoCarga()))) &&
            ((this.cepOrigem==null && other.getCepOrigem()==null) || 
             (this.cepOrigem!=null &&
              this.cepOrigem.equals(other.getCepOrigem()))) &&
            ((this.cepDestino==null && other.getCepDestino()==null) || 
             (this.cepDestino!=null &&
              this.cepDestino.equals(other.getCepDestino()))) &&
            ((this.distanciaPercorrida==null && other.getDistanciaPercorrida()==null) || 
             (this.distanciaPercorrida!=null &&
              this.distanciaPercorrida.equals(other.getDistanciaPercorrida())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCodigoIdentificacaoOperacao() != null) {
            _hashCode += getCodigoIdentificacaoOperacao().hashCode();
        }
        if (getDataInicioViagem() != null) {
            _hashCode += getDataInicioViagem().hashCode();
        }
        if (getDataFimViagem() != null) {
            _hashCode += getDataFimViagem().hashCode();
        }
        if (getCodigoNCMNaturezaCarga() != null) {
            _hashCode += getCodigoNCMNaturezaCarga().hashCode();
        }
        if (getPesoCarga() != null) {
            _hashCode += getPesoCarga().hashCode();
        }
        if (getCodigoMunicipioOrigem() != null) {
            _hashCode += getCodigoMunicipioOrigem().hashCode();
        }
        if (getCodigoMunicipioDestino() != null) {
            _hashCode += getCodigoMunicipioDestino().hashCode();
        }
        if (getVeiculos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getVeiculos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getVeiculos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getQuantidadeSaques() != null) {
            _hashCode += getQuantidadeSaques().hashCode();
        }
        if (getQuantidadeTransferencias() != null) {
            _hashCode += getQuantidadeTransferencias().hashCode();
        }
        if (getValorSaques() != null) {
            _hashCode += getValorSaques().hashCode();
        }
        if (getValorTransferencias() != null) {
            _hashCode += getValorTransferencias().hashCode();
        }
        if (getCodigoTipoCarga() != null) {
            _hashCode += getCodigoTipoCarga().hashCode();
        }
        if (getCepOrigem() != null) {
            _hashCode += getCepOrigem().hashCode();
        }
        if (getCepDestino() != null) {
            _hashCode += getCepDestino().hashCode();
        }
        if (getDistanciaPercorrida() != null) {
            _hashCode += getDistanciaPercorrida().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RetificarOperacaoTransporteRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "RetificarOperacaoTransporteRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoIdentificacaoOperacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CodigoIdentificacaoOperacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataInicioViagem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DataInicioViagem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataFimViagem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DataFimViagem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoNCMNaturezaCarga");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CodigoNCMNaturezaCarga"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pesoCarga");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "PesoCarga"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoMunicipioOrigem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CodigoMunicipioOrigem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoMunicipioDestino");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CodigoMunicipioDestino"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("veiculos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/RetificarOperacaoTransporte", "Veiculos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/RetificarOperacaoTransporte", "ArrayOfVeiculo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantidadeSaques");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "QuantidadeSaques"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantidadeTransferencias");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "QuantidadeTransferencias"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valorSaques");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ValorSaques"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valorTransferencias");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ValorTransferencias"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoTipoCarga");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CodigoTipoCarga"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedShort"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepOrigem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CepOrigem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepDestino");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CepDestino"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("distanciaPercorrida");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DistanciaPercorrida"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
