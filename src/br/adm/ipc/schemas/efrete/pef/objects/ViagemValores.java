/**
 * ViagemValores.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.objects;

public class ViagemValores  implements java.io.Serializable {
    private java.math.BigDecimal totalOperacao;

    private java.math.BigDecimal totalViagem;

    private java.math.BigDecimal totalDeAdiantamento;

    private java.math.BigDecimal totalDeQuitacao;

    private java.math.BigDecimal combustivel;

    private java.math.BigDecimal pedagio;

    private java.math.BigDecimal outrosCreditos;

    private java.lang.String justificativaOutrosCreditos;

    private java.math.BigDecimal seguro;

    private java.math.BigDecimal outrosDebitos;

    private java.lang.String justificativaOutrosDebitos;

    public ViagemValores() {
    }

    public ViagemValores(
           java.math.BigDecimal totalOperacao,
           java.math.BigDecimal totalViagem,
           java.math.BigDecimal totalDeAdiantamento,
           java.math.BigDecimal totalDeQuitacao,
           java.math.BigDecimal combustivel,
           java.math.BigDecimal pedagio,
           java.math.BigDecimal outrosCreditos,
           java.lang.String justificativaOutrosCreditos,
           java.math.BigDecimal seguro,
           java.math.BigDecimal outrosDebitos,
           java.lang.String justificativaOutrosDebitos) {
           this.totalOperacao = totalOperacao;
           this.totalViagem = totalViagem;
           this.totalDeAdiantamento = totalDeAdiantamento;
           this.totalDeQuitacao = totalDeQuitacao;
           this.combustivel = combustivel;
           this.pedagio = pedagio;
           this.outrosCreditos = outrosCreditos;
           this.justificativaOutrosCreditos = justificativaOutrosCreditos;
           this.seguro = seguro;
           this.outrosDebitos = outrosDebitos;
           this.justificativaOutrosDebitos = justificativaOutrosDebitos;
    }


    /**
     * Gets the totalOperacao value for this ViagemValores.
     * 
     * @return totalOperacao
     */
    public java.math.BigDecimal getTotalOperacao() {
        return totalOperacao;
    }


    /**
     * Sets the totalOperacao value for this ViagemValores.
     * 
     * @param totalOperacao
     */
    public void setTotalOperacao(java.math.BigDecimal totalOperacao) {
        this.totalOperacao = totalOperacao;
    }


    /**
     * Gets the totalViagem value for this ViagemValores.
     * 
     * @return totalViagem
     */
    public java.math.BigDecimal getTotalViagem() {
        return totalViagem;
    }


    /**
     * Sets the totalViagem value for this ViagemValores.
     * 
     * @param totalViagem
     */
    public void setTotalViagem(java.math.BigDecimal totalViagem) {
        this.totalViagem = totalViagem;
    }


    /**
     * Gets the totalDeAdiantamento value for this ViagemValores.
     * 
     * @return totalDeAdiantamento
     */
    public java.math.BigDecimal getTotalDeAdiantamento() {
        return totalDeAdiantamento;
    }


    /**
     * Sets the totalDeAdiantamento value for this ViagemValores.
     * 
     * @param totalDeAdiantamento
     */
    public void setTotalDeAdiantamento(java.math.BigDecimal totalDeAdiantamento) {
        this.totalDeAdiantamento = totalDeAdiantamento;
    }


    /**
     * Gets the totalDeQuitacao value for this ViagemValores.
     * 
     * @return totalDeQuitacao
     */
    public java.math.BigDecimal getTotalDeQuitacao() {
        return totalDeQuitacao;
    }


    /**
     * Sets the totalDeQuitacao value for this ViagemValores.
     * 
     * @param totalDeQuitacao
     */
    public void setTotalDeQuitacao(java.math.BigDecimal totalDeQuitacao) {
        this.totalDeQuitacao = totalDeQuitacao;
    }


    /**
     * Gets the combustivel value for this ViagemValores.
     * 
     * @return combustivel
     */
    public java.math.BigDecimal getCombustivel() {
        return combustivel;
    }


    /**
     * Sets the combustivel value for this ViagemValores.
     * 
     * @param combustivel
     */
    public void setCombustivel(java.math.BigDecimal combustivel) {
        this.combustivel = combustivel;
    }


    /**
     * Gets the pedagio value for this ViagemValores.
     * 
     * @return pedagio
     */
    public java.math.BigDecimal getPedagio() {
        return pedagio;
    }


    /**
     * Sets the pedagio value for this ViagemValores.
     * 
     * @param pedagio
     */
    public void setPedagio(java.math.BigDecimal pedagio) {
        this.pedagio = pedagio;
    }


    /**
     * Gets the outrosCreditos value for this ViagemValores.
     * 
     * @return outrosCreditos
     */
    public java.math.BigDecimal getOutrosCreditos() {
        return outrosCreditos;
    }


    /**
     * Sets the outrosCreditos value for this ViagemValores.
     * 
     * @param outrosCreditos
     */
    public void setOutrosCreditos(java.math.BigDecimal outrosCreditos) {
        this.outrosCreditos = outrosCreditos;
    }


    /**
     * Gets the justificativaOutrosCreditos value for this ViagemValores.
     * 
     * @return justificativaOutrosCreditos
     */
    public java.lang.String getJustificativaOutrosCreditos() {
        return justificativaOutrosCreditos;
    }


    /**
     * Sets the justificativaOutrosCreditos value for this ViagemValores.
     * 
     * @param justificativaOutrosCreditos
     */
    public void setJustificativaOutrosCreditos(java.lang.String justificativaOutrosCreditos) {
        this.justificativaOutrosCreditos = justificativaOutrosCreditos;
    }


    /**
     * Gets the seguro value for this ViagemValores.
     * 
     * @return seguro
     */
    public java.math.BigDecimal getSeguro() {
        return seguro;
    }


    /**
     * Sets the seguro value for this ViagemValores.
     * 
     * @param seguro
     */
    public void setSeguro(java.math.BigDecimal seguro) {
        this.seguro = seguro;
    }


    /**
     * Gets the outrosDebitos value for this ViagemValores.
     * 
     * @return outrosDebitos
     */
    public java.math.BigDecimal getOutrosDebitos() {
        return outrosDebitos;
    }


    /**
     * Sets the outrosDebitos value for this ViagemValores.
     * 
     * @param outrosDebitos
     */
    public void setOutrosDebitos(java.math.BigDecimal outrosDebitos) {
        this.outrosDebitos = outrosDebitos;
    }


    /**
     * Gets the justificativaOutrosDebitos value for this ViagemValores.
     * 
     * @return justificativaOutrosDebitos
     */
    public java.lang.String getJustificativaOutrosDebitos() {
        return justificativaOutrosDebitos;
    }


    /**
     * Sets the justificativaOutrosDebitos value for this ViagemValores.
     * 
     * @param justificativaOutrosDebitos
     */
    public void setJustificativaOutrosDebitos(java.lang.String justificativaOutrosDebitos) {
        this.justificativaOutrosDebitos = justificativaOutrosDebitos;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ViagemValores)) return false;
        ViagemValores other = (ViagemValores) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.totalOperacao==null && other.getTotalOperacao()==null) || 
             (this.totalOperacao!=null &&
              this.totalOperacao.equals(other.getTotalOperacao()))) &&
            ((this.totalViagem==null && other.getTotalViagem()==null) || 
             (this.totalViagem!=null &&
              this.totalViagem.equals(other.getTotalViagem()))) &&
            ((this.totalDeAdiantamento==null && other.getTotalDeAdiantamento()==null) || 
             (this.totalDeAdiantamento!=null &&
              this.totalDeAdiantamento.equals(other.getTotalDeAdiantamento()))) &&
            ((this.totalDeQuitacao==null && other.getTotalDeQuitacao()==null) || 
             (this.totalDeQuitacao!=null &&
              this.totalDeQuitacao.equals(other.getTotalDeQuitacao()))) &&
            ((this.combustivel==null && other.getCombustivel()==null) || 
             (this.combustivel!=null &&
              this.combustivel.equals(other.getCombustivel()))) &&
            ((this.pedagio==null && other.getPedagio()==null) || 
             (this.pedagio!=null &&
              this.pedagio.equals(other.getPedagio()))) &&
            ((this.outrosCreditos==null && other.getOutrosCreditos()==null) || 
             (this.outrosCreditos!=null &&
              this.outrosCreditos.equals(other.getOutrosCreditos()))) &&
            ((this.justificativaOutrosCreditos==null && other.getJustificativaOutrosCreditos()==null) || 
             (this.justificativaOutrosCreditos!=null &&
              this.justificativaOutrosCreditos.equals(other.getJustificativaOutrosCreditos()))) &&
            ((this.seguro==null && other.getSeguro()==null) || 
             (this.seguro!=null &&
              this.seguro.equals(other.getSeguro()))) &&
            ((this.outrosDebitos==null && other.getOutrosDebitos()==null) || 
             (this.outrosDebitos!=null &&
              this.outrosDebitos.equals(other.getOutrosDebitos()))) &&
            ((this.justificativaOutrosDebitos==null && other.getJustificativaOutrosDebitos()==null) || 
             (this.justificativaOutrosDebitos!=null &&
              this.justificativaOutrosDebitos.equals(other.getJustificativaOutrosDebitos())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTotalOperacao() != null) {
            _hashCode += getTotalOperacao().hashCode();
        }
        if (getTotalViagem() != null) {
            _hashCode += getTotalViagem().hashCode();
        }
        if (getTotalDeAdiantamento() != null) {
            _hashCode += getTotalDeAdiantamento().hashCode();
        }
        if (getTotalDeQuitacao() != null) {
            _hashCode += getTotalDeQuitacao().hashCode();
        }
        if (getCombustivel() != null) {
            _hashCode += getCombustivel().hashCode();
        }
        if (getPedagio() != null) {
            _hashCode += getPedagio().hashCode();
        }
        if (getOutrosCreditos() != null) {
            _hashCode += getOutrosCreditos().hashCode();
        }
        if (getJustificativaOutrosCreditos() != null) {
            _hashCode += getJustificativaOutrosCreditos().hashCode();
        }
        if (getSeguro() != null) {
            _hashCode += getSeguro().hashCode();
        }
        if (getOutrosDebitos() != null) {
            _hashCode += getOutrosDebitos().hashCode();
        }
        if (getJustificativaOutrosDebitos() != null) {
            _hashCode += getJustificativaOutrosDebitos().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ViagemValores.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ViagemValores"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalOperacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TotalOperacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalViagem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TotalViagem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalDeAdiantamento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TotalDeAdiantamento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalDeQuitacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TotalDeQuitacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("combustivel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Combustivel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pedagio");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Pedagio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outrosCreditos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "OutrosCreditos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("justificativaOutrosCreditos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "JustificativaOutrosCreditos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seguro");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Seguro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outrosDebitos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "OutrosDebitos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("justificativaOutrosDebitos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "JustificativaOutrosDebitos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
