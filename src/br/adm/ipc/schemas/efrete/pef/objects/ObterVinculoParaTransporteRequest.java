/**
 * ObterVinculoParaTransporteRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.objects;

public class ObterVinculoParaTransporteRequest  extends br.adm.ipc.schemas.efrete.objects.Request  implements java.io.Serializable {
    private long transportadoraInteressadaCNPJ;

    private long CNPJ;

    private long RNTRC;

    private br.adm.ipc.schemas.efrete.pef.ObterVinculoParaTransporteObjects.VeiculoRequest[] veiculos;

    public ObterVinculoParaTransporteRequest() {
    }

    public ObterVinculoParaTransporteRequest(
           java.lang.String token,
           java.lang.String integrador,
           int versao,
           long transportadoraInteressadaCNPJ,
           long CNPJ,
           long RNTRC,
           br.adm.ipc.schemas.efrete.pef.ObterVinculoParaTransporteObjects.VeiculoRequest[] veiculos) {
        super(
            token,
            integrador,
            versao);
        this.transportadoraInteressadaCNPJ = transportadoraInteressadaCNPJ;
        this.CNPJ = CNPJ;
        this.RNTRC = RNTRC;
        this.veiculos = veiculos;
    }


    /**
     * Gets the transportadoraInteressadaCNPJ value for this ObterVinculoParaTransporteRequest.
     * 
     * @return transportadoraInteressadaCNPJ
     */
    public long getTransportadoraInteressadaCNPJ() {
        return transportadoraInteressadaCNPJ;
    }


    /**
     * Sets the transportadoraInteressadaCNPJ value for this ObterVinculoParaTransporteRequest.
     * 
     * @param transportadoraInteressadaCNPJ
     */
    public void setTransportadoraInteressadaCNPJ(long transportadoraInteressadaCNPJ) {
        this.transportadoraInteressadaCNPJ = transportadoraInteressadaCNPJ;
    }


    /**
     * Gets the CNPJ value for this ObterVinculoParaTransporteRequest.
     * 
     * @return CNPJ
     */
    public long getCNPJ() {
        return CNPJ;
    }


    /**
     * Sets the CNPJ value for this ObterVinculoParaTransporteRequest.
     * 
     * @param CNPJ
     */
    public void setCNPJ(long CNPJ) {
        this.CNPJ = CNPJ;
    }


    /**
     * Gets the RNTRC value for this ObterVinculoParaTransporteRequest.
     * 
     * @return RNTRC
     */
    public long getRNTRC() {
        return RNTRC;
    }


    /**
     * Sets the RNTRC value for this ObterVinculoParaTransporteRequest.
     * 
     * @param RNTRC
     */
    public void setRNTRC(long RNTRC) {
        this.RNTRC = RNTRC;
    }


    /**
     * Gets the veiculos value for this ObterVinculoParaTransporteRequest.
     * 
     * @return veiculos
     */
    public br.adm.ipc.schemas.efrete.pef.ObterVinculoParaTransporteObjects.VeiculoRequest[] getVeiculos() {
        return veiculos;
    }


    /**
     * Sets the veiculos value for this ObterVinculoParaTransporteRequest.
     * 
     * @param veiculos
     */
    public void setVeiculos(br.adm.ipc.schemas.efrete.pef.ObterVinculoParaTransporteObjects.VeiculoRequest[] veiculos) {
        this.veiculos = veiculos;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ObterVinculoParaTransporteRequest)) return false;
        ObterVinculoParaTransporteRequest other = (ObterVinculoParaTransporteRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.transportadoraInteressadaCNPJ == other.getTransportadoraInteressadaCNPJ() &&
            this.CNPJ == other.getCNPJ() &&
            this.RNTRC == other.getRNTRC() &&
            ((this.veiculos==null && other.getVeiculos()==null) || 
             (this.veiculos!=null &&
              java.util.Arrays.equals(this.veiculos, other.getVeiculos())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += new Long(getTransportadoraInteressadaCNPJ()).hashCode();
        _hashCode += new Long(getCNPJ()).hashCode();
        _hashCode += new Long(getRNTRC()).hashCode();
        if (getVeiculos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getVeiculos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getVeiculos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ObterVinculoParaTransporteRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterVinculoParaTransporteRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transportadoraInteressadaCNPJ");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TransportadoraInteressadaCNPJ"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CNPJ");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CNPJ"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RNTRC");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "RNTRC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("veiculos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Veiculos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterVinculoParaTransporteObjects", "VeiculoRequest"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "VeiculoRequest"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
