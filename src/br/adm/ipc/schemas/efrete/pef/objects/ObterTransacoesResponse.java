/**
 * ObterTransacoesResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.objects;

public class ObterTransacoesResponse  extends br.adm.ipc.schemas.efrete.objects.Response  implements java.io.Serializable {
    private br.adm.ipc.schemas.efrete.pef.ObterTransacoesObjects.Transacao[] transacoes;

    public ObterTransacoesResponse() {
    }

    public ObterTransacoesResponse(
           int versao,
           boolean sucesso,
           br.adm.ipc.schemas.efrete.objects.Excecao excecao,
           java.lang.Long protocoloServico,
           br.adm.ipc.schemas.efrete.pef.ObterTransacoesObjects.Transacao[] transacoes) {
        super(
            versao,
            sucesso,
            excecao,
            protocoloServico);
        this.transacoes = transacoes;
    }


    /**
     * Gets the transacoes value for this ObterTransacoesResponse.
     * 
     * @return transacoes
     */
    public br.adm.ipc.schemas.efrete.pef.ObterTransacoesObjects.Transacao[] getTransacoes() {
        return transacoes;
    }


    /**
     * Sets the transacoes value for this ObterTransacoesResponse.
     * 
     * @param transacoes
     */
    public void setTransacoes(br.adm.ipc.schemas.efrete.pef.ObterTransacoesObjects.Transacao[] transacoes) {
        this.transacoes = transacoes;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ObterTransacoesResponse)) return false;
        ObterTransacoesResponse other = (ObterTransacoesResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.transacoes==null && other.getTransacoes()==null) || 
             (this.transacoes!=null &&
              java.util.Arrays.equals(this.transacoes, other.getTransacoes())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getTransacoes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTransacoes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTransacoes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ObterTransacoesResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterTransacoesResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transacoes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Transacoes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ObterTransacoesObjects", "Transacao"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Transacao"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
