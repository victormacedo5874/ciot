/**
 * NotaFiscal.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.objects.RegistrarPagamentoQuitacao;

public class NotaFiscal  implements java.io.Serializable {
    private java.lang.String numero;

    private java.lang.String serie;

    private java.math.BigDecimal quantidadeDaMercadoriaNoDesembarque;

    public NotaFiscal() {
    }

    public NotaFiscal(
           java.lang.String numero,
           java.lang.String serie,
           java.math.BigDecimal quantidadeDaMercadoriaNoDesembarque) {
           this.numero = numero;
           this.serie = serie;
           this.quantidadeDaMercadoriaNoDesembarque = quantidadeDaMercadoriaNoDesembarque;
    }


    /**
     * Gets the numero value for this NotaFiscal.
     * 
     * @return numero
     */
    public java.lang.String getNumero() {
        return numero;
    }


    /**
     * Sets the numero value for this NotaFiscal.
     * 
     * @param numero
     */
    public void setNumero(java.lang.String numero) {
        this.numero = numero;
    }


    /**
     * Gets the serie value for this NotaFiscal.
     * 
     * @return serie
     */
    public java.lang.String getSerie() {
        return serie;
    }


    /**
     * Sets the serie value for this NotaFiscal.
     * 
     * @param serie
     */
    public void setSerie(java.lang.String serie) {
        this.serie = serie;
    }


    /**
     * Gets the quantidadeDaMercadoriaNoDesembarque value for this NotaFiscal.
     * 
     * @return quantidadeDaMercadoriaNoDesembarque
     */
    public java.math.BigDecimal getQuantidadeDaMercadoriaNoDesembarque() {
        return quantidadeDaMercadoriaNoDesembarque;
    }


    /**
     * Sets the quantidadeDaMercadoriaNoDesembarque value for this NotaFiscal.
     * 
     * @param quantidadeDaMercadoriaNoDesembarque
     */
    public void setQuantidadeDaMercadoriaNoDesembarque(java.math.BigDecimal quantidadeDaMercadoriaNoDesembarque) {
        this.quantidadeDaMercadoriaNoDesembarque = quantidadeDaMercadoriaNoDesembarque;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof NotaFiscal)) return false;
        NotaFiscal other = (NotaFiscal) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.numero==null && other.getNumero()==null) || 
             (this.numero!=null &&
              this.numero.equals(other.getNumero()))) &&
            ((this.serie==null && other.getSerie()==null) || 
             (this.serie!=null &&
              this.serie.equals(other.getSerie()))) &&
            ((this.quantidadeDaMercadoriaNoDesembarque==null && other.getQuantidadeDaMercadoriaNoDesembarque()==null) || 
             (this.quantidadeDaMercadoriaNoDesembarque!=null &&
              this.quantidadeDaMercadoriaNoDesembarque.equals(other.getQuantidadeDaMercadoriaNoDesembarque())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNumero() != null) {
            _hashCode += getNumero().hashCode();
        }
        if (getSerie() != null) {
            _hashCode += getSerie().hashCode();
        }
        if (getQuantidadeDaMercadoriaNoDesembarque() != null) {
            _hashCode += getQuantidadeDaMercadoriaNoDesembarque().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(NotaFiscal.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects/RegistrarPagamentoQuitacao", "NotaFiscal"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numero");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects/RegistrarPagamentoQuitacao", "Numero"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serie");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects/RegistrarPagamentoQuitacao", "Serie"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantidadeDaMercadoriaNoDesembarque");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects/RegistrarPagamentoQuitacao", "QuantidadeDaMercadoriaNoDesembarque"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
