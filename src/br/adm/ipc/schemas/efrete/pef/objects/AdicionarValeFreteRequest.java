/**
 * AdicionarValeFreteRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.objects;

public class AdicionarValeFreteRequest  extends br.adm.ipc.schemas.efrete.objects.Request  implements java.io.Serializable {
    private java.lang.String contratanteCpfOuCnpj;

    private java.lang.String contratadoCpfOuCnpj;

    private long contratadoRNTRC;

    private java.lang.String contratadoPlaca;

    private java.lang.String motoristaCPF;

    private java.lang.String motoristaCelular;

    private java.math.BigDecimal valor;

    private java.lang.String idOperacaoCliente;

    public AdicionarValeFreteRequest() {
    }

    public AdicionarValeFreteRequest(
           java.lang.String token,
           java.lang.String integrador,
           int versao,
           java.lang.String contratanteCpfOuCnpj,
           java.lang.String contratadoCpfOuCnpj,
           long contratadoRNTRC,
           java.lang.String contratadoPlaca,
           java.lang.String motoristaCPF,
           java.lang.String motoristaCelular,
           java.math.BigDecimal valor,
           java.lang.String idOperacaoCliente) {
        super(
            token,
            integrador,
            versao);
        this.contratanteCpfOuCnpj = contratanteCpfOuCnpj;
        this.contratadoCpfOuCnpj = contratadoCpfOuCnpj;
        this.contratadoRNTRC = contratadoRNTRC;
        this.contratadoPlaca = contratadoPlaca;
        this.motoristaCPF = motoristaCPF;
        this.motoristaCelular = motoristaCelular;
        this.valor = valor;
        this.idOperacaoCliente = idOperacaoCliente;
    }


    /**
     * Gets the contratanteCpfOuCnpj value for this AdicionarValeFreteRequest.
     * 
     * @return contratanteCpfOuCnpj
     */
    public java.lang.String getContratanteCpfOuCnpj() {
        return contratanteCpfOuCnpj;
    }


    /**
     * Sets the contratanteCpfOuCnpj value for this AdicionarValeFreteRequest.
     * 
     * @param contratanteCpfOuCnpj
     */
    public void setContratanteCpfOuCnpj(java.lang.String contratanteCpfOuCnpj) {
        this.contratanteCpfOuCnpj = contratanteCpfOuCnpj;
    }


    /**
     * Gets the contratadoCpfOuCnpj value for this AdicionarValeFreteRequest.
     * 
     * @return contratadoCpfOuCnpj
     */
    public java.lang.String getContratadoCpfOuCnpj() {
        return contratadoCpfOuCnpj;
    }


    /**
     * Sets the contratadoCpfOuCnpj value for this AdicionarValeFreteRequest.
     * 
     * @param contratadoCpfOuCnpj
     */
    public void setContratadoCpfOuCnpj(java.lang.String contratadoCpfOuCnpj) {
        this.contratadoCpfOuCnpj = contratadoCpfOuCnpj;
    }


    /**
     * Gets the contratadoRNTRC value for this AdicionarValeFreteRequest.
     * 
     * @return contratadoRNTRC
     */
    public long getContratadoRNTRC() {
        return contratadoRNTRC;
    }


    /**
     * Sets the contratadoRNTRC value for this AdicionarValeFreteRequest.
     * 
     * @param contratadoRNTRC
     */
    public void setContratadoRNTRC(long contratadoRNTRC) {
        this.contratadoRNTRC = contratadoRNTRC;
    }


    /**
     * Gets the contratadoPlaca value for this AdicionarValeFreteRequest.
     * 
     * @return contratadoPlaca
     */
    public java.lang.String getContratadoPlaca() {
        return contratadoPlaca;
    }


    /**
     * Sets the contratadoPlaca value for this AdicionarValeFreteRequest.
     * 
     * @param contratadoPlaca
     */
    public void setContratadoPlaca(java.lang.String contratadoPlaca) {
        this.contratadoPlaca = contratadoPlaca;
    }


    /**
     * Gets the motoristaCPF value for this AdicionarValeFreteRequest.
     * 
     * @return motoristaCPF
     */
    public java.lang.String getMotoristaCPF() {
        return motoristaCPF;
    }


    /**
     * Sets the motoristaCPF value for this AdicionarValeFreteRequest.
     * 
     * @param motoristaCPF
     */
    public void setMotoristaCPF(java.lang.String motoristaCPF) {
        this.motoristaCPF = motoristaCPF;
    }


    /**
     * Gets the motoristaCelular value for this AdicionarValeFreteRequest.
     * 
     * @return motoristaCelular
     */
    public java.lang.String getMotoristaCelular() {
        return motoristaCelular;
    }


    /**
     * Sets the motoristaCelular value for this AdicionarValeFreteRequest.
     * 
     * @param motoristaCelular
     */
    public void setMotoristaCelular(java.lang.String motoristaCelular) {
        this.motoristaCelular = motoristaCelular;
    }


    /**
     * Gets the valor value for this AdicionarValeFreteRequest.
     * 
     * @return valor
     */
    public java.math.BigDecimal getValor() {
        return valor;
    }


    /**
     * Sets the valor value for this AdicionarValeFreteRequest.
     * 
     * @param valor
     */
    public void setValor(java.math.BigDecimal valor) {
        this.valor = valor;
    }


    /**
     * Gets the idOperacaoCliente value for this AdicionarValeFreteRequest.
     * 
     * @return idOperacaoCliente
     */
    public java.lang.String getIdOperacaoCliente() {
        return idOperacaoCliente;
    }


    /**
     * Sets the idOperacaoCliente value for this AdicionarValeFreteRequest.
     * 
     * @param idOperacaoCliente
     */
    public void setIdOperacaoCliente(java.lang.String idOperacaoCliente) {
        this.idOperacaoCliente = idOperacaoCliente;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AdicionarValeFreteRequest)) return false;
        AdicionarValeFreteRequest other = (AdicionarValeFreteRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.contratanteCpfOuCnpj==null && other.getContratanteCpfOuCnpj()==null) || 
             (this.contratanteCpfOuCnpj!=null &&
              this.contratanteCpfOuCnpj.equals(other.getContratanteCpfOuCnpj()))) &&
            ((this.contratadoCpfOuCnpj==null && other.getContratadoCpfOuCnpj()==null) || 
             (this.contratadoCpfOuCnpj!=null &&
              this.contratadoCpfOuCnpj.equals(other.getContratadoCpfOuCnpj()))) &&
            this.contratadoRNTRC == other.getContratadoRNTRC() &&
            ((this.contratadoPlaca==null && other.getContratadoPlaca()==null) || 
             (this.contratadoPlaca!=null &&
              this.contratadoPlaca.equals(other.getContratadoPlaca()))) &&
            ((this.motoristaCPF==null && other.getMotoristaCPF()==null) || 
             (this.motoristaCPF!=null &&
              this.motoristaCPF.equals(other.getMotoristaCPF()))) &&
            ((this.motoristaCelular==null && other.getMotoristaCelular()==null) || 
             (this.motoristaCelular!=null &&
              this.motoristaCelular.equals(other.getMotoristaCelular()))) &&
            ((this.valor==null && other.getValor()==null) || 
             (this.valor!=null &&
              this.valor.equals(other.getValor()))) &&
            ((this.idOperacaoCliente==null && other.getIdOperacaoCliente()==null) || 
             (this.idOperacaoCliente!=null &&
              this.idOperacaoCliente.equals(other.getIdOperacaoCliente())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getContratanteCpfOuCnpj() != null) {
            _hashCode += getContratanteCpfOuCnpj().hashCode();
        }
        if (getContratadoCpfOuCnpj() != null) {
            _hashCode += getContratadoCpfOuCnpj().hashCode();
        }
        _hashCode += new Long(getContratadoRNTRC()).hashCode();
        if (getContratadoPlaca() != null) {
            _hashCode += getContratadoPlaca().hashCode();
        }
        if (getMotoristaCPF() != null) {
            _hashCode += getMotoristaCPF().hashCode();
        }
        if (getMotoristaCelular() != null) {
            _hashCode += getMotoristaCelular().hashCode();
        }
        if (getValor() != null) {
            _hashCode += getValor().hashCode();
        }
        if (getIdOperacaoCliente() != null) {
            _hashCode += getIdOperacaoCliente().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AdicionarValeFreteRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AdicionarValeFreteRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contratanteCpfOuCnpj");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ContratanteCpfOuCnpj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contratadoCpfOuCnpj");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ContratadoCpfOuCnpj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contratadoRNTRC");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ContratadoRNTRC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contratadoPlaca");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ContratadoPlaca"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("motoristaCPF");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "MotoristaCPF"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("motoristaCelular");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "MotoristaCelular"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valor");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Valor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idOperacaoCliente");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "IdOperacaoCliente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
