/**
 * AdicionarViagemRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.objects;

public class AdicionarViagemRequest  extends br.adm.ipc.schemas.efrete.objects.Request  implements java.io.Serializable {
    private java.lang.String codigoIdentificacaoOperacao;

    private br.adm.ipc.schemas.efrete.pef.AdicionarViagem.Viagem[] viagens;

    private br.adm.ipc.schemas.efrete.pef.AdicionarViagem.Pagamento[] pagamentos;

    private java.lang.Boolean naoAdicionarParcialmente;

    public AdicionarViagemRequest() {
    }

    public AdicionarViagemRequest(
           java.lang.String token,
           java.lang.String integrador,
           int versao,
           java.lang.String codigoIdentificacaoOperacao,
           br.adm.ipc.schemas.efrete.pef.AdicionarViagem.Viagem[] viagens,
           br.adm.ipc.schemas.efrete.pef.AdicionarViagem.Pagamento[] pagamentos,
           java.lang.Boolean naoAdicionarParcialmente) {
        super(
            token,
            integrador,
            versao);
        this.codigoIdentificacaoOperacao = codigoIdentificacaoOperacao;
        this.viagens = viagens;
        this.pagamentos = pagamentos;
        this.naoAdicionarParcialmente = naoAdicionarParcialmente;
    }


    /**
     * Gets the codigoIdentificacaoOperacao value for this AdicionarViagemRequest.
     * 
     * @return codigoIdentificacaoOperacao
     */
    public java.lang.String getCodigoIdentificacaoOperacao() {
        return codigoIdentificacaoOperacao;
    }


    /**
     * Sets the codigoIdentificacaoOperacao value for this AdicionarViagemRequest.
     * 
     * @param codigoIdentificacaoOperacao
     */
    public void setCodigoIdentificacaoOperacao(java.lang.String codigoIdentificacaoOperacao) {
        this.codigoIdentificacaoOperacao = codigoIdentificacaoOperacao;
    }


    /**
     * Gets the viagens value for this AdicionarViagemRequest.
     * 
     * @return viagens
     */
    public br.adm.ipc.schemas.efrete.pef.AdicionarViagem.Viagem[] getViagens() {
        return viagens;
    }


    /**
     * Sets the viagens value for this AdicionarViagemRequest.
     * 
     * @param viagens
     */
    public void setViagens(br.adm.ipc.schemas.efrete.pef.AdicionarViagem.Viagem[] viagens) {
        this.viagens = viagens;
    }


    /**
     * Gets the pagamentos value for this AdicionarViagemRequest.
     * 
     * @return pagamentos
     */
    public br.adm.ipc.schemas.efrete.pef.AdicionarViagem.Pagamento[] getPagamentos() {
        return pagamentos;
    }


    /**
     * Sets the pagamentos value for this AdicionarViagemRequest.
     * 
     * @param pagamentos
     */
    public void setPagamentos(br.adm.ipc.schemas.efrete.pef.AdicionarViagem.Pagamento[] pagamentos) {
        this.pagamentos = pagamentos;
    }


    /**
     * Gets the naoAdicionarParcialmente value for this AdicionarViagemRequest.
     * 
     * @return naoAdicionarParcialmente
     */
    public java.lang.Boolean getNaoAdicionarParcialmente() {
        return naoAdicionarParcialmente;
    }


    /**
     * Sets the naoAdicionarParcialmente value for this AdicionarViagemRequest.
     * 
     * @param naoAdicionarParcialmente
     */
    public void setNaoAdicionarParcialmente(java.lang.Boolean naoAdicionarParcialmente) {
        this.naoAdicionarParcialmente = naoAdicionarParcialmente;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AdicionarViagemRequest)) return false;
        AdicionarViagemRequest other = (AdicionarViagemRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.codigoIdentificacaoOperacao==null && other.getCodigoIdentificacaoOperacao()==null) || 
             (this.codigoIdentificacaoOperacao!=null &&
              this.codigoIdentificacaoOperacao.equals(other.getCodigoIdentificacaoOperacao()))) &&
            ((this.viagens==null && other.getViagens()==null) || 
             (this.viagens!=null &&
              java.util.Arrays.equals(this.viagens, other.getViagens()))) &&
            ((this.pagamentos==null && other.getPagamentos()==null) || 
             (this.pagamentos!=null &&
              java.util.Arrays.equals(this.pagamentos, other.getPagamentos()))) &&
            ((this.naoAdicionarParcialmente==null && other.getNaoAdicionarParcialmente()==null) || 
             (this.naoAdicionarParcialmente!=null &&
              this.naoAdicionarParcialmente.equals(other.getNaoAdicionarParcialmente())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCodigoIdentificacaoOperacao() != null) {
            _hashCode += getCodigoIdentificacaoOperacao().hashCode();
        }
        if (getViagens() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getViagens());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getViagens(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPagamentos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPagamentos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPagamentos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getNaoAdicionarParcialmente() != null) {
            _hashCode += getNaoAdicionarParcialmente().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AdicionarViagemRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "AdicionarViagemRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoIdentificacaoOperacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CodigoIdentificacaoOperacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("viagens");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarViagem", "Viagens"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarViagem", "ArrayOfViagem"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pagamentos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarViagem", "Pagamentos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarViagem", "ArrayOfPagamento"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("naoAdicionarParcialmente");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "NaoAdicionarParcialmente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
