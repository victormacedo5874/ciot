/**
 * ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.objects;

public class ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteRequest  extends br.adm.ipc.schemas.efrete.objects.Request  implements java.io.Serializable {
    private long matrizCNPJ;

    private java.lang.String idOperacaoCliente;

    public ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteRequest() {
    }

    public ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteRequest(
           java.lang.String token,
           java.lang.String integrador,
           int versao,
           long matrizCNPJ,
           java.lang.String idOperacaoCliente) {
        super(
            token,
            integrador,
            versao);
        this.matrizCNPJ = matrizCNPJ;
        this.idOperacaoCliente = idOperacaoCliente;
    }


    /**
     * Gets the matrizCNPJ value for this ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteRequest.
     * 
     * @return matrizCNPJ
     */
    public long getMatrizCNPJ() {
        return matrizCNPJ;
    }


    /**
     * Sets the matrizCNPJ value for this ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteRequest.
     * 
     * @param matrizCNPJ
     */
    public void setMatrizCNPJ(long matrizCNPJ) {
        this.matrizCNPJ = matrizCNPJ;
    }


    /**
     * Gets the idOperacaoCliente value for this ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteRequest.
     * 
     * @return idOperacaoCliente
     */
    public java.lang.String getIdOperacaoCliente() {
        return idOperacaoCliente;
    }


    /**
     * Sets the idOperacaoCliente value for this ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteRequest.
     * 
     * @param idOperacaoCliente
     */
    public void setIdOperacaoCliente(java.lang.String idOperacaoCliente) {
        this.idOperacaoCliente = idOperacaoCliente;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteRequest)) return false;
        ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteRequest other = (ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.matrizCNPJ == other.getMatrizCNPJ() &&
            ((this.idOperacaoCliente==null && other.getIdOperacaoCliente()==null) || 
             (this.idOperacaoCliente!=null &&
              this.idOperacaoCliente.equals(other.getIdOperacaoCliente())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += new Long(getMatrizCNPJ()).hashCode();
        if (getIdOperacaoCliente() != null) {
            _hashCode += getIdOperacaoCliente().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterCodigoIdentificacaoOperacaoTransportePorIdOperacaoClienteRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("matrizCNPJ");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "MatrizCNPJ"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idOperacaoCliente");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "IdOperacaoCliente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
