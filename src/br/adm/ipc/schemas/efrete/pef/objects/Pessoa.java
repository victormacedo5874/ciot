/**
 * Pessoa.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.objects;

public class Pessoa  implements java.io.Serializable {
    private java.lang.String nomeOuRazaoSocial;

    private String cpfOuCnpj;

    private br.adm.ipc.schemas.efrete.objects.Endereco endereco;

    private java.lang.String EMail;

    private br.adm.ipc.schemas.efrete.objects.Telefones telefones;

    private boolean responsavelPeloPagamento;

    public Pessoa() {
    }

    public Pessoa(
           java.lang.String nomeOuRazaoSocial,
           String cpfOuCnpj,
           br.adm.ipc.schemas.efrete.objects.Endereco endereco,
           java.lang.String EMail,
           br.adm.ipc.schemas.efrete.objects.Telefones telefones,
           boolean responsavelPeloPagamento) {
           this.nomeOuRazaoSocial = nomeOuRazaoSocial;
           this.cpfOuCnpj = cpfOuCnpj;
           this.endereco = endereco;
           this.EMail = EMail;
           this.telefones = telefones;
           this.responsavelPeloPagamento = responsavelPeloPagamento;
    }


    /**
     * Gets the nomeOuRazaoSocial value for this Pessoa.
     * 
     * @return nomeOuRazaoSocial
     */
    public java.lang.String getNomeOuRazaoSocial() {
        return nomeOuRazaoSocial;
    }


    /**
     * Sets the nomeOuRazaoSocial value for this Pessoa.
     * 
     * @param nomeOuRazaoSocial
     */
    public void setNomeOuRazaoSocial(java.lang.String nomeOuRazaoSocial) {
        this.nomeOuRazaoSocial = nomeOuRazaoSocial;
    }


    /**
     * Gets the cpfOuCnpj value for this Pessoa.
     * 
     * @return cpfOuCnpj
     */
    public String getCpfOuCnpj() {
        return cpfOuCnpj;
    }


    /**
     * Sets the cpfOuCnpj value for this Pessoa.
     * 
     * @param cpfOuCnpj
     */
    public void setCpfOuCnpj(String cpfOuCnpj) {
        this.cpfOuCnpj = cpfOuCnpj;
    }


    /**
     * Gets the endereco value for this Pessoa.
     * 
     * @return endereco
     */
    public br.adm.ipc.schemas.efrete.objects.Endereco getEndereco() {
        return endereco;
    }


    /**
     * Sets the endereco value for this Pessoa.
     * 
     * @param endereco
     */
    public void setEndereco(br.adm.ipc.schemas.efrete.objects.Endereco endereco) {
        this.endereco = endereco;
    }


    /**
     * Gets the EMail value for this Pessoa.
     * 
     * @return EMail
     */
    public java.lang.String getEMail() {
        return EMail;
    }


    /**
     * Sets the EMail value for this Pessoa.
     * 
     * @param EMail
     */
    public void setEMail(java.lang.String EMail) {
        this.EMail = EMail;
    }


    /**
     * Gets the telefones value for this Pessoa.
     * 
     * @return telefones
     */
    public br.adm.ipc.schemas.efrete.objects.Telefones getTelefones() {
        return telefones;
    }


    /**
     * Sets the telefones value for this Pessoa.
     * 
     * @param telefones
     */
    public void setTelefones(br.adm.ipc.schemas.efrete.objects.Telefones telefones) {
        this.telefones = telefones;
    }


    /**
     * Gets the responsavelPeloPagamento value for this Pessoa.
     * 
     * @return responsavelPeloPagamento
     */
    public boolean isResponsavelPeloPagamento() {
        return responsavelPeloPagamento;
    }


    /**
     * Sets the responsavelPeloPagamento value for this Pessoa.
     * 
     * @param responsavelPeloPagamento
     */
    public void setResponsavelPeloPagamento(boolean responsavelPeloPagamento) {
        this.responsavelPeloPagamento = responsavelPeloPagamento;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Pessoa)) return false;
        Pessoa other = (Pessoa) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.nomeOuRazaoSocial==null && other.getNomeOuRazaoSocial()==null) || 
             (this.nomeOuRazaoSocial!=null &&
              this.nomeOuRazaoSocial.equals(other.getNomeOuRazaoSocial()))) &&
            this.cpfOuCnpj == other.getCpfOuCnpj() &&
            ((this.endereco==null && other.getEndereco()==null) || 
             (this.endereco!=null &&
              this.endereco.equals(other.getEndereco()))) &&
            ((this.EMail==null && other.getEMail()==null) || 
             (this.EMail!=null &&
              this.EMail.equals(other.getEMail()))) &&
            ((this.telefones==null && other.getTelefones()==null) || 
             (this.telefones!=null &&
              this.telefones.equals(other.getTelefones()))) &&
            this.responsavelPeloPagamento == other.isResponsavelPeloPagamento();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNomeOuRazaoSocial() != null) {
            _hashCode += getNomeOuRazaoSocial().hashCode();
        }
        _hashCode += new Long(getCpfOuCnpj()).hashCode();
        if (getEndereco() != null) {
            _hashCode += getEndereco().hashCode();
        }
        if (getEMail() != null) {
            _hashCode += getEMail().hashCode();
        }
        if (getTelefones() != null) {
            _hashCode += getTelefones().hashCode();
        }
        _hashCode += (isResponsavelPeloPagamento() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Pessoa.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Pessoa"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomeOuRazaoSocial");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "NomeOuRazaoSocial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cpfOuCnpj");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CpfOuCnpj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endereco");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Endereco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/objects", "Endereco"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EMail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "EMail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("telefones");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Telefones"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/objects", "Telefones"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responsavelPeloPagamento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ResponsavelPeloPagamento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
