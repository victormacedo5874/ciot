/**
 * TipoToleranciaDePerdaDeMercadoria.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.objects;

public class TipoToleranciaDePerdaDeMercadoria implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected TipoToleranciaDePerdaDeMercadoria(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _Nenhum = "Nenhum";
    public static final java.lang.String _Porcentagem = "Porcentagem";
    public static final java.lang.String _ValorAbsoluto = "ValorAbsoluto";
    public static final TipoToleranciaDePerdaDeMercadoria Nenhum = new TipoToleranciaDePerdaDeMercadoria(_Nenhum);
    public static final TipoToleranciaDePerdaDeMercadoria Porcentagem = new TipoToleranciaDePerdaDeMercadoria(_Porcentagem);
    public static final TipoToleranciaDePerdaDeMercadoria ValorAbsoluto = new TipoToleranciaDePerdaDeMercadoria(_ValorAbsoluto);
    public java.lang.String getValue() { return _value_;}
    public static TipoToleranciaDePerdaDeMercadoria fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        TipoToleranciaDePerdaDeMercadoria enumeration = (TipoToleranciaDePerdaDeMercadoria)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static TipoToleranciaDePerdaDeMercadoria fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TipoToleranciaDePerdaDeMercadoria.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TipoToleranciaDePerdaDeMercadoria"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
