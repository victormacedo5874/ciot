/**
 * ObterTransacoesRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.objects;

public class ObterTransacoesRequest  extends br.adm.ipc.schemas.efrete.objects.Request  implements java.io.Serializable {
    private java.util.Calendar dataInicial;

    private java.util.Calendar dataFinal;

    private long[] matrizCNPJ;

    private java.lang.String[] CIOTs;

    public ObterTransacoesRequest() {
    }

    public ObterTransacoesRequest(
           java.lang.String token,
           java.lang.String integrador,
           int versao,
           java.util.Calendar dataInicial,
           java.util.Calendar dataFinal,
           long[] matrizCNPJ,
           java.lang.String[] CIOTs) {
        super(
            token,
            integrador,
            versao);
        this.dataInicial = dataInicial;
        this.dataFinal = dataFinal;
        this.matrizCNPJ = matrizCNPJ;
        this.CIOTs = CIOTs;
    }


    /**
     * Gets the dataInicial value for this ObterTransacoesRequest.
     * 
     * @return dataInicial
     */
    public java.util.Calendar getDataInicial() {
        return dataInicial;
    }


    /**
     * Sets the dataInicial value for this ObterTransacoesRequest.
     * 
     * @param dataInicial
     */
    public void setDataInicial(java.util.Calendar dataInicial) {
        this.dataInicial = dataInicial;
    }


    /**
     * Gets the dataFinal value for this ObterTransacoesRequest.
     * 
     * @return dataFinal
     */
    public java.util.Calendar getDataFinal() {
        return dataFinal;
    }


    /**
     * Sets the dataFinal value for this ObterTransacoesRequest.
     * 
     * @param dataFinal
     */
    public void setDataFinal(java.util.Calendar dataFinal) {
        this.dataFinal = dataFinal;
    }


    /**
     * Gets the matrizCNPJ value for this ObterTransacoesRequest.
     * 
     * @return matrizCNPJ
     */
    public long[] getMatrizCNPJ() {
        return matrizCNPJ;
    }


    /**
     * Sets the matrizCNPJ value for this ObterTransacoesRequest.
     * 
     * @param matrizCNPJ
     */
    public void setMatrizCNPJ(long[] matrizCNPJ) {
        this.matrizCNPJ = matrizCNPJ;
    }


    /**
     * Gets the CIOTs value for this ObterTransacoesRequest.
     * 
     * @return CIOTs
     */
    public java.lang.String[] getCIOTs() {
        return CIOTs;
    }


    /**
     * Sets the CIOTs value for this ObterTransacoesRequest.
     * 
     * @param CIOTs
     */
    public void setCIOTs(java.lang.String[] CIOTs) {
        this.CIOTs = CIOTs;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ObterTransacoesRequest)) return false;
        ObterTransacoesRequest other = (ObterTransacoesRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.dataInicial==null && other.getDataInicial()==null) || 
             (this.dataInicial!=null &&
              this.dataInicial.equals(other.getDataInicial()))) &&
            ((this.dataFinal==null && other.getDataFinal()==null) || 
             (this.dataFinal!=null &&
              this.dataFinal.equals(other.getDataFinal()))) &&
            ((this.matrizCNPJ==null && other.getMatrizCNPJ()==null) || 
             (this.matrizCNPJ!=null &&
              java.util.Arrays.equals(this.matrizCNPJ, other.getMatrizCNPJ()))) &&
            ((this.CIOTs==null && other.getCIOTs()==null) || 
             (this.CIOTs!=null &&
              java.util.Arrays.equals(this.CIOTs, other.getCIOTs())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDataInicial() != null) {
            _hashCode += getDataInicial().hashCode();
        }
        if (getDataFinal() != null) {
            _hashCode += getDataFinal().hashCode();
        }
        if (getMatrizCNPJ() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMatrizCNPJ());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMatrizCNPJ(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCIOTs() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCIOTs());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCIOTs(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ObterTransacoesRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterTransacoesRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataInicial");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DataInicial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataFinal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DataFinal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("matrizCNPJ");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "MatrizCNPJ"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "long"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CIOTs");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CIOTs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "string"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
