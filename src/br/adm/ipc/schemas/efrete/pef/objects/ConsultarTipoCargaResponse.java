/**
 * ConsultarTipoCargaResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.objects;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ConsultarTipoCargaResponse  extends br.adm.ipc.schemas.efrete.objects.Response  implements java.io.Serializable {
    private br.adm.ipc.schemas.efrete.pef.ConsultarTipoCarga.TipoCarga[] tipoCargas;

    public ConsultarTipoCargaResponse() {
    }

    public ConsultarTipoCargaResponse(
           int versao,
           boolean sucesso,
           br.adm.ipc.schemas.efrete.objects.Excecao excecao,
           java.lang.Long protocoloServico,
           br.adm.ipc.schemas.efrete.pef.ConsultarTipoCarga.TipoCarga[] tipoCargas) {
        super(
            versao,
            sucesso,
            excecao,
            protocoloServico);
        this.tipoCargas = tipoCargas;
    }


    /**
     * Gets the tipoCargas value for this ConsultarTipoCargaResponse.
     * 
     * @return tipoCargas
     */
    public br.adm.ipc.schemas.efrete.pef.ConsultarTipoCarga.TipoCarga[] getTipoCargas() {
        return tipoCargas;
    }


    /**
     * Sets the tipoCargas value for this ConsultarTipoCargaResponse.
     * 
     * @param tipoCargas
     */
    public void setTipoCargas(br.adm.ipc.schemas.efrete.pef.ConsultarTipoCarga.TipoCarga[] tipoCargas) {
        this.tipoCargas = tipoCargas;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ConsultarTipoCargaResponse)) return false;
        ConsultarTipoCargaResponse other = (ConsultarTipoCargaResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.tipoCargas==null && other.getTipoCargas()==null) || 
             (this.tipoCargas!=null &&
              java.util.Arrays.equals(this.tipoCargas, other.getTipoCargas())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getTipoCargas() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTipoCargas());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTipoCargas(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ConsultarTipoCargaResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ConsultarTipoCargaResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoCargas");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TipoCargas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/ConsultarTipoCarga", "TipoCarga"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TipoCarga"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
