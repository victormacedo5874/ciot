/**
 * EncerrarOperacaoTransporteRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.objects;

public class EncerrarOperacaoTransporteRequest  extends br.adm.ipc.schemas.efrete.objects.Request  implements java.io.Serializable {
    private java.lang.String codigoIdentificacaoOperacao;

    private java.math.BigDecimal pesoCarga;

    private br.adm.ipc.schemas.efrete.pef.EncerrarOperacaoTransporte.Viagem[] viagens;

    private br.adm.ipc.schemas.efrete.pef.EncerrarOperacaoTransporte.Pagamento[] pagamentos;

    private br.adm.ipc.schemas.efrete.pef.objects.Impostos impostos;

    private java.lang.Integer quantidadeSaques;

    private java.lang.Integer quantidadeTransferencias;

    private java.math.BigDecimal valorSaques;

    private java.math.BigDecimal valorTransferencias;

    public EncerrarOperacaoTransporteRequest() {
    }

    public EncerrarOperacaoTransporteRequest(
           java.lang.String token,
           java.lang.String integrador,
           int versao,
           java.lang.String codigoIdentificacaoOperacao,
           java.math.BigDecimal pesoCarga,
           br.adm.ipc.schemas.efrete.pef.EncerrarOperacaoTransporte.Viagem[] viagens,
           br.adm.ipc.schemas.efrete.pef.EncerrarOperacaoTransporte.Pagamento[] pagamentos,
           br.adm.ipc.schemas.efrete.pef.objects.Impostos impostos,
           java.lang.Integer quantidadeSaques,
           java.lang.Integer quantidadeTransferencias,
           java.math.BigDecimal valorSaques,
           java.math.BigDecimal valorTransferencias) {
        super(
            token,
            integrador,
            versao);
        this.codigoIdentificacaoOperacao = codigoIdentificacaoOperacao;
        this.pesoCarga = pesoCarga;
        this.viagens = viagens;
        this.pagamentos = pagamentos;
        this.impostos = impostos;
        this.quantidadeSaques = quantidadeSaques;
        this.quantidadeTransferencias = quantidadeTransferencias;
        this.valorSaques = valorSaques;
        this.valorTransferencias = valorTransferencias;
    }


    /**
     * Gets the codigoIdentificacaoOperacao value for this EncerrarOperacaoTransporteRequest.
     * 
     * @return codigoIdentificacaoOperacao
     */
    public java.lang.String getCodigoIdentificacaoOperacao() {
        return codigoIdentificacaoOperacao;
    }


    /**
     * Sets the codigoIdentificacaoOperacao value for this EncerrarOperacaoTransporteRequest.
     * 
     * @param codigoIdentificacaoOperacao
     */
    public void setCodigoIdentificacaoOperacao(java.lang.String codigoIdentificacaoOperacao) {
        this.codigoIdentificacaoOperacao = codigoIdentificacaoOperacao;
    }


    /**
     * Gets the pesoCarga value for this EncerrarOperacaoTransporteRequest.
     * 
     * @return pesoCarga
     */
    public java.math.BigDecimal getPesoCarga() {
        return pesoCarga;
    }


    /**
     * Sets the pesoCarga value for this EncerrarOperacaoTransporteRequest.
     * 
     * @param pesoCarga
     */
    public void setPesoCarga(java.math.BigDecimal pesoCarga) {
        this.pesoCarga = pesoCarga;
    }


    /**
     * Gets the viagens value for this EncerrarOperacaoTransporteRequest.
     * 
     * @return viagens
     */
    public br.adm.ipc.schemas.efrete.pef.EncerrarOperacaoTransporte.Viagem[] getViagens() {
        return viagens;
    }


    /**
     * Sets the viagens value for this EncerrarOperacaoTransporteRequest.
     * 
     * @param viagens
     */
    public void setViagens(br.adm.ipc.schemas.efrete.pef.EncerrarOperacaoTransporte.Viagem[] viagens) {
        this.viagens = viagens;
    }


    /**
     * Gets the pagamentos value for this EncerrarOperacaoTransporteRequest.
     * 
     * @return pagamentos
     */
    public br.adm.ipc.schemas.efrete.pef.EncerrarOperacaoTransporte.Pagamento[] getPagamentos() {
        return pagamentos;
    }


    /**
     * Sets the pagamentos value for this EncerrarOperacaoTransporteRequest.
     * 
     * @param pagamentos
     */
    public void setPagamentos(br.adm.ipc.schemas.efrete.pef.EncerrarOperacaoTransporte.Pagamento[] pagamentos) {
        this.pagamentos = pagamentos;
    }


    /**
     * Gets the impostos value for this EncerrarOperacaoTransporteRequest.
     * 
     * @return impostos
     */
    public br.adm.ipc.schemas.efrete.pef.objects.Impostos getImpostos() {
        return impostos;
    }


    /**
     * Sets the impostos value for this EncerrarOperacaoTransporteRequest.
     * 
     * @param impostos
     */
    public void setImpostos(br.adm.ipc.schemas.efrete.pef.objects.Impostos impostos) {
        this.impostos = impostos;
    }


    /**
     * Gets the quantidadeSaques value for this EncerrarOperacaoTransporteRequest.
     * 
     * @return quantidadeSaques
     */
    public java.lang.Integer getQuantidadeSaques() {
        return quantidadeSaques;
    }


    /**
     * Sets the quantidadeSaques value for this EncerrarOperacaoTransporteRequest.
     * 
     * @param quantidadeSaques
     */
    public void setQuantidadeSaques(java.lang.Integer quantidadeSaques) {
        this.quantidadeSaques = quantidadeSaques;
    }


    /**
     * Gets the quantidadeTransferencias value for this EncerrarOperacaoTransporteRequest.
     * 
     * @return quantidadeTransferencias
     */
    public java.lang.Integer getQuantidadeTransferencias() {
        return quantidadeTransferencias;
    }


    /**
     * Sets the quantidadeTransferencias value for this EncerrarOperacaoTransporteRequest.
     * 
     * @param quantidadeTransferencias
     */
    public void setQuantidadeTransferencias(java.lang.Integer quantidadeTransferencias) {
        this.quantidadeTransferencias = quantidadeTransferencias;
    }


    /**
     * Gets the valorSaques value for this EncerrarOperacaoTransporteRequest.
     * 
     * @return valorSaques
     */
    public java.math.BigDecimal getValorSaques() {
        return valorSaques;
    }


    /**
     * Sets the valorSaques value for this EncerrarOperacaoTransporteRequest.
     * 
     * @param valorSaques
     */
    public void setValorSaques(java.math.BigDecimal valorSaques) {
        this.valorSaques = valorSaques;
    }


    /**
     * Gets the valorTransferencias value for this EncerrarOperacaoTransporteRequest.
     * 
     * @return valorTransferencias
     */
    public java.math.BigDecimal getValorTransferencias() {
        return valorTransferencias;
    }


    /**
     * Sets the valorTransferencias value for this EncerrarOperacaoTransporteRequest.
     * 
     * @param valorTransferencias
     */
    public void setValorTransferencias(java.math.BigDecimal valorTransferencias) {
        this.valorTransferencias = valorTransferencias;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EncerrarOperacaoTransporteRequest)) return false;
        EncerrarOperacaoTransporteRequest other = (EncerrarOperacaoTransporteRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.codigoIdentificacaoOperacao==null && other.getCodigoIdentificacaoOperacao()==null) || 
             (this.codigoIdentificacaoOperacao!=null &&
              this.codigoIdentificacaoOperacao.equals(other.getCodigoIdentificacaoOperacao()))) &&
            ((this.pesoCarga==null && other.getPesoCarga()==null) || 
             (this.pesoCarga!=null &&
              this.pesoCarga.equals(other.getPesoCarga()))) &&
            ((this.viagens==null && other.getViagens()==null) || 
             (this.viagens!=null &&
              java.util.Arrays.equals(this.viagens, other.getViagens()))) &&
            ((this.pagamentos==null && other.getPagamentos()==null) || 
             (this.pagamentos!=null &&
              java.util.Arrays.equals(this.pagamentos, other.getPagamentos()))) &&
            ((this.impostos==null && other.getImpostos()==null) || 
             (this.impostos!=null &&
              this.impostos.equals(other.getImpostos()))) &&
            ((this.quantidadeSaques==null && other.getQuantidadeSaques()==null) || 
             (this.quantidadeSaques!=null &&
              this.quantidadeSaques.equals(other.getQuantidadeSaques()))) &&
            ((this.quantidadeTransferencias==null && other.getQuantidadeTransferencias()==null) || 
             (this.quantidadeTransferencias!=null &&
              this.quantidadeTransferencias.equals(other.getQuantidadeTransferencias()))) &&
            ((this.valorSaques==null && other.getValorSaques()==null) || 
             (this.valorSaques!=null &&
              this.valorSaques.equals(other.getValorSaques()))) &&
            ((this.valorTransferencias==null && other.getValorTransferencias()==null) || 
             (this.valorTransferencias!=null &&
              this.valorTransferencias.equals(other.getValorTransferencias())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCodigoIdentificacaoOperacao() != null) {
            _hashCode += getCodigoIdentificacaoOperacao().hashCode();
        }
        if (getPesoCarga() != null) {
            _hashCode += getPesoCarga().hashCode();
        }
        if (getViagens() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getViagens());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getViagens(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPagamentos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPagamentos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPagamentos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getImpostos() != null) {
            _hashCode += getImpostos().hashCode();
        }
        if (getQuantidadeSaques() != null) {
            _hashCode += getQuantidadeSaques().hashCode();
        }
        if (getQuantidadeTransferencias() != null) {
            _hashCode += getQuantidadeTransferencias().hashCode();
        }
        if (getValorSaques() != null) {
            _hashCode += getValorSaques().hashCode();
        }
        if (getValorTransferencias() != null) {
            _hashCode += getValorTransferencias().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EncerrarOperacaoTransporteRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "EncerrarOperacaoTransporteRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoIdentificacaoOperacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CodigoIdentificacaoOperacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pesoCarga");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "PesoCarga"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("viagens");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/EncerrarOperacaoTransporte", "Viagens"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/EncerrarOperacaoTransporte", "ArrayOfViagem"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pagamentos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/EncerrarOperacaoTransporte", "Pagamentos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/EncerrarOperacaoTransporte", "ArrayOfPagamento"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("impostos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Impostos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Impostos"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantidadeSaques");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "QuantidadeSaques"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantidadeTransferencias");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "QuantidadeTransferencias"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valorSaques");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ValorSaques"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valorTransferencias");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ValorTransferencias"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
