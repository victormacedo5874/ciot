/**
 * DiferencaDeFrete.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.objects;

public class DiferencaDeFrete  implements java.io.Serializable {
    private br.adm.ipc.schemas.efrete.pef.objects.DiferencaFreteTipo tipo;

    private br.adm.ipc.schemas.efrete.pef.objects.DiferencaFreteBaseCalculo base;

    private br.adm.ipc.schemas.efrete.pef.objects.DiferencaFreteTolerancia tolerancia;

    private br.adm.ipc.schemas.efrete.pef.objects.DiferencaFreteMargem margemGanho;

    private br.adm.ipc.schemas.efrete.pef.objects.DiferencaFreteMargem margemPerda;

    public DiferencaDeFrete() {
    }

    public DiferencaDeFrete(
           br.adm.ipc.schemas.efrete.pef.objects.DiferencaFreteTipo tipo,
           br.adm.ipc.schemas.efrete.pef.objects.DiferencaFreteBaseCalculo base,
           br.adm.ipc.schemas.efrete.pef.objects.DiferencaFreteTolerancia tolerancia,
           br.adm.ipc.schemas.efrete.pef.objects.DiferencaFreteMargem margemGanho,
           br.adm.ipc.schemas.efrete.pef.objects.DiferencaFreteMargem margemPerda) {
           this.tipo = tipo;
           this.base = base;
           this.tolerancia = tolerancia;
           this.margemGanho = margemGanho;
           this.margemPerda = margemPerda;
    }


    /**
     * Gets the tipo value for this DiferencaDeFrete.
     * 
     * @return tipo
     */
    public br.adm.ipc.schemas.efrete.pef.objects.DiferencaFreteTipo getTipo() {
        return tipo;
    }


    /**
     * Sets the tipo value for this DiferencaDeFrete.
     * 
     * @param tipo
     */
    public void setTipo(br.adm.ipc.schemas.efrete.pef.objects.DiferencaFreteTipo tipo) {
        this.tipo = tipo;
    }


    /**
     * Gets the base value for this DiferencaDeFrete.
     * 
     * @return base
     */
    public br.adm.ipc.schemas.efrete.pef.objects.DiferencaFreteBaseCalculo getBase() {
        return base;
    }


    /**
     * Sets the base value for this DiferencaDeFrete.
     * 
     * @param base
     */
    public void setBase(br.adm.ipc.schemas.efrete.pef.objects.DiferencaFreteBaseCalculo base) {
        this.base = base;
    }


    /**
     * Gets the tolerancia value for this DiferencaDeFrete.
     * 
     * @return tolerancia
     */
    public br.adm.ipc.schemas.efrete.pef.objects.DiferencaFreteTolerancia getTolerancia() {
        return tolerancia;
    }


    /**
     * Sets the tolerancia value for this DiferencaDeFrete.
     * 
     * @param tolerancia
     */
    public void setTolerancia(br.adm.ipc.schemas.efrete.pef.objects.DiferencaFreteTolerancia tolerancia) {
        this.tolerancia = tolerancia;
    }


    /**
     * Gets the margemGanho value for this DiferencaDeFrete.
     * 
     * @return margemGanho
     */
    public br.adm.ipc.schemas.efrete.pef.objects.DiferencaFreteMargem getMargemGanho() {
        return margemGanho;
    }


    /**
     * Sets the margemGanho value for this DiferencaDeFrete.
     * 
     * @param margemGanho
     */
    public void setMargemGanho(br.adm.ipc.schemas.efrete.pef.objects.DiferencaFreteMargem margemGanho) {
        this.margemGanho = margemGanho;
    }


    /**
     * Gets the margemPerda value for this DiferencaDeFrete.
     * 
     * @return margemPerda
     */
    public br.adm.ipc.schemas.efrete.pef.objects.DiferencaFreteMargem getMargemPerda() {
        return margemPerda;
    }


    /**
     * Sets the margemPerda value for this DiferencaDeFrete.
     * 
     * @param margemPerda
     */
    public void setMargemPerda(br.adm.ipc.schemas.efrete.pef.objects.DiferencaFreteMargem margemPerda) {
        this.margemPerda = margemPerda;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DiferencaDeFrete)) return false;
        DiferencaDeFrete other = (DiferencaDeFrete) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.tipo==null && other.getTipo()==null) || 
             (this.tipo!=null &&
              this.tipo.equals(other.getTipo()))) &&
            ((this.base==null && other.getBase()==null) || 
             (this.base!=null &&
              this.base.equals(other.getBase()))) &&
            ((this.tolerancia==null && other.getTolerancia()==null) || 
             (this.tolerancia!=null &&
              this.tolerancia.equals(other.getTolerancia()))) &&
            ((this.margemGanho==null && other.getMargemGanho()==null) || 
             (this.margemGanho!=null &&
              this.margemGanho.equals(other.getMargemGanho()))) &&
            ((this.margemPerda==null && other.getMargemPerda()==null) || 
             (this.margemPerda!=null &&
              this.margemPerda.equals(other.getMargemPerda())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTipo() != null) {
            _hashCode += getTipo().hashCode();
        }
        if (getBase() != null) {
            _hashCode += getBase().hashCode();
        }
        if (getTolerancia() != null) {
            _hashCode += getTolerancia().hashCode();
        }
        if (getMargemGanho() != null) {
            _hashCode += getMargemGanho().hashCode();
        }
        if (getMargemPerda() != null) {
            _hashCode += getMargemPerda().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DiferencaDeFrete.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DiferencaDeFrete"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Tipo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DiferencaFreteTipo"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("base");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Base"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DiferencaFreteBaseCalculo"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tolerancia");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Tolerancia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DiferencaFreteTolerancia"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("margemGanho");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "MargemGanho"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DiferencaFreteMargem"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("margemPerda");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "MargemPerda"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "DiferencaFreteMargem"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
