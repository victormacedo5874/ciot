/**
 * RegistrarPagamentoResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.objects;

public class RegistrarPagamentoResponse  extends br.adm.ipc.schemas.efrete.objects.Response  implements java.io.Serializable {
    private java.lang.String codigoIdentificacaoOperacao;

    private java.lang.String transacaoId;

    private java.math.BigDecimal valorLiquido;

    public RegistrarPagamentoResponse() {
    }

    public RegistrarPagamentoResponse(
           int versao,
           boolean sucesso,
           br.adm.ipc.schemas.efrete.objects.Excecao excecao,
           java.lang.Long protocoloServico,
           java.lang.String codigoIdentificacaoOperacao,
           java.lang.String transacaoId,
           java.math.BigDecimal valorLiquido) {
        super(
            versao,
            sucesso,
            excecao,
            protocoloServico);
        this.codigoIdentificacaoOperacao = codigoIdentificacaoOperacao;
        this.transacaoId = transacaoId;
        this.valorLiquido = valorLiquido;
    }


    /**
     * Gets the codigoIdentificacaoOperacao value for this RegistrarPagamentoResponse.
     * 
     * @return codigoIdentificacaoOperacao
     */
    public java.lang.String getCodigoIdentificacaoOperacao() {
        return codigoIdentificacaoOperacao;
    }


    /**
     * Sets the codigoIdentificacaoOperacao value for this RegistrarPagamentoResponse.
     * 
     * @param codigoIdentificacaoOperacao
     */
    public void setCodigoIdentificacaoOperacao(java.lang.String codigoIdentificacaoOperacao) {
        this.codigoIdentificacaoOperacao = codigoIdentificacaoOperacao;
    }


    /**
     * Gets the transacaoId value for this RegistrarPagamentoResponse.
     * 
     * @return transacaoId
     */
    public java.lang.String getTransacaoId() {
        return transacaoId;
    }


    /**
     * Sets the transacaoId value for this RegistrarPagamentoResponse.
     * 
     * @param transacaoId
     */
    public void setTransacaoId(java.lang.String transacaoId) {
        this.transacaoId = transacaoId;
    }


    /**
     * Gets the valorLiquido value for this RegistrarPagamentoResponse.
     * 
     * @return valorLiquido
     */
    public java.math.BigDecimal getValorLiquido() {
        return valorLiquido;
    }


    /**
     * Sets the valorLiquido value for this RegistrarPagamentoResponse.
     * 
     * @param valorLiquido
     */
    public void setValorLiquido(java.math.BigDecimal valorLiquido) {
        this.valorLiquido = valorLiquido;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RegistrarPagamentoResponse)) return false;
        RegistrarPagamentoResponse other = (RegistrarPagamentoResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.codigoIdentificacaoOperacao==null && other.getCodigoIdentificacaoOperacao()==null) || 
             (this.codigoIdentificacaoOperacao!=null &&
              this.codigoIdentificacaoOperacao.equals(other.getCodigoIdentificacaoOperacao()))) &&
            ((this.transacaoId==null && other.getTransacaoId()==null) || 
             (this.transacaoId!=null &&
              this.transacaoId.equals(other.getTransacaoId()))) &&
            ((this.valorLiquido==null && other.getValorLiquido()==null) || 
             (this.valorLiquido!=null &&
              this.valorLiquido.equals(other.getValorLiquido())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCodigoIdentificacaoOperacao() != null) {
            _hashCode += getCodigoIdentificacaoOperacao().hashCode();
        }
        if (getTransacaoId() != null) {
            _hashCode += getTransacaoId().hashCode();
        }
        if (getValorLiquido() != null) {
            _hashCode += getValorLiquido().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RegistrarPagamentoResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "RegistrarPagamentoResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoIdentificacaoOperacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "CodigoIdentificacaoOperacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transacaoId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "TransacaoId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valorLiquido");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ValorLiquido"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
