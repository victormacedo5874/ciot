/**
 * ObterOperacaoTransporteParaImpressaoResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.objects;

public class ObterOperacaoTransporteParaImpressaoResponse  extends br.adm.ipc.schemas.efrete.objects.Response  implements java.io.Serializable {
    private java.lang.String html;

    public ObterOperacaoTransporteParaImpressaoResponse() {
    }

    public ObterOperacaoTransporteParaImpressaoResponse(
           int versao,
           boolean sucesso,
           br.adm.ipc.schemas.efrete.objects.Excecao excecao,
           java.lang.Long protocoloServico,
           java.lang.String html) {
        super(
            versao,
            sucesso,
            excecao,
            protocoloServico);
        this.html = html;
    }


    /**
     * Gets the html value for this ObterOperacaoTransporteParaImpressaoResponse.
     * 
     * @return html
     */
    public java.lang.String getHtml() {
        return html;
    }


    /**
     * Sets the html value for this ObterOperacaoTransporteParaImpressaoResponse.
     * 
     * @param html
     */
    public void setHtml(java.lang.String html) {
        this.html = html;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ObterOperacaoTransporteParaImpressaoResponse)) return false;
        ObterOperacaoTransporteParaImpressaoResponse other = (ObterOperacaoTransporteParaImpressaoResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.html==null && other.getHtml()==null) || 
             (this.html!=null &&
              this.html.equals(other.getHtml())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getHtml() != null) {
            _hashCode += getHtml().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ObterOperacaoTransporteParaImpressaoResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "ObterOperacaoTransporteParaImpressaoResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("html");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Html"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
