/**
 * Motorista.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte;

public class Motorista  implements java.io.Serializable {
    private String cpfOuCnpj;

    private long CNH;

    private br.adm.ipc.schemas.efrete.objects.Telefone celular;

    public Motorista() {
    }

    public Motorista(
           String cpfOuCnpj,
           long CNH,
           br.adm.ipc.schemas.efrete.objects.Telefone celular) {
           this.cpfOuCnpj = cpfOuCnpj;
           this.CNH = CNH;
           this.celular = celular;
    }


    /**
     * Gets the cpfOuCnpj value for this Motorista.
     * 
     * @return cpfOuCnpj
     */
    public String getCpfOuCnpj() {
        return cpfOuCnpj;
    }


    /**
     * Sets the cpfOuCnpj value for this Motorista.
     * 
     * @param cpfOuCnpj
     */
    public void setCpfOuCnpj(String cpfOuCnpj) {
        this.cpfOuCnpj = cpfOuCnpj;
    }


    /**
     * Gets the CNH value for this Motorista.
     * 
     * @return CNH
     */
    public long getCNH() {
        return CNH;
    }


    /**
     * Sets the CNH value for this Motorista.
     * 
     * @param CNH
     */
    public void setCNH(long CNH) {
        this.CNH = CNH;
    }


    /**
     * Gets the celular value for this Motorista.
     * 
     * @return celular
     */
    public br.adm.ipc.schemas.efrete.objects.Telefone getCelular() {
        return celular;
    }


    /**
     * Sets the celular value for this Motorista.
     * 
     * @param celular
     */
    public void setCelular(br.adm.ipc.schemas.efrete.objects.Telefone celular) {
        this.celular = celular;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Motorista)) return false;
        Motorista other = (Motorista) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.cpfOuCnpj == other.getCpfOuCnpj() &&
            this.CNH == other.getCNH() &&
            ((this.celular==null && other.getCelular()==null) || 
             (this.celular!=null &&
              this.celular.equals(other.getCelular())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += new Long(getCpfOuCnpj()).hashCode();
        _hashCode += new Long(getCNH()).hashCode();
        if (getCelular() != null) {
            _hashCode += getCelular().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Motorista.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarOperacaoTransporte", "Motorista"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cpfOuCnpj");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarOperacaoTransporte", "CpfOuCnpj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CNH");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/AdicionarOperacaoTransporte", "CNH"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("celular");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/pef/objects", "Celular"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/objects", "Telefone"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
