package br.adm.ipc.schemas.efrete.proprietarios;

public class ProprietariosServiceSoapProxy implements br.adm.ipc.schemas.efrete.proprietarios.ProprietariosServiceSoap {
  private String _endpoint = null;
  private br.adm.ipc.schemas.efrete.proprietarios.ProprietariosServiceSoap proprietariosServiceSoap = null;
  
  public ProprietariosServiceSoapProxy() {
    _initProprietariosServiceSoapProxy();
  }
  
  public ProprietariosServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initProprietariosServiceSoapProxy();
  }
  
  private void _initProprietariosServiceSoapProxy() {
    try {
      proprietariosServiceSoap = (new br.adm.ipc.schemas.efrete.proprietarios.ProprietariosServiceLocator()).getProprietariosServiceSoap();
      if (proprietariosServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)proprietariosServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)proprietariosServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (proprietariosServiceSoap != null)
      ((javax.xml.rpc.Stub)proprietariosServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public br.adm.ipc.schemas.efrete.proprietarios.ProprietariosServiceSoap getProprietariosServiceSoap() {
    if (proprietariosServiceSoap == null)
      _initProprietariosServiceSoapProxy();
    return proprietariosServiceSoap;
  }
  
  public br.adm.ipc.schemas.efrete.proprietarios.objects.ObterResponse obter(br.adm.ipc.schemas.efrete.proprietarios.objects.ObterRequest obterRequest) throws java.rmi.RemoteException{
    if (proprietariosServiceSoap == null)
      _initProprietariosServiceSoapProxy();
    return proprietariosServiceSoap.obter(obterRequest);
  }
  
  public br.adm.ipc.schemas.efrete.proprietarios.objects.GravarResponse gravar(br.adm.ipc.schemas.efrete.proprietarios.objects.GravarRequest gravarRequest) throws java.rmi.RemoteException{
    if (proprietariosServiceSoap == null)
      _initProprietariosServiceSoapProxy();
    return proprietariosServiceSoap.gravar(gravarRequest);
  }
  
  
}