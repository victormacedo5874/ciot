/**
 * ProprietariosServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.proprietarios;

public interface ProprietariosServiceSoap extends java.rmi.Remote {
    public br.adm.ipc.schemas.efrete.proprietarios.objects.ObterResponse obter(br.adm.ipc.schemas.efrete.proprietarios.objects.ObterRequest obterRequest) throws java.rmi.RemoteException;
    public br.adm.ipc.schemas.efrete.proprietarios.objects.GravarResponse gravar(br.adm.ipc.schemas.efrete.proprietarios.objects.GravarRequest gravarRequest) throws java.rmi.RemoteException;
}
