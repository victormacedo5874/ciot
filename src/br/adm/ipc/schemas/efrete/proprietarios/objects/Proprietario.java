/**
 * Proprietario.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.proprietarios.objects;

public class Proprietario  implements java.io.Serializable {
    private long CNPJ;

    private br.adm.ipc.schemas.efrete.proprietarios.objects.TipoPessoa tipoPessoa;

    private java.lang.String razaoSocial;

    private long RNTRC;

    private br.adm.ipc.schemas.efrete.proprietarios.objects.TipoTransportador tipo;

    private boolean TACouEquiparado;

    private br.adm.ipc.schemas.efrete.objects.Endereco endereco;

    private br.adm.ipc.schemas.efrete.objects.Telefones telefones;

    private java.util.Calendar dataValidadeRNTRC;

    private boolean RNTRCAtivo;

    public Proprietario() {
    }

    public Proprietario(
           long CNPJ,
           br.adm.ipc.schemas.efrete.proprietarios.objects.TipoPessoa tipoPessoa,
           java.lang.String razaoSocial,
           long RNTRC,
           br.adm.ipc.schemas.efrete.proprietarios.objects.TipoTransportador tipo,
           boolean TACouEquiparado,
           br.adm.ipc.schemas.efrete.objects.Endereco endereco,
           br.adm.ipc.schemas.efrete.objects.Telefones telefones,
           java.util.Calendar dataValidadeRNTRC,
           boolean RNTRCAtivo) {
           this.CNPJ = CNPJ;
           this.tipoPessoa = tipoPessoa;
           this.razaoSocial = razaoSocial;
           this.RNTRC = RNTRC;
           this.tipo = tipo;
           this.TACouEquiparado = TACouEquiparado;
           this.endereco = endereco;
           this.telefones = telefones;
           this.dataValidadeRNTRC = dataValidadeRNTRC;
           this.RNTRCAtivo = RNTRCAtivo;
    }


    /**
     * Gets the CNPJ value for this Proprietario.
     * 
     * @return CNPJ
     */
    public long getCNPJ() {
        return CNPJ;
    }


    /**
     * Sets the CNPJ value for this Proprietario.
     * 
     * @param CNPJ
     */
    public void setCNPJ(long CNPJ) {
        this.CNPJ = CNPJ;
    }


    /**
     * Gets the tipoPessoa value for this Proprietario.
     * 
     * @return tipoPessoa
     */
    public br.adm.ipc.schemas.efrete.proprietarios.objects.TipoPessoa getTipoPessoa() {
        return tipoPessoa;
    }


    /**
     * Sets the tipoPessoa value for this Proprietario.
     * 
     * @param tipoPessoa
     */
    public void setTipoPessoa(br.adm.ipc.schemas.efrete.proprietarios.objects.TipoPessoa tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }


    /**
     * Gets the razaoSocial value for this Proprietario.
     * 
     * @return razaoSocial
     */
    public java.lang.String getRazaoSocial() {
        return razaoSocial;
    }


    /**
     * Sets the razaoSocial value for this Proprietario.
     * 
     * @param razaoSocial
     */
    public void setRazaoSocial(java.lang.String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }


    /**
     * Gets the RNTRC value for this Proprietario.
     * 
     * @return RNTRC
     */
    public long getRNTRC() {
        return RNTRC;
    }


    /**
     * Sets the RNTRC value for this Proprietario.
     * 
     * @param RNTRC
     */
    public void setRNTRC(long RNTRC) {
        this.RNTRC = RNTRC;
    }


    /**
     * Gets the tipo value for this Proprietario.
     * 
     * @return tipo
     */
    public br.adm.ipc.schemas.efrete.proprietarios.objects.TipoTransportador getTipo() {
        return tipo;
    }


    /**
     * Sets the tipo value for this Proprietario.
     * 
     * @param tipo
     */
    public void setTipo(br.adm.ipc.schemas.efrete.proprietarios.objects.TipoTransportador tipo) {
        this.tipo = tipo;
    }


    /**
     * Gets the TACouEquiparado value for this Proprietario.
     * 
     * @return TACouEquiparado
     */
    public boolean isTACouEquiparado() {
        return TACouEquiparado;
    }


    /**
     * Sets the TACouEquiparado value for this Proprietario.
     * 
     * @param TACouEquiparado
     */
    public void setTACouEquiparado(boolean TACouEquiparado) {
        this.TACouEquiparado = TACouEquiparado;
    }


    /**
     * Gets the endereco value for this Proprietario.
     * 
     * @return endereco
     */
    public br.adm.ipc.schemas.efrete.objects.Endereco getEndereco() {
        return endereco;
    }


    /**
     * Sets the endereco value for this Proprietario.
     * 
     * @param endereco
     */
    public void setEndereco(br.adm.ipc.schemas.efrete.objects.Endereco endereco) {
        this.endereco = endereco;
    }


    /**
     * Gets the telefones value for this Proprietario.
     * 
     * @return telefones
     */
    public br.adm.ipc.schemas.efrete.objects.Telefones getTelefones() {
        return telefones;
    }


    /**
     * Sets the telefones value for this Proprietario.
     * 
     * @param telefones
     */
    public void setTelefones(br.adm.ipc.schemas.efrete.objects.Telefones telefones) {
        this.telefones = telefones;
    }


    /**
     * Gets the dataValidadeRNTRC value for this Proprietario.
     * 
     * @return dataValidadeRNTRC
     */
    public java.util.Calendar getDataValidadeRNTRC() {
        return dataValidadeRNTRC;
    }


    /**
     * Sets the dataValidadeRNTRC value for this Proprietario.
     * 
     * @param dataValidadeRNTRC
     */
    public void setDataValidadeRNTRC(java.util.Calendar dataValidadeRNTRC) {
        this.dataValidadeRNTRC = dataValidadeRNTRC;
    }


    /**
     * Gets the RNTRCAtivo value for this Proprietario.
     * 
     * @return RNTRCAtivo
     */
    public boolean isRNTRCAtivo() {
        return RNTRCAtivo;
    }


    /**
     * Sets the RNTRCAtivo value for this Proprietario.
     * 
     * @param RNTRCAtivo
     */
    public void setRNTRCAtivo(boolean RNTRCAtivo) {
        this.RNTRCAtivo = RNTRCAtivo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Proprietario)) return false;
        Proprietario other = (Proprietario) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.CNPJ == other.getCNPJ() &&
            ((this.tipoPessoa==null && other.getTipoPessoa()==null) || 
             (this.tipoPessoa!=null &&
              this.tipoPessoa.equals(other.getTipoPessoa()))) &&
            ((this.razaoSocial==null && other.getRazaoSocial()==null) || 
             (this.razaoSocial!=null &&
              this.razaoSocial.equals(other.getRazaoSocial()))) &&
            this.RNTRC == other.getRNTRC() &&
            ((this.tipo==null && other.getTipo()==null) || 
             (this.tipo!=null &&
              this.tipo.equals(other.getTipo()))) &&
            this.TACouEquiparado == other.isTACouEquiparado() &&
            ((this.endereco==null && other.getEndereco()==null) || 
             (this.endereco!=null &&
              this.endereco.equals(other.getEndereco()))) &&
            ((this.telefones==null && other.getTelefones()==null) || 
             (this.telefones!=null &&
              this.telefones.equals(other.getTelefones()))) &&
            ((this.dataValidadeRNTRC==null && other.getDataValidadeRNTRC()==null) || 
             (this.dataValidadeRNTRC!=null &&
              this.dataValidadeRNTRC.equals(other.getDataValidadeRNTRC()))) &&
            this.RNTRCAtivo == other.isRNTRCAtivo();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += new Long(getCNPJ()).hashCode();
        if (getTipoPessoa() != null) {
            _hashCode += getTipoPessoa().hashCode();
        }
        if (getRazaoSocial() != null) {
            _hashCode += getRazaoSocial().hashCode();
        }
        _hashCode += new Long(getRNTRC()).hashCode();
        if (getTipo() != null) {
            _hashCode += getTipo().hashCode();
        }
        _hashCode += (isTACouEquiparado() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getEndereco() != null) {
            _hashCode += getEndereco().hashCode();
        }
        if (getTelefones() != null) {
            _hashCode += getTelefones().hashCode();
        }
        if (getDataValidadeRNTRC() != null) {
            _hashCode += getDataValidadeRNTRC().hashCode();
        }
        _hashCode += (isRNTRCAtivo() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Proprietario.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/proprietarios/objects", "Proprietario"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CNPJ");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/proprietarios/objects", "CNPJ"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoPessoa");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/proprietarios/objects", "TipoPessoa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/proprietarios/objects", "TipoPessoa"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("razaoSocial");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/proprietarios/objects", "RazaoSocial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RNTRC");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/proprietarios/objects", "RNTRC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/proprietarios/objects", "Tipo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/proprietarios/objects", "TipoTransportador"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TACouEquiparado");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/proprietarios/objects", "TACouEquiparado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endereco");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/proprietarios/objects", "Endereco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/objects", "Endereco"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("telefones");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/proprietarios/objects", "Telefones"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/objects", "Telefones"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataValidadeRNTRC");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/proprietarios/objects", "DataValidadeRNTRC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RNTRCAtivo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/proprietarios/objects", "RNTRCAtivo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
