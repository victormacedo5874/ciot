/**
 * GravarRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.proprietarios.objects;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class GravarRequest  extends br.adm.ipc.schemas.efrete.objects.Request  implements java.io.Serializable {
    private String CNPJ;

    private br.adm.ipc.schemas.efrete.proprietarios.objects.TipoPessoa tipoPessoa;

    private java.lang.String razaoSocial;

    private String RNTRC;

    private br.adm.ipc.schemas.efrete.objects.Endereco endereco;

    private br.adm.ipc.schemas.efrete.objects.Telefones telefones;

    public GravarRequest() {
    }

    public GravarRequest(
           java.lang.String token,
           java.lang.String integrador,
           int versao,
           String CNPJ,
           br.adm.ipc.schemas.efrete.proprietarios.objects.TipoPessoa tipoPessoa,
           java.lang.String razaoSocial,
           String RNTRC,
           br.adm.ipc.schemas.efrete.objects.Endereco endereco,
           br.adm.ipc.schemas.efrete.objects.Telefones telefones) {
        super(
            token,
            integrador,
            versao);
        this.CNPJ = CNPJ;
        this.tipoPessoa = tipoPessoa;
        this.razaoSocial = razaoSocial;
        this.RNTRC = RNTRC;
        this.endereco = endereco;
        this.telefones = telefones;
    }


    /**
     * Gets the CNPJ value for this GravarRequest.
     * 
     * @return CNPJ
     */
    public String getCNPJ() {
        return CNPJ;
    }


    /**
     * Sets the CNPJ value for this GravarRequest.
     * 
     * @param CNPJ
     */
    public void setCNPJ(String CNPJ) {
        this.CNPJ = CNPJ;
    }


    /**
     * Gets the tipoPessoa value for this GravarRequest.
     * 
     * @return tipoPessoa
     */
    public br.adm.ipc.schemas.efrete.proprietarios.objects.TipoPessoa getTipoPessoa() {
        return tipoPessoa;
    }


    /**
     * Sets the tipoPessoa value for this GravarRequest.
     * 
     * @param tipoPessoa
     */
    public void setTipoPessoa(br.adm.ipc.schemas.efrete.proprietarios.objects.TipoPessoa tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }


    /**
     * Gets the razaoSocial value for this GravarRequest.
     * 
     * @return razaoSocial
     */
    public java.lang.String getRazaoSocial() {
        return razaoSocial;
    }


    /**
     * Sets the razaoSocial value for this GravarRequest.
     * 
     * @param razaoSocial
     */
    public void setRazaoSocial(java.lang.String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }


    /**
     * Gets the RNTRC value for this GravarRequest.
     * 
     * @return RNTRC
     */
    public String getRNTRC() {
        return RNTRC;
    }


    /**
     * Sets the RNTRC value for this GravarRequest.
     * 
     * @param RNTRC
     */
    public void setRNTRC(String RNTRC) {
        this.RNTRC = RNTRC;
    }


    /**
     * Gets the endereco value for this GravarRequest.
     * 
     * @return endereco
     */
    public br.adm.ipc.schemas.efrete.objects.Endereco getEndereco() {
        return endereco;
    }


    /**
     * Sets the endereco value for this GravarRequest.
     * 
     * @param endereco
     */
    public void setEndereco(br.adm.ipc.schemas.efrete.objects.Endereco endereco) {
        this.endereco = endereco;
    }


    /**
     * Gets the telefones value for this GravarRequest.
     * 
     * @return telefones
     */
    public br.adm.ipc.schemas.efrete.objects.Telefones getTelefones() {
        return telefones;
    }


    /**
     * Sets the telefones value for this GravarRequest.
     * 
     * @param telefones
     */
    public void setTelefones(br.adm.ipc.schemas.efrete.objects.Telefones telefones) {
        this.telefones = telefones;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GravarRequest)) return false;
        GravarRequest other = (GravarRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.CNPJ == other.getCNPJ() &&
            ((this.tipoPessoa==null && other.getTipoPessoa()==null) || 
             (this.tipoPessoa!=null &&
              this.tipoPessoa.equals(other.getTipoPessoa()))) &&
            ((this.razaoSocial==null && other.getRazaoSocial()==null) || 
             (this.razaoSocial!=null &&
              this.razaoSocial.equals(other.getRazaoSocial()))) &&
            this.RNTRC == other.getRNTRC() &&
            ((this.endereco==null && other.getEndereco()==null) || 
             (this.endereco!=null &&
              this.endereco.equals(other.getEndereco()))) &&
            ((this.telefones==null && other.getTelefones()==null) || 
             (this.telefones!=null &&
              this.telefones.equals(other.getTelefones())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += new Long(getCNPJ()).hashCode();
        if (getTipoPessoa() != null) {
            _hashCode += getTipoPessoa().hashCode();
        }
        if (getRazaoSocial() != null) {
            _hashCode += getRazaoSocial().hashCode();
        }
        _hashCode += new Long(getRNTRC()).hashCode();
        if (getEndereco() != null) {
            _hashCode += getEndereco().hashCode();
        }
        if (getTelefones() != null) {
            _hashCode += getTelefones().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GravarRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/proprietarios/objects", "GravarRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CNPJ");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/proprietarios/objects", "CNPJ"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoPessoa");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/proprietarios/objects", "TipoPessoa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/proprietarios/objects", "TipoPessoa"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("razaoSocial");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/proprietarios/objects", "RazaoSocial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RNTRC");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/proprietarios/objects", "RNTRC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endereco");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/proprietarios/objects", "Endereco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/objects", "Endereco"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("telefones");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/proprietarios/objects", "Telefones"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/objects", "Telefones"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
