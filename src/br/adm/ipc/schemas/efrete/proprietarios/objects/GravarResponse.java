/**
 * GravarResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.proprietarios.objects;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class GravarResponse  extends br.adm.ipc.schemas.efrete.objects.Response  implements java.io.Serializable {
    private br.adm.ipc.schemas.efrete.proprietarios.objects.Proprietario proprietario;

    public GravarResponse() {
    }

    public GravarResponse(
           int versao,
           boolean sucesso,
           br.adm.ipc.schemas.efrete.objects.Excecao excecao,
           java.lang.Long protocoloServico,
           br.adm.ipc.schemas.efrete.proprietarios.objects.Proprietario proprietario) {
        super(
            versao,
            sucesso,
            excecao,
            protocoloServico);
        this.proprietario = proprietario;
    }


    /**
     * Gets the proprietario value for this GravarResponse.
     * 
     * @return proprietario
     */
    public br.adm.ipc.schemas.efrete.proprietarios.objects.Proprietario getProprietario() {
        return proprietario;
    }


    /**
     * Sets the proprietario value for this GravarResponse.
     * 
     * @param proprietario
     */
    public void setProprietario(br.adm.ipc.schemas.efrete.proprietarios.objects.Proprietario proprietario) {
        this.proprietario = proprietario;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GravarResponse)) return false;
        GravarResponse other = (GravarResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.proprietario==null && other.getProprietario()==null) || 
             (this.proprietario!=null &&
              this.proprietario.equals(other.getProprietario())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getProprietario() != null) {
            _hashCode += getProprietario().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GravarResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/proprietarios/objects", "GravarResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("proprietario");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/proprietarios/objects", "Proprietario"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/proprietarios/objects", "Proprietario"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
