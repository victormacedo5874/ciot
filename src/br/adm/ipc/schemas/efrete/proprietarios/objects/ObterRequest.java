/**
 * ObterRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.proprietarios.objects;

public class ObterRequest  extends br.adm.ipc.schemas.efrete.objects.Request  implements java.io.Serializable {
    private long CNPJ;

    private br.adm.ipc.schemas.efrete.proprietarios.objects.TipoPessoa tipoPessoa;

    private long RNTRC;

    public ObterRequest() {
    }

    public ObterRequest(
           java.lang.String token,
           java.lang.String integrador,
           int versao,
           long CNPJ,
           br.adm.ipc.schemas.efrete.proprietarios.objects.TipoPessoa tipoPessoa,
           long RNTRC) {
        super(
            token,
            integrador,
            versao);
        this.CNPJ = CNPJ;
        this.tipoPessoa = tipoPessoa;
        this.RNTRC = RNTRC;
    }


    /**
     * Gets the CNPJ value for this ObterRequest.
     * 
     * @return CNPJ
     */
    public long getCNPJ() {
        return CNPJ;
    }


    /**
     * Sets the CNPJ value for this ObterRequest.
     * 
     * @param CNPJ
     */
    public void setCNPJ(long CNPJ) {
        this.CNPJ = CNPJ;
    }


    /**
     * Gets the tipoPessoa value for this ObterRequest.
     * 
     * @return tipoPessoa
     */
    public br.adm.ipc.schemas.efrete.proprietarios.objects.TipoPessoa getTipoPessoa() {
        return tipoPessoa;
    }


    /**
     * Sets the tipoPessoa value for this ObterRequest.
     * 
     * @param tipoPessoa
     */
    public void setTipoPessoa(br.adm.ipc.schemas.efrete.proprietarios.objects.TipoPessoa tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }


    /**
     * Gets the RNTRC value for this ObterRequest.
     * 
     * @return RNTRC
     */
    public long getRNTRC() {
        return RNTRC;
    }


    /**
     * Sets the RNTRC value for this ObterRequest.
     * 
     * @param RNTRC
     */
    public void setRNTRC(long RNTRC) {
        this.RNTRC = RNTRC;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ObterRequest)) return false;
        ObterRequest other = (ObterRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.CNPJ == other.getCNPJ() &&
            ((this.tipoPessoa==null && other.getTipoPessoa()==null) || 
             (this.tipoPessoa!=null &&
              this.tipoPessoa.equals(other.getTipoPessoa()))) &&
            this.RNTRC == other.getRNTRC();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += new Long(getCNPJ()).hashCode();
        if (getTipoPessoa() != null) {
            _hashCode += getTipoPessoa().hashCode();
        }
        _hashCode += new Long(getRNTRC()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ObterRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/proprietarios/objects", "ObterRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CNPJ");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/proprietarios/objects", "CNPJ"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoPessoa");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/proprietarios/objects", "TipoPessoa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/proprietarios/objects", "TipoPessoa"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RNTRC");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/proprietarios/objects", "RNTRC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
