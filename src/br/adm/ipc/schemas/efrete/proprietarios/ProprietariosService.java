/**
 * ProprietariosService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.proprietarios;

public interface ProprietariosService extends javax.xml.rpc.Service {
    public java.lang.String getProprietariosServiceSoapAddress();

    public br.adm.ipc.schemas.efrete.proprietarios.ProprietariosServiceSoap getProprietariosServiceSoap() throws javax.xml.rpc.ServiceException;

    public br.adm.ipc.schemas.efrete.proprietarios.ProprietariosServiceSoap getProprietariosServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
