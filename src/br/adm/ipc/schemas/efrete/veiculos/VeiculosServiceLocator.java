/**
 * VeiculosServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.veiculos;

public class VeiculosServiceLocator extends org.apache.axis.client.Service implements br.adm.ipc.schemas.efrete.veiculos.VeiculosService {

    public VeiculosServiceLocator() {
    }


    public VeiculosServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public VeiculosServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for VeiculosServiceSoap
//    private java.lang.String VeiculosServiceSoap_address = "https://dev.efrete.com.br/Services/VeiculosService.asmx";
    private java.lang.String VeiculosServiceSoap_address = "https://sistema.efrete.com.br/Services/VeiculosService.asmx";

    public java.lang.String getVeiculosServiceSoapAddress() {
        return VeiculosServiceSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String VeiculosServiceSoapWSDDServiceName = "VeiculosServiceSoap";

    public java.lang.String getVeiculosServiceSoapWSDDServiceName() {
        return VeiculosServiceSoapWSDDServiceName;
    }

    public void setVeiculosServiceSoapWSDDServiceName(java.lang.String name) {
        VeiculosServiceSoapWSDDServiceName = name;
    }

    public br.adm.ipc.schemas.efrete.veiculos.VeiculosServiceSoap getVeiculosServiceSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(VeiculosServiceSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getVeiculosServiceSoap(endpoint);
    }

    public br.adm.ipc.schemas.efrete.veiculos.VeiculosServiceSoap getVeiculosServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            br.adm.ipc.schemas.efrete.veiculos.VeiculosServiceSoapStub _stub = new br.adm.ipc.schemas.efrete.veiculos.VeiculosServiceSoapStub(portAddress, this);
            _stub.setPortName(getVeiculosServiceSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setVeiculosServiceSoapEndpointAddress(java.lang.String address) {
        VeiculosServiceSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (br.adm.ipc.schemas.efrete.veiculos.VeiculosServiceSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                br.adm.ipc.schemas.efrete.veiculos.VeiculosServiceSoapStub _stub = new br.adm.ipc.schemas.efrete.veiculos.VeiculosServiceSoapStub(new java.net.URL(VeiculosServiceSoap_address), this);
                _stub.setPortName(getVeiculosServiceSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("VeiculosServiceSoap".equals(inputPortName)) {
            return getVeiculosServiceSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/veiculos", "VeiculosService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/veiculos", "VeiculosServiceSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("VeiculosServiceSoap".equals(portName)) {
            setVeiculosServiceSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
