/**
 * VeiculosService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.veiculos;

public interface VeiculosService extends javax.xml.rpc.Service {
    public java.lang.String getVeiculosServiceSoapAddress();

    public br.adm.ipc.schemas.efrete.veiculos.VeiculosServiceSoap getVeiculosServiceSoap() throws javax.xml.rpc.ServiceException;

    public br.adm.ipc.schemas.efrete.veiculos.VeiculosServiceSoap getVeiculosServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
