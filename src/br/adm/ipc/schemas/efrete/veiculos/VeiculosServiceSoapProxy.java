package br.adm.ipc.schemas.efrete.veiculos;

public class VeiculosServiceSoapProxy implements br.adm.ipc.schemas.efrete.veiculos.VeiculosServiceSoap {
  private String _endpoint = null;
  private br.adm.ipc.schemas.efrete.veiculos.VeiculosServiceSoap veiculosServiceSoap = null;
  
  public VeiculosServiceSoapProxy() {
    _initVeiculosServiceSoapProxy();
  }
  
  public VeiculosServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initVeiculosServiceSoapProxy();
  }
  
  private void _initVeiculosServiceSoapProxy() {
    try {
      veiculosServiceSoap = (new br.adm.ipc.schemas.efrete.veiculos.VeiculosServiceLocator()).getVeiculosServiceSoap();
      if (veiculosServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)veiculosServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)veiculosServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (veiculosServiceSoap != null)
      ((javax.xml.rpc.Stub)veiculosServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public br.adm.ipc.schemas.efrete.veiculos.VeiculosServiceSoap getVeiculosServiceSoap() {
    if (veiculosServiceSoap == null)
      _initVeiculosServiceSoapProxy();
    return veiculosServiceSoap;
  }
  
  public br.adm.ipc.schemas.efrete.veiculos.objects.ObterPorPlacaResponse obterPorPlaca(br.adm.ipc.schemas.efrete.veiculos.objects.ObterPorPlacaRequest obterPorPlacaRequest) throws java.rmi.RemoteException{
    if (veiculosServiceSoap == null)
      _initVeiculosServiceSoapProxy();
    return veiculosServiceSoap.obterPorPlaca(obterPorPlacaRequest);
  }
  
  public br.adm.ipc.schemas.efrete.veiculos.objects.GravarResponse gravar(br.adm.ipc.schemas.efrete.veiculos.objects.GravarRequest gravarRequest) throws java.rmi.RemoteException{
    if (veiculosServiceSoap == null)
      _initVeiculosServiceSoapProxy();
    return veiculosServiceSoap.gravar(gravarRequest);
  }
  
  
}