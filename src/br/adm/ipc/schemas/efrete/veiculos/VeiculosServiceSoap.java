/**
 * VeiculosServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.veiculos;

public interface VeiculosServiceSoap extends java.rmi.Remote {
    public br.adm.ipc.schemas.efrete.veiculos.objects.ObterPorPlacaResponse obterPorPlaca(br.adm.ipc.schemas.efrete.veiculos.objects.ObterPorPlacaRequest obterPorPlacaRequest) throws java.rmi.RemoteException;
    public br.adm.ipc.schemas.efrete.veiculos.objects.GravarResponse gravar(br.adm.ipc.schemas.efrete.veiculos.objects.GravarRequest gravarRequest) throws java.rmi.RemoteException;
}
