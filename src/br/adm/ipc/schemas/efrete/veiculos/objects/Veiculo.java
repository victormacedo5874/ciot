/**
 * Veiculo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.veiculos.objects;

public class Veiculo  implements java.io.Serializable {
    private java.lang.String placa;

    private String renavam;

    private java.lang.String chassi;

    private String RNTRC;

    private java.lang.Integer numeroDeEixos;

    private String codigoMunicipio;

    private java.lang.String marca;

    private java.lang.String modelo;

    private java.lang.Integer anoFabricacao;

    private java.lang.Integer anoModelo;

    private java.lang.String cor;

    private java.lang.Integer tara;

    private java.lang.Integer capacidadeKg;

    private java.lang.Integer capacidadeM3;

    private br.adm.ipc.schemas.efrete.veiculos.objects.TipoRodado tipoRodado;

    private br.adm.ipc.schemas.efrete.veiculos.objects.TipoCarroceria tipoCarroceria;

    public Veiculo() {
    }

    public Veiculo(
           java.lang.String placa,
           String renavam,
           java.lang.String chassi,
           String RNTRC,
           java.lang.Integer numeroDeEixos,
           String codigoMunicipio,
           java.lang.String marca,
           java.lang.String modelo,
           java.lang.Integer anoFabricacao,
           java.lang.Integer anoModelo,
           java.lang.String cor,
           java.lang.Integer tara,
           java.lang.Integer capacidadeKg,
           java.lang.Integer capacidadeM3,
           br.adm.ipc.schemas.efrete.veiculos.objects.TipoRodado tipoRodado,
           br.adm.ipc.schemas.efrete.veiculos.objects.TipoCarroceria tipoCarroceria) {
           this.placa = placa;
           this.renavam = renavam;
           this.chassi = chassi;
           this.RNTRC = RNTRC;
           this.numeroDeEixos = numeroDeEixos;
           this.codigoMunicipio = codigoMunicipio;
           this.marca = marca;
           this.modelo = modelo;
           this.anoFabricacao = anoFabricacao;
           this.anoModelo = anoModelo;
           this.cor = cor;
           this.tara = tara;
           this.capacidadeKg = capacidadeKg;
           this.capacidadeM3 = capacidadeM3;
           this.tipoRodado = tipoRodado;
           this.tipoCarroceria = tipoCarroceria;
    }


    /**
     * Gets the placa value for this Veiculo.
     * 
     * @return placa
     */
    public java.lang.String getPlaca() {
        return placa;
    }


    /**
     * Sets the placa value for this Veiculo.
     * 
     * @param placa
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }


    /**
     * Gets the renavam value for this Veiculo.
     * 
     * @return renavam
     */
    public String getRenavam() {
        return renavam;
    }


    /**
     * Sets the renavam value for this Veiculo.
     * 
     * @param renavam
     */
    public void setRenavam(String renavam) {
        this.renavam = renavam;
    }


    /**
     * Gets the chassi value for this Veiculo.
     * 
     * @return chassi
     */
    public java.lang.String getChassi() {
        return chassi;
    }


    /**
     * Sets the chassi value for this Veiculo.
     * 
     * @param chassi
     */
    public void setChassi(java.lang.String chassi) {
        this.chassi = chassi;
    }


    /**
     * Gets the RNTRC value for this Veiculo.
     * 
     * @return RNTRC
     */
    public String getRNTRC() {
        return RNTRC;
    }


    /**
     * Sets the RNTRC value for this Veiculo.
     * 
     * @param RNTRC
     */
    public void setRNTRC(String RNTRC) {
        this.RNTRC = RNTRC;
    }


    /**
     * Gets the numeroDeEixos value for this Veiculo.
     * 
     * @return numeroDeEixos
     */
    public java.lang.Integer getNumeroDeEixos() {
        return numeroDeEixos;
    }


    /**
     * Sets the numeroDeEixos value for this Veiculo.
     * 
     * @param numeroDeEixos
     */
    public void setNumeroDeEixos(java.lang.Integer numeroDeEixos) {
        this.numeroDeEixos = numeroDeEixos;
    }


    /**
     * Gets the codigoMunicipio value for this Veiculo.
     * 
     * @return codigoMunicipio
     */
    public String getCodigoMunicipio() {
        return codigoMunicipio;
    }


    /**
     * Sets the codigoMunicipio value for this Veiculo.
     * 
     * @param codigoMunicipio
     */
    public void setCodigoMunicipio(String codigoMunicipio) {
        this.codigoMunicipio = codigoMunicipio;
    }


    /**
     * Gets the marca value for this Veiculo.
     * 
     * @return marca
     */
    public java.lang.String getMarca() {
        return marca;
    }


    /**
     * Sets the marca value for this Veiculo.
     * 
     * @param marca
     */
    public void setMarca(java.lang.String marca) {
        this.marca = marca;
    }


    /**
     * Gets the modelo value for this Veiculo.
     * 
     * @return modelo
     */
    public java.lang.String getModelo() {
        return modelo;
    }


    /**
     * Sets the modelo value for this Veiculo.
     * 
     * @param modelo
     */
    public void setModelo(java.lang.String modelo) {
        this.modelo = modelo;
    }


    /**
     * Gets the anoFabricacao value for this Veiculo.
     * 
     * @return anoFabricacao
     */
    public java.lang.Integer getAnoFabricacao() {
        return anoFabricacao;
    }


    /**
     * Sets the anoFabricacao value for this Veiculo.
     * 
     * @param anoFabricacao
     */
    public void setAnoFabricacao(java.lang.Integer anoFabricacao) {
        this.anoFabricacao = anoFabricacao;
    }


    /**
     * Gets the anoModelo value for this Veiculo.
     * 
     * @return anoModelo
     */
    public java.lang.Integer getAnoModelo() {
        return anoModelo;
    }


    /**
     * Sets the anoModelo value for this Veiculo.
     * 
     * @param anoModelo
     */
    public void setAnoModelo(java.lang.Integer anoModelo) {
        this.anoModelo = anoModelo;
    }


    /**
     * Gets the cor value for this Veiculo.
     * 
     * @return cor
     */
    public java.lang.String getCor() {
        return cor;
    }


    /**
     * Sets the cor value for this Veiculo.
     * 
     * @param cor
     */
    public void setCor(java.lang.String cor) {
        this.cor = cor;
    }


    /**
     * Gets the tara value for this Veiculo.
     * 
     * @return tara
     */
    public java.lang.Integer getTara() {
        return tara;
    }


    /**
     * Sets the tara value for this Veiculo.
     * 
     * @param tara
     */
    public void setTara(java.lang.Integer tara) {
        this.tara = tara;
    }


    /**
     * Gets the capacidadeKg value for this Veiculo.
     * 
     * @return capacidadeKg
     */
    public java.lang.Integer getCapacidadeKg() {
        return capacidadeKg;
    }


    /**
     * Sets the capacidadeKg value for this Veiculo.
     * 
     * @param capacidadeKg
     */
    public void setCapacidadeKg(java.lang.Integer capacidadeKg) {
        this.capacidadeKg = capacidadeKg;
    }


    /**
     * Gets the capacidadeM3 value for this Veiculo.
     * 
     * @return capacidadeM3
     */
    public java.lang.Integer getCapacidadeM3() {
        return capacidadeM3;
    }


    /**
     * Sets the capacidadeM3 value for this Veiculo.
     * 
     * @param capacidadeM3
     */
    public void setCapacidadeM3(java.lang.Integer capacidadeM3) {
        this.capacidadeM3 = capacidadeM3;
    }


    /**
     * Gets the tipoRodado value for this Veiculo.
     * 
     * @return tipoRodado
     */
    public br.adm.ipc.schemas.efrete.veiculos.objects.TipoRodado getTipoRodado() {
        return tipoRodado;
    }


    /**
     * Sets the tipoRodado value for this Veiculo.
     * 
     * @param tipoRodado
     */
    public void setTipoRodado(br.adm.ipc.schemas.efrete.veiculos.objects.TipoRodado tipoRodado) {
        this.tipoRodado = tipoRodado;
    }


    /**
     * Gets the tipoCarroceria value for this Veiculo.
     * 
     * @return tipoCarroceria
     */
    public br.adm.ipc.schemas.efrete.veiculos.objects.TipoCarroceria getTipoCarroceria() {
        return tipoCarroceria;
    }


    /**
     * Sets the tipoCarroceria value for this Veiculo.
     * 
     * @param tipoCarroceria
     */
    public void setTipoCarroceria(br.adm.ipc.schemas.efrete.veiculos.objects.TipoCarroceria tipoCarroceria) {
        this.tipoCarroceria = tipoCarroceria;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Veiculo)) return false;
        Veiculo other = (Veiculo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.placa==null && other.getPlaca()==null) || 
             (this.placa!=null &&
              this.placa.equals(other.getPlaca()))) &&
            this.renavam == other.getRenavam() &&
            ((this.chassi==null && other.getChassi()==null) || 
             (this.chassi!=null &&
              this.chassi.equals(other.getChassi()))) &&
            this.RNTRC == other.getRNTRC() &&
            ((this.numeroDeEixos==null && other.getNumeroDeEixos()==null) || 
             (this.numeroDeEixos!=null &&
              this.numeroDeEixos.equals(other.getNumeroDeEixos()))) &&
            this.codigoMunicipio == other.getCodigoMunicipio() &&
            ((this.marca==null && other.getMarca()==null) || 
             (this.marca!=null &&
              this.marca.equals(other.getMarca()))) &&
            ((this.modelo==null && other.getModelo()==null) || 
             (this.modelo!=null &&
              this.modelo.equals(other.getModelo()))) &&
            ((this.anoFabricacao==null && other.getAnoFabricacao()==null) || 
             (this.anoFabricacao!=null &&
              this.anoFabricacao.equals(other.getAnoFabricacao()))) &&
            ((this.anoModelo==null && other.getAnoModelo()==null) || 
             (this.anoModelo!=null &&
              this.anoModelo.equals(other.getAnoModelo()))) &&
            ((this.cor==null && other.getCor()==null) || 
             (this.cor!=null &&
              this.cor.equals(other.getCor()))) &&
            ((this.tara==null && other.getTara()==null) || 
             (this.tara!=null &&
              this.tara.equals(other.getTara()))) &&
            ((this.capacidadeKg==null && other.getCapacidadeKg()==null) || 
             (this.capacidadeKg!=null &&
              this.capacidadeKg.equals(other.getCapacidadeKg()))) &&
            ((this.capacidadeM3==null && other.getCapacidadeM3()==null) || 
             (this.capacidadeM3!=null &&
              this.capacidadeM3.equals(other.getCapacidadeM3()))) &&
            ((this.tipoRodado==null && other.getTipoRodado()==null) || 
             (this.tipoRodado!=null &&
              this.tipoRodado.equals(other.getTipoRodado()))) &&
            ((this.tipoCarroceria==null && other.getTipoCarroceria()==null) || 
             (this.tipoCarroceria!=null &&
              this.tipoCarroceria.equals(other.getTipoCarroceria())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPlaca() != null) {
            _hashCode += getPlaca().hashCode();
        }
        _hashCode += new Long(getRenavam()).hashCode();
        if (getChassi() != null) {
            _hashCode += getChassi().hashCode();
        }
        _hashCode += new Long(getRNTRC()).hashCode();
        if (getNumeroDeEixos() != null) {
            _hashCode += getNumeroDeEixos().hashCode();
        }
        if (getMarca() != null) {
            _hashCode += getMarca().hashCode();
        }
        if (getModelo() != null) {
            _hashCode += getModelo().hashCode();
        }
        if (getAnoFabricacao() != null) {
            _hashCode += getAnoFabricacao().hashCode();
        }
        if (getAnoModelo() != null) {
            _hashCode += getAnoModelo().hashCode();
        }
        if (getCor() != null) {
            _hashCode += getCor().hashCode();
        }
        if (getTara() != null) {
            _hashCode += getTara().hashCode();
        }
        if (getCapacidadeKg() != null) {
            _hashCode += getCapacidadeKg().hashCode();
        }
        if (getCapacidadeM3() != null) {
            _hashCode += getCapacidadeM3().hashCode();
        }
        if (getTipoRodado() != null) {
            _hashCode += getTipoRodado().hashCode();
        }
        if (getTipoCarroceria() != null) {
            _hashCode += getTipoCarroceria().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Veiculo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/veiculos/objects", "Veiculo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("placa");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/veiculos/objects", "Placa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("renavam");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/veiculos/objects", "Renavam"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("chassi");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/veiculos/objects", "Chassi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RNTRC");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/veiculos/objects", "RNTRC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numeroDeEixos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/veiculos/objects", "NumeroDeEixos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoMunicipio");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/veiculos/objects", "CodigoMunicipio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("marca");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/veiculos/objects", "Marca"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modelo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/veiculos/objects", "Modelo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("anoFabricacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/veiculos/objects", "AnoFabricacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("anoModelo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/veiculos/objects", "AnoModelo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cor");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/veiculos/objects", "Cor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tara");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/veiculos/objects", "Tara"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("capacidadeKg");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/veiculos/objects", "CapacidadeKg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("capacidadeM3");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/veiculos/objects", "CapacidadeM3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoRodado");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/veiculos/objects", "TipoRodado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/veiculos/objects", "TipoRodado"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoCarroceria");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/veiculos/objects", "TipoCarroceria"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/veiculos/objects", "TipoCarroceria"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
