/**
 * TipoCarroceria.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.veiculos.objects;

public class TipoCarroceria implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected TipoCarroceria(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _NaoAplicavel = "NaoAplicavel";
    public static final java.lang.String _Aberta = "Aberta";
    public static final java.lang.String _FechadaOuBau = "FechadaOuBau";
    public static final java.lang.String _Granelera = "Granelera";
    public static final java.lang.String _PortaContainer = "PortaContainer";
    public static final java.lang.String _Sider = "Sider";
    public static final TipoCarroceria NaoAplicavel = new TipoCarroceria(_NaoAplicavel);
    public static final TipoCarroceria Aberta = new TipoCarroceria(_Aberta);
    public static final TipoCarroceria FechadaOuBau = new TipoCarroceria(_FechadaOuBau);
    public static final TipoCarroceria Granelera = new TipoCarroceria(_Granelera);
    public static final TipoCarroceria PortaContainer = new TipoCarroceria(_PortaContainer);
    public static final TipoCarroceria Sider = new TipoCarroceria(_Sider);
    public java.lang.String getValue() { return _value_;}
    public static TipoCarroceria fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        TipoCarroceria enumeration = (TipoCarroceria)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static TipoCarroceria fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TipoCarroceria.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/veiculos/objects", "TipoCarroceria"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
