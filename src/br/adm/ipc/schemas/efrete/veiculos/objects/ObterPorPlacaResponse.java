/**
 * ObterPorPlacaResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.veiculos.objects;

public class ObterPorPlacaResponse  extends br.adm.ipc.schemas.efrete.objects.Response  implements java.io.Serializable {
    private br.adm.ipc.schemas.efrete.veiculos.objects.Veiculo veiculo;

    public ObterPorPlacaResponse() {
    }

    public ObterPorPlacaResponse(
           int versao,
           boolean sucesso,
           br.adm.ipc.schemas.efrete.objects.Excecao excecao,
           java.lang.Long protocoloServico,
           br.adm.ipc.schemas.efrete.veiculos.objects.Veiculo veiculo) {
        super(
            versao,
            sucesso,
            excecao,
            protocoloServico);
        this.veiculo = veiculo;
    }


    /**
     * Gets the veiculo value for this ObterPorPlacaResponse.
     * 
     * @return veiculo
     */
    public br.adm.ipc.schemas.efrete.veiculos.objects.Veiculo getVeiculo() {
        return veiculo;
    }


    /**
     * Sets the veiculo value for this ObterPorPlacaResponse.
     * 
     * @param veiculo
     */
    public void setVeiculo(br.adm.ipc.schemas.efrete.veiculos.objects.Veiculo veiculo) {
        this.veiculo = veiculo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ObterPorPlacaResponse)) return false;
        ObterPorPlacaResponse other = (ObterPorPlacaResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.veiculo==null && other.getVeiculo()==null) || 
             (this.veiculo!=null &&
              this.veiculo.equals(other.getVeiculo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getVeiculo() != null) {
            _hashCode += getVeiculo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ObterPorPlacaResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/veiculos/objects", "ObterPorPlacaResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("veiculo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/veiculos/objects", "Veiculo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/veiculos/objects", "Veiculo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
