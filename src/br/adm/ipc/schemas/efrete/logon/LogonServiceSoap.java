/**
 * LogonServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.logon;

public interface LogonServiceSoap extends java.rmi.Remote {
    public br.adm.ipc.schemas.efrete.logon.objects.LoginResponse login(br.adm.ipc.schemas.efrete.logon.objects.LoginRequest loginRequest) throws java.rmi.RemoteException;
    public br.adm.ipc.schemas.efrete.logon.objects.LogoutResponse logout(br.adm.ipc.schemas.efrete.logon.objects.LogoutRequest logoutRequest) throws java.rmi.RemoteException;
}
