/**
 * LogonServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.adm.ipc.schemas.efrete.logon;

public class LogonServiceLocator extends org.apache.axis.client.Service implements br.adm.ipc.schemas.efrete.logon.LogonService {

    public LogonServiceLocator() {
    }


    public LogonServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public LogonServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for LogonServiceSoap
//    private java.lang.String LogonServiceSoap_address = "https://dev.efrete.com.br/Services/LogonService.asmx";
    private java.lang.String LogonServiceSoap_address = "https://sistema.efrete.com.br/Services/LogonService.asmx";

    public java.lang.String getLogonServiceSoapAddress() {
        return LogonServiceSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String LogonServiceSoapWSDDServiceName = "LogonServiceSoap";

    public java.lang.String getLogonServiceSoapWSDDServiceName() {
        return LogonServiceSoapWSDDServiceName;
    }

    public void setLogonServiceSoapWSDDServiceName(java.lang.String name) {
        LogonServiceSoapWSDDServiceName = name;
    }

    public br.adm.ipc.schemas.efrete.logon.LogonServiceSoap getLogonServiceSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(LogonServiceSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getLogonServiceSoap(endpoint);
    }

    public br.adm.ipc.schemas.efrete.logon.LogonServiceSoap getLogonServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            br.adm.ipc.schemas.efrete.logon.LogonServiceSoapStub _stub = new br.adm.ipc.schemas.efrete.logon.LogonServiceSoapStub(portAddress, this);
            _stub.setPortName(getLogonServiceSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setLogonServiceSoapEndpointAddress(java.lang.String address) {
        LogonServiceSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (br.adm.ipc.schemas.efrete.logon.LogonServiceSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                br.adm.ipc.schemas.efrete.logon.LogonServiceSoapStub _stub = new br.adm.ipc.schemas.efrete.logon.LogonServiceSoapStub(new java.net.URL(LogonServiceSoap_address), this);
                _stub.setPortName(getLogonServiceSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("LogonServiceSoap".equals(inputPortName)) {
            return getLogonServiceSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/logon", "LogonService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://schemas.ipc.adm.br/efrete/logon", "LogonServiceSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("LogonServiceSoap".equals(portName)) {
            setLogonServiceSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
