package br.adm.ipc.schemas.efrete.logon;

public class LogonServiceSoapProxy implements br.adm.ipc.schemas.efrete.logon.LogonServiceSoap {
  private String _endpoint = null;
  private br.adm.ipc.schemas.efrete.logon.LogonServiceSoap logonServiceSoap = null;
  
  public LogonServiceSoapProxy() {
    _initLogonServiceSoapProxy();
  }
  
  public LogonServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initLogonServiceSoapProxy();
  }
  
  private void _initLogonServiceSoapProxy() {
    try {
      logonServiceSoap = (new br.adm.ipc.schemas.efrete.logon.LogonServiceLocator()).getLogonServiceSoap();
      if (logonServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)logonServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)logonServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (logonServiceSoap != null)
      ((javax.xml.rpc.Stub)logonServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public br.adm.ipc.schemas.efrete.logon.LogonServiceSoap getLogonServiceSoap() {
    if (logonServiceSoap == null)
      _initLogonServiceSoapProxy();
    return logonServiceSoap;
  }
  
  public br.adm.ipc.schemas.efrete.logon.objects.LoginResponse login(br.adm.ipc.schemas.efrete.logon.objects.LoginRequest loginRequest) throws java.rmi.RemoteException{
    if (logonServiceSoap == null)
      _initLogonServiceSoapProxy();
    return logonServiceSoap.login(loginRequest);
  }
  
  public br.adm.ipc.schemas.efrete.logon.objects.LogoutResponse logout(br.adm.ipc.schemas.efrete.logon.objects.LogoutRequest logoutRequest) throws java.rmi.RemoteException{
    if (logonServiceSoap == null)
      _initLogonServiceSoapProxy();
    return logonServiceSoap.logout(logoutRequest);
  }
  
  
}