package br.adm.ipc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.axis.types.UnsignedInt;
import org.apache.axis.types.UnsignedShort;

import br.adm.ipc.schemas.efrete.logon.LogonServiceSoap;
import br.adm.ipc.schemas.efrete.logon.LogonServiceSoapProxy;
import br.adm.ipc.schemas.efrete.logon.objects.LoginRequest;
import br.adm.ipc.schemas.efrete.logon.objects.LoginResponse;
import br.adm.ipc.schemas.efrete.motoristas.MotoristasServiceSoap;
import br.adm.ipc.schemas.efrete.motoristas.MotoristasServiceSoapProxy;
import br.adm.ipc.schemas.efrete.motoristas.objects.GravarRequest;
import br.adm.ipc.schemas.efrete.objects.Endereco;
import br.adm.ipc.schemas.efrete.objects.Response;
import br.adm.ipc.schemas.efrete.objects.Telefone;
import br.adm.ipc.schemas.efrete.objects.Telefones;
import br.adm.ipc.schemas.efrete.pef.PefServiceSoap;
import br.adm.ipc.schemas.efrete.pef.PefServiceSoapProxy;
import br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Contratado;
import br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Motorista;
import br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.NotaFiscal;
import br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Pagamento;
import br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.TipoToleranciaDePerdaDeMercadoria;
import br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.ToleranciaDePerdaDeMercadoria;
import br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.UnidadeDeMedidaDaMercadoria;
import br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Veiculo;
import br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.Viagem;
import br.adm.ipc.schemas.efrete.pef.AdicionarOperacaoTransporte.ViagemTipoDeCalculo;
import br.adm.ipc.schemas.efrete.pef.objects.AdicionarOperacaoTransporteRequest;
import br.adm.ipc.schemas.efrete.pef.objects.AdicionarOperacaoTransporteResponse;
import br.adm.ipc.schemas.efrete.pef.objects.CategoriaPagamento;
import br.adm.ipc.schemas.efrete.pef.objects.Consignatario;
import br.adm.ipc.schemas.efrete.pef.objects.ConsultarTipoCargaRequest;
import br.adm.ipc.schemas.efrete.pef.objects.ConsultarTipoCargaResponse;
import br.adm.ipc.schemas.efrete.pef.objects.Contratante;
import br.adm.ipc.schemas.efrete.pef.objects.Destinatario;
import br.adm.ipc.schemas.efrete.pef.objects.DownloadDocumentoRequest;
import br.adm.ipc.schemas.efrete.pef.objects.EncerrarOperacaoTransporteRequest;
import br.adm.ipc.schemas.efrete.pef.objects.EncerrarOperacaoTransporteResponse;
import br.adm.ipc.schemas.efrete.pef.objects.Impostos;
import br.adm.ipc.schemas.efrete.pef.objects.InformacoesBancarias;
import br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransportePdfRequest;
import br.adm.ipc.schemas.efrete.pef.objects.ObterOperacaoTransportePdfResponse;
import br.adm.ipc.schemas.efrete.pef.objects.ProprietarioCarga;
import br.adm.ipc.schemas.efrete.pef.objects.Remetente;
import br.adm.ipc.schemas.efrete.pef.objects.Subcontratante;
import br.adm.ipc.schemas.efrete.pef.objects.TipoConta;
import br.adm.ipc.schemas.efrete.pef.objects.TipoEmbalagem;
import br.adm.ipc.schemas.efrete.pef.objects.TipoEntregaDocumentacao;
import br.adm.ipc.schemas.efrete.pef.objects.TipoPagamento;
import br.adm.ipc.schemas.efrete.pef.objects.TipoViagem;
import br.adm.ipc.schemas.efrete.pef.objects.TomadorServico;
import br.adm.ipc.schemas.efrete.proprietarios.ProprietariosServiceSoap;
import br.adm.ipc.schemas.efrete.proprietarios.ProprietariosServiceSoapProxy;
import br.adm.ipc.schemas.efrete.proprietarios.objects.TipoPessoa;
import br.adm.ipc.schemas.efrete.veiculos.VeiculosServiceSoap;
import br.adm.ipc.schemas.efrete.veiculos.VeiculosServiceSoapProxy;

public class Teste {

	private static String integrador = "1973ae33-a969-4e56-a7c6-78419c75f2bc";
	public static void main(String[] args) throws JAXBException, IllegalAccessException, InvocationTargetException, IOException {
		// TODO Auto-generated method stub
		
		/**
		 *	PERGUNTAS A SEREM FEITAS
		 * 1 - VAI SER NECESSÁRIO FAZER UM CADASTRO NO SITE DO E-FRETE
		 * 2 - CADASTRO DE CNH, DATA DE NASCIMENTO,  
		 * 3 - LOCAL PARA INFORMAR TOLERANCIA DE PERDA
		 * 4 - INFORMAÇÕES COMPLEMENTARES PARA O VINCULO DA NOTA (QUANTIDADE MERCADORIA EMBARQUE, TIPO DE CALCULO, VALOR DA MERCADORIA POR UNIDADE,VALOR DE FRETE POR UNIDADE DE MERCADORIA)
		 * 5 - IMPLEMENTAR INFORMAÇÃO DE DISTANCIA PERCORRIDA
		 * 6 - ENTENDER OS TIPOS DE PAGAMENTO (E-FRETE, TRANSFERENCIA BANCARIA, OUTROS)
		 */
		
		LogonServiceSoap logonServiceSoap = new LogonServiceSoapProxy();
		PefServiceSoap pefServiceSoap = new PefServiceSoapProxy("https://dev.efrete.com.br/Services/PefService.asmx");
		MotoristasServiceSoap motoristasServiceSoap = new MotoristasServiceSoapProxy();
		ProprietariosServiceSoap proprietariosServiceSoap = new ProprietariosServiceSoapProxy();
		VeiculosServiceSoap veiculosService = new VeiculosServiceSoapProxy(); 
		
		
//		Calendar calendar = Calendar.getInstance();
//		calendar.setTime(new Date());
//		
		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setIntegrador(integrador);
		loginRequest.setUsuario("usuario@victsoft");
		loginRequest.setSenha("usuario#victsoft");
		loginRequest.setVersao(1);
		
		LoginResponse loginResponse = logonServiceSoap.login(loginRequest);
//		
//		ConsultarTipoCargaRequest consultarTipoCargaRequest = new ConsultarTipoCargaRequest();
//		consultarTipoCargaRequest.setToken(loginResponse.getToken());
//		consultarTipoCargaRequest.setIntegrador(integrador);
//		consultarTipoCargaRequest.setVersao(1);
//		
//		ConsultarTipoCargaResponse cargaResponse = pefServiceSoap.consultarTipoCarga(consultarTipoCargaRequest);
//		toXML(ConsultarTipoCargaResponse.class, cargaResponse);
//		
////		assert true;
//		
//		System.out.println("******************************************");
//		System.out.println("LOGIN");
//		System.out.println("******************************************");
//		
////		toXML(LoginResponse.class, loginResponse);
////		toXML(LoginRequest.class, loginRequest);
//		
//		System.out.println("******************************************");
//		System.out.println("MOTORISTA");
//		System.out.println("******************************************");
//		
//		Telefone telefoneConsig = new Telefone("43", "999835721");
//		
//		Telefones telefonesConsig = new Telefones();
//		telefonesConsig.setCelular(telefoneConsig);
//		
//		Endereco enderecoMot = new Endereco();
//		enderecoMot.setBairro("CENTRO");
//		enderecoMot.setCEP("86801360");
//		enderecoMot.setCodigoMunicipio("4101408");
//		enderecoMot.setNumero("120");
//		enderecoMot.setRua("CLOTARIO PORTUGAL");
//		
//		GravarRequest gravarRequestMot = new GravarRequest();
//		gravarRequestMot.setCNH("1837164374");
//		gravarRequestMot.setEndereco(enderecoMot);
//		gravarRequestMot.setCPF("27269448841");
//		calendar.add(Calendar.YEAR, -23);
//		gravarRequestMot.setDataNascimento(calendar);
//		gravarRequestMot.setIntegrador(integrador);
//		gravarRequestMot.setNome("ADILSON GOMES DOS SANTOS MOREIRA");
//		gravarRequestMot.setVersao(2);
//		gravarRequestMot.setTelefones(telefonesConsig);
//		gravarRequestMot.setToken(loginResponse.getToken());
////		gravarRequestMot.setNomeDeSolteiraDaMae("FATIMA MARIA PEDRO");
//		
//		br.adm.ipc.schemas.efrete.motoristas.objects.GravarResponse gravarResponseMot = motoristasServiceSoap.gravar(gravarRequestMot);
//		
//		toXML(br.adm.ipc.schemas.efrete.motoristas.objects.GravarResponse.class, gravarResponseMot);
//		toXML(br.adm.ipc.schemas.efrete.motoristas.objects.GravarRequest.class, gravarRequestMot);
////		
//		Motorista motorista = new Motorista();
//		motorista.setCNH(gravarResponseMot.getMotorista().getCNH());
//		motorista.setCpfOuCnpj(String.valueOf(gravarResponseMot.getMotorista().getCPF()));
//		motorista.setCelular(gravarRequestMot.getTelefones().getCelular());
//		
//		
//		System.out.println("******************************************");
//		System.out.println("PROPRIETARIOS");
//		System.out.println("******************************************");
//		
//		Telefone telefoneProps = new Telefone("43", "999835725");
//		
//		Telefones telefonesProps = new Telefones();
//		telefonesProps.setCelular(telefoneProps);
//		
////		Endereco enderecoProp = new Endereco();
////		enderecoProp.setBairro("CENTRO");
////		enderecoProp.setCEP("86801360");
////		enderecoProp.setCodigoMunicipio("4101408");
////		enderecoProp.setNumero("120");
////		enderecoProp.setRua("CLOTARIO PORTUGAL");
////		
////		br.adm.ipc.schemas.efrete.proprietarios.objects.GravarRequest gravarRequestPro = new br.adm.ipc.schemas.efrete.proprietarios.objects.GravarRequest();
////		gravarRequestPro.setCNPJ("04204491839");
////		gravarRequestPro.setEndereco(enderecoProp);
////		gravarRequestPro.setRazaoSocial("ANTONIO DE OLIVEIRA");
////		gravarRequestPro.setIntegrador(integrador);
////		gravarRequestPro.setRNTRC("00178200");
////		gravarRequestPro.setTelefones(telefonesProps);
////		gravarRequestPro.setTipoPessoa(TipoPessoa.Fisica);
////		gravarRequestPro.setToken(loginResponse.getToken());
////		gravarRequestPro.setVersao(3);
////		
////		br.adm.ipc.schemas.efrete.proprietarios.objects.GravarResponse gravarResponseProp = proprietariosServiceSoap.gravar(gravarRequestPro);
//		
////		toXML(br.adm.ipc.schemas.efrete.proprietarios.objects.GravarResponse.class, gravarResponseProp);
////		toXML(br.adm.ipc.schemas.efrete.proprietarios.objects.GravarRequest.class, gravarRequestPro);
//		
//		System.out.println("******************************************");
//		System.out.println("VEICULO ");
//		System.out.println("******************************************");
//		
//		br.adm.ipc.schemas.efrete.veiculos.objects.GravarRequest gravarRequestVeic = new br.adm.ipc.schemas.efrete.veiculos.objects.GravarRequest(); 
//		gravarRequestVeic.setIntegrador(integrador);
//		gravarRequestVeic.setToken(loginResponse.getToken());
//		br.adm.ipc.schemas.efrete.veiculos.objects.Veiculo veiculo = new br.adm.ipc.schemas.efrete.veiculos.objects.Veiculo();
//		veiculo.setAnoFabricacao(2018);
//		veiculo.setAnoModelo(2018);
//		veiculo.setCapacidadeKg(13000);
//		veiculo.setCapacidadeM3(1300);
//		veiculo.setChassi("9BSG4X200A3654421");
//		veiculo.setRNTRC("13060");
//		veiculo.setPlaca("CUD5728");
//		veiculo.setRenavam("112151353");
//		veiculo.setCodigoMunicipio("123456");
//		gravarRequestVeic.setVeiculo(veiculo);
//		gravarRequestVeic.setVersao(1);
//		
//		br.adm.ipc.schemas.efrete.veiculos.objects.GravarResponse gravarResponseVeic = veiculosService.gravar(gravarRequestVeic);
////		
////		toXML(br.adm.ipc.schemas.efrete.veiculos.objects.GravarResponse.class, gravarResponseVeic);
////		toXML(br.adm.ipc.schemas.efrete.veiculos.objects.GravarRequest.class, gravarRequestVeic);
//		
//		System.out.println("******************************************");
//		System.out.println("ADICIONAR OPERAÇÃO DE TRANSPORTE");
//		System.out.println("******************************************");
//		/*
//		 * CONSIGNATARIO
//		 */
//		Endereco enderecoConsig = new Endereco();
//		enderecoConsig.setBairro("CENTRO");
//		enderecoConsig.setCEP("86801360");
//		enderecoConsig.setCodigoMunicipio("4101408");
//		enderecoConsig.setNumero("120");
//		enderecoConsig.setRua("CLOTARIO PORTUGAL");
//		
//		Telefone telefoneMot = new Telefone("43", "999832724");
//		
//		Telefones telefonesMot = new Telefones();
//		telefonesMot.setCelular(telefoneMot);
//		
//		Consignatario consignatario = new Consignatario();
//		consignatario.setCpfOuCnpj("14408664000107");
//		consignatario.setEMail("victormacedo400@gmail.com");
//		consignatario.setEndereco(enderecoConsig);
//		consignatario.setNomeOuRazaoSocial("BRA1");
//		consignatario.setResponsavelPeloPagamento(false);
//		consignatario.setTelefones(telefonesMot);
//		
//		TomadorServico tm = new TomadorServico();
//		BeanUtils.copyProperties(tm, consignatario);
//		System.out.println(tm.getCpfOuCnpj());
//		
////		codigoMunicipio
//		/*
//		 * CONTRATADO
//		 */
//		Contratado contratado = new Contratado();
//		contratado.setCpfOuCnpj("04067753000175");
//		contratado.setRNTRC("3858134");
//		
//		/*
//		 * CONTRATANTE
//		 */
//		Endereco enderecoContrat = new Endereco();
//		enderecoContrat.setBairro("CENTRO");
//		enderecoContrat.setCEP("86801360");
//		enderecoContrat.setCodigoMunicipio("4101408");
//		enderecoContrat.setNumero("120");
//		enderecoContrat.setRua("CLOTARIO PORTUGAL");
//		
//		Telefone telefoneContratante = new Telefone("43", "979035724");
//		
//		Telefones telefonesContratante = new Telefones();
//		telefonesContratante.setCelular(telefoneContratante);
//		
//		Contratante contratante = new Contratante();
//		contratante.setCpfOuCnpj("14408664000107");
//		contratante.setEMail("victormacedo400@gmail.com");
//		contratante.setEndereco(enderecoContrat);
//		contratante.setNomeOuRazaoSocial("BRA1");
//		contratante.setResponsavelPeloPagamento(true);
//		contratante.setRNTRC("12345455");
//		contratante.setTelefones(telefonesContratante);
//		
//		/*
//		 * DESTINATARIO
//		 */
//		Endereco enderecoDest = new Endereco();
//		enderecoDest.setBairro("CENTRO");
//		enderecoDest.setCEP("86801360");
//		enderecoDest.setCodigoMunicipio("4101408");
//		enderecoDest.setNumero("120");
//		enderecoDest.setRua("CLOTARIO PORTUGAL");
//		
//		Telefone telefoneDest = new Telefone("43", "999835725");
//		
//		Telefones telefonesDest = new Telefones();
//		telefonesDest.setCelular(telefoneDest);
//		
//		Destinatario destinatario = new Destinatario();
//		destinatario.setCpfOuCnpj("14408664000107");
//		destinatario.setEMail("victormacedo400@gmail.com");
//		destinatario.setEndereco(enderecoDest);
//		destinatario.setNomeOuRazaoSocial("BRA1");
//		destinatario.setResponsavelPeloPagamento(false);
//		destinatario.setTelefones(telefonesDest);
//		
//		ToleranciaDePerdaDeMercadoria toleranciaDePerdaDeMercadoria = new ToleranciaDePerdaDeMercadoria();
//		toleranciaDePerdaDeMercadoria.setTipo(TipoToleranciaDePerdaDeMercadoria.Nenhum);
//		
//		NotaFiscal notaFiscal = new NotaFiscal();
//		calendar.setTime(new Date());
//		notaFiscal.setData(calendar);
//		notaFiscal.setCodigoNCMNaturezaCarga("0001");
//		notaFiscal.setDescricaoDaMercadoria("AVULSO");
//		notaFiscal.setNumero("22234");
//		notaFiscal.setSerie("1");
//		notaFiscal.setQuantidadeDaMercadoriaNoEmbarque(new BigDecimal("10"));
//		notaFiscal.setTipoDeCalculo(ViagemTipoDeCalculo.SemQuebra);
//		notaFiscal.setToleranciaDePerdaDeMercadoria(toleranciaDePerdaDeMercadoria);
//		notaFiscal.setUnidadeDeMedidaDaMercadoria(UnidadeDeMedidaDaMercadoria.Kg);
////		notaFiscal.setValorDaMercadoriaPorUnidade(new BigDecimal("10"));
////		notaFiscal.setValorDoFretePorUnidadeDeMercadoria(new BigDecimal("10"));
//		notaFiscal.setValorTotal(new BigDecimal("10"));
////		
//		InformacoesBancarias informacoesBancarias = new InformacoesBancarias();
//		informacoesBancarias.setAgencia("0327");
//		informacoesBancarias.setConta("0710181-3");
//		informacoesBancarias.setInstituicaoBancaria("237");
//		informacoesBancarias.setTipoConta(TipoConta.ContaCorrente);
//		
//		Viagem viagem = new Viagem();
//		viagem.setCepDestino("86801300");
//		viagem.setCepOrigem("86800360");
//		viagem.setCodigoMunicipioDestino("4101408");
//		viagem.setCodigoMunicipioOrigem("4101408");
//		viagem.setDistanciaPercorrida(new UnsignedInt(1000));
//		viagem.setDocumentoViagem("1234");
//		viagem.setTipoPagamento(TipoPagamento.TransferenciaBancaria);
////		viagem.setInformacoesBancarias(informacoesBancarias);
//		viagem.setNotasFiscais(new NotaFiscal[] {notaFiscal});
//		
//		Impostos impostos = new Impostos();
//		impostos.setINSS(new BigDecimal("0"));
//		impostos.setIRRF(new BigDecimal("0"));
//		impostos.setISSQN(new BigDecimal("0"));
//		impostos.setOutrosImpostos(new BigDecimal("0"));
//		impostos.setSestSenat(new BigDecimal("0"));
//		
//		Pagamento pagamento = new Pagamento();
//		pagamento.setCategoria(CategoriaPagamento.SemCategoria);
////		pagamento.setCnpjFilialAbastecimento(null);
//		pagamento.setDataDeLiberacao(calendar);
////		pagamento.setDocumento(null);
//		pagamento.setIdPagamentoCliente("2");
////		pagamento.setInformacaoAdicional(null);
//		pagamento.setInformacoesBancarias(informacoesBancarias);
//		pagamento.setTipoPagamento(TipoPagamento.TransferenciaBancaria);
//		pagamento.setValor(BigDecimal.TEN);
//		
//		/*
//		 * PROPRIETARIO DA CARGA
//		 */
//		
////		Telefone telefoneProp = new Telefone("43", "999835725");
////		
////		Telefones telefonesProp = new Telefones();
////		telefonesProps.setCelular(telefoneProp);
////		
////		ProprietarioCarga proprietarioCarga = new ProprietarioCarga();
////		proprietarioCarga.setCpfOuCnpj("14408664000107");
////		proprietarioCarga.setEMail("victormacedo400@gmail.com");
////		proprietarioCarga.setEndereco(enderecoProp);
////		proprietarioCarga.setNomeOuRazaoSocial("BRA1");
////		proprietarioCarga.setResponsavelPeloPagamento(false);
////		proprietarioCarga.setTelefones(telefonesProp);
//		
//		/*
//		 * REMETENTE
//		 */
//		Endereco enderecoRemet = new Endereco();
//		enderecoRemet.setBairro("CENTRO");
//		enderecoRemet.setCEP("86801360");
//		enderecoRemet.setCodigoMunicipio("4101408");
//		enderecoRemet.setNumero("120");
//		enderecoRemet.setRua("CLOTARIO PORTUGAL");
//		
//		Telefone telefoneRemet = new Telefone("43", "999835725");
//		
//		Telefones telefonesRemet = new Telefones();
//		telefonesRemet.setCelular(telefoneRemet);
//		
//		Remetente remetente = new Remetente();
//		remetente.setCpfOuCnpj("14408664000107");
//		remetente.setEMail("victormacedo400@gmail.com");
//		remetente.setEndereco(enderecoRemet);
//		remetente.setNomeOuRazaoSocial("BRA1");
//		remetente.setResponsavelPeloPagamento(false);
//		remetente.setTelefones(telefonesRemet);
//		
//		/*
//		 * SUBCONTRATANTE
//		 */
//		Endereco enderecoSubContratante = new Endereco();
//		enderecoSubContratante.setBairro("CENTRO");
//		enderecoSubContratante.setCEP("86801360");
//		enderecoSubContratante.setCodigoMunicipio("4101408");
//		enderecoSubContratante.setNumero("120");
//		enderecoSubContratante.setRua("CLOTARIO PORTUGAL");
//		
//		Telefone telefoneSubContratante = new Telefone("43", "999835725");
//		
//		Telefones telefonesSubContratante = new Telefones();
//		telefonesSubContratante.setCelular(telefoneSubContratante);
//		
//		Subcontratante subcontratante = new Subcontratante();
//		subcontratante.setCpfOuCnpj("14408664000107");
//		subcontratante.setEMail("victormacedo400@gmail.com");
//		subcontratante.setEndereco(enderecoSubContratante);
//		subcontratante.setNomeOuRazaoSocial("BRA1");
//		subcontratante.setResponsavelPeloPagamento(false);
//		subcontratante.setTelefones(telefonesSubContratante);
//		
//		/*
//		 * TOMADOR
//		 */
//		Endereco enderecoToma = new Endereco();
//		enderecoToma.setBairro("CENTRO");
//		enderecoToma.setCEP("86801360");
//		enderecoToma.setCodigoMunicipio("4101408");
//		enderecoToma.setNumero("120");
//		enderecoToma.setRua("CLOTARIO PORTUGAL");
//		
//		Telefone telefoneToma = new Telefone("43", "999835725");
//		
//		Telefones telefonesToma = new Telefones();
//		telefonesToma.setCelular(telefoneToma);
//		
//		TomadorServico tomadorServico = new TomadorServico();
//		tomadorServico.setCpfOuCnpj("14408664000107");
//		tomadorServico.setEMail("victormacedo400@gmail.com");
//		tomadorServico.setEndereco(enderecoSubContratante);
//		tomadorServico.setNomeOuRazaoSocial("BRA1");
//		tomadorServico.setResponsavelPeloPagamento(false);
//		tomadorServico.setTelefones(telefonesSubContratante);
//		
//		Veiculo veic = new Veiculo();
//		veic.setPlaca("CUD5728");
//		
//		AdicionarOperacaoTransporteRequest adicionarOperacaoTransporteRequest = new AdicionarOperacaoTransporteRequest();
//		adicionarOperacaoTransporteRequest.setAltoDesempenho(false);
//		adicionarOperacaoTransporteRequest.setBloquearNaoEquiparado(true);
//		adicionarOperacaoTransporteRequest.setCepRetorno("86801361");
//		adicionarOperacaoTransporteRequest.setCodigoIdentificacaoOperacaoPrincipal(null);
//		adicionarOperacaoTransporteRequest.setCodigoNCMNaturezaCarga("0001");
//		adicionarOperacaoTransporteRequest.setCodigoTipoCarga(null);
////		adicionarOperacaoTransporteRequest.setConsignatario(consignatario);
//		adicionarOperacaoTransporteRequest.setContratado(contratado);
//		adicionarOperacaoTransporteRequest.setContratante(contratante);
//		adicionarOperacaoTransporteRequest.setDataFimViagem(calendar);
//		adicionarOperacaoTransporteRequest.setDataInicioViagem(calendar);
//		adicionarOperacaoTransporteRequest.setDestinacaoComercial(false);
//		adicionarOperacaoTransporteRequest.setDestinatario(destinatario);
//		adicionarOperacaoTransporteRequest.setDistanciaRetorno(null);
//		adicionarOperacaoTransporteRequest.setEmissaoGratuita(true);
//		adicionarOperacaoTransporteRequest.setEntregaDocumentacao(TipoEntregaDocumentacao.Cliente);
//		adicionarOperacaoTransporteRequest.setFilialCNPJ(null);
//		adicionarOperacaoTransporteRequest.setFreteRetorno(false);
//		adicionarOperacaoTransporteRequest.setIdOperacaoCliente("11");
//		adicionarOperacaoTransporteRequest.setImpostos(impostos);
//		adicionarOperacaoTransporteRequest.setIntegrador(null);
//		adicionarOperacaoTransporteRequest.setMatrizCNPJ("14408664000107");
//		adicionarOperacaoTransporteRequest.setMotorista(motorista);
//		adicionarOperacaoTransporteRequest.setObservacoesAoCredenciado(null);
//		adicionarOperacaoTransporteRequest.setObservacoesAoTransportador(null);
//		adicionarOperacaoTransporteRequest.setPagamentos(new Pagamento[] {pagamento});
//		adicionarOperacaoTransporteRequest.setPesoCarga(BigDecimal.TEN);
////		adicionarOperacaoTransporteRequest.setProprietarioCarga(proprietarioCarga);
//		adicionarOperacaoTransporteRequest.setQuantidadeSaques(1);
//		adicionarOperacaoTransporteRequest.setQuantidadeTransferencias(1);
////		adicionarOperacaoTransporteRequest.setRemetente(remetente);
////		adicionarOperacaoTransporteRequest.setSubcontratante(subcontratante);
//		adicionarOperacaoTransporteRequest.setTipoEmbalagem(TipoEmbalagem.Bigbag);
//		adicionarOperacaoTransporteRequest.setTipoPagamento(TipoPagamento.TransferenciaBancaria);
//		adicionarOperacaoTransporteRequest.setTipoViagem(TipoViagem.Padrao);
//		adicionarOperacaoTransporteRequest.setToken(loginResponse.getToken());
//		adicionarOperacaoTransporteRequest.setTomadorServico(tomadorServico);
////		adicionarOperacaoTransporteRequest.setValorSaques(BigDecimal.TEN);
////		adicionarOperacaoTransporteRequest.setValorTransferencias(BigDecimal.TEN);
//		adicionarOperacaoTransporteRequest.setVeiculos(new Veiculo[] {veic});
//		adicionarOperacaoTransporteRequest.setVersao(7);
//		adicionarOperacaoTransporteRequest.setViagens(new Viagem[] {viagem});
//		adicionarOperacaoTransporteRequest.setIntegrador(integrador);
//		adicionarOperacaoTransporteRequest.setCodigoTipoCarga(new UnsignedShort(1));
//		
//		AdicionarOperacaoTransporteResponse adicionarOperacaoTransporteResponse = pefServiceSoap.adicionarOperacaoTransporte(adicionarOperacaoTransporteRequest);
//		
//		toXML(AdicionarOperacaoTransporteResponse.class, adicionarOperacaoTransporteResponse);
//		toXML(AdicionarOperacaoTransporteRequest.class, adicionarOperacaoTransporteRequest);
//		
//		ObterOperacaoTransportePdfRequest obterOperacaoTransportePdfRequest = new ObterOperacaoTransportePdfRequest();
//		obterOperacaoTransportePdfRequest.setCodigoIdentificacaoOperacao(adicionarOperacaoTransporteResponse.getCodigoIdentificacaoOperacao().replace("/", ""));
//		obterOperacaoTransportePdfRequest.setIntegrador(integrador);
//		obterOperacaoTransportePdfRequest.setToken(loginResponse.getToken());
//		obterOperacaoTransportePdfRequest.setVersao(1);
//		
//		ObterOperacaoTransportePdfResponse obterOperacaoTransportePdfResponse = pefServiceSoap.obterOperacaoTransportePdf(obterOperacaoTransportePdfRequest);
//		
//		File file = new File("newfile.pdf");;
//		FileOutputStream fop = new FileOutputStream(file);
//
//		fop.write(obterOperacaoTransportePdfResponse.getPdf());
//		fop.flush();
//		fop.close();
	
		
		EncerrarOperacaoTransporteRequest encerrarOperacaoTransporteRequest = new EncerrarOperacaoTransporteRequest();
		encerrarOperacaoTransporteRequest.setCodigoIdentificacaoOperacao("1660000190328800");
		encerrarOperacaoTransporteRequest.setIntegrador(integrador);
		encerrarOperacaoTransporteRequest.setToken(loginResponse.getToken());
		encerrarOperacaoTransporteRequest.setVersao(2);
		encerrarOperacaoTransporteRequest.setPesoCarga(new BigDecimal(100));
		
		EncerrarOperacaoTransporteResponse encerrarOperacaoTransporteResponse = pefServiceSoap.encerrarOperacaoTransporte(encerrarOperacaoTransporteRequest);
		if(encerrarOperacaoTransporteResponse.isSucesso()) {
			System.out.println("Suc");
		}else {
			System.out.println(encerrarOperacaoTransporteResponse.getExcecao().getMensagem());
		}
		
//		
	}
	
	
	public static <T> void toXML(Class<T> clazz, Object object) throws JAXBException {
		StringWriter sw = new StringWriter();
		JAXBContext jaxbContextR = JAXBContext.newInstance(clazz);
		Marshaller jaxbMarshallerR = jaxbContextR.createMarshaller();
		jaxbMarshallerR.marshal(object, sw);
		
		if(object instanceof Response){
			Response response = (Response) object;
			System.err.println(response.getExcecao().getMensagem());
		}
		
		String xmlStringR = sw.toString();
		System.out.println(xmlStringR);
	}
	

}

