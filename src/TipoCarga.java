
public class TipoCarga {
	
	 private Integer codigoTipoCarga;
	 private String descricaoTipoCarga;

	public Integer getCodigoTipoCarga() {
		return codigoTipoCarga;
	}

	public void setCodigoTipoCarga(Integer codigoTipoCarga) {
		this.codigoTipoCarga = codigoTipoCarga;
	}

	public String getDescricaoTipoCarga() {
		return descricaoTipoCarga;
	}

	public void setDescricaoTipoCarga(String descricaoTipoCarga) {
		this.descricaoTipoCarga = descricaoTipoCarga;
	}
	 
}
